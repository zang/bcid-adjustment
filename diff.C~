#include "TCanvas.h"
#include "TFile.h"
#include "TGraph2D.h"
#include "TROOT.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TRandom.h"
#include "TGraphErrors.h"
#include "TChain.h"
#include "iostream"
#include "vector"
#include <sstream>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
using namespace std;



TH1D *FindTH1D(const TString & name, TString object)
{
  TFile * tf1 = new TFile(name);
  TH1D *hist = (TH1D*)(gROOT->FindObject(object.Data()));
  return hist;
}

TH1D * MakeTH1D(const TString & name, int bin, double xmin, double xmax)
{
  TH1D *h = new TH1D(name, name, bin, xmin, xmax);
  h->Sumw2(false);
  return h;
}


TGraph * MakeTGraph(const TString & title, Color_t color)
{
  TGraph *g1 = new TGraph();
  g1->SetTitle(title);
  g1->SetLineColor(color);
  g1->SetLineWidth(3);
  g1->SetMarkerSize(2);
  g1->SetMarkerStyle(20);
  g1->SetMarkerColor(color);
  /*
    g1->GetXaxis()->SetLabelSize(0.04);
    g1->GetXaxis()->SetTitleSize(0.04);
    g1->GetYaxis()->SetLabelSize(0.04);
    g1->GetYaxis()->SetTitleSize(0.04);
  */
  return g1;
}

TGraph2D * MakeTGraph2D(const TString & title)
{
  TGraph2D *g2 = new TGraph2D();
  g2->SetTitle(title);    
  g2->SetLineWidth(3);
  /* 
     g2->GetXaxis()->SetLabelSize(0.02);
     g2->GetXaxis()->SetTitleSize(0.02);
     g2->GetYaxis()->SetLabelSize(0.02);
     g2->GetYaxis()->SetTitleSize(0.02);
     g2->GetZaxis()->SetLabelSize(0.02);
     g2->GetZaxis()->SetTitleSize(0.02);
  */
  return g2;
}

//eta2>=eta1, phi2>=phi1
void Fill_hist(TH2D *hist, double eta1, double eta2, double phi1, double phi2, double eta_bin, double phi_bin,int status) {
  int eta_size=abs(eta2-eta1)/eta_bin;
  int phi_size=abs(phi2-phi1)/phi_bin;
  if (eta1 == eta2) eta_size = 1;
  if (phi1 == phi2) phi_size = 1;
  for (int ieta=0; ieta<eta_size;ieta++) {
    for (int iphi=0; iphi<phi_size;iphi++) {      
      double tmp_eta=eta1+ieta*eta_bin;
      double tmp_phi=phi1+iphi*phi_bin;
      if (status == 1) hist->Fill(tmp_eta,tmp_phi,1); //hit
      if (status == 2) hist->Fill(tmp_eta,tmp_phi,500); //shift
      if (status == 3) hist->Fill(tmp_eta,tmp_phi,1000); //miss           
    }
  }
}


void diff() {

  //read offset values
  vector<int> BCID_list;  
  vector<int> set_list;  
  map<int, map<int, pair<int, int> > > offset_list; //latome_fiber_oldshift-newshift
  map<int, map<int, pair<int, int> > > valid_list; //latome_fiber_oldcheckflag-newcheckflag, to aviod the 0 input mix with the inial value

  //old set file
  char oldOffset[100];
  //sprintf(oldOffset,"offset_diff/v7.txt"); //v7
  //sprintf(oldOffset,"offset_diff/v8.txt"); //v8
  //sprintf(oldOffset,"offset_diff/v8_00407549.txt"); //v8
  //sprintf(oldOffset,"offset_diff/v8new_00407549.txt"); //first optimization
  //sprintf(oldOffset,"offset_diff/v8new_avg_00407549.txt"); //first optimization with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_00407549.txt"); //second optimization with 1 
  //sprintf(oldOffset,"offset_diff/v8_fixed_2_00407549.txt"); //second optimization with 2
  //sprintf(oldOffset,"offset_diff/v8_fixed_4_avg_00407549.txt"); //second optimization with 4 and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_4_avg_00407551.txt");  //second optimization with 4 and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height30_avg_00407549.txt"); //fixed second optimization with height/30 and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height30_avg_00407551.txt"); //fixed second optimization with height/30 and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_0_avg_00407551.txt");  //fixed second optimization with 0 cut and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_0_avg_00407549.txt");  //fixed second optimization with 0 cut and with averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_0_avg2_00407549.txt");  //fixed second optimization with 0 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height20_avg2_00407549.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height20_avg2_00407551.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height10_avg2_00407549.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height10_avg2_408524.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_height20_avg2_408524.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_0_avg2_408524.txt");  //fixed second optimization with 0 cut and with fixed averaged peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_weight_0.02_0cut_avg2_408524.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_weight_0.1_0cut_avg2_408524.txt");  //fixed second optimization with 0 cut and with weighted peak position
  sprintf(oldOffset,"offset_diff/v8_fixed_weight_0_0cut_avg2_408590.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(oldOffset,"offset_diff/v8_fixed_weight_0_0cut_avg2_408591.txt");  //fixed second optimization with 0 cut and with weighted peak position

  //set values
  char newOffset[100];
  //sprintf(newOffset,"offset_diff/v8.txt");  //new v8 result
  //sprintf(newOffset,"offset_diff/v8new_00407551.txt");  //first optimization
  //sprintf(newOffset,"offset_diff/v8_00407554.txt");  //first optimization
  //sprintf(newOffset,"offset_diff/v8new_avg_00407551.txt");  //second optimization with averaged peak position
  //sprintf(newOffset,"offset_diff/v8new_avg_00407554.txt");  //second optimization with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_00407551.txt");  //second optimization with 1
  //sprintf(newOffset,"offset_diff/v8_fixed_2_00407551.txt");  //second optimization with 2
  //sprintf(newOffset,"offset_diff/v8_fixed_00407554.txt");  //second optimization with 1
  //sprintf(newOffset,"offset_diff/v8_fixed_4_avg_00407551.txt");  //second optimization with 4 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_4_avg_00407554.txt");  //second optimization with 4 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height30_avg_00407551.txt");  //fixed second optimization with height/30 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height30_avg_00407554.txt");  //fixed second optimization with height/30 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg_00407554.txt");  //fixed second optimization with 0 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg_00407551.txt");  //fixed second optimization with 0 and with averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg2_00407551.txt");  //fixed second optimization with 0 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg2_00407554.txt");  //fixed second optimization with 0 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height20_avg2_00407551.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height20_avg2_00407554.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height10_avg2_00407551.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height10_avg2_00407554.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height10_avg2_408522.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height10_avg2_408520.txt");  //fixed second optimization with height/10 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg2_408522.txt");  //fixed second optimization with 0 cut and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_0_avg2_408520.txt");  //fixed second optimization with 0 cut and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height20_avg2_408522.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_height20_avg2_408520.txt");  //fixed second optimization with height/20 and with fixed averaged peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0.02_0cut_avg2_408522.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0.02_0cut_avg2_408520.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0.1_0cut_avg2_408522.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0.1_0cut_avg2_408520.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0_0cut_avg2_408591.txt");  //fixed second optimization with 0 cut and with weighted peak position
  //sprintf(newOffset,"offset_diff/v8_fixed_weight_0_0cut_avg2_408593.txt");  //fixed second optimization with 0 cut and with weighted peak position
  sprintf(newOffset,"offset_diff/v8_fixed_weight_0_0cut_avg2_408594.txt");  //fixed second optimization with 0 cut and with weighted peak position
  

  //start to read oldset value 
  ifstream setfile(oldOffset);
  //cout<<"******************************Start to process old offset file***********************"<<endl;
  if (!setfile.is_open())
    {
      cout << "Unable to open old offset file: " << oldOffset <<"!" << endl;
      exit;
    }
  else {
    vector<string> vec;
    string temp;
    int row=0;
    while (getline(setfile, temp))
      {
	vec.push_back(temp);
      }
    for (auto it = vec.begin(); it != vec.end(); it++)
      {
	istringstream is(*it);
	string s;
	int column = 0;
	int tmpLATOME=0;
	double tmpfiber=-1;
	int tmpoffset=-9999;
	while (is >> s)
	  {
	    if (column ==0) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpLATOME = stoi(s);
	    }
	    if (column ==1) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpfiber = stoi(s);
	    }
	    if (column ==2) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpoffset = stoi(s);
	    }
	    column++;
	  }
	if (tmpoffset == -9999) continue;
	offset_list[tmpLATOME][tmpfiber].first=tmpoffset;
	valid_list[tmpLATOME][tmpfiber].first=1;
	//cout << "LATOME"<<tmpLATOME << "  F" << tmpfiber <<" oldshift="<<tmpoffset<<"; "<< endl; //debug
	row++;
      }
    //cout<<set_BCID.size()<<" fibers are read!"<< endl;
    setfile.close();
  }
  //end of reading set BCID

  //start to read oldset value 
  ifstream newsetfile(newOffset);
  //cout<<"******************************Start to process new offset file***********************"<<endl;
  if (!newsetfile.is_open())
    {
      cout << "Unable to open new offset file: " << newOffset  <<"!" << endl;
      exit;
    }
  else {
    vector<string> vec;
    string temp;
    int row=0;
    while (getline(newsetfile, temp))
      {
	vec.push_back(temp);
      }
    for (auto it = vec.begin(); it != vec.end(); it++)
      {
	istringstream is(*it);
	string s;
	int column = 0;
	int tmpLATOME=0;
	double tmpfiber=-1;
	int tmpoffset=-9999;
	while (is >> s)
	  {
	    if (column ==0) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpLATOME = stoi(s);
	    }
	    if (column ==1) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpfiber = stoi(s);
	    }
	    if (column ==2) {
	      int pos=s.find("=");
	      s.assign(s,pos+1,string::npos);
	      tmpoffset = stoi(s);
	    }
	    column++;
	  }
	if (tmpoffset == -9999) continue;
	offset_list[tmpLATOME][tmpfiber].second=tmpoffset;
	valid_list[tmpLATOME][tmpfiber].second=1;
	//cout << "LATOME"<<tmpLATOME << "  F" << tmpfiber <<" newshift="<<tmpoffset<<"; "<< endl; //debug
	row++;
      }
    //cout<<set_BCID.size()<<" fibers are read!"<< endl;
    newsetfile.close();
  }
  //end of reading offset values

  //calculate diff and draw plot
  double max=20;
  double min=-20;
  double mean=3463;
  double rms_max=10;
  double rms_min=-10;
  int bin=max-min;
  TH1D *BCID_Plot= MakeTH1D("BCID_Plot", bin, mean+min, mean+max);
  TH1D *Set_Plot= MakeTH1D("Set_Plot", bin, mean+min, mean+max);
  TH1D *oldShiftBC_dis= MakeTH1D("oldShiftBC_dis", bin, min, max);
  TH1D *newShiftBC_dis= MakeTH1D("newShiftBC_dis", bin, min, max);
  TH1D *shiftBC_diff= MakeTH1D("shiftBC_diff", 10, -5, 5);
  for (auto latome_it=offset_list.begin(); latome_it!=offset_list.end(); latome_it++) { //latome loop
    int fiber_counter=0;
    for (auto fiber_it=latome_it->second.begin(); fiber_it!=latome_it->second.end(); fiber_it++) { //fiber loop
      if (valid_list[latome_it->first][fiber_it->first].first == 1 && valid_list[latome_it->first][fiber_it->first].second == 1) { //valid input check
	int oldshift=fiber_it->second.first;
	int newshift=fiber_it->second.second;
	int BCdiff=newshift-oldshift;
	shiftBC_diff->Fill(BCdiff);
	oldShiftBC_dis->Fill(oldshift);
	newShiftBC_dis->Fill(newshift);
	if (BCdiff!=0) cout<<"latome"<< latome_it->first<<" F"<<fiber_it->first<<" oldOffset="<<oldshift<<"; newOffset="<<newshift<<"; shiftBCdiff="<<BCdiff<<endl;
	fiber_counter++;
      }
    }
  }


  TCanvas *c1 = new TCanvas("c1","c1",10,10,1000,800);
  c1->SetBottomMargin(0.20);
  c1->SetLogy();
  TPostScript ps1("myplots.ps",111);

  ps1.NewPage();
  c1->cd();
  oldShiftBC_dis->SetTitle("ShiftBC");
  oldShiftBC_dis->SetLineColor(kBlue);
  oldShiftBC_dis->SetLineWidth(3);
  oldShiftBC_dis->SetFillColor(kBlue);
  oldShiftBC_dis->SetFillStyle(3144);
  oldShiftBC_dis->Draw("h");
  newShiftBC_dis->SetLineColor(kRed);
  newShiftBC_dis->SetLineWidth(3);
  newShiftBC_dis->SetFillColor(kRed);
  newShiftBC_dis->SetFillStyle(3244);
  newShiftBC_dis->Draw("h SAME");
  c1->Update();
  

  /*

   */

  ps1.NewPage();
  c1->cd();
  shiftBC_diff->Draw("h");
  c1->Update();



}

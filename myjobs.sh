
#!/bin/sh

#./BCID_check.sh -i LArDigits_211013-144636_EMECA -t EMECA > results/process_log/EMECA_mon03_out.log 2>&1 &
#./BCID_check.sh -i LArDigits_211013-085933_EMECA -t EMECA > results/process_log/EMECA_mon02_out.log 2>&1 &
#./BCID_check.sh -i LArDigits_211013-143407_EMECC -t EMECC > results/process_log/EMECC_mon03_out.log 2>&1 &
#./BCID_check.sh -i LArDigits_211013-084805_EMECC -t EMECC > results/process_log/EMECC_mon01_out.log 2>&1 &
#./BCID_check.sh -i LArDigits_211013-085933_EMBA_EMECA -t EMBA > results/process_log/EMBA_out.log 2>&1 &
#./BCID_check.sh -i LArDigits_211013-084805_EMBC_EMECC -t EMBC > results/process_log/EMBC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_1000evts -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_1000evts -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_1000evts -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_1000evts -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_1000evts -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_1000evts -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_v8_new -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_v8_new -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_v8_new -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_v8_new -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_v8_new -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_v8_new -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_00407551 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407551 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407551 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407551 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407551 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407551 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_00407549 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407549 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407549 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407549 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407549 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407549 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_00407554 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407554 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407554 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407554 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407554 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407554 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_00407557 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407557 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407557 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_00407557 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407557 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_00407557 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#new data
./BCID_check.sh -i LArDigits_pusleall_408524 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408524 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408524 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408524 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408524 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408524 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_408522 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408522 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408522 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408522 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408522 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408522 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_408520 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408520 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408520 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408520 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408520 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408520 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#12/21

./BCID_check.sh -i LArDigits_pusleall_408590 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408590 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408590 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408590 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408590 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408590 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_408591 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408591 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408591 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408591 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408591 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408591 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_408593 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408593 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408593 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408593 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408593 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408593 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pusleall_408594 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408594 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408594 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pusleall_408594 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408594 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pusleall_408594 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_408670 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408670 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408670 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408670 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408670 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408670 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_408671 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408671 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408671 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408671 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408671 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408671 -t FCALC > results/process_log/FCALC_out.log 2>&1 &


#12/28 v9 data
./BCID_check.sh -i LArDigits_pulseall_408677 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408677 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408677 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408677 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408677 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408677 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_408678 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408678 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408678 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408678 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408678 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408678 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_408676 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408676 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408676 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408676 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408676 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408676 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_408679 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408679 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408679 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_408679 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408679 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_408679 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#12/27 C side check
./BCID_check.sh -i LArDigits_Cside -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_Cside -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_Cside -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_Cside -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_Cside -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_Cside -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#03/17 v11 update run
./BCID_check.sh -i LArDigits_pulseall_414414 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414414 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414414 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414414 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_414414 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_414414 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

# v11 data
./BCID_check.sh -i LArDigits_pulseall_414504 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414504 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414504 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_414504 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_414504 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_414504 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

# v11 data (updated latome92 F26 in v11)
./BCID_check.sh -i LArDigits_pulseall_416131 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416131 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416131 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416131 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416131 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416131 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_416134 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416134 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416134 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416134 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416134 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416134 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_416137 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416137 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416137 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416137 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416137 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416137 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS TTC offset comparison (beam1-Cside, beam2-Aside)
./BCID_check.sh -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam2._SFO-ALL._0001 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam2._SFO-ALL._0001 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam2._SFO-ALL._0001 -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam1._SFO-ALL._0001 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam1._SFO-ALL._0001 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i data21_comm.00405495.physics_L1Calo.merge.RAW._beam1._SFO-ALL._0001 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#v11 data 04/06
./BCID_check.sh -i LArDigits_pulseall_416961 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416961 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416961 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416961 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416961 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416961 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#v11 data 04/06
./BCID_check.sh -i LArDigits_pulseall_416976 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416976 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416976 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_416976 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416976 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_416976 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#v12 data  04/15
./BCID_check.sh -i LArDigits_pulseall_417993 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_417993 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_417993 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_417993 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_417993 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_417993 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#v14 data  04/22                                                                                                                 
./BCID_check.sh -i LArDigits_pulseall_418646 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418646 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418646 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418646 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_418646 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_418646 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

./BCID_check.sh -i LArDigits_pulseall_418322 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418322 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418322 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_418322 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_418322 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_418322 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS TTC offset comparison (beam1-Cside, beam2-Aside) 
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam2_calib -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam2_calib -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam2_calib -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam1_calib -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam1_calib -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00418554.physics_L1Calo.merge.RAW._beam1_calib -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS splash check (beam1-Cside, beam2-Aside) 
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb1175._SFO-ALL._0001 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb1175._SFO-ALL._0001 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb1175._SFO-ALL._0001 -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb0499._SFO-ALL._0001 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb0499._SFO-ALL._0001 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00418554.physics_L1Calo.merge.RAW._lb0499._SFO-ALL._0001 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS splash check (beam1-Cside, beam2-Aside) 
./BCID_check.sh -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0662_1404_beam2 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0662_1404_beam2 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0662_1404_beam2 -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0451_0644_beam1 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0451_0644_beam1 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00419373.physics_L1Calo.merge.RAW._lb0451_0644_beam1 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS collision check (beam1-Cside, beam2-Aside) 
./BCID_check.sh -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i  hist_latome_data22_900GeV.00422632.physics_CosmicCalo.merge.RAW._lb0308._SFO-ALL._0001_7rms -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#6/17 data, pulseall used for new reference test
./BCID_check.sh -i LArDigits_pulseall_425186 -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_425186 -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_425186 -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -i LArDigits_pulseall_425186 -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_425186 -t FCALA > results/process_log/FCALA_out.log 2>&1 &
./BCID_check.sh -f -i LArDigits_pulseall_425186 -t FCALC > results/process_log/FCALC_out.log 2>&1 &

#ATLAS splash check (beam1-Cside, beam2-Aside) 
./BCID_check.sh -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam2_all -t EMECA > results/process_log/EMECA_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam2_all -t EMBA > results/process_log/EMBA_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam2_all -t FCALA > results/process_log/FCALA_out.log 2>&1 &

./BCID_check.sh -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam1_all -t EMECC > results/process_log/EMECC_out.log 2>&1 &
./BCID_check.sh -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam1_all -t EMBC > results/process_log/EMBC_out.log 2>&1 &
./BCID_check.sh -f -i data22_comm.00420624.physics_L1Calo.merge.RAW.beam1_all -t FCALC > results/process_log/FCALC_out.log 2>&1 &


#end

#!/bin/sh


if [ ! -d "combined_output" ]
then
    echo "creating combined_output folder"
    mkdir "combined_output"
fi
./combine_macro.sh >& combined_output/combine.log

chmod 777 extra_files.sh
./extra_files.sh

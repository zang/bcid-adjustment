#!/bin/bash


while getopts ":a:b:o:" arg;
do
    case $arg in
        a)
            dir_a="$OPTARG"
	    if [ ! -d ${dir_a} ]; then
		echo "Can't find directory a: ${dir_a}!"
		exit
	    fi       	
            ;;
        b)
            dir_b="$OPTARG"
	    if [ ! -d ${dir_b} ]; then
		echo "Can't find directory b: ${dir_b}!"
		exit
	    fi       	
            ;;
        o)
            outputpath="$OPTARG"	    
	    if [ ! -d "$outputpath" ]
	    then
		echo "Can't find output directory!"
		echo "Creating new path ${outputpath}"
		mkdir -p ${outputpath}
                if [ $? -ne 0 ] ; then #check if mkdir command success
                    echo "Unable to make directory... exiting"
                    exit
                fi
	    else
		echo "outputpath = ${outputpath}"
	    fi
            ;;
    esac
done

for filename in `find "${dir_a}" -maxdepth 1 -name "*.ini" -printf "%P\n"` #find basename in dir a
do
    if [ -f "${dir_b}/$filename" ]
    then
	echo "copy reptitive file ${filename} in ${dir_a} and ${dir_b} to ${outputpath}! "
	cp ${dir_a}/${filename} ${outputpath}/${dir_a}_${filename}
	cp ${dir_b}/${filename} ${outputpath}/${dir_b}_${filename}
    fi
done

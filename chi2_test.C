#include <numeric>
#include <TH2.h>
#include "TFile.h"
#include "TTree.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <TGraphErrors.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TError.h>
#include <iomanip>
#include <sstream>
#include <map>

using namespace std;

struct SCinfo {
  Int_t      channelId;
  Int_t      barrel_ec;
  Int_t      pos_neg;
  Int_t      FT;
  Int_t      slot;
  Int_t      channel;
  Int_t      calibLine;
  Int_t      isConnected;
  Int_t      channelHash;
  Int_t      febHash;
  Int_t      oflHash;
  Int_t      offlineId;
  Int_t      layer;
  Float_t    eta;
  Float_t    phi;
  Int_t      IEvent;
  Short_t    latomeChannel;
  Int_t      Nsamples;
  Short_t    samples[32];
  Short_t    BCID;
  Int_t      latomeSourceId;
  Int_t      l1idLATOMEHEAD;
  Int_t      detector;
  Double_t   max_rms;
  Double_t   chi2;
  Int_t      nEvents;
  Double_t   new_mean[32];
  Double_t   new_rms[32];
  Double_t   ref_mean[32];
};



void chi2_test(string input_filename, string ref_filename, string output_dir, const double chi2_cut, const double rms_cut)
{

  int height_threshold=5; //pulse trigger
  int max_chi2_hist=20; //maximum ouput png chi2 histogram number
  int max_rms_hist=20; //maximum ouput png rms histogram number
  map<int, short> large_rms_list={{976748544, 1}, {976749056, 1}, {976906240, 1}, {976906752, 1}, {976751104, 1}, {976750592, 1}}; //list of good SCs have large rms

  //skipped channel summary
  cout<<"****** Notice: skipped large rms SC: *****"<<endl;
  int skipSC_counter=0;
  for (auto &it: large_rms_list) {
    cout<<"SC_ONL_ID = "<<it.first<<endl;
    skipSC_counter++;
  }
  cout<<skipSC_counter<<" SCs are skipped for large rms check"<<endl<<endl;

  //calculate pedstal info and make reftree
  map< int, vector<short>[32] > channel_samples;  
  map<Int_t, Double_t[32]> channel_rms;
  map<Int_t, Double_t[32]> channel_meanerror;
  map<Int_t, Double_t[32]> channel_mean;
  map< Int_t,  Float_t > channel_eta;
  map< Int_t,  Float_t > channel_phi;
  map< Int_t,  Int_t > channel_layer;
  map< Int_t,  Int_t > channel_detector;
  

  //read all samples for each SC in test run
  char *test_filename = const_cast<char *>(input_filename.c_str());
  TFile *ftest=TFile::Open(test_filename);
  cout<<"test_tree file opened: "<<test_filename<<endl;
  TTree* test_tree = (TTree*) ftest->Get("LARDIGITS");
  Short_t   samples[32];
  Int_t     channelId;
  Float_t   eta;
  Float_t   phi;
  Int_t layer,detector;
  test_tree->SetBranchAddress("channelId", &channelId);
  test_tree->SetBranchAddress("samples", &samples);
  test_tree->SetBranchAddress("eta", &eta);
  test_tree->SetBranchAddress("phi", &phi);
  test_tree->SetBranchAddress("layer", &layer);
  test_tree->SetBranchAddress("detector", &detector);
  Int_t test_nentries = (Int_t)test_tree->GetEntries();
  //test_tree->Print();
  //cout <<"test_nentries="<<test_nentries<<endl;
  for (Long64_t jentry=0; jentry<test_nentries;jentry++) {
    test_tree->GetEntry(jentry);
    //if (jentry>50000) break; //debug
    pair<Float_t, Float_t> position(eta,phi);
    channel_layer[channelId]=layer;
    channel_detector[channelId]=detector;
    channel_eta[channelId]=eta;
    channel_phi[channelId]=phi;    
    for (unsigned i=0;i<32;i++) {
      if (samples[i] > 0) {
	channel_samples[channelId][i].push_back(samples[i]);
      }
    }
  }

  int channel_No=channel_samples.size();
  cout << channel_No << " SC in all detected!" <<endl;

  //read reference pulse
  char *reftmp_filename = const_cast<char *>(ref_filename.c_str());
  TFile *fref=TFile::Open(reftmp_filename);  
  cout<<"ref_tree file opened: "<<ref_filename<<endl;
  TTree* ref_tree = (TTree*) fref->Get("Reftree");
  //vector<double> ref_mean,ref_rms,ref_mse;
  Double_t ref_mean[32],ref_rms[32],ref_mse[32];
  ref_tree->SetBranchAddress("channelId", &channelId);
  ref_tree->SetBranchAddress("mean", &ref_mean);
  ref_tree->SetBranchAddress("rms", &ref_rms);
  ref_tree->SetBranchAddress("eta", &eta);
  ref_tree->SetBranchAddress("phi", &phi);
  ref_tree->SetBranchAddress("layer", &layer);
  ref_tree->SetBranchAddress("detector", &detector);
  Int_t ref_nentries = (Int_t)ref_tree->GetEntries();
  map< int, Double_t[32]> ref_samples; //channelId_refsamples
  for (Long64_t jentry=0; jentry<ref_nentries;jentry++) {
    ref_tree->GetEntry(jentry);
    for (unsigned i=0;i<32;i++) {
      ref_samples[channelId][i]=ref_mean[i];
    }
  }
  
  //calculate mean, peak flag and mse for each SC in test run
  string output_filename=output_dir+"/chi2_hist.root";
  char *out_filename = const_cast<char *>(output_filename.c_str());
  TFile* newfile = new TFile(out_filename, "recreate");
  newfile->cd();
  TTree* newtree = new TTree("Chi2","tree with computed chi2 (ndf=32)");
  Int_t new_channelId,new_layer,new_detector;

  Int_t new_Nsamples[32];
  Double_t new_rms[32],new_mean[32],new_meanerror[32],new_chi2,new_meanchi2;
  Short_t new_refpeak, new_peak;

  Float_t new_eta,new_phi;
  //LARDIGITS branch
  newtree->Branch("channelId", &new_channelId);
  newtree->Branch("eta", &new_eta);
  newtree->Branch("phi", &new_phi);
  newtree->Branch("layer", &new_layer);
  newtree->Branch("detector", &new_detector);
  //new branches
  newtree->Branch("mean", new_mean,"mean[32]/D");
  newtree->Branch("ref_mean", ref_mean,"mean[32]/D");
  newtree->Branch("rms", new_rms,"rms[32]/D");
  newtree->Branch("meanerror", new_meanerror,"meanerror[32]/D");
  newtree->Branch("chi2", &new_chi2);
  newtree->Branch("meanchi2", &new_meanchi2);
  newtree->Branch("Nsamples", new_Nsamples,"Nsamples[32]/I");
  newtree->Branch("refpeak", &new_refpeak);
  newtree->Branch("peak", &new_peak);
  
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gErrorIgnoreLevel = kWarning;
  std::map<double, SCinfo> problematicSC_chi2;
  std::map<int, SCinfo> no_refpulse_SC; //list for no ref pulse channel (channel_SCinfo)
  std::map<int, SCinfo> no_testpulse_SC; //list for no test pulse channel
  std::map<double, SCinfo> problematicSC_rms;
  TH1D* chi2_dis = new TH1D("chi2_dis", "chi2_dis", 500, 0., 10000.);	
  TH1D* rms_dis = new TH1D("RMS_dis", "RMS_dis", 100, 0., 100.);	

  int counter=0;
  ofstream outputtxt(output_dir+"/chi2_hist.txt");
  outputtxt<<"inputfile: "<<input_filename<<"\n";
  outputtxt<<"max_RMS cut="<<rms_cut<<setw(3)<<" "<<"chi2 cut="<<chi2_cut<<"\n";
  outputtxt<<left<<setw(11)<<"Index"<<setw(15)<<"SC_ONL_ID"<<setw(11)<<"eta"<<setw(11)<<"phi"<<setw(11)<<"nEvents"<<setw(11)<<"max_RMS"<<setw(1)<<"chi2"<<"\n";  
  for (auto it = channel_samples.begin(); it != channel_samples.end(); it++)
    {
      counter++;
      //if (counter>5000) break; //debug
      new_channelId=it->first;
      new_eta=channel_eta[new_channelId];
      new_phi=channel_phi[new_channelId]; 
      new_layer=channel_layer[new_channelId];
      new_detector=channel_detector[new_channelId];
      double chi2=0;
      double max_rms=0;
      int sample_No=0;
      pair<short, double> tmp_refmax; //SC peak position of reference run
      pair<short, double> tmp_max; //SC peak position of test run      
      for (int sam=0; sam<32; sam++) {
	Double_t mean=0;
	Double_t refmean=0;
	Double_t rms=0;
	Double_t mse=0;
	sample_No=it->second[sam].size();
	Double_t sum = std::accumulate(std::begin(it->second[sam]), std::end(it->second[sam]), 0.0);
	if (sample_No != 0) {
	  mean = sum/double(sample_No);
	  refmean = ref_samples[new_channelId][sam];
	  //find peak position
	  if (refmean>tmp_refmax.second) {
	    tmp_refmax.first=sam;
	    tmp_refmax.second=refmean;
	  }
	  if (mean>tmp_max.second) {
	    tmp_max.first=sam;
	    tmp_max.second=mean;
	  }
	  
	  Double_t var=0;	
	  Double_t diff=0;	
	  for (int i=0;i<sample_No;i++) {
	    var += pow(it->second[sam][i]-mean,2);
	  }
	  if (var != 0) rms = sqrt(var/double(sample_No));
	  else rms = 1/sqrt(12);
	  //mse = rms/sqrt(sample_No); //tight cut
	  mse=rms;//loose cut
	  chi2+=pow((mean-refmean)/mse,2);	    
	}	
	ref_mean[sam]=refmean;
	new_mean[sam]=mean;
	new_rms[sam]=rms;
	new_meanerror[sam]=mse;
	new_Nsamples[sam]=sample_No;	
	if (new_rms[sam]>max_rms)  max_rms=new_rms[sam];
      }
      
      new_peak=tmp_max.first;
      new_refpeak=tmp_refmax.first;      
      new_chi2=chi2;
      new_meanchi2=chi2/32;
      newtree->Fill();

      //if (tmp_max.second-new_mean[0] < height_threshold || tmp_refmax.second-ref_mean[0] < height_threshold) continue; //debug
      
      //make plots and list for problematic SCs	
      chi2_dis->Fill(chi2);    
      rms_dis->Fill(max_rms);

      //Fill SCinfo
      SCinfo tmp_SC;
      tmp_SC.channelId=new_channelId;
      tmp_SC.detector=new_detector;
      tmp_SC.layer=new_layer;
      tmp_SC.chi2=chi2;
      tmp_SC.max_rms=max_rms;
      tmp_SC.eta=new_eta;
      tmp_SC.phi=new_phi;
      tmp_SC.nEvents=sample_No;
      copy(new_mean,new_mean+32,tmp_SC.new_mean);
      copy(new_rms,new_rms+32,tmp_SC.new_rms);
      copy(ref_mean,ref_mean+32,tmp_SC.ref_mean);
      //cout<<"channelId="<<new_channelId<<"; height=" << tmp_max.second-new_mean[0]<<endl;

      //make list for different kind of SCs
      if (tmp_refmax.second-ref_mean[0] < height_threshold) no_refpulse_SC.insert(std::make_pair(new_channelId,tmp_SC));    //list for no reference pulse SC
      if (tmp_max.second-new_mean[0] < height_threshold) no_testpulse_SC.insert(std::make_pair(new_channelId,tmp_SC));      //list for no test pulse SC	
      if (chi2>chi2_cut) problematicSC_chi2.insert(std::make_pair(chi2,tmp_SC));	                                    //list for large chi2 SC
      if (max_rms>rms_cut && large_rms_list[new_channelId]!=1) problematicSC_rms.insert(std::make_pair(max_rms,tmp_SC));    //list for large rms SC
    }
  cout <<"ProblematicSC_chi2.size()="<<problematicSC_chi2.size()<<endl;
  cout <<"ProblematicSC_rms.size()="<<problematicSC_rms.size()<<endl;
  cout <<"no_refpusle_SC.size()="<<no_refpulse_SC.size()<<endl;
  cout <<"no_testpusle_SC.size()="<<no_testpulse_SC.size()<<endl;

  //make not pulsed sc list
  cout<<"****** following SCs are not pulsed in both reference run and test run (height<"<<height_threshold<<"):"<<endl;
  ofstream nopulsetxt(output_dir+"/not_pulsed_SC.txt");
  nopulsetxt<<"inputfile: "<<input_filename<<"\n";
  nopulsetxt<<"Height cut="<<height_threshold<<"\n";
  nopulsetxt<<left<<setw(11)<<"Index"<<setw(15)<<"SC_ONL_ID"<<setw(11)<<"eta"<<setw(11)<<"phi"<<setw(11)<<"nEvents"<<setw(11)<<"max_RMS"<<setw(1)<<"chi2"<<"\n"; 
  int nopulse_counter=0;
  for (auto &it: no_refpulse_SC) {
    if (no_testpulse_SC.find(it.first) != no_testpulse_SC.end()) {
      cout<<"SC_ONL_ID = "<< it.first <<endl;
      nopulsetxt<<left<<setw(11)<<nopulse_counter<<setw(15)<<it.second.channelId<<setw(11)<<it.second.eta<<setw(11)<<it.second.phi<<setw(11)<<it.second.nEvents<<setw(11)<<it.second.max_rms<<setw(11)<<it.second.chi2<<"\n";        
      nopulse_counter++;
    }
  }
  nopulsetxt.close();
  

  string outputpdf=output_dir+"/chi2_hist.pdf";
  int nsc=0;
  int index=0;
  int chi2_counter=0;
  std::map<double,SCinfo>::reverse_iterator iter = problematicSC_chi2.rbegin();

  double eta_min=-5;
  double eta_max=5;
  double phi_min=-3.2;
  double phi_max=3.2;
  int eta_bin=100;
  int phi_bin=100;
  double eta_step=(eta_max-eta_min)/double(eta_bin);
  double phi_step=(phi_max-phi_min)/double(phi_bin);
  TH2D* chi2_sum = new TH2D("chi2_summary", "chi2_summary", eta_bin, eta_min, eta_max, phi_bin, phi_min, phi_max);
  TH2D* rms_sum = new TH2D("rms_summary", "rms_summary", eta_bin, eta_min, eta_max, phi_bin, phi_min, phi_max);
  TH2D* sum_plt = new TH2D("summary_plot", "summary_plot", eta_bin, eta_min, eta_max, phi_bin, phi_min, phi_max);
  

  cout<<endl<<"***********************   Start to process chi2 plots   **********************"<<endl;
  for(; iter!=problematicSC_chi2.rend(); ++iter){
    //output txt file
    double chi2=0;
    double max_rms=0;
    int sample_No=0;
    double *test_samples;
    double *test_rms;
    double *ref_samples;
    
    index++;    
    outputtxt<<left<<setw(11)<<index<<setw(15)<<iter->second.channelId<<setw(11)<<iter->second.eta<<setw(11)<<iter->second.phi<<setw(11)<<iter->second.nEvents<<setw(11)<<iter->second.max_rms<<setw(11)<<iter->second.chi2<<"\n";        
    chi2_sum->SetBinContent((iter->second.eta-eta_min)/eta_step+1,(iter->second.phi-phi_min)/phi_step+1,1);
    sum_plt->SetBinContent((iter->second.eta-eta_min)/eta_step+1,(iter->second.phi-phi_min)/phi_step+1,1);
    if (nsc>=max_chi2_hist) continue;

    new_channelId=iter->second.channelId;  
    new_detector=iter->second.detector;
    new_layer=iter->second.layer;
    chi2=iter->second.chi2;
    max_rms=iter->second.max_rms;
    new_eta=iter->second.eta;
    new_phi=iter->second.phi;
    sample_No=iter->second.nEvents;
    test_samples=iter->second.new_mean;
    test_rms=iter->second.new_rms;
    ref_samples=iter->second.ref_mean;    
    cout<<"channelId="<<new_channelId<<"; detector="<<new_detector<<"; eta="<<new_eta<<endl; //debug_mode
      
    char histname[100];
    char ref_histname[100];
    char cname[100];
    std::stringstream chi2_ss;
    chi2_ss << std::setprecision(3) << chi2;
    std::stringstream rms_ss;
    rms_ss << std::setprecision(3) << max_rms;
    sprintf(histname,"SC%d_detector%d_layer%d_chi2_%s_maxrms_%s",new_channelId,detector,layer,chi2_ss.str().c_str(),rms_ss.str().c_str());
    sprintf(ref_histname,"ref_SC%d_detector%d_layer%d",new_channelId,detector,layer);
    sprintf(cname,"Canvas_%d_detector%d_layer%d",new_channelId,detector,layer);
   
    TCanvas *c1 = new TCanvas(cname,cname,1500,800);
    THStack *hs = new THStack("hs",histname);
    TH1D* test_pulse = new TH1D(histname, histname, 32, 0., 32.);
    test_pulse->SetDirectory(0);
    TH1D* rms_hist = new TH1D("rms_hist", "rms_hist", 32, 0., 32.);
    rms_hist->SetDirectory(0);
    TH1D* ref_pulse = new TH1D(ref_histname, ref_histname, 32, 0., 32.);	
    ref_pulse->SetDirectory(0);
	
    double fontsize=0.04;
    for (int sam=0;sam<32;sam++) {
      test_pulse->SetBinContent(sam+1, test_samples[sam]);
      ref_pulse->SetBinContent(sam+1, ref_samples[sam]);
      rms_hist->SetBinContent(sam+1, test_rms[sam]);
    }

    string str_hist=histname;
    string title=str_hist+";samples;ADC";
    test_pulse->SetLineColor(kGreen);
    ref_pulse->SetLineColor(kBlue);
	
    test_pulse->SetLineWidth(2);
    ref_pulse->SetLineWidth(1);


    test_pulse->SetMarkerSize(1);
    ref_pulse->SetMarkerSize(1);


    hs->SetTitle(title.c_str());
    hs->Add(test_pulse);
    hs->Add(ref_pulse);
    hs->Draw("nostack");
    hs->GetYaxis()->SetTitleOffset(0.9);

    hs->GetXaxis()->SetTitleSize(fontsize);
    hs->GetYaxis()->SetTitleSize(fontsize);

    c1->cd();
	
    //create a transparent pad drawn on top of the main pad
    TPad *pad = new TPad("pad","",0,0,1,1);
    pad->SetFillStyle(4000);
    pad->SetFillColor(0);
    pad->SetFrameFillStyle(4000);	
    pad->Draw();
    pad->cd();
    rms_hist->SetTitle(";;RMS");
    rms_hist->GetYaxis()->SetTitleOffset(0.6);
    rms_hist->GetYaxis()->SetAxisColor(kRed);
    rms_hist->GetYaxis()->SetLabelColor(kRed);
    rms_hist->GetYaxis()->SetTitleColor(kRed);
    rms_hist->GetYaxis()->SetTitleSize(fontsize);
    rms_hist->SetLineColor(kRed);
    rms_hist->SetLineWidth(1);
    rms_hist->SetLineStyle(8);	
    rms_hist->Draw("Y+");
	
    //draw legend
    auto legend = new TLegend(0.3, 0.7, 0.3+0.25, 0.7+0.15); // (x1, y1, x2, y2)
    legend->AddEntry(test_pulse, "Test Pulse" );
    legend->AddEntry(ref_pulse, "Reference Pulse" );
    legend->AddEntry(rms_hist, "Test Pulse RMS" );
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
    legend->Draw();
	

    //draw SC info with TLatex
    string str_channelId="SC_ONL_ID="+to_string(int(new_channelId));	
    string str_entry="# of Entries="+to_string(int(sample_No));	
    string str_etaphi="#eta="+to_string(float(new_eta))+"  #phi="+to_string(float(new_phi));	
    string str_chi2 ="#chi^{2}/ndf="+chi2_ss.str()+"/32";
    string str_layer;
    string str_detector;
    switch (new_detector) {
    case 0:
      str_detector="EMB";
      break;
    case 1:
      str_detector="EMEC OW";
      break;
    case 2:
      str_detector="EMEC IW";
      break;
    case 3:
      str_detector="HEC";
      break;
    case 4:
      str_detector="FCAL";
      break;	  
    }
    switch (new_layer) {
    case 0: 
      str_layer="presampler";
      break;
    case 1:
      str_layer="front layer";
      break;
    case 2:
      str_layer="middle layer";
      break;
    case 3:
      str_layer="back layer";
      break;
    }
	
    double x= 0.6;
    double y=0.83;
    double text_size=0.03;
    double y_step=0.04;
    TLatex L1;
    L1.SetNDC();
    L1.SetTextSize(text_size);
    L1.SetTextFont(42);
    L1.SetTextColor(kBlack);
    L1.DrawLatex(x,y,str_entry.c_str());

    TLatex L2_1;
    L2_1.SetNDC();
    L2_1.SetTextSize(text_size);
    L2_1.SetTextFont(42);
    L2_1.SetTextColor(kBlack);
    L2_1.DrawLatex(x,y-y_step,str_channelId.c_str());
	
    TLatex L3;
    L3.SetNDC();
    L3.SetTextSize(text_size);
    L3.SetTextFont(42);
    L3.SetTextColor(kBlack);
    L3.DrawLatex(x,y-2*y_step,str_detector.c_str());

    TLatex L4;
    L4.SetNDC();
    L4.SetTextSize(text_size);
    L4.SetTextFont(42);
    L4.SetTextColor(kBlack);
    L4.DrawLatex(x,y-3*y_step,str_layer.c_str());

    TLatex L5;
    L5.SetNDC();
    L5.SetTextSize(text_size);
    L5.SetTextFont(42);
    L5.SetTextColor(kBlack);
    L5.DrawLatex(x,y-4*y_step,str_chi2.c_str());

    TLatex L6;
    L6.SetNDC();
    L6.SetTextSize(text_size);
    L6.SetTextFont(42);
    L6.SetTextColor(kBlack);
    L6.DrawLatex(x,y-5*y_step,str_etaphi.c_str());
    //end of drawing latex	

    cout<<"channelId="<<iter->second.channelId<<"; eta="<<iter->second.eta<<"; phi="<<iter->second.phi<<"; detector="<<new_detector<<"; nEvents="<<iter->second.nEvents<<"; max_rms="<<iter->second.max_rms<<"; chi2="<<iter->second.chi2<<"\n";
    string outputpng=output_dir+"/"+histname+".png";
    c1->Print(outputpng.c_str()); //debug
    ++chi2_counter;
    
    if (problematicSC_chi2.size()+problematicSC_rms.size() > 1) {
      if(nsc==0) {
	string pdf_start=outputpdf+"[";
	c1->Print(pdf_start.c_str());
	c1->Print(outputpdf.c_str());
	c1->Write();      
	delete c1;
	++nsc;
	continue;
      }
      if(problematicSC_rms.size()==0 && (nsc==max_chi2_hist-1 || nsc==(int)problematicSC_chi2.size()-1)){
	string pdf_end=outputpdf+"]";
	c1->Print(outputpdf.c_str());
	c1->Print(pdf_end.c_str());
	c1->Write();
	delete c1;
	++nsc;
	continue;
      }
    }
    c1->Print(outputpdf.c_str());
    c1->Write();
    delete c1;
    ++nsc;    
    if (nsc%10 == 0) cout<<"Number of SC processed="<<nsc<<endl;
  }

  //output rms plots
  int rms_counter=0;
  std::map<double,SCinfo>::reverse_iterator rms_iter = problematicSC_rms.rbegin();
  cout<<endl<<"***********************   Start to process RMS plots   **********************"<<endl;
  for(; rms_iter!=problematicSC_rms.rend(); ++rms_iter){
    //output txt file
    double chi2=0;
    double max_rms=0;
    int sample_No=0;
    double *test_samples;
    double *test_rms;
    double *ref_samples;
    
    if (rms_iter->second.chi2 <= chi2_cut) {
      index++;
      outputtxt<<left<<setw(11)<<index<<setw(15)<<rms_iter->second.channelId<<setw(11)<<rms_iter->second.eta<<setw(11)<<rms_iter->second.phi<<setw(11)<<rms_iter->second.nEvents<<setw(11)<<rms_iter->second.max_rms<<setw(11)<<rms_iter->second.chi2<<"\n";  
    }

    rms_sum->SetBinContent((rms_iter->second.eta-eta_min)/eta_step+1,(rms_iter->second.phi-phi_min)/phi_step+1,1);
    sum_plt->SetBinContent((rms_iter->second.eta-eta_min)/eta_step+1,(rms_iter->second.phi-phi_min)/phi_step+1,1000);
    if (rms_counter>=max_rms_hist) continue;
    
    new_channelId=rms_iter->second.channelId;  
    new_detector=rms_iter->second.detector;
    new_layer=rms_iter->second.layer;
    chi2=rms_iter->second.chi2;
    max_rms=rms_iter->second.max_rms;
    new_eta=rms_iter->second.eta;
    new_phi=rms_iter->second.phi;
    sample_No=rms_iter->second.nEvents;
    test_samples=rms_iter->second.new_mean;
    test_rms=rms_iter->second.new_rms;
    ref_samples=rms_iter->second.ref_mean;    


    char histname[100];
    char ref_histname[100];
    char cname[100];
    std::stringstream chi2_ss;
    std::stringstream rms_ss;
    chi2_ss << std::setprecision(3) << chi2;
    rms_ss << std::setprecision(3) << max_rms;
    sprintf(histname,"SC%d_detector%d_layer%d_chi2_%s_maxrms_%s",new_channelId,new_detector,layer,chi2_ss.str().c_str(),rms_ss.str().c_str());
    sprintf(ref_histname,"ref_SC%d_detector%d_layer%d",new_channelId,new_detector,layer);
    sprintf(cname,"Canvas_%d_detector%d_layer%d",new_channelId,new_detector,layer);
   
    TCanvas *c1 = new TCanvas(cname,cname,1500,800);
    THStack *hs = new THStack("hs",histname);
    TH1D* test_pulse = new TH1D(histname, histname, 32, 0., 32.);
    test_pulse->SetDirectory(0);
    TH1D* rms_hist = new TH1D("rms_hist", "rms_hist", 32, 0., 32.);
    rms_hist->SetDirectory(0);
    TH1D* ref_pulse = new TH1D(ref_histname, ref_histname, 32, 0., 32.);	
    ref_pulse->SetDirectory(0);
	
    double fontsize=0.04;
    for (int sam=0;sam<32;sam++) {
      test_pulse->SetBinContent(sam+1, test_samples[sam]);
      ref_pulse->SetBinContent(sam+1, ref_samples[sam]);
      rms_hist->SetBinContent(sam+1, test_rms[sam]);
    }

    string str_hist=histname;
    string title=str_hist+";samples;ADC";
    test_pulse->SetLineColor(kGreen);
    ref_pulse->SetLineColor(kBlue);
	
    test_pulse->SetLineWidth(2);
    ref_pulse->SetLineWidth(1);


    test_pulse->SetMarkerSize(1);
    ref_pulse->SetMarkerSize(1);


    hs->SetTitle(title.c_str());
    hs->Add(test_pulse);
    hs->Add(ref_pulse);
    hs->Draw("nostack");
    hs->GetYaxis()->SetTitleOffset(0.9);

    hs->GetXaxis()->SetTitleSize(fontsize);
    hs->GetYaxis()->SetTitleSize(fontsize);

    c1->cd();
	
    //create a transparent pad drawn on top of the main pad
    TPad *pad = new TPad("pad","",0,0,1,1);
    pad->SetFillStyle(4000);
    pad->SetFillColor(0);
    pad->SetFrameFillStyle(4000);	
    pad->Draw();
    pad->cd();
    rms_hist->SetTitle(";;RMS");
    rms_hist->GetYaxis()->SetTitleOffset(0.6);
    rms_hist->GetYaxis()->SetAxisColor(kRed);
    rms_hist->GetYaxis()->SetLabelColor(kRed);
    rms_hist->GetYaxis()->SetTitleColor(kRed);
    rms_hist->GetYaxis()->SetTitleSize(fontsize);
    rms_hist->SetLineColor(kRed);
    rms_hist->SetLineWidth(1);
    rms_hist->SetLineStyle(8);	
    rms_hist->Draw("Y+");
	
    //draw legend
    auto legend = new TLegend(0.3, 0.7, 0.3+0.25, 0.7+0.15); // (x1, y1, x2, y2)
    legend->AddEntry(test_pulse, "Test Pulse" );
    legend->AddEntry(ref_pulse, "Reference Pulse" );
    legend->AddEntry(rms_hist, "Test Pulse RMS" );
    legend->SetBorderSize(0);
    legend->SetFillStyle(0);
    legend->Draw();
	

    //draw SC info with TLatex
    string str_channelId="SC_ONL_ID="+to_string(int(new_channelId));	
    string str_entry="# of Entries="+to_string(int(sample_No));	
    string str_etaphi="#eta="+to_string(float(new_eta))+"  #phi="+to_string(float(new_phi));	
    string str_chi2 ="#chi^{2}/ndf="+chi2_ss.str()+"/32";
    string str_layer;
    string str_detector;
    switch (new_detector) {
    case 0:
      str_detector="EMB";
      break;
    case 1:
      str_detector="EMEC OW";
      break;
    case 2:
      str_detector="EMEC IW";
      break;
    case 3:
      str_detector="HEC";
      break;
    case 4:
      str_detector="FCAL";
      break;	  
    }
    switch (new_layer) {
    case 0: 
      str_layer="presampler";
      break;
    case 1:
      str_layer="front layer";
      break;
    case 2:
      str_layer="middle layer";
      break;
    case 3:
      str_layer="back layer";
      break;
    }
	
    double x= 0.6;
    double y=0.83;
    double text_size=0.03;
    double y_step=0.04;
    TLatex L1;
    L1.SetNDC();
    L1.SetTextSize(text_size);
    L1.SetTextFont(42);
    L1.SetTextColor(kBlack);
    L1.DrawLatex(x,y,str_entry.c_str());

    TLatex L2_1;
    L2_1.SetNDC();
    L2_1.SetTextSize(text_size);
    L2_1.SetTextFont(42);
    L2_1.SetTextColor(kBlack);
    L2_1.DrawLatex(x,y-y_step,str_channelId.c_str());
	
    TLatex L3;
    L3.SetNDC();
    L3.SetTextSize(text_size);
    L3.SetTextFont(42);
    L3.SetTextColor(kBlack);
    L3.DrawLatex(x,y-2*y_step,str_detector.c_str());

    TLatex L4;
    L4.SetNDC();
    L4.SetTextSize(text_size);
    L4.SetTextFont(42);
    L4.SetTextColor(kBlack);
    L4.DrawLatex(x,y-3*y_step,str_layer.c_str());

    TLatex L5;
    L5.SetNDC();
    L5.SetTextSize(text_size);
    L5.SetTextFont(42);
    L5.SetTextColor(kBlack);
    L5.DrawLatex(x,y-4*y_step,str_chi2.c_str());

    TLatex L6;
    L6.SetNDC();
    L6.SetTextSize(text_size);
    L6.SetTextFont(42);
    L6.SetTextColor(kBlack);
    L6.DrawLatex(x,y-5*y_step,str_etaphi.c_str());
    //end of drawing latex	

    cout<<"channelId="<<rms_iter->second.channelId<<"; eta="<<rms_iter->second.eta<<"; phi="<<rms_iter->second.phi<<"; nEvents="<<rms_iter->second.nEvents<<"; max_rms="<<rms_iter->second.max_rms<<"; chi2="<<rms_iter->second.chi2<<"\n";        
    string outputpng=output_dir+"/"+histname+".png";
    c1->Print(outputpng.c_str()); //debug
    ++rms_counter;

    if (int(problematicSC_chi2.size()+problematicSC_rms.size()) > 1) {
      if(nsc==0) {
	string pdf_start=outputpdf+"[";
	c1->Print(pdf_start.c_str());
	c1->Print(outputpdf.c_str());
	c1->Write();      
	delete c1;
	++nsc;
	continue;
      }
      if(rms_counter==(int)problematicSC_rms.size() || rms_counter==max_rms_hist ){
	string pdf_end=outputpdf+"]";
	c1->Print(outputpdf.c_str());
	c1->Print(pdf_end.c_str());
	c1->Write();
	delete c1;
	++nsc;
	continue;
      }
    }
    c1->Print(outputpdf.c_str());
    c1->Write();
    delete c1;
    ++nsc;
    if (nsc%10 == 0) cout<<"Number of SC processed="<<nsc<<endl;
  }
  cout <<"Total number of histograms in PDF="<<nsc<<endl;

  
  outputtxt.close();
  
  //draw summary plot
  TCanvas *c1 = new TCanvas("summary_plot","summary_plot",1300,950);  
  //gStyle->SetOptStat(0);
  gPad->SetRightMargin(0.14);
  gPad->SetLeftMargin(0.12);
  gPad->SetTopMargin(0.08);
  gPad->SetBottomMargin(0.16);
  sum_plt->SetTitle(";#eta;#phi");
  sum_plt->GetXaxis()->SetLabelSize(0.05);
  sum_plt->GetXaxis()->SetTitleOffset(1.1);
  sum_plt->GetXaxis()->SetTitleSize(0.07);
  sum_plt->GetYaxis()->SetLabelSize(0.05);
  sum_plt->GetYaxis()->SetTitleOffset(0.7);
  sum_plt->GetYaxis()->SetTitleSize(0.07);
  gPad->Modified();
  gStyle->SetPalette(1);
  sum_plt->Draw("COL");

  double x=0.14;
  double y=0.88;
  string chi2_plot="Blue: #chi^{2} > "+ to_string(int(chi2_cut));
  string rms_plot= "Red: RMS > "+ to_string(int(rms_cut));

  TLatex l1; //legend line1
  l1.SetNDC();
  l1.SetTextSize(0.04);
  l1.SetTextFont(42);
  l1.SetTextColor(kBlue);
  l1.DrawLatex(x,y, chi2_plot.c_str());
  
  TLatex l2; //legend line2
  l2.SetNDC();
  l2.SetTextSize(0.04);
  l2.SetTextFont(42);
  l2.SetTextColor(kRed);
  l2.DrawLatex(x,y-0.05, rms_plot.c_str());

  TLatex l3;//set title
  l3.SetNDC();
  l3.SetTextSize(0.06);
  l3.SetTextFont(42);
  l3.SetTextColor(kBlack);
  l3.DrawLatex(0.35,0.95, "#chi^{2} test summary");

  c1->Write();
  string summary_name=output_dir+"/summary_plot.png";
  c1->Print(summary_name.c_str());
  //sum_plt->Write();  
  delete c1;

  chi2_dis->SetTitle("#chi^{2} distribution;#chi^{2};Entries");
  chi2_dis->SetStats(1);
  chi2_dis->Write();
  rms_dis->SetTitle("max RMS distribution;max RMS;Entries");
  rms_dis->SetStats(1);
  rms_dis->Write();
  chi2_sum->Write();
  rms_sum->Write();
  newtree->AutoSave();
  newfile->Close();

  //gApplication->Terminate();
}

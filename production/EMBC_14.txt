# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMB
# Side: C
# QuadrantID: 3
# AMCHashNumber: 1
# CarrierID: 26
# AMCNumberInCarrier: 4

# Input information section
# Number connected LTDBs: 2
# Connected LTDBs: EMB(H14R)  EMB(H15L)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   H14R_01_10  RX01_03  B_1F_B1  B_2F_B1  B_4F_B1  B_3F_B1  B_6F_B1  B_5F_B1  B_8F_B1  B_7F_B1  
IN F4   H14R_01_09  RX01_04  B_2F_M2  B_2F_M1  B_2F_M4  B_2F_M3  B_1F_M3  B_1F_M4  B_1F_M1  B_1F_M2  
IN F5   H14R_01_08  RX01_05  B_3F_M1  B_3F_M2  B_3F_M4  B_3F_M3  B_4F_M4  B_4F_M3  B_4F_M2  B_4F_M1  
IN F6   H14R_01_07  RX01_06  B_6F_F4  B_6F_F3  B_6F_F2  B_6F_F1  B_5F_F1  B_5F_F2  B_5F_F3  B_5F_F4  
IN F7   H14R_01_06  RX01_07  B_7F_M1  B_7F_M2  B_7F_M3  B_7F_M4  B_8F_M3  B_8F_M4  B_8F_M2  B_8F_M1  
IN F8   H14R_01_05  RX01_08  B_8F_P1  B_7F_P1  B_5F_P1  B_6F_P1  B_4F_P1  B_3F_P1  B_2F_P1  B_1F_P1  
IN F9   H14R_01_04  RX01_09  B_5F_M1  B_5F_M2  B_5F_M3  B_5F_M4  B_6F_M4  B_6F_M3  B_6F_M1  B_6F_M2  
IN F10  H14R_01_03  RX01_10  B_4F_F1  B_4F_F2  B_4F_F4  B_4F_F3  B_3F_F4  B_3F_F3  B_3F_F2  B_3F_F1  
IN F11  H14R_01_02  RX01_11  B_7F_F1  B_7F_F2  B_7F_F3  B_7F_F4  B_8F_F1  B_8F_F2  B_8F_F4  B_8F_F3  
IN F12  H14R_01_01  RX01_12  B_2F_F3  B_2F_F4  B_2F_F2  B_2F_F1  B_1F_F4  B_1F_F3  B_1F_F2  B_1F_F1  
IN F15  H14R_04_10  RX02_03  B_1E_F2  B_1E_F1  B_1E_F4  B_1E_F3  B_2E_F1  B_2E_F2  B_2E_F3  B_2E_F4  
IN F16  H14R_04_09  RX02_04  B_8E_M2  B_8E_M1  B_8E_M4  B_8E_M3  B_7E_M4  B_7E_M3  B_7E_M1  B_7E_M2  
IN F17  H14R_04_08  RX02_05  B_3E_F1  B_3E_F2  B_3E_F3  B_3E_F4  B_4E_F1  B_4E_F2  B_4E_F4  B_4E_F3  
IN F18  H14R_04_07  RX02_06  B_6E_M3  B_6E_M4  B_6E_M1  B_6E_M2  B_5E_M2  B_5E_M1  B_5E_M4  B_5E_M3  
IN F19  H14R_04_06  RX02_07  B_5E_F2  B_5E_F1  B_5E_F3  B_5E_F4  B_6E_F4  B_6E_F3  B_6E_F1  B_6E_F2  
IN F20  H14R_04_05  RX02_08  B_3E_M1  B_3E_M2  B_3E_M3  B_3E_M4  B_4E_M4  B_4E_M3  B_4E_M1  B_4E_M2  
IN F21  H14R_04_04  RX02_09  B_7E_F2  B_7E_F1  B_7E_F3  B_7E_F4  B_8E_F2  B_8E_F1  B_8E_F4  B_8E_F3  
IN F22  H14R_04_03  RX02_10  B_7E_P1  B_8E_P1  B_6E_P1  B_5E_P1  B_2E_P1  B_1E_P1  B_3E_P1  B_4E_P1  
IN F23  H14R_04_02  RX02_11  B_1E_M2  B_1E_M1  B_1E_M3  B_1E_M4  B_2E_M4  B_2E_M3  B_2E_M1  B_2E_M2  
IN F24  H14R_04_01  RX02_12  B_7E_B1  B_8E_B1  B_6E_B1  B_5E_B1  B_1E_B1  B_2E_B1  B_3E_B1  B_4E_B1  
IN F27  H15L_01_10  RX03_03  B_1H_B1  B_2H_B1  B_4H_B1  B_3H_B1  B_6H_B1  B_5H_B1  B_8H_B1  B_7H_B1  
IN F28  H15L_01_09  RX03_04  B_2H_M2  B_2H_M1  B_2H_M4  B_2H_M3  B_1H_M3  B_1H_M4  B_1H_M1  B_1H_M2  
IN F29  H15L_01_08  RX03_05  B_3H_M1  B_3H_M2  B_3H_M4  B_3H_M3  B_4H_M4  B_4H_M3  B_4H_M2  B_4H_M1  
IN F30  H15L_01_07  RX03_06  B_6H_F4  B_6H_F3  B_6H_F2  B_6H_F1  B_5H_F1  B_5H_F2  B_5H_F3  B_5H_F4  
IN F31  H15L_01_06  RX03_07  B_7H_M1  B_7H_M2  B_7H_M3  B_7H_M4  B_8H_M3  B_8H_M4  B_8H_M2  B_8H_M1  
IN F32  H15L_01_05  RX03_08  B_8H_P1  B_7H_P1  B_5H_P1  B_6H_P1  B_4H_P1  B_3H_P1  B_2H_P1  B_1H_P1  
IN F33  H15L_01_04  RX03_09  B_5H_M1  B_5H_M2  B_5H_M3  B_5H_M4  B_6H_M4  B_6H_M3  B_6H_M1  B_6H_M2  
IN F34  H15L_01_03  RX03_10  B_4H_F1  B_4H_F2  B_4H_F4  B_4H_F3  B_3H_F4  B_3H_F3  B_3H_F2  B_3H_F1  
IN F35  H15L_01_02  RX03_11  B_7H_F1  B_7H_F2  B_7H_F3  B_7H_F4  B_8H_F1  B_8H_F2  B_8H_F4  B_8H_F3  
IN F36  H15L_01_01  RX03_12  B_2H_F3  B_2H_F4  B_2H_F2  B_2H_F1  B_1H_F4  B_1H_F3  B_1H_F2  B_1H_F1  
IN F39  H15L_04_10  RX04_03  B_1G_F2  B_1G_F1  B_1G_F4  B_1G_F3  B_2G_F1  B_2G_F2  B_2G_F3  B_2G_F4  
IN F40  H15L_04_09  RX04_04  B_8G_M2  B_8G_M1  B_8G_M4  B_8G_M3  B_7G_M4  B_7G_M3  B_7G_M1  B_7G_M2  
IN F41  H15L_04_08  RX04_05  B_3G_F1  B_3G_F2  B_3G_F3  B_3G_F4  B_4G_F1  B_4G_F2  B_4G_F4  B_4G_F3  
IN F42  H15L_04_07  RX04_06  B_6G_M3  B_6G_M4  B_6G_M1  B_6G_M2  B_5G_M2  B_5G_M1  B_5G_M4  B_5G_M3  
IN F43  H15L_04_06  RX04_07  B_5G_F2  B_5G_F1  B_5G_F3  B_5G_F4  B_6G_F4  B_6G_F3  B_6G_F1  B_6G_F2  
IN F44  H15L_04_05  RX04_08  B_3G_M1  B_3G_M2  B_3G_M3  B_3G_M4  B_4G_M4  B_4G_M3  B_4G_M1  B_4G_M2  
IN F45  H15L_04_04  RX04_09  B_7G_F2  B_7G_F1  B_7G_F3  B_7G_F4  B_8G_F2  B_8G_F1  B_8G_F4  B_8G_F3  
IN F46  H15L_04_03  RX04_10  B_7G_P1  B_8G_P1  B_6G_P1  B_5G_P1  B_2G_P1  B_1G_P1  B_3G_P1  B_4G_P1  
IN F47  H15L_04_02  RX04_11  B_1G_M2  B_1G_M1  B_1G_M3  B_1G_M4  B_2G_M4  B_2G_M3  B_2G_M1  B_2G_M2  
IN F48  H15L_04_01  RX04_12  B_7G_B1  B_8G_B1  B_6G_B1  B_5G_B1  B_1G_B1  B_2G_B1  B_3G_B1  B_4G_B1  

# Output information section
# Number connected output fibers: 28 (unique=19)
# Number eFEX fibers: 20 (unique=16)
# Number jFEX fibers: 6 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   B_7E          B_8E          
OUT F2   eFEX   B_7E          B_8E          
OUT F3   eFEX   B_5E          B_6E          
OUT F4   eFEX   B_3E          B_4E          
OUT F5   eFEX   B_1E          B_2E          
OUT F7   eFEX   B_7F          B_8F          
OUT F8   eFEX   B_7F          B_8F          
OUT F9   eFEX   B_5F          B_6F          
OUT F10  eFEX   B_3F          B_4F          
OUT F11  eFEX   B_1F          B_2F          
OUT F19  eFEX   B_7G          B_8G          
OUT F20  eFEX   B_7G          B_8G          
OUT F21  eFEX   B_5G          B_6G          
OUT F22  eFEX   B_3G          B_4G          
OUT F23  eFEX   B_1G          B_2G          
OUT F31  eFEX   B_7H          B_8H          
OUT F32  eFEX   B_7H          B_8H          
OUT F33  eFEX   B_5H          B_6H          
OUT F34  eFEX   B_3H          B_4H          
OUT F35  eFEX   B_1H          B_2H          
OUT F37  jFEX   B_5E          B_5F          B_5G          B_5H          B_6E          B_6F          B_6G          B_6H          B_7E          B_7F          B_7G          B_7H          B_8E          B_8F          B_8G          B_8H          
OUT F38  jFEX   B_5E          B_5F          B_5G          B_5H          B_6E          B_6F          B_6G          B_6H          B_7E          B_7F          B_7G          B_7H          B_8E          B_8F          B_8G          B_8H          
OUT F39  jFEX   B_5E          B_5F          B_5G          B_5H          B_6E          B_6F          B_6G          B_6H          B_7E          B_7F          B_7G          B_7H          B_8E          B_8F          B_8G          B_8H          
OUT F41  jFEX   B_1E          B_1F          B_1G          B_1H          B_2E          B_2F          B_2G          B_2H          B_3E          B_3F          B_3G          B_3H          B_4E          B_4F          B_4G          B_4H          
OUT F42  jFEX   B_1E          B_1F          B_1G          B_1H          B_2E          B_2F          B_2G          B_2H          B_3E          B_3F          B_3G          B_3H          B_4E          B_4F          B_4G          B_4H          
OUT F43  jFEX   B_1E          B_1F          B_1G          B_1H          B_2E          B_2F          B_2G          B_2H          B_3E          B_3F          B_3G          B_3H          B_4E          B_4F          B_4G          B_4H          
OUT F47  gFEX   B_1E+B_2E+B_1F+B_2F  B_1G+B_2G+B_1H+B_2H  B_3E+B_4E+B_3F+B_4F  B_3G+B_4G+B_3H+B_4H  B_5E+B_6E+B_5F+B_6F  B_5G+B_6G+B_5H+B_6H  B_7E+B_8E+B_7F+B_8F  B_7G+B_8G+B_7H+B_8H  
OUT F48  gFEX   B_1E+B_2E+B_1F+B_2F  B_1G+B_2G+B_1H+B_2H  B_3E+B_4E+B_3F+B_4F  B_3G+B_4G+B_3H+B_4H  B_5E+B_6E+B_5F+B_6F  B_5G+B_6G+B_5H+B_6H  B_7E+B_8E+B_7F+B_8F  B_7G+B_8G+B_7H+B_8H  

# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMB
# Side: A
# QuadrantID: 3
# AMCHashNumber: 3
# CarrierID: 12
# AMCNumberInCarrier: 4

# Input information section
# Number connected LTDBs: 2
# Connected LTDBs: EMB(I16L)  EMB(I01R)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   I16L_01_10  RX01_03  B_1M_B1  B_2M_B1  B_4M_B1  B_3M_B1  B_6M_B1  B_5M_B1  B_8M_B1  B_7M_B1  
IN F4   I16L_01_09  RX01_04  B_2M_M2  B_2M_M1  B_2M_M4  B_2M_M3  B_1M_M3  B_1M_M4  B_1M_M1  B_1M_M2  
IN F5   I16L_01_08  RX01_05  B_3M_M1  B_3M_M2  B_3M_M4  B_3M_M3  B_4M_M4  B_4M_M3  B_4M_M2  B_4M_M1  
IN F6   I16L_01_07  RX01_06  B_6M_F4  B_6M_F3  B_6M_F2  B_6M_F1  B_5M_F1  B_5M_F2  B_5M_F3  B_5M_F4  
IN F7   I16L_01_06  RX01_07  B_7M_M1  B_7M_M2  B_7M_M3  B_7M_M4  B_8M_M3  B_8M_M4  B_8M_M2  B_8M_M1  
IN F8   I16L_01_05  RX01_08  B_8M_P1  B_7M_P1  B_5M_P1  B_6M_P1  B_4M_P1  B_3M_P1  B_2M_P1  B_1M_P1  
IN F9   I16L_01_04  RX01_09  B_5M_M1  B_5M_M2  B_5M_M3  B_5M_M4  B_6M_M4  B_6M_M3  B_6M_M1  B_6M_M2  
IN F10  I16L_01_03  RX01_10  B_4M_F1  B_4M_F2  B_4M_F4  B_4M_F3  B_3M_F4  B_3M_F3  B_3M_F2  B_3M_F1  
IN F11  I16L_01_02  RX01_11  B_7M_F1  B_7M_F2  B_7M_F3  B_7M_F4  B_8M_F1  B_8M_F2  B_8M_F4  B_8M_F3  
IN F12  I16L_01_01  RX01_12  B_2M_F3  B_2M_F4  B_2M_F2  B_2M_F1  B_1M_F4  B_1M_F3  B_1M_F2  B_1M_F1  
IN F15  I16L_04_10  RX02_03  B_1N_F2  B_1N_F1  B_1N_F4  B_1N_F3  B_2N_F1  B_2N_F2  B_2N_F3  B_2N_F4  
IN F16  I16L_04_09  RX02_04  B_8N_M2  B_8N_M1  B_8N_M4  B_8N_M3  B_7N_M4  B_7N_M3  B_7N_M1  B_7N_M2  
IN F17  I16L_04_08  RX02_05  B_3N_F1  B_3N_F2  B_3N_F3  B_3N_F4  B_4N_F1  B_4N_F2  B_4N_F4  B_4N_F3  
IN F18  I16L_04_07  RX02_06  B_6N_M3  B_6N_M4  B_6N_M1  B_6N_M2  B_5N_M2  B_5N_M1  B_5N_M4  B_5N_M3  
IN F19  I16L_04_06  RX02_07  B_5N_F2  B_5N_F1  B_5N_F3  B_5N_F4  B_6N_F4  B_6N_F3  B_6N_F1  B_6N_F2  
IN F20  I16L_04_05  RX02_08  B_3N_M1  B_3N_M2  B_3N_M3  B_3N_M4  B_4N_M4  B_4N_M3  B_4N_M1  B_4N_M2  
IN F21  I16L_04_04  RX02_09  B_7N_F2  B_7N_F1  B_7N_F3  B_7N_F4  B_8N_F2  B_8N_F1  B_8N_F4  B_8N_F3  
IN F22  I16L_04_03  RX02_10  B_7N_P1  B_8N_P1  B_6N_P1  B_5N_P1  B_2N_P1  B_1N_P1  B_3N_P1  B_4N_P1  
IN F23  I16L_04_02  RX02_11  B_1N_M2  B_1N_M1  B_1N_M3  B_1N_M4  B_2N_M4  B_2N_M3  B_2N_M1  B_2N_M2  
IN F24  I16L_04_01  RX02_12  B_7N_B1  B_8N_B1  B_6N_B1  B_5N_B1  B_1N_B1  B_2N_B1  B_3N_B1  B_4N_B1  
IN F27  I01R_01_10  RX03_03  B_1O_B1  B_2O_B1  B_4O_B1  B_3O_B1  B_6O_B1  B_5O_B1  B_8O_B1  B_7O_B1  
IN F28  I01R_01_09  RX03_04  B_2O_M2  B_2O_M1  B_2O_M4  B_2O_M3  B_1O_M3  B_1O_M4  B_1O_M1  B_1O_M2  
IN F29  I01R_01_08  RX03_05  B_3O_M1  B_3O_M2  B_3O_M4  B_3O_M3  B_4O_M4  B_4O_M3  B_4O_M2  B_4O_M1  
IN F30  I01R_01_07  RX03_06  B_6O_F4  B_6O_F3  B_6O_F2  B_6O_F1  B_5O_F1  B_5O_F2  B_5O_F3  B_5O_F4  
IN F31  I01R_01_06  RX03_07  B_7O_M1  B_7O_M2  B_7O_M3  B_7O_M4  B_8O_M3  B_8O_M4  B_8O_M2  B_8O_M1  
IN F32  I01R_01_05  RX03_08  B_8O_P1  B_7O_P1  B_5O_P1  B_6O_P1  B_4O_P1  B_3O_P1  B_2O_P1  B_1O_P1  
IN F33  I01R_01_04  RX03_09  B_5O_M1  B_5O_M2  B_5O_M3  B_5O_M4  B_6O_M4  B_6O_M3  B_6O_M1  B_6O_M2  
IN F34  I01R_01_03  RX03_10  B_4O_F1  B_4O_F2  B_4O_F4  B_4O_F3  B_3O_F4  B_3O_F3  B_3O_F2  B_3O_F1  
IN F35  I01R_01_02  RX03_11  B_7O_F1  B_7O_F2  B_7O_F3  B_7O_F4  B_8O_F1  B_8O_F2  B_8O_F4  B_8O_F3  
IN F36  I01R_01_01  RX03_12  B_2O_F3  B_2O_F4  B_2O_F2  B_2O_F1  B_1O_F4  B_1O_F3  B_1O_F2  B_1O_F1  
IN F39  I01R_04_10  RX04_03  B_1P_F2  B_1P_F1  B_1P_F4  B_1P_F3  B_2P_F1  B_2P_F2  B_2P_F3  B_2P_F4  
IN F40  I01R_04_09  RX04_04  B_8P_M2  B_8P_M1  B_8P_M4  B_8P_M3  B_7P_M4  B_7P_M3  B_7P_M1  B_7P_M2  
IN F41  I01R_04_08  RX04_05  B_3P_F1  B_3P_F2  B_3P_F3  B_3P_F4  B_4P_F1  B_4P_F2  B_4P_F4  B_4P_F3  
IN F42  I01R_04_07  RX04_06  B_6P_M3  B_6P_M4  B_6P_M1  B_6P_M2  B_5P_M2  B_5P_M1  B_5P_M4  B_5P_M3  
IN F43  I01R_04_06  RX04_07  B_5P_F2  B_5P_F1  B_5P_F3  B_5P_F4  B_6P_F4  B_6P_F3  B_6P_F1  B_6P_F2  
IN F44  I01R_04_05  RX04_08  B_3P_M1  B_3P_M2  B_3P_M3  B_3P_M4  B_4P_M4  B_4P_M3  B_4P_M1  B_4P_M2  
IN F45  I01R_04_04  RX04_09  B_7P_F2  B_7P_F1  B_7P_F3  B_7P_F4  B_8P_F2  B_8P_F1  B_8P_F4  B_8P_F3  
IN F46  I01R_04_03  RX04_10  B_7P_P1  B_8P_P1  B_6P_P1  B_5P_P1  B_2P_P1  B_1P_P1  B_3P_P1  B_4P_P1  
IN F47  I01R_04_02  RX04_11  B_1P_M2  B_1P_M1  B_1P_M3  B_1P_M4  B_2P_M4  B_2P_M3  B_2P_M1  B_2P_M2  
IN F48  I01R_04_01  RX04_12  B_7P_B1  B_8P_B1  B_6P_B1  B_5P_B1  B_1P_B1  B_2P_B1  B_3P_B1  B_4P_B1  

# Output information section
# Number connected output fibers: 28 (unique=19)
# Number eFEX fibers: 20 (unique=16)
# Number jFEX fibers: 6 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   B_7M          B_8M          
OUT F5   eFEX   B_7M          B_8M          
OUT F2   eFEX   B_1M          B_2M          
OUT F3   eFEX   B_3M          B_4M          
OUT F4   eFEX   B_5M          B_6M          
OUT F7   eFEX   B_7N          B_8N          
OUT F11  eFEX   B_7N          B_8N          
OUT F8   eFEX   B_1N          B_2N          
OUT F9   eFEX   B_3N          B_4N          
OUT F10  eFEX   B_5N          B_6N          
OUT F19  eFEX   B_7O          B_8O          
OUT F23  eFEX   B_7O          B_8O          
OUT F20  eFEX   B_1O          B_2O          
OUT F21  eFEX   B_3O          B_4O          
OUT F22  eFEX   B_5O          B_6O          
OUT F31  eFEX   B_7P          B_8P          
OUT F35  eFEX   B_7P          B_8P          
OUT F32  eFEX   B_1P          B_2P          
OUT F33  eFEX   B_3P          B_4P          
OUT F34  eFEX   B_5P          B_6P          
OUT F37  jFEX   B_1M          B_1N          B_1O          B_1P          B_2M          B_2N          B_2O          B_2P          B_3M          B_3N          B_3O          B_3P          B_4M          B_4N          B_4O          B_4P          
OUT F38  jFEX   B_1M          B_1N          B_1O          B_1P          B_2M          B_2N          B_2O          B_2P          B_3M          B_3N          B_3O          B_3P          B_4M          B_4N          B_4O          B_4P          
OUT F39  jFEX   B_1M          B_1N          B_1O          B_1P          B_2M          B_2N          B_2O          B_2P          B_3M          B_3N          B_3O          B_3P          B_4M          B_4N          B_4O          B_4P          
OUT F41  jFEX   B_5M          B_5N          B_5O          B_5P          B_6M          B_6N          B_6O          B_6P          B_7M          B_7N          B_7O          B_7P          B_8M          B_8N          B_8O          B_8P          
OUT F42  jFEX   B_5M          B_5N          B_5O          B_5P          B_6M          B_6N          B_6O          B_6P          B_7M          B_7N          B_7O          B_7P          B_8M          B_8N          B_8O          B_8P          
OUT F43  jFEX   B_5M          B_5N          B_5O          B_5P          B_6M          B_6N          B_6O          B_6P          B_7M          B_7N          B_7O          B_7P          B_8M          B_8N          B_8O          B_8P          
OUT F47  gFEX   B_1M+B_2M+B_1N+B_2N  B_1O+B_2O+B_1P+B_2P  B_3M+B_4M+B_3N+B_4N  B_3O+B_4O+B_3P+B_4P  B_5M+B_6M+B_5N+B_6N  B_5O+B_6O+B_5P+B_6P  B_7M+B_8M+B_7N+B_8N  B_7O+B_8O+B_7P+B_8P  
OUT F48  gFEX   B_1M+B_2M+B_1N+B_2N  B_1O+B_2O+B_1P+B_2P  B_3M+B_4M+B_3N+B_4N  B_3O+B_4O+B_3P+B_4P  B_5M+B_6M+B_5N+B_6N  B_5O+B_6O+B_5P+B_6P  B_7M+B_8M+B_7N+B_8N  B_7O+B_8O+B_7P+B_8P  

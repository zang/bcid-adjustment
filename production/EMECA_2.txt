# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMEC
# Side: A
# QuadrantID: 0
# AMCHashNumber: 1
# CarrierID: 3
# AMCNumberInCarrier: 2

# Input information section
# Number connected LTDBs: 1
# Connected LTDBs: EMEC(A01L)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   A01L_01_10  RX01_03  E_7E_B1  E_8E_B1  E_8F_B1  E_7F_B1  E_10E_B1 E_9E_B1  E_10F_B1 E_9F_B1  
IN F4   A01L_01_09  RX01_04  E_4E_M2  E_4E_M1  E_4E_M4  E_4E_M3  E_4F_M3  E_4F_M4  E_4F_M1  E_4F_M2  
IN F5   A01L_01_08  RX01_05  E_3F_M1  E_3F_M2  E_3F_M4  E_3F_M3  E_3E_M4  E_3E_M3  E_3E_M2  E_3E_M1  
IN F6   A01L_01_07  RX01_06  E_5F_F4  E_5F_F3  E_5F_F2  E_5F_F1  E_5E_F4  E_5E_F3  E_5E_F2  E_5E_F1  
IN F7   A01L_01_06  RX01_07  E_6E_M1  E_6E_M2  E_6E_M3  E_6E_M4  E_6F_M3  E_6F_M4  E_6F_M2  E_6F_M1  
IN F8   A01L_01_05  RX01_08  GND      GND      GND      GND      E_4F_P1  E_4E_P1  E_3F_P1  E_3E_P1  
IN F9   A01L_01_04  RX01_09  E_5E_M1  E_5E_M2  E_5E_M3  E_5E_M4  E_5F_M4  E_5F_M3  E_5F_M1  E_5F_M2  
IN F10  A01L_01_03  RX01_10  E_4F_F1  E_4F_F2  E_4F_F4  E_4F_F3  E_4E_F4  E_4E_F3  E_4E_F2  E_4E_F1  
IN F11  A01L_01_02  RX01_11  E_6E_F2  E_6E_F1  E_5E_F6  E_5E_F5  E_5F_F5  E_5F_F6  E_6F_F2  E_6F_F1  
IN F12  A01L_01_01  RX01_12  E_3F_F3  E_3F_F4  E_3F_F2  E_3F_F1  E_3E_F4  E_3E_F3  E_3E_F2  E_3E_F1  
IN F15  A01L_02_10  RX02_03  E_4H_B1  E_3H_B1  E_4G_B1  E_3G_B1  E_6H_B1  E_5H_B1  E_5G_B1  E_6G_B1  
IN F16  A01L_02_09  RX02_04  E_9F_F2  E_9F_F1  E_9F_F4  E_9F_F3  E_9E_F4  E_9E_F3  E_9E_F1  E_9E_F2  
IN F17  A01L_02_08  RX02_05  E_10H_F4 E_10H_F3 E_10H_F2 E_10H_F1 E_10G_F4 E_10G_F3 E_10G_F2 E_10G_F1 
IN F18  A01L_02_07  RX02_06  E_6F_F4  E_6F_F3  E_6F_F6  E_6F_F5  E_6E_F5  E_6E_F6  E_6E_F3  E_6E_F4  
IN F19  A01L_02_06  RX02_07  E_8E_F3  E_8E_F4  E_8E_F2  E_8E_F1  E_8F_F1  E_8F_F2  E_8F_F4  E_8F_F3  
IN F20  A01L_02_05  RX02_08  E_10F_M4 E_10F_M3 E_10F_M2 E_10F_M1 E_10E_M4 E_10E_M3 E_10E_M1 E_10E_M2 
IN F21  A01L_02_04  RX02_09  E_7E_F2  E_7E_F1  E_7E_F4  E_7E_F3  E_7F_F1  E_7F_F2  E_7F_F4  E_7F_F3  
IN F22  A01L_02_03  RX02_10  E_8E_M4  E_8E_M3  E_8E_M1  E_8E_M2  E_8F_M4  E_8F_M3  E_8F_M1  E_8F_M2  
IN F23  A01L_02_02  RX02_11  E_9F_M4  E_9F_M3  E_9F_M2  E_9F_M1  E_9E_M1  E_9E_M2  E_9E_M4  E_9E_M3  
IN F24  A01L_02_01  RX02_12  E_7F_M2  E_7F_M1  E_7F_M4  E_7F_M3  E_7E_M4  E_7E_M3  E_7E_M1  E_7E_M2  
IN F27  A01L_03_10  RX03_03  E_9H_M1  E_9H_M2  E_9H_M4  E_9H_M3  E_9G_M3  E_9G_M4  E_9G_M1  E_9G_M2  
IN F28  A01L_03_09  RX03_04  E_5G_F1  E_5G_F2  E_5G_F4  E_5G_F3  E_5H_F4  E_5H_F3  E_5H_F1  E_5H_F2  
IN F29  A01L_03_08  RX03_05  E_10G_M4 E_10G_M3 E_10G_M1 E_10G_M2 E_10H_M2 E_10H_M1 E_10H_M4 E_10H_M3 
IN F30  A01L_03_07  RX03_06  E_8H_M4  E_8H_M3  E_8H_M2  E_8H_M1  E_8G_M1  E_8G_M2  E_8G_M3  E_8G_M4  
IN F31  A01L_03_06  RX03_07  E_7G_M3  E_7G_M4  E_7G_M1  E_7G_M2  E_7H_M1  E_7H_M2  E_7H_M4  E_7H_M3  
IN F32  A01L_03_05  RX03_08  E_8H_F1  E_8H_F2  E_8H_F3  E_8H_F4  E_8G_F1  E_8G_F2  E_8G_F3  E_8G_F4  
IN F33  A01L_03_04  RX03_09  E_7G_F2  E_7G_F1  E_7G_F4  E_7G_F3  E_7H_F2  E_7H_F1  E_7H_F4  E_7H_F3  
IN F34  A01L_03_03  RX03_10  E_5E_B1  E_6E_B1  E_5F_B1  E_6F_B1  E_3E_B1  E_4E_B1  E_3F_B1  E_4F_B1  
IN F35  A01L_03_02  RX03_11  E_9G_F4  E_9G_F3  E_9G_F1  E_9G_F2  E_9H_F1  E_9H_F2  E_9H_F4  E_9H_F3  
IN F36  A01L_03_01  RX03_12  E_10F_F4 E_10F_F3 E_10F_F1 E_10F_F2 E_10E_F1 E_10E_F2 E_10E_F4 E_10E_F3 
IN F39  A01L_04_10  RX04_03  E_3G_F2  E_3G_F1  E_3G_F4  E_3G_F3  E_3H_F1  E_3H_F2  E_3H_F3  E_3H_F4  
IN F40  A01L_04_09  RX04_04  E_5H_M3  E_5H_M4  E_5H_M1  E_5H_M2  E_5G_M1  E_5G_M2  E_5G_M4  E_5G_M3  
IN F41  A01L_04_08  RX04_05  E_4G_F1  E_4G_F2  E_4G_F3  E_4G_F4  E_4H_F1  E_4H_F2  E_4H_F4  E_4H_F3  
IN F42  A01L_04_07  RX04_06  E_6H_M2  E_6H_M1  E_6H_M4  E_6H_M3  E_6G_M3  E_6G_M4  E_6G_M1  E_6G_M2  
IN F43  A01L_04_06  RX04_07  E_5G_F6  E_5G_F5  E_6G_F1  E_6G_F2  E_5H_F5  E_5H_F6  E_6H_F2  E_6H_F1  
IN F44  A01L_04_05  RX04_08  E_4G_M4  E_4G_M3  E_4G_M2  E_4G_M1  E_4H_M4  E_4H_M3  E_4H_M1  E_4H_M2  
IN F45  A01L_04_04  RX04_09  E_6G_F4  E_6G_F3  E_6G_F5  E_6G_F6  E_6H_F5  E_6H_F6  E_6H_F3  E_6H_F4  
IN F46  A01L_04_03  RX04_10  GND      GND      GND      GND      E_3H_P1  E_3G_P1  E_4G_P1  E_4H_P1  
IN F47  A01L_04_02  RX04_11  E_3G_M2  E_3G_M1  E_3G_M3  E_3G_M4  E_3H_M4  E_3H_M3  E_3H_M1  E_3H_M2  
IN F48  A01L_04_01  RX04_12  E_9H_B1  E_10H_B1 E_10G_B1 E_9G_B1  E_7G_B1  E_8G_B1  E_7H_B1  E_8H_B1  

# Output information section
# Number connected output fibers: 22 (unique=19)
# Number eFEX fibers: 16 (unique=16)
# Number jFEX fibers: 4 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F2   eFEX   E_3E          E_4E          
OUT F3   eFEX   E_5E          E_6E          
OUT F4   eFEX   E_7E          E_8E          
OUT F5   eFEX   E_10E         E_9E          
OUT F8   eFEX   E_3F          E_4F          
OUT F9   eFEX   E_5F          E_6F          
OUT F10  eFEX   E_7F          E_8F          
OUT F11  eFEX   E_10F         E_9F          
OUT F20  eFEX   E_3G          E_4G          
OUT F21  eFEX   E_5G          E_6G          
OUT F22  eFEX   E_7G          E_8G          
OUT F23  eFEX   E_10G         E_9G          
OUT F32  eFEX   E_3H          E_4H          
OUT F33  eFEX   E_5H          E_6H          
OUT F34  eFEX   E_7H          E_8H          
OUT F35  eFEX   E_10H         E_9H          
OUT F37  jFEX   E_3E          E_3F          E_3G          E_3H          E_4E          E_4F          E_4G          E_4H          E_5E          E_5F          E_5G          E_5H          E_6E          E_6F          E_6G          E_6H          
OUT F38  jFEX   E_3E          E_3F          E_3G          E_3H          E_4E          E_4F          E_4G          E_4H          E_5E          E_5F          E_5G          E_5H          E_6E          E_6F          E_6G          E_6H          
OUT F41  jFEX   E_10E         E_10F         E_10G         E_10H         E_7E          E_7F          E_7G          E_7H          E_8E          E_8F          E_8G          E_8H          E_9E          E_9F          E_9G          E_9H          
OUT F42  jFEX   E_10E         E_10F         E_10G         E_10H         E_7E          E_7F          E_7G          E_7H          E_8E          E_8F          E_8G          E_8H          E_9E          E_9F          E_9G          E_9H          
OUT F47  gFEX   E_3E+E_4E+E_3F+E_4F  E_3G+E_4G+E_3H+E_4H  E_5E+E_6E+E_5F+E_6F  E_5G+E_6G+E_5H+E_6H  E_7E+E_8E+E_7F+E_8F  E_7G+E_8G+E_7H+E_8H  E_9E+E_10E+E_9F+E_10F  E_9G+E_10G+E_9H+E_10H  
OUT F48  gFEX   E_3E+E_4E+E_3F+E_4F  E_3G+E_4G+E_3H+E_4H  E_5E+E_6E+E_5F+E_6F  E_5G+E_6G+E_5H+E_6H  E_7E+E_8E+E_7F+E_8F  E_7G+E_8G+E_7H+E_8H  E_9E+E_10E+E_9F+E_10F  E_9G+E_10G+E_9H+E_10H  

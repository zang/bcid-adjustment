# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMBEMEC
# Side: A
# QuadrantID: 2
# AMCHashNumber: 2
# CarrierID: 10
# AMCNumberInCarrier: 1

# Input information section
# Number connected LTDBs: 3
# Connected LTDBs: EMB(I11L)  EMB(I12R)  EMECSpec0(A09S0)  
# Number connected input MTPs: 5
# Number connected input fibers: 44
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F1   A09S0_03_08 RX01_01  E_2L_F1  E_2L_F2  E_2L_F3  E_2L_F4  E_2K_F4  E_2K_F3  E_2K_F1  E_2K_F2  
IN F2   A09S0_03_07 RX01_02  E_2J_F2  E_2J_F1  E_2J_F3  E_2J_F4  E_2I_F3  E_2I_F4  E_2I_F1  E_2I_F2  
IN F4   I11L_03_09  RX01_04  GND      B_15J_P1 B_13J_P1 B_14J_P1 B_9J_P1  B_10J_P1 B_12J_P1 B_11J_P1 
IN F5   I11L_03_08  RX01_05  B_13J_M1 B_13J_M2 B_13J_M4 B_13J_M3 B_14J_M3 B_14J_M4 B_14J_M1 B_14J_M2 
IN F6   I11L_03_07  RX01_06  B_10J_M1 B_10J_M2 B_10J_M3 B_10J_M4 B_9J_M4  B_9J_M3  B_9J_M2  B_9J_M1  
IN F7   I11L_03_06  RX01_07  B_11J_M3 B_11J_M4 B_11J_M1 B_11J_M2 B_12J_M1 B_12J_M2 B_12J_M4 B_12J_M3 
IN F8   I11L_03_05  RX01_08  B_12J_F1 B_12J_F2 B_12J_F3 B_12J_F4 B_11J_F1 B_11J_F2 B_11J_F3 B_11J_F4 
IN F9   I11L_03_04  RX01_09  B_9J_F2  B_9J_F1  B_9J_F4  B_9J_F3  B_10J_F2 B_10J_F1 B_10J_F4 B_10J_F3 
IN F10  I11L_03_03  RX01_10  B_13I_B1 B_14I_B1 GND      GND      B_9I_B1  B_10I_B1 B_11I_B1 B_12I_B1 
IN F11  I11L_03_02  RX01_11  B_13J_F4 B_13J_F3 B_13J_F1 B_13J_F2 B_14J_F1 B_14J_F2 B_14J_F4 B_14J_F3 
IN F12  I11L_03_01  RX01_12  B_15I_B4 B_15I_B3 B_15I_B1 B_15I_B2 GND      GND      GND      GND      
IN F13  A09S0_03_04 RX02_01  E_2K_M2  E_2K_M1  E_2K_M4  E_2K_M3  E_2L_M4  E_2L_M3  E_2L_M1  E_2L_M2  
IN F14  A09S0_03_03 RX02_02  E_1J_M1  E_1J_M2  E_1J_M3  E_1J_M4  E_1I_M1  E_1I_M2  E_1I_M3  E_1I_M4  
IN F15  I12R_02_10  RX02_03  B_12L_B1 B_11L_B1 B_10L_B1 B_9L_B1  GND      GND      B_13L_B1 B_14L_B1 
IN F16  I12R_02_09  RX02_04  B_14K_F2 B_14K_F1 B_14K_F4 B_14K_F3 B_13K_F4 B_13K_F3 B_13K_F1 B_13K_F2 
IN F17  I12R_02_08  RX02_05  B_15L_B1 B_15L_B2 B_15L_B3 B_15L_B4 GND      GND      GND      GND      
IN F18  I12R_02_07  RX02_06  B_14K_P1 B_13K_P1 GND      B_15K_P1 B_11K_P1 B_12K_P1 B_9K_P1  B_10K_P1 
IN F19  I12R_02_06  RX02_07  B_11K_F2 B_11K_F1 B_11K_F3 B_11K_F4 B_12K_F1 B_12K_F2 B_12K_F4 B_12K_F3 
IN F20  I12R_02_05  RX02_08  B_9K_M1  B_9K_M2  B_9K_M3  B_9K_M4  B_10K_M1 B_10K_M2 B_10K_M4 B_10K_M3 
IN F21  I12R_02_04  RX02_09  B_9K_F2  B_9K_F1  B_9K_F4  B_9K_F3  B_10K_F1 B_10K_F2 B_10K_F4 B_10K_F3 
IN F22  I12R_02_03  RX02_10  B_14K_M4 B_14K_M3 B_14K_M1 B_14K_M2 B_13K_M4 B_13K_M3 B_13K_M1 B_13K_M2 
IN F23  I12R_02_02  RX02_11  B_11K_M1 B_11K_M2 B_11K_M3 B_11K_M4 B_12K_M4 B_12K_M3 B_12K_M1 B_12K_M2 
IN F25  A09S0_03_02 RX03_01  GND      GND      GND      GND      E_1K_F1  E_1L_F1  E_1J_F1  E_1I_F1  
IN F26  A09S0_03_01 RX03_02  E_1K_M1  E_1K_M2  E_1K_M3  E_1K_M4  E_1L_M1  E_1L_M2  E_1L_M4  E_1L_M3  
IN F28  I12R_03_09  RX03_04  GND      B_15L_P1 B_13L_P1 B_14L_P1 B_9L_P1  B_10L_P1 B_12L_P1 B_11L_P1 
IN F29  I12R_03_08  RX03_05  B_13L_M1 B_13L_M2 B_13L_M4 B_13L_M3 B_14L_M3 B_14L_M4 B_14L_M1 B_14L_M2 
IN F30  I12R_03_07  RX03_06  B_10L_M1 B_10L_M2 B_10L_M3 B_10L_M4 B_9L_M4  B_9L_M3  B_9L_M2  B_9L_M1  
IN F31  I12R_03_06  RX03_07  B_11L_M3 B_11L_M4 B_11L_M1 B_11L_M2 B_12L_M1 B_12L_M2 B_12L_M4 B_12L_M3 
IN F32  I12R_03_05  RX03_08  B_12L_F1 B_12L_F2 B_12L_F3 B_12L_F4 B_11L_F1 B_11L_F2 B_11L_F3 B_11L_F4 
IN F33  I12R_03_04  RX03_09  B_9L_F2  B_9L_F1  B_9L_F4  B_9L_F3  B_10L_F2 B_10L_F1 B_10L_F4 B_10L_F3 
IN F34  I12R_03_03  RX03_10  B_13K_B1 B_14K_B1 GND      GND      B_9K_B1  B_10K_B1 B_11K_B1 B_12K_B1 
IN F35  I12R_03_02  RX03_11  B_13L_F4 B_13L_F3 B_13L_F1 B_13L_F2 B_14L_F1 B_14L_F2 B_14L_F4 B_14L_F3 
IN F36  I12R_03_01  RX03_12  B_15K_B4 B_15K_B3 B_15K_B1 B_15K_B2 GND      GND      GND      GND      
IN F37  A09S0_03_06 RX04_01  E_2L_P1  E_2K_P1  E_2I_P1  E_2J_P1  E_2I_B1  E_2J_B1  E_2L_B1  E_2K_B1  
IN F38  A09S0_03_05 RX04_02  E_2J_M1  E_2J_M2  E_2J_M3  E_2J_M4  E_2I_M1  E_2I_M2  E_2I_M3  E_2I_M4  
IN F39  I11L_02_10  RX04_03  B_12J_B1 B_11J_B1 B_10J_B1 B_9J_B1  GND      GND      B_13J_B1 B_14J_B1 
IN F40  I11L_02_09  RX04_04  B_14I_F2 B_14I_F1 B_14I_F4 B_14I_F3 B_13I_F4 B_13I_F3 B_13I_F1 B_13I_F2 
IN F41  I11L_02_08  RX04_05  B_15J_B1 B_15J_B2 B_15J_B3 B_15J_B4 GND      GND      GND      GND      
IN F42  I11L_02_07  RX04_06  B_14I_P1 B_13I_P1 GND      B_15I_P1 B_11I_P1 B_12I_P1 B_9I_P1  B_10I_P1 
IN F43  I11L_02_06  RX04_07  B_11I_F2 B_11I_F1 B_11I_F3 B_11I_F4 B_12I_F1 B_12I_F2 B_12I_F4 B_12I_F3 
IN F44  I11L_02_05  RX04_08  B_9I_M1  B_9I_M2  B_9I_M3  B_9I_M4  B_10I_M1 B_10I_M2 B_10I_M4 B_10I_M3 
IN F45  I11L_02_04  RX04_09  B_9I_F2  B_9I_F1  B_9I_F4  B_9I_F3  B_10I_F1 B_10I_F2 B_10I_F4 B_10I_F3 
IN F46  I11L_02_03  RX04_10  B_14I_M4 B_14I_M3 B_14I_M1 B_14I_M2 B_13I_M4 B_13I_M3 B_13I_M1 B_13I_M2 
IN F47  I11L_02_02  RX04_11  B_11I_M1 B_11I_M2 B_11I_M3 B_11I_M4 B_12I_M4 B_12I_M3 B_12I_M1 B_12I_M2 

# Output information section
# Number connected output fibers: 38 (unique=19)
# Number eFEX fibers: 30 (unique=16)
# Number jFEX fibers: 6 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   B_10I         B_9I          
OUT F2   eFEX   B_10I         B_9I          
OUT F3   eFEX   B_11I         B_12I         
OUT F4   eFEX   B_13I         B_14I         
OUT F5   eFEX   B_15I+E_1I    E_2I          
OUT F7   eFEX   B_10J         B_9J          
OUT F8   eFEX   B_10J         B_9J          
OUT F13  eFEX   B_10J         B_9J          
OUT F14  eFEX   B_10J         B_9J          
OUT F9   eFEX   B_11J         B_12J         
OUT F15  eFEX   B_11J         B_12J         
OUT F10  eFEX   B_13J         B_14J         
OUT F16  eFEX   B_13J         B_14J         
OUT F11  eFEX   B_15J+E_1J    E_2J          
OUT F17  eFEX   B_15J+E_1J    E_2J          
OUT F19  eFEX   B_10K         B_9K          
OUT F20  eFEX   B_10K         B_9K          
OUT F25  eFEX   B_10K         B_9K          
OUT F26  eFEX   B_10K         B_9K          
OUT F21  eFEX   B_11K         B_12K         
OUT F27  eFEX   B_11K         B_12K         
OUT F22  eFEX   B_13K         B_14K         
OUT F28  eFEX   B_13K         B_14K         
OUT F23  eFEX   B_15K+E_1K    E_2K          
OUT F29  eFEX   B_15K+E_1K    E_2K          
OUT F31  eFEX   B_10L         B_9L          
OUT F32  eFEX   B_10L         B_9L          
OUT F33  eFEX   B_11L         B_12L         
OUT F34  eFEX   B_13L         B_14L         
OUT F35  eFEX   B_15L+E_1L    E_2L          
OUT F37  jFEX   B_10I         B_10J         B_10K         B_10L         B_11I         B_11J         B_11K         B_11L         B_12I         B_12J         B_12K         B_12L         B_9I          B_9J          B_9K          B_9L          
OUT F38  jFEX   B_10I         B_10J         B_10K         B_10L         B_11I         B_11J         B_11K         B_11L         B_12I         B_12J         B_12K         B_12L         B_9I          B_9J          B_9K          B_9L          
OUT F39  jFEX   B_10I         B_10J         B_10K         B_10L         B_11I         B_11J         B_11K         B_11L         B_12I         B_12J         B_12K         B_12L         B_9I          B_9J          B_9K          B_9L          
OUT F41  jFEX   B_13I         B_13J         B_13K         B_13L         B_14I         B_14J         B_14K         B_14L         B_15I+E_1I    B_15J+E_1J    B_15K+E_1K    B_15L+E_1L    E_2I          E_2J          E_2K          E_2L          
OUT F42  jFEX   B_13I         B_13J         B_13K         B_13L         B_14I         B_14J         B_14K         B_14L         B_15I+E_1I    B_15J+E_1J    B_15K+E_1K    B_15L+E_1L    E_2I          E_2J          E_2K          E_2L          
OUT F43  jFEX   B_13I         B_13J         B_13K         B_13L         B_14I         B_14J         B_14K         B_14L         B_15I+E_1I    B_15J+E_1J    B_15K+E_1K    B_15L+E_1L    E_2I          E_2J          E_2K          E_2L          
OUT F47  gFEX   B_11I+B_12I+B_11J+B_12J  B_11K+B_12K+B_11L+B_12L  B_13I+B_14I+B_13J+B_14J  B_13K+B_14K+B_13L+B_14L  B_15I+B_15J+E_1I+E_2I+E_1J+E_2J  B_15K+B_15L+E_1K+E_2K+E_1L+E_2L  B_9I+B_10I+B_9J+B_10J  B_9K+B_10K+B_9L+B_10L  
OUT F48  gFEX   B_11I+B_12I+B_11J+B_12J  B_11K+B_12K+B_11L+B_12L  B_13I+B_14I+B_13J+B_14J  B_13K+B_14K+B_13L+B_14L  B_15I+B_15J+E_1I+E_2I+E_1J+E_2J  B_15K+B_15L+E_1K+E_2K+E_1L+E_2L  B_9I+B_10I+B_9J+B_10J  B_9K+B_10K+B_9L+B_10L  

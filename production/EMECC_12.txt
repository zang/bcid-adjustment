# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMEC
# Side: C
# QuadrantID: 2
# AMCHashNumber: 3
# CarrierID: 25
# AMCNumberInCarrier: 4

# Input information section
# Number connected LTDBs: 1
# Connected LTDBs: EMEC(C10R)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   C10R_01_10  RX01_03  E_7P_B1  E_8P_B1  E_8O_B1  E_7O_B1  E_10P_B1 E_9P_B1  E_10O_B1 E_9O_B1  
IN F4   C10R_01_09  RX01_04  E_4P_M2  E_4P_M1  E_4P_M4  E_4P_M3  E_4O_M3  E_4O_M4  E_4O_M1  E_4O_M2  
IN F5   C10R_01_08  RX01_05  E_3O_M1  E_3O_M2  E_3O_M4  E_3O_M3  E_3P_M4  E_3P_M3  E_3P_M2  E_3P_M1  
IN F6   C10R_01_07  RX01_06  E_5O_F4  E_5O_F3  E_5O_F2  E_5O_F1  E_5P_F4  E_5P_F3  E_5P_F2  E_5P_F1  
IN F7   C10R_01_06  RX01_07  E_6P_M1  E_6P_M2  E_6P_M3  E_6P_M4  E_6O_M3  E_6O_M4  E_6O_M2  E_6O_M1  
IN F8   C10R_01_05  RX01_08  GND      GND      GND      GND      E_4O_P1  E_4P_P1  E_3O_P1  E_3P_P1  
IN F9   C10R_01_04  RX01_09  E_5P_M1  E_5P_M2  E_5P_M3  E_5P_M4  E_5O_M4  E_5O_M3  E_5O_M1  E_5O_M2  
IN F10  C10R_01_03  RX01_10  E_4O_F1  E_4O_F2  E_4O_F4  E_4O_F3  E_4P_F4  E_4P_F3  E_4P_F2  E_4P_F1  
IN F11  C10R_01_02  RX01_11  E_6P_F2  E_6P_F1  E_5P_F6  E_5P_F5  E_5O_F5  E_5O_F6  E_6O_F2  E_6O_F1  
IN F12  C10R_01_01  RX01_12  E_3O_F3  E_3O_F4  E_3O_F2  E_3O_F1  E_3P_F4  E_3P_F3  E_3P_F2  E_3P_F1  
IN F15  C10R_02_10  RX02_03  E_4M_B1  E_3M_B1  E_4N_B1  E_3N_B1  E_6M_B1  E_5M_B1  E_5N_B1  E_6N_B1  
IN F16  C10R_02_09  RX02_04  E_9O_F2  E_9O_F1  E_9O_F4  E_9O_F3  E_9P_F4  E_9P_F3  E_9P_F1  E_9P_F2  
IN F17  C10R_02_08  RX02_05  E_10M_F4 E_10M_F3 E_10M_F2 E_10M_F1 E_10N_F4 E_10N_F3 E_10N_F2 E_10N_F1 
IN F18  C10R_02_07  RX02_06  E_6O_F4  E_6O_F3  E_6O_F6  E_6O_F5  E_6P_F5  E_6P_F6  E_6P_F3  E_6P_F4  
IN F19  C10R_02_06  RX02_07  E_8P_F3  E_8P_F4  E_8P_F2  E_8P_F1  E_8O_F1  E_8O_F2  E_8O_F4  E_8O_F3  
IN F20  C10R_02_05  RX02_08  E_10O_M4 E_10O_M3 E_10O_M2 E_10O_M1 E_10P_M4 E_10P_M3 E_10P_M1 E_10P_M2 
IN F21  C10R_02_04  RX02_09  E_7P_F2  E_7P_F1  E_7P_F4  E_7P_F3  E_7O_F1  E_7O_F2  E_7O_F4  E_7O_F3  
IN F22  C10R_02_03  RX02_10  E_8P_M4  E_8P_M3  E_8P_M1  E_8P_M2  E_8O_M4  E_8O_M3  E_8O_M1  E_8O_M2  
IN F23  C10R_02_02  RX02_11  E_9O_M4  E_9O_M3  E_9O_M2  E_9O_M1  E_9P_M1  E_9P_M2  E_9P_M4  E_9P_M3  
IN F24  C10R_02_01  RX02_12  E_7O_M2  E_7O_M1  E_7O_M4  E_7O_M3  E_7P_M4  E_7P_M3  E_7P_M1  E_7P_M2  
IN F27  C10R_03_10  RX03_03  E_9M_M1  E_9M_M2  E_9M_M4  E_9M_M3  E_9N_M3  E_9N_M4  E_9N_M1  E_9N_M2  
IN F28  C10R_03_09  RX03_04  E_5N_F1  E_5N_F2  E_5N_F4  E_5N_F3  E_5M_F4  E_5M_F3  E_5M_F1  E_5M_F2  
IN F29  C10R_03_08  RX03_05  E_10N_M4 E_10N_M3 E_10N_M1 E_10N_M2 E_10M_M2 E_10M_M1 E_10M_M4 E_10M_M3 
IN F30  C10R_03_07  RX03_06  E_8M_M4  E_8M_M3  E_8M_M2  E_8M_M1  E_8N_M1  E_8N_M2  E_8N_M3  E_8N_M4  
IN F31  C10R_03_06  RX03_07  E_7N_M3  E_7N_M4  E_7N_M1  E_7N_M2  E_7M_M1  E_7M_M2  E_7M_M4  E_7M_M3  
IN F32  C10R_03_05  RX03_08  E_8M_F1  E_8M_F2  E_8M_F3  E_8M_F4  E_8N_F1  E_8N_F2  E_8N_F3  E_8N_F4  
IN F33  C10R_03_04  RX03_09  E_7N_F2  E_7N_F1  E_7N_F4  E_7N_F3  E_7M_F2  E_7M_F1  E_7M_F4  E_7M_F3  
IN F34  C10R_03_03  RX03_10  E_5P_B1  E_6P_B1  E_5O_B1  E_6O_B1  E_3P_B1  E_4P_B1  E_3O_B1  E_4O_B1  
IN F35  C10R_03_02  RX03_11  E_9N_F4  E_9N_F3  E_9N_F1  E_9N_F2  E_9M_F1  E_9M_F2  E_9M_F4  E_9M_F3  
IN F36  C10R_03_01  RX03_12  E_10O_F4 E_10O_F3 E_10O_F1 E_10O_F2 E_10P_F1 E_10P_F2 E_10P_F4 E_10P_F3 
IN F39  C10R_04_10  RX04_03  E_3N_F2  E_3N_F1  E_3N_F4  E_3N_F3  E_3M_F1  E_3M_F2  E_3M_F3  E_3M_F4  
IN F40  C10R_04_09  RX04_04  E_5M_M3  E_5M_M4  E_5M_M1  E_5M_M2  E_5N_M1  E_5N_M2  E_5N_M4  E_5N_M3  
IN F41  C10R_04_08  RX04_05  E_4N_F1  E_4N_F2  E_4N_F3  E_4N_F4  E_4M_F1  E_4M_F2  E_4M_F4  E_4M_F3  
IN F42  C10R_04_07  RX04_06  E_6M_M2  E_6M_M1  E_6M_M4  E_6M_M3  E_6N_M3  E_6N_M4  E_6N_M1  E_6N_M2  
IN F43  C10R_04_06  RX04_07  E_5N_F6  E_5N_F5  E_6N_F1  E_6N_F2  E_5M_F5  E_5M_F6  E_6M_F2  E_6M_F1  
IN F44  C10R_04_05  RX04_08  E_4N_M4  E_4N_M3  E_4N_M2  E_4N_M1  E_4M_M4  E_4M_M3  E_4M_M1  E_4M_M2  
IN F45  C10R_04_04  RX04_09  E_6N_F4  E_6N_F3  E_6N_F5  E_6N_F6  E_6M_F5  E_6M_F6  E_6M_F3  E_6M_F4  
IN F46  C10R_04_03  RX04_10  GND      GND      GND      GND      E_3M_P1  E_3N_P1  E_4N_P1  E_4M_P1  
IN F47  C10R_04_02  RX04_11  E_3N_M2  E_3N_M1  E_3N_M3  E_3N_M4  E_3M_M4  E_3M_M3  E_3M_M1  E_3M_M2  
IN F48  C10R_04_01  RX04_12  E_9M_B1  E_10M_B1 E_10N_B1 E_9N_B1  E_7N_B1  E_8N_B1  E_7M_B1  E_8M_B1  

# Output information section
# Number connected output fibers: 22 (unique=19)
# Number eFEX fibers: 16 (unique=16)
# Number jFEX fibers: 4 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F2   eFEX   E_10M         E_9M          
OUT F3   eFEX   E_7M          E_8M          
OUT F4   eFEX   E_5M          E_6M          
OUT F5   eFEX   E_3M          E_4M          
OUT F8   eFEX   E_10N         E_9N          
OUT F9   eFEX   E_7N          E_8N          
OUT F10  eFEX   E_5N          E_6N          
OUT F11  eFEX   E_3N          E_4N          
OUT F20  eFEX   E_10O         E_9O          
OUT F21  eFEX   E_7O          E_8O          
OUT F22  eFEX   E_5O          E_6O          
OUT F23  eFEX   E_3O          E_4O          
OUT F32  eFEX   E_10P         E_9P          
OUT F33  eFEX   E_7P          E_8P          
OUT F34  eFEX   E_5P          E_6P          
OUT F35  eFEX   E_3P          E_4P          
OUT F37  jFEX   E_10M         E_10N         E_10O         E_10P         E_7M          E_7N          E_7O          E_7P          E_8M          E_8N          E_8O          E_8P          E_9M          E_9N          E_9O          E_9P          
OUT F38  jFEX   E_10M         E_10N         E_10O         E_10P         E_7M          E_7N          E_7O          E_7P          E_8M          E_8N          E_8O          E_8P          E_9M          E_9N          E_9O          E_9P          
OUT F41  jFEX   E_3M          E_3N          E_3O          E_3P          E_4M          E_4N          E_4O          E_4P          E_5M          E_5N          E_5O          E_5P          E_6M          E_6N          E_6O          E_6P          
OUT F42  jFEX   E_3M          E_3N          E_3O          E_3P          E_4M          E_4N          E_4O          E_4P          E_5M          E_5N          E_5O          E_5P          E_6M          E_6N          E_6O          E_6P          
OUT F47  gFEX   E_3M+E_4M+E_3N+E_4N  E_3O+E_4O+E_3P+E_4P  E_5M+E_6M+E_5N+E_6N  E_5O+E_6O+E_5P+E_6P  E_7M+E_8M+E_7N+E_8N  E_7O+E_8O+E_7P+E_8P  E_9M+E_10M+E_9N+E_10N  E_9O+E_10O+E_9P+E_10P  
OUT F48  gFEX   E_3M+E_4M+E_3N+E_4N  E_3O+E_4O+E_3P+E_4P  E_5M+E_6M+E_5N+E_6N  E_5O+E_6O+E_5P+E_6P  E_7M+E_8M+E_7N+E_8N  E_7O+E_8O+E_7P+E_8P  E_9M+E_10M+E_9N+E_10N  E_9O+E_10O+E_9P+E_10P  

#include "tools.cxx"
#include <TStyle.h>
#include <iomanip>
#include <sstream>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>


using namespace std;

struct SCinfo {
  Int_t      channelId;
  Int_t      layer;
  Float_t    eta;
  Float_t    phi;
  Int_t      IEvent;
  Short_t    samples[32];
  Double_t   rms[32];
  Short_t    BCID;
  Int_t      latomeSourceId;
  Int_t      l1idLATOMEHEAD;
  Int_t      detector;
  Double_t   max_rms;
  Int_t      nEvents;
  Short_t    peakpos;
  Double_t   peakrms; 
  Double_t   TOF;
  Short_t    optim_bcid;
  Short_t    optim_ofc;
  Short_t    current_bcid;  //defined by bcidlatomeHEAD-bcidVec[0]
  bool       match_flag; //if maxEt phase matches minTau phase (within 1 ofcphase), then true
  map<Short_t, Short_t> latome_fiber;  
};

//Jiaqi2
void load_latome_fiber_SC(map<pair<string,short>, vector<int>> &latome_fiber_channelId, map<string, string> &latome_latomeId) {    //map<pair<latomeNo,fiber>, vector<channelId>>

  //all enabled latome
  //vector<int> LATOME_list={124, 47, 101, 51, 120, 78, 164, 99, 32, 77, 30, 137, 54, 165, 109, 88, 97, 36, 149, 139, 162, 85, 104, 103, 81, 122, 53, 73, 142, 153, 74, 106, 92, 31, 71, 111, 141, 151, 94, 102, 118, 147, 84, 105, 37, 87, 150, 61, 33, 100, 145, 64, 91, 96, 113, 90, 50, 56, 57, 63, 79, 38, 148, 140, 152, 163, 60, 156, 114, 146, 49, 83, 89, 138, 86, 112, 93, 157, 116, 167, 39, 155, 52, 72, 158, 66, 125, 121, 117, 119, 166, 75, 48, 159, 136, 59, 107, 62, 95, 143, 65, 161, 76, 55, 126, 160, 144, 98, 80, 108, 115, 82, 154, 110, 123, 58,};
  
  //make list of latome and latomeId, step1
  //map<string, string> latome_latomeId;
  TString latome_list="latome-fiber_list.txt";
  ifstream latome_file(latome_list);
  if (!latome_file.is_open()) {
    cout << "Unable to open latome list: " << latome_list <<"!" << endl;
  }
  else {
    cout<<endl<<"******************** Start to read latome list: "<<"********************"<<endl;
    vector<string> vec;
    string temp;
    int row=0;
    while (getline(latome_file, temp)) {
      vec.push_back(temp);
    }
    for (auto it = vec.begin(); it != vec.end(); it++)
      {
        istringstream is(*it);
        string s;
        int column = 0;
        string tmpLATOME;
	string tmpLATOMEId;
        while (is >> s) {
	  if (column == 0) {
	    tmpLATOMEId=s;
	  }
	  if (column == 1) {
	    tmpLATOME = s;
	  }
	  column++;
	}
	latome_latomeId[tmpLATOME]=tmpLATOMEId;
	cout<<"tmpLATOME="<<tmpLATOME<<"; tmpLATOMEId="<<tmpLATOMEId<<endl;
        row++;
      }
    cout<<latome_latomeId.size()<<" LATOMEs are read!"<<endl;
    latome_file.close();
  }


  //make list of SC name and channelId from LArId_Info.dat, step2
  map<pair<string, string>, int> latome_SCname_channelId;
  TString LArId="LArId_Info.dat";  
  ifstream SCname(LArId);
  if (!SCname.is_open())
    {
      cout << "Unable to open LArId info file: " << LArId <<"!" << endl;
    }
  else {
    cout<<"******************** Start to read LArId (SC_ONL_ID) info: "<<"********************"<<endl;
    vector<string> vec;
    string temp;
    int row=0;
    while (getline(SCname, temp))
      {
        vec.push_back(temp);
      }
    for (auto it = vec.begin(); it != vec.end(); it++)
      {
        istringstream is(*it);
        string s;
        int column = 0;
        string tmpLATOME;
	string tmpSCname; 
        int tmpchannelId;
        while (is >> s)
          {
            if (column == 0)
              {
                tmpchannelId = stoi(s);
              }
            if (column == 5)
              {
                int pos=s.find("_");
                s.assign(s,pos+1,string::npos);
                tmpLATOME = s;
              }
            if (column == 6)
              {
                tmpSCname = s;
              }
            column++;
          }
	latome_SCname_channelId[make_pair(tmpLATOME,tmpSCname)]=tmpchannelId;
	//cout<<"LATOME="<<tmpLATOME<<"; tmpSCname="<<tmpSCname<<"; tmpchannelId="<<tmpchannelId<<endl;
        row++;
      }
    cout<<latome_SCname_channelId.size()<<" SCs are read!"<< endl;
    SCname.close();
  }
  //end of making SCname list
  
  //make list of latome and fiber, step3
  //map<pair<string,short>, vector<int>> latome_fiber_channelId;
  cout<<endl<<"******************** Start to read fiber information: "<<"********************"<<endl;
  for (auto latome_it=latome_latomeId.begin(); latome_it!=latome_latomeId.end(); latome_it++) {
    TString FiberInfo="fiber_mapping/"+latome_it->first+".txt";
    string tmpLATOME=latome_it->first;
    ifstream fiber_info(FiberInfo);
    if (!fiber_info.is_open())
      {
	cout << "Unable to open fiber info file: " << FiberInfo << "!"<< endl;
      }
    else {
      vector<string> vec;
      string temp;
      int row=0;
      bool read_flag = false;
      cout<< "***Start to read latome: "<<tmpLATOME<<" ***"<<endl;      
      while (getline(fiber_info, temp))
	{
	  vec.push_back(temp);
	}
      for (auto it = vec.begin(); it != vec.end(); it++)
	{
	  istringstream is(*it);
	  string s;
	  string tmpstr;
	  int column = 0;
	  int fiber_No;
	  int tmpchannelId;
	  int input_flag =0;
	  vector<int> SC_eachfiber;
	  while (is >> s)
	    {
	      if (column == 0 && s=="IN") input_flag=1;

	      if (column == 1 && input_flag)
		{
		  for (auto c:s) {
		    if (c>='0' && c<='9') tmpstr+=c;
		  }
		  fiber_No = atoi(tmpstr.c_str());
		}

	      for (int i=0;i<8;i++)
		{
		  if (column == i+4 && input_flag)
		    {
		      if (tmpLATOME.find("FCAL") == string::npos) { //FCAL SCname format is different than the others
			int pos=s.find("_");
			s.assign(s,pos+1,string::npos);
		      }

		      //Convert LATOME name format                                                                                            
		      string c;
		      for (auto &c:tmpLATOME) {
			if ((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0'&&c<='9')) continue;
			else c='_';
		      }
		      //end of format change
		      
		      if (s.compare("GND")==0) continue;  //skip GND SCs	      
		      tmpchannelId=latome_SCname_channelId[make_pair(tmpLATOME,s)];
		      SC_eachfiber.push_back(tmpchannelId);
		      //cout<<"tmpLATOME="<<tmpLATOME<<"; fiber="<<fiber_No<<"; SCname="<<s<<"; channelId="<<tmpchannelId<<endl;
		    }
		}
	      column++;
	    }
	  if (input_flag) {
	    latome_fiber_channelId[make_pair(latome_it->first,fiber_No)]=SC_eachfiber;
	    //cout <<"row="<<row<<"; SC_eachfiber.size="<<SC_eachfiber.size()<<endl;                                          
	  }
	  row++;
	}
      fiber_info.close();
    }
  }  
  cout<< latome_fiber_channelId.size() << " fibers are read!" <<endl;
  //end of reading fiber info                                                                                                     
}

void loadSwrodToMap_allPhases_splash_fiber_average(string swrod_filename, int bside,
        map<string, int> &map_latome_side,
        map<pair<string,short>, vector<int>> &map_latome_fiber_channelId,
        map<pair<string, short>, short> &map_latome_fiber_shift,
        map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases_splash_fiber"<<endl;
	//cout<<map_latome_fiber_channelId.size()<<" fibers are read!"<<endl;
	
	//int bside=1; //-1 for beam1, 1 for beam2 
	TFile file((swrod_filename).c_str(),"read");
	TFile *f = new TFile("ofcphase_check_splash.root","recreate");
	TH2F *th6 = new TH2F("th6","optimized bcid", 150, -5, 5, 20, 0,20); //optim_bcid plot
	TH2F *th13 = new TH2F("th13","optimized ofc phase", 100, -5, 5, 50, 0,50); //optim_ofc plot
	TH2F* evt_shift[100];
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;
	Int_t layer;
	UShort_t bcidLATOMEHEAD;
	UShort_t bcidVec[32];


	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD);
	tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidVec", bcidVec);
	tree->SetBranchAddress("layer", &layer);

	int hit_SC_counter=0;
	int pre_IEvent=0;
	TH1F* hit_SC = new TH1F("SC_hitR","SC_hitR", 60, 0., 60.);
	int nevents=0;
	int event_counter=0;
	int height_threshold=30;
	int entries = tree->GetEntries();
	int maxevents=54;
	map<Int_t, vector<Short_t>[32] > channel_samples;
	map<Int_t, vector<Short_t> > channel_peakpos;
	map<Int_t, Double_t[32]> channel_peakrms;
	map<Int_t, Double_t[32]> channel_mean;
	map< Int_t,  Float_t > channel_eta;
	map< Int_t,  Float_t > channel_phi;
	map< Int_t,  Int_t > channel_layer;
	map< Int_t,  Int_t > channel_detector;
	map<Int_t, Double_t> channel_TOF;

	for (unsigned int i=0; i<entries; i++) {
	  tree->GetEntry(i);
	  //pulse check
	  int tmpmax=0;
	  int tmpmin=0;
	  int peak_flag = 0;
	  int saturation_flag = 0;
	  int trough_flag = 0;
	  int bipolar_flag = 0;
	  int flip_flag = 0;
	  int tail_flag = 0;

	  if (pre_IEvent==0 || pre_IEvent!=IEvent) {
	    hit_SC->Fill(nevents, double(hit_SC_counter)/34048.);
	    hit_SC_counter=0;		  
	    nevents++;
	    pre_IEvent=IEvent;
	  }


	  for (int isa=0;isa<32;isa++) {
	    if (samples[isa]>samples[tmpmax]) tmpmax=isa;
	    if (samples[isa]<samples[tmpmin] && samples[isa] !=0) tmpmin=isa;
	  }
	  int height = samples[tmpmax]-samples[0];
	  //peak detection                                              
	  if ((tmpmax >= 2 && samples[tmpmax-2] <= samples[tmpmax-1] && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold) || (tmpmax < 2 && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold)) peak_flag=1;
	  //saturation detection
	  if (samples[tmpmax]>=2500) saturation_flag=1;

	  //filp detection                                                                                                                                           
	  for (int jsa=1; jsa<32;jsa++) {
	    if (abs(samples[jsa]-samples[jsa-1])>=height) flip_flag = 1;
	  }
	  //trough detection                                            
	  for (int jsa=tmpmax+10;jsa<32;jsa++){
	    if (samples[jsa]>(double)(samples[tmpmax]+samples[0])/2) break;
	    if (jsa==31) trough_flag = 1;
	  }
	  //bipolar detection                                           
	  if (tmpmax < tmpmin) bipolar_flag = 1;
	  //tail detection                                              
	  for (int isa=0;isa<26;isa++) {
	    if (samples[isa]>samples[isa+1] && samples[isa+1] > samples[isa+2] && samples[isa+2] > samples[isa+3] && samples[isa+3] > samples[isa+4] && samples[isa+4] > samples[isa+5] && samples[isa+5] > samples[isa+6]) {
	      tail_flag = 1;
	    }
	  }
	  for (int isa=6;isa<32;isa++) {
	    if (samples[isa]>samples[isa-1] && samples[isa-1] > samples[isa-2] && samples[isa-2] > samples[isa-3] && samples[isa-3] > samples[isa-4] && samples[isa-4] > samples[isa-5] && samples[isa-5] > samples[isa-6]) {
	      tail_flag = 1;
	    }
	  }
	  event_counter++;
	  //if (!peak_flag || flip_flag || !trough_flag || !bipolar_flag) continue;
	  if (!peak_flag || !trough_flag || !bipolar_flag || saturation_flag) continue;
	  //if (!peak_flag) continue;
	  hit_SC_counter++;
	  //pulse check end
	  
	  channel_layer[channelId]=layer;
	  channel_detector[channelId]=detector;
	  channel_eta[channelId]=eta;
	  channel_phi[channelId]=phi;	   	  
	  channel_peakpos[channelId].push_back(tmpmax);
	  for (unsigned i=0;i<32;i++) {
	    if (samples[i] > 0) {
	      channel_samples[channelId][i].push_back(samples[i]);
	    }
	  }	  
	}
	cout<<"Detected SC numeber="<<channel_samples.size()<<endl;
        map<int, SCinfo> SC_list; //channelId_SCinfo
	for (auto it = channel_samples.begin(); it != channel_samples.end(); it++) {
	  double chi2=0;
	  double max_rms=0;
	  int sample_No=0;
	  pair<short, double> tmp_max; //SC peak position
	  SCinfo tmpSC;
	  for (int sam=0; sam<32; sam++) {
	    Double_t mean=0;
	    sample_No=it->second[sam].size();	      
	    Double_t sum = std::accumulate(std::begin(it->second[sam]), std::end(it->second[sam]), 0.0);
	    if (sample_No != 0) 
	      {
		mean = sum/double(sample_No);
		//find peak position                                                                                                                                
		if (mean>tmp_max.second) {
		  tmp_max.first=sam;
		  tmp_max.second=mean;
		}
	      }
	    else {
	      mean=0;
	      tmp_max.first=0;
	      tmp_max.second=0;
	    }
	      
	    tmpSC.samples[sam]=mean;	      
	  }
	  
	  //compute peak position rms
	  Double_t sum_peakpos = std::accumulate(std::begin(channel_peakpos[it->first]), std::end(channel_peakpos[it->first]), 0.0);
	  double mean_peakpos= sum_peakpos/double(channel_peakpos[it->first].size());
	  Double_t var=0;
	  Double_t diff=0;
	  double peakrms=0;
	  for (int i=0;i<channel_peakpos[it->first].size();i++) {
	    var += pow(channel_peakpos[it->first][i]-mean_peakpos,2);
	  }
	  if (channel_peakpos[it->first].size() != 0) peakrms = sqrt(var/double(channel_peakpos[it->first].size()));
	  else peakrms = 1/sqrt(12);
	  //end computation
	  //cout<<"variance="<<var<<"; Nevents="<< channel_peakpos[it->first].size()<<" peakrms="<<peakrms<<endl;
	  
	  int channelId=0;
	  channelId=it->first;
	  tmpSC.channelId=channelId;
	  tmpSC.eta=channel_eta[channelId];
	  tmpSC.phi=channel_phi[channelId];
	  tmpSC.layer=channel_layer[channelId];
	  tmpSC.detector=channel_detector[channelId];
	  tmpSC.peakrms=peakrms;
	  tmpSC.current_bcid=bcidLATOMEHEAD-bcidVec[0];
	  SC_list[channelId]=tmpSC;
	  
	  /*
	  if (peakrms>0.5) {
	    //cout<<"Large peakrms channel:"<<channelId<<"; eta="<<tmpSC.eta<<"; phi="<<tmpSC.phi<<"; layer="<<tmpSC.layer<<" detector="<<tmpSC.detector<<"; RMS="<<tmpSC.peakrms<<endl;
	    char histname[100];
	    sprintf(histname,"SC%d_largerms%f",channelId,tmpSC.peakrms);
	    TH1D *tmph = new TH1D(histname,histname, 32, 0, 32);
	    for (int i=0;i<32;i++) {
	      tmph->SetBinContent(i+1,tmpSC.samples[i]);
	    }
	    tmph->Write();
	    delete tmph;
	  }
	  */

	  //TOF correction
	  double TOF=0;
	  float aeta=fabs(tmpSC.eta);
	  //barrel depth
	  if (tmpSC.detector==0) {
	    double rr=1600;
	    if (aeta<=1.4) {
	      if (tmpSC.layer==2) {
		if (aeta<0.8) rr=1730;
		else rr=1680;		
	      }
	      else if (tmpSC.layer==1) {
		if (aeta<0.8) rr=1540;
		else rr=1520;		
	      }
	      else if (tmpSC.layer==3) rr=1800;	      
	      else if (tmpSC.layer==0) rr=1450;
	    }
	    else {
	      if (tmpSC.layer==2) rr= (1689.621619 + 2.682993*aeta - 70.165741*aeta*aeta);
	      else rr = (1522.775373 + 27.970192*aeta - 21.104108*aeta*aeta);
	    }
	    TOF= rr/300.*(bside*sinh(tmpSC.eta)-cosh(tmpSC.eta)); //TOF factor
	    //if (aeta<=1.4) cout<<"eta="<<eta<<"; TOF="<<TOF<<endl;
	    //TOF=-fabs(rr)/300.*(bside*rr/fabs(rr)-1./fabs(tanh(tmpSC.eta)));
	    tmpSC.TOF=TOF;
	  }
	  else {
	    double zz=3800;
	    //end-cap depth
	    if (tmpSC.detector==1 || tmpSC.detector==2) {
	      if (tmpSC.layer==0) zz=3600;	    
	      else if (tmpSC.layer==1) {
		if (aeta<1.5) zz=(12453.297448 - 5735.787116*aeta);
		else zz=3790.671754;
	      }
	      else if (tmpSC.layer==2) {
		if (aeta < 1.5) zz=(8027.574119 - 2717.653528*aeta);
		else zz=(3473.473909 + 453.941515*aeta - 119.101945*aeta*aeta);
	      }
	      else if (tmpSC.layer==3) zz=4150;	      
	    }
	    //HEC depth
	    else if (tmpSC.detector==3) {
	      if (tmpSC.layer==0) zz=4510;
	      else if (tmpSC.layer==1) zz=4930;
	      else if (tmpSC.layer==2) zz=5390;
	      else if (tmpSC.layer==3) zz=5880;
	    }	   
	    //FCAL depth
	    else if (tmpSC.detector==4){
	      if (tmpSC.layer==1) zz=4920;
              else if (tmpSC.layer==2) zz=5380;
              else if (tmpSC.layer==3) zz=5840;
	    }   
	    if (tmpSC.eta<0) zz*=-1; 
	    TOF=fabs(zz)/300.*(bside*zz/fabs(zz)-1./fabs(tanh(tmpSC.eta)));
	    tmpSC.TOF=TOF;
	    //cout<<"eta="<<tmpSC.eta<<"; TOF="<<TOF<<endl;
	   }
	  //TOF correction end

	}
	cout<<"maxevent="<<nevents<<endl;
	
	//write ttree of shift phase
	TTree* newtree = new TTree("sc_shift","tree with shift ofc phase and shfit BCID");
	Int_t new_channelId,new_layer,new_detector;
	Float_t new_eta,new_phi;
	Short_t optim_bcid,optim_ofc;
	newtree->Branch("channelId", &new_channelId);
	newtree->Branch("layer", &new_layer);
	newtree->Branch("detector", &new_detector);
	newtree->Branch("eta", &new_eta);
	newtree->Branch("phi", &new_phi);
	newtree->Branch("optim_bcid", &optim_bcid);
	newtree->Branch("optim_ofc", &optim_ofc);
	
	for (auto sc_it=SC_list.begin(); sc_it!=SC_list.end(); sc_it++) {
	  //tree->GetEntry(i);
		if ((bside==1 && sc_it->second.eta<0) || (bside==-1 && sc_it->second.eta>0)) continue; //-1 for beam1, 1 for beam2  
		//if (sc_it->second.layer!=0) continue; //layer_diff
		double e2et = TMath::Sin(2*TMath::ATan(exp(-sc_it->second.eta)));
		if (map_scid_lsb.find(sc_it->second.channelId)==map_scid_lsb.end() || map_scid_ped.find(sc_it->second.channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(sc_it->second.channelId)==0 || map_scid_lsb.at(sc_it->second.channelId)<=0) continue;
		if (map_scid_ofca_allPhases.find(sc_it->second.channelId)==map_scid_ofca_allPhases.end()) continue;
		//if (map_scid_ofca_allPhases.at(sc_it->second.channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<sc_it->second.channelId<<" = "<<map_scid_ofca_allPhases.at(sc_it->second.channelId).size()<<endl;	
		//get max et phase and min tau phase with averaged pulse
		int j=23;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(sc_it->second.channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(sc_it->second.channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==bcidLATOMEHEAD) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(sc_it->second.samples[l+k]-map_scid_ped.at(sc_it->second.channelId))*a[l];
				ETTau+=e2et*(sc_it->second.samples[l+k]-map_scid_ped.at(sc_it->second.channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		int phase=BCET;
		float maxET_ofcphase=0;
		float minTau_ofcphase=0;	
		float maxET_ofc=0;
		float mintau_ofc=999.;
		float maxphase_tau=999.;
		float minphase_tau=999.;
		for (int j=0; j<50; j++) {//loop over phases
		  for (int k=0; k<4; k++) {
		    a[k]=map_scid_ofca_allPhases.at(sc_it->second.channelId).at(j)[k];
		    b[k]=map_scid_ofcb_allPhases.at(sc_it->second.channelId).at(j)[k];
		  }	
		  float ET=0;
		  float ETTau=0.;
		  for (int l=0; l<4; l++) {
		    ET+=e2et*(sc_it->second.samples[l+phase]-map_scid_ped.at(sc_it->second.channelId))*a[l];
		    ETTau+=e2et*(sc_it->second.samples[l+phase]-map_scid_ped.at(sc_it->second.channelId))*b[l];
		  }
		  if (ET>maxET_ofc) {
		    maxET_ofcphase=j;
		    maxET_ofc=ET;
		    maxphase_tau=(float)ETTau/ET;
		  }
		  if (fabs((float)ETTau/ET)<mintau_ofc && ET>0) {
		    minTau_ofcphase=j;
		    mintau_ofc=fabs((float)ETTau/ET);
		    minphase_tau=(float)ETTau/ET;
		  }
		  //cout<<"At channelId "<<sc_it->second.channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(sc_it->second.channelId)<<endl;
		}
		
		//write output ttree, including optimized phase
		new_channelId=sc_it->second.channelId;
		new_layer=sc_it->second.layer;
		new_detector=sc_it->second.detector;
		new_eta=sc_it->second.eta;
		new_phi=sc_it->second.phi;		
		//if ((bside==1 && new_eta<0) || (bside==-1 && new_eta>0)) continue; //-1 for beam1, 1 for beam2  		
		optim_bcid=phase; //4 samples with max Et
	        //optim_ofc=maxET_ofcphase; //TOF uncorrected
		optim_ofc=maxET_ofcphase+round(sc_it->second.TOF/1.04); //TOF uncorrected
		
		
		//if (maxET_ofcphase<minTau_ofcphase-1 || maxET_ofcphase>minTau_ofcphase+1 || sc_it->second.peakrms>0.5) sc_it->second.match_flag=false;
		if (optim_bcid<0 || maxET_ofcphase<minTau_ofcphase-1 || maxET_ofcphase>minTau_ofcphase+1 || sc_it->second.peakrms>0.5) sc_it->second.match_flag=false;
		else sc_it->second.match_flag=true;
		
		sc_it->second.optim_bcid=phase;
		sc_it->second.optim_ofc=optim_ofc;

		//optim_ofc=maxET_ofcphase+round(sc_it->second.TOF/1.04); //TOF corrected
		if (optim_bcid>3||sc_it->second.peakrms>0.5)  { //pulse check
		  cout<<"channelId="<<sc_it->second.channelId<<"; optim_bcid="<<phase<<"; optimc_ofc="<<maxET_ofcphase<<"; peakrms="<<sc_it->second.peakrms<<"; detector="<<sc_it->second.detector<<endl;
		  char histname[100];
		  sprintf(histname,"SC%d_optbcid%hd_maxEtofc%d_minTauofc%d_peakrms_%f_detector%d",sc_it->second.channelId,optim_bcid,int(maxET_ofcphase),int(minTau_ofcphase),sc_it->second.peakrms,sc_it->second.detector);
		  TH1D *tmph = new TH1D(histname,histname, 32, 0, 32);
		  for (int j=0;j<32;j++) {
		    tmph->SetBinContent(j+1,sc_it->second.samples[j]);
		  }		  
		  tmph->Write();
		  delete tmph;
		}

		  th6->Fill(sc_it->second.eta,optim_bcid);
		  th13->Fill(sc_it->second.eta,optim_ofc);		

		newtree->Fill();	
	        
	}
	th6->SetTitle("Optimized bcid phase;#eta;bcid phase");
	th6->Write();
	th13->SetTitle("Optimized OFC phase;#eta;ofc phase");
	th13->Write();
	newtree->AutoSave();
	

	//compute fiber shift
	TH2D *bcid_shift_sc= new TH2D("bcid_shift_sc","bcid_shift_sc", 200, -5., 5., 10, -5., 5.);
	TH2D *bcid_shift_fiber= new TH2D("bcid_shift_fiber","bcid_shift_fiber", 5, 0., 5., 10, -5., 5.);
	TH2D *shift_rms= new TH2D("shift_rms","shift_rms", 5, 0., 5., 20, 0., 0.8);
	//map<pair<string, short>, short> map_latome_fiber_shift;
	for (auto fiber_it=map_latome_fiber_channelId.begin(); fiber_it!=map_latome_fiber_channelId.end(); fiber_it++) {
	  int sc_no=fiber_it->second.size();
	  int fiber_offset=0;
	  short fiber_detector=-1;

	  //set latome bside
	  for (auto sc_it: fiber_it->second) {  
	    int SC_channelId=sc_it; 
	    if (SC_list[SC_channelId].eta>0) map_latome_side[fiber_it->first.first]=1;
	    else map_latome_side[fiber_it->first.first]=-1;
	    //cout<<"set_latome="<<fiber_it->first.first<<"; bside="<<bside<<"; map_latome_side="<<map_latome_side[fiber_it->first.first]<<"; eta="<<SC_list[SC_channelId].eta<<endl;
	  }
	  //end of bside set
	  
	  //calulate mean shift  shift=optim_bcid-current_bcid
	  int sc_counter=0;
	  for (auto sc_it: fiber_it->second) {
	    int SC_channelId=sc_it;
	    //if ((bside==1 && SC_list[SC_channelId].eta<0) || (bside==-1 && SC_list[SC_channelId].eta>0)) continue; //-1 for beam1, 1 for beam2
	    if (map_latome_side[fiber_it->first.first] != bside) continue;
	    if (!SC_list[SC_channelId].match_flag) continue;	    
            int tmp_offset=SC_list[SC_channelId].optim_bcid-SC_list[SC_channelId].current_bcid;	    
	    bcid_shift_sc->Fill(SC_list[SC_channelId].eta,tmp_offset);
	    fiber_offset+=tmp_offset;	    
	    fiber_detector=SC_list[SC_channelId].detector;
	    sc_counter++;
	  }	  
	  if (fiber_detector == -1) continue;
	  double fiber_offset_mean=double(fiber_offset)/double(sc_counter);
	  //mean computation end

	  //compute rms
	  double sqr_dev=0;
	  for (auto sc_it: fiber_it->second) {
	    int SC_channelId=sc_it;
	    //if ((bside==1 && SC_list[SC_channelId].eta<0) || (bside==-1 && SC_list[SC_channelId].eta>0)) continue; //-1 for beam1, 1 for beam2                                                                     
	    if (map_latome_side[fiber_it->first.first] != bside) continue;
            if (!SC_list[SC_channelId].match_flag) continue;
            int tmp_offset=SC_list[SC_channelId].optim_bcid-SC_list[SC_channelId].current_bcid;	    
	    sqr_dev+=pow(fiber_offset_mean-tmp_offset,2);
	  }
	  double fiber_offset_rms=sqrt(sqr_dev/(double)sc_counter);
	  shift_rms->Fill(fiber_detector,fiber_offset_rms);	  
	  if (fiber_offset_rms == 0) bcid_shift_fiber->Fill(fiber_detector,round(fiber_offset_mean));
	  //rms computation end
	  
	  //Calibrated fiber info
	  if (fiber_offset_rms==0) {
	    //cout<<"**** fiber shift list ****"<<endl;
	    map_latome_fiber_shift[fiber_it->first]=round(fiber_offset_mean);
	    cout<<"latome="<<fiber_it->first.first<<"; fiber="<<fiber_it->first.second<<"; shift="<<fiber_offset_mean<<endl;
	  }
	  //fiber info end

	  //large shift fiber check
	  if (abs(round(fiber_offset_mean))<=1) continue; 
	  cout<<endl<<"**** large shift fiber ****"<<endl;
	  cout<<"latome="<<fiber_it->first.first<<"; sc_counter="<<sc_counter<<"; fiber_offset_mean="<<fiber_offset_mean<<"; fiber_offset_rms="<<fiber_offset_rms<<endl;
	  for (auto sc_it: fiber_it->second) {
	    int SC_channelId=sc_it;
            int tmp_offset=SC_list[SC_channelId].optim_bcid-SC_list[SC_channelId].current_bcid;	    
	    if (abs(round(fiber_offset_mean))>1) cout<<"channelId="<<SC_channelId<<"; detector="<<SC_list[SC_channelId].detector<<"; current_bcid="<<SC_list[SC_channelId].current_bcid<<"; optim_bcid="<<SC_list[SC_channelId].optim_bcid<<"; tmp_offset="<<tmp_offset<<"; match_flag="<<SC_list[SC_channelId].match_flag<<"; eta="<<SC_list[SC_channelId].eta<<endl;
	  }
	  cout<<endl;
	  //check end	  
	}
	bcid_shift_sc->SetTitle("bcid_shift_sc;#eta;shift BC");
	bcid_shift_sc->Write();
	bcid_shift_fiber->SetTitle("bcid_shift_fiber;detector;shift BC");
	bcid_shift_fiber->Write();
	shift_rms->SetTitle("rms distribution;detector;shift BC rms");
	shift_rms->Write();
	f->Close();
}


void set_new_fiber_BCID(string old_version_path, string new_version_path, int bside, map<string, int> &map_latome_side, map<pair<string, short>, short> &map_latome_fiber_shift, map<string, string> &map_latome_latomeId) {   //bside: beam1 for -1, beam2 for 1

  cout<<endl<<endl<<"***************** Start to apply shift to old ini files ***************"<<endl;
  int shift_counter[2]={0};  
  for (auto &latome_it : map_latome_latomeId) {

    string set_latome=latome_it.first;
    string set_latomeId=latome_it.second;
    
    cout<<endl<<endl<<"**** processing LATOME:"<<set_latome<<"; bside="<<bside<<"; map_latome_side="<<map_latome_side[set_latome]<<" ********"<<endl;
    if (map_latome_side[set_latome] != bside) continue;

    string old_ini_file=old_version_path+"/"+set_latomeId+".ini";
    string new_ini_file=new_version_path+"/"+set_latomeId+".ini";

    //start to read ini files                                                                                                                  
    map<int, int> old_fiber_bcid;
    ifstream old_ini(old_ini_file);
    if (!old_ini.is_open())
      {
        cout << "Unable to open old_ini latome file: " << old_ini_file <<"!" << endl;
        continue;
      }
    else {
      cout<<"******** Start to read old_ini BCID from "<<set_latomeId<<" ********"<<endl;                                             
      //cout<<"input file: "<<latomeNo<<endl;                                                                                                  
      vector<string> vec;
      string temp;
      int row=0;
      while (getline(old_ini, temp))
        {
          vec.push_back(temp);
        }
      for (auto it = vec.begin(); it != vec.end(); it++)
        {
          istringstream is(*it);
          string s;
          int column = 0;
          int tmpBCID=0;
          while (is >> s)
            {
              if (column==0) {
                int pos=s.find("=");
                s.assign(s,pos+1,string::npos);
                tmpBCID = stoi(s);
              }
              column++;
            }
          //if (tmpBCID != 0) BCID_list.push_back(tmpBCID);//debug                                                                               
          old_fiber_bcid[row]=tmpBCID;	  
          cout << "F"<<row << "  BCID=" << old_fiber_bcid[row]<<"; "<< endl;                                                                       
          row++;
        }
      //cout<<old_fiber_bcid.size()<<" fibers are read!"<< endl;                                                                                     
      old_ini.close();
    }
    //end of reading old_ini BCID                                                                                                                  

    //Start to set new ini files
    cout<<endl<<"******************** Start to set new_ini BCID for "<<set_latomeId<<" ********************"<<endl;
    ofstream outputtxt(new_ini_file);
    for (auto &fiber_it : old_fiber_bcid) {
      short fiber_no=fiber_it.first;
      short old_bcid=fiber_it.second;      
      short new_bcid=0;
      
      if (old_bcid == 0) {
        cout<<"latome="<<set_latome<<"; latomeId="<<set_latomeId<<"; fiber_no="<<fiber_no<<"; new_bcid=0"<<"; old_bcid="<<old_bcid<<"; shift="<<map_latome_fiber_shift[make_pair(set_latome, fiber_it.first)]<<endl;
	outputtxt<<"istage.decode_stage_"<<fiber_no<<".control.ltdb_bcid_bcr=0"<<endl;
      }
      else {
	if (map_latome_fiber_shift.find(make_pair(set_latome, fiber_no)) != map_latome_fiber_shift.end()) {
	  new_bcid=old_bcid+map_latome_fiber_shift[make_pair(set_latome, fiber_no)];
	  cout<<"shift found";
	  if (map_latome_fiber_shift[make_pair(set_latome, fiber_no)]==1) shift_counter[0]++;
	  if (map_latome_fiber_shift[make_pair(set_latome, fiber_no)]==-1) shift_counter[1]++;
	}
	else { 
	  new_bcid=old_bcid;
	  cout<<"no shift found";
	}
	if (new_bcid>3564) new_bcid-=3564;
	if (new_bcid<0) new_bcid+=3564;
        cout<<"; latome="<<set_latome<<"; latomeId="<<set_latomeId<<"; fiber_no="<<fiber_no<<"; new_bcid="<<new_bcid<<"; old_bcid="<<old_bcid<<"; shift="<<map_latome_fiber_shift[make_pair(set_latome, fiber_it.first)]<<endl;
	outputtxt<<"istage.decode_stage_"<<fiber_no<<".control.ltdb_bcid_bcr="<<new_bcid<<endl;
      }                  
    }    
    //end of setting new ini files    
    outputtxt.close();
  }
  cout<< shift_counter[0]<<" fibers shift by 1"<<"; "<<shift_counter[1]<<" fibers shift by -1"<<endl;
}


void loadSwrodToMap_allPhases_collision_average(string collision_filename, string shift_filename,
        map<int,map<string,vector<float>>> &map_swrod_ET_allPhases,
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases_collision"<<endl;	
	

	//read optimized phase
	//TFile shift_file((shift_filename).c_str(),"read");
	TFile* shift_file = TFile::Open((shift_filename).c_str());
	TTree *shift_tree = (TTree*)shift_file->Get("sc_shift");
        Int_t shift_channelId;
        Float_t shift_eta;
        Float_t shift_phi;
        Int_t shift_detector;
        Int_t shift_layer;
	Short_t optim_bcid;
	Short_t optim_ofc;


        shift_tree->SetBranchAddress("channelId", &shift_channelId);
        shift_tree->SetBranchAddress("eta", &shift_eta);
        shift_tree->SetBranchAddress("phi", &shift_phi);
        shift_tree->SetBranchAddress("detector", &shift_detector);
        shift_tree->SetBranchAddress("layer", &shift_layer);
        shift_tree->SetBranchAddress("optim_bcid", &optim_bcid);
        shift_tree->SetBranchAddress("optim_ofc", &optim_ofc);
	int shift_entries=shift_tree->GetEntriesFast();
	map <int, pair<short,short>> channel_bcid_ofc;
	for (unsigned int i=0;i<shift_entries;i++) {
	  shift_tree->GetEntry(i);
	  channel_bcid_ofc[shift_channelId].first=optim_bcid;
	  channel_bcid_ofc[shift_channelId].second=optim_ofc;	  
	  //cout<<"channelId="<<shift_channelId<<"; optim_bcid="<<optim_bcid<<"; optim_ofc"<<optim_ofc<<endl;
	}
	shift_file->Close();
	//read optimized phase end
      
	//TFile file((collision_filename).c_str(),"read");
	TFile* file = TFile::Open((collision_filename).c_str());

	TFile *f = new TFile("ofcphase_check_collision.root","recreate");
	TH1F *th0 = new TH1F("th0","Max ET ofc phase", 200,0,50);
	TH1F *th1 = new TH1F("th1","Min Tau ofc phase", 200,0,50);
	TH2F *th2 = new TH2F("th2","Max ET ofc phase 2d", 100, -5, 5, 50,0,50);
	TH1F *th3 = new TH1F("th3","bcid phase", 32,0,32);
	TH2F *th4 = new TH2F("th4","Max ET ofc phase 2d", 100, -5, 5, 200,-50,150);
	TH2F *th5 = new TH2F("th5","phase_eta", 100, -5, 5, 30,-15,15);
	TH2F *th6 = new TH2F("th6","allshift", 150, -5, 5, 150,-80,100);
	//TH2F *th6 = new TH2F("th6","allshift", 150, -5, 5, 100,-500,500);
	TH2F *th7 = new TH2F("th7","Min Tau ofc phase 2d", 100, -5, 5, 50,0,50);
	TH2F *th8 = new TH2F("th8","Min Tau 2d", 100, -5, 5, 100, -5,5);
	TH2F *th9 = new TH2F("th9","Max Et Min Tau diff", 100, -5, 5, 50, -25,25);
	TH2F *th10 = new TH2F("th10","Tau with Max ET ofc phase", 100, -5, 5, 200, -25,25);
	TH2F *th11 = new TH2F("th11","Peak sample rms", 100, -5, 5, 100, 0,1);
	TH2F *th12 = new TH2F("th12","MaxEt vs Optimized phase Et", 100, 0., 0., 100, 0., 0.);
	TH2F* evt_shift[100];
	//TTree *tree = (TTree*)file->Get("tree_sc");
	TTree *tree = (TTree*)file->Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;
	Int_t layer;
	UShort_t bcidLATOMEHEAD;
	UShort_t bcidVec[32];


	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD);
	tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidVec", bcidVec);
	tree->SetBranchAddress("layer", &layer);

	int hit_SC_counter=0;
	int pre_IEvent=0;
	TH1F* hit_SC = new TH1F("SC_hitR","SC_hitR", 60, 0., 60.);
	int nevents=0;
	int event_counter=0;
	int height_threshold=30;
	int entries = tree->GetEntries();
	int maxevents=54;
	map<Int_t, vector<Short_t>[32] > channel_samples;
	map<Int_t, vector<Short_t> > channel_peakpos;
	map<Int_t, Double_t[32]> channel_peakrms;
	map<Int_t, Double_t[32]> channel_mean;
	map< Int_t,  Float_t > channel_eta;
	map< Int_t,  Float_t > channel_phi;
	map< Int_t,  Int_t > channel_layer;
	map< Int_t,  Int_t > channel_detector;

	for (unsigned int i=0; i<entries; i++) {
	  tree->GetEntry(i);
	  //pulse check
	  int tmpmax=0;
	  int tmpmin=0;
	  int peak_flag = 0;
	  int saturation_flag = 0;
	  int trough_flag = 0;
	  int bipolar_flag = 0;
	  int flip_flag = 0;
	  int tail_flag = 0;

	  if (pre_IEvent==0 || pre_IEvent!=IEvent) {
	    hit_SC->Fill(nevents, double(hit_SC_counter)/34048.);
	    hit_SC_counter=0;		  
	    nevents++;
	    pre_IEvent=IEvent;
	  }


	  for (int isa=0;isa<32;isa++) {
	    if (samples[isa]>samples[tmpmax]) tmpmax=isa;
	    if (samples[isa]<samples[tmpmin] && samples[isa] !=0) tmpmin=isa;
	  }
	  int height = samples[tmpmax]-samples[0];
	  //peak detection                                              
	  if ((tmpmax >= 2 && samples[tmpmax-2] <= samples[tmpmax-1] && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold) || (tmpmax < 2 && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold)) peak_flag=1;
	  //saturation detection
	  if (samples[tmpmax]>=2500) saturation_flag=1;

	  //filp detection                                                                                                                                           
	  for (int jsa=1; jsa<32;jsa++) {
	    if (abs(samples[jsa]-samples[jsa-1])>=height) flip_flag = 1;
	  }
	  //trough detection                                            
	  for (int jsa=tmpmax+10;jsa<32;jsa++){
	    if (samples[jsa]>(double)(samples[tmpmax]+samples[0])/2) break;
	    if (jsa==31) trough_flag = 1;
	  }
	  //bipolar detection                                           
	  if (tmpmax < tmpmin) bipolar_flag = 1;
	  //tail detection                                              
	  for (int isa=0;isa<26;isa++) {
	    if (samples[isa]>samples[isa+1] && samples[isa+1] > samples[isa+2] && samples[isa+2] > samples[isa+3] && samples[isa+3] > samples[isa+4] && samples[isa+4] > samples[isa+5] && samples[isa+5] > samples[isa+6]) {
	      tail_flag = 1;
	    }
	  }
	  for (int isa=6;isa<32;isa++) {
	    if (samples[isa]>samples[isa-1] && samples[isa-1] > samples[isa-2] && samples[isa-2] > samples[isa-3] && samples[isa-3] > samples[isa-4] && samples[isa-4] > samples[isa-5] && samples[isa-5] > samples[isa-6]) {
	      tail_flag = 1;
	    }
	  }
	  event_counter++;
	  //if (!peak_flag || flip_flag || !trough_flag || !bipolar_flag) continue;
	  //if (!peak_flag || !trough_flag || !bipolar_flag) continue;
	  //if (!peak_flag) continue;
	  if (height<15) continue;
	  hit_SC_counter++;
	  //pulse check end
	  
	  channel_layer[channelId]=layer;
	  channel_detector[channelId]=detector;
	  channel_eta[channelId]=eta;
	  channel_phi[channelId]=phi;	   	  
	  channel_peakpos[channelId].push_back(tmpmax);
	  for (unsigned i=0;i<32;i++) {
	    if (samples[i] > 0) {
	      channel_samples[channelId][i].push_back(samples[i]);
	    }
	  }	  
	}
	cout<<"Detected SC numeber="<<channel_samples.size()<<endl;
	vector<SCinfo> SC_list;	
	for (auto it = channel_samples.begin(); it != channel_samples.end(); it++) {
	  double chi2=0;
	  double max_rms=0;
	  int sample_No=0;
	  pair<short, double> tmp_max; //SC peak position
	  SCinfo tmpSC;
	  for (int sam=0; sam<32; sam++) {
	    Double_t mean=0;
	    sample_No=it->second[sam].size();	      
	    Double_t sum = std::accumulate(std::begin(it->second[sam]), std::end(it->second[sam]), 0.0);
	    if (sample_No != 0) 
	      {
		mean = sum/double(sample_No);
		//find peak position                                                                                                                                
		if (mean>tmp_max.second) {
		  tmp_max.first=sam;
		  tmp_max.second=mean;
		}
	      }
	    else {
	      mean=0;
	      tmp_max.first=0;
	      tmp_max.second=0;
	    }
	      
	    tmpSC.samples[sam]=mean;	      
	  }
	  
	  //compute peak position rms
	  Double_t sum_peakpos = std::accumulate(std::begin(channel_peakpos[it->first]), std::end(channel_peakpos[it->first]), 0.0);
	  double mean_peakpos= sum_peakpos/double(channel_peakpos[it->first].size());
	  Double_t var=0;
	  Double_t diff=0;
	  double peakrms=0;
	  for (int i=0;i<channel_peakpos[it->first].size();i++) {
	    var += pow(channel_peakpos[it->first][i]-mean_peakpos,2);
	  }
	  if (channel_peakpos[it->first].size() != 0) peakrms = sqrt(var/double(channel_peakpos[it->first].size()));
	  else peakrms = 1/sqrt(12);
	  //end computation
	  //cout<<"variance="<<var<<"; Nevents="<< channel_peakpos[it->first].size()<<" peakrms="<<peakrms<<endl;
	  
	  int channelId=0;
	  channelId=it->first;
	  tmpSC.channelId=channelId;
	  tmpSC.eta=channel_eta[channelId];
	  tmpSC.phi=channel_phi[channelId];
	  tmpSC.layer=channel_layer[channelId];
	  tmpSC.detector=channel_detector[channelId];
	  tmpSC.peakrms=peakrms;
	  SC_list.push_back(tmpSC);
	  
	  if (peakrms>0.5) {
	    //cout<<"Large peakrms channel:"<<channelId<<"; eta="<<tmpSC.eta<<"; phi="<<tmpSC.phi<<"; layer="<<tmpSC.layer<<" detector="<<tmpSC.detector<<"; RMS="<<tmpSC.peakrms<<endl;
	    char histname[100];
	    sprintf(histname,"SC%d_rms%f",channelId,tmpSC.peakrms);
	    TH1D *tmph = new TH1D(histname,histname, 32, 0, 32);
	    for (int i=0;i<32;i++) {
	      tmph->SetBinContent(i+1,tmpSC.samples[i]);
	    }
	    //tmph->Write();
	    delete tmph;
	  }
	  
	}
	cout<<"maxevent="<<nevents<<endl;
	
	//write ttree of shift phase
	
	for (unsigned int i=0; i<SC_list.size(); i++) {
		tree->GetEntry(i);
		//if (SC_list[i].layer!=0) continue; //layer_diff
		double e2et = TMath::Sin(2*TMath::ATan(exp(-SC_list[i].eta)));
		if (map_scid_lsb.find(SC_list[i].channelId)==map_scid_lsb.end() || map_scid_ped.find(SC_list[i].channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(SC_list[i].channelId)==0 || map_scid_lsb.at(SC_list[i].channelId)<=0) continue;
		if (map_scid_ofca_allPhases.find(SC_list[i].channelId)==map_scid_ofca_allPhases.end()) continue;
		//if (map_scid_ofca_allPhases.at(SC_list[i].channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<SC_list[i].channelId<<" = "<<map_scid_ofca_allPhases.at(SC_list[i].channelId).size()<<endl;	
		//get max et phase and min tau phase with averaged pulse
		int j=23;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(SC_list[i].channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(SC_list[i].channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==bcidLATOMEHEAD) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(SC_list[i].samples[l+k]-map_scid_ped.at(SC_list[i].channelId))*a[l];
				ETTau+=e2et*(SC_list[i].samples[l+k]-map_scid_ped.at(SC_list[i].channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		int phase=BCET;
	        int optim_phase=channel_bcid_ofc[SC_list[i].channelId].first; //optimized bcid phase from splash
		float maxET_ofcphase=0;
		float minTau_ofcphase=0;	
		float maxET_ofc=0;
		float mintau_ofc=999.;
		float maxphase_tau=999.;
		float minphase_tau=999.;
	        float optim_maxet=0;
		float optim_mintau=999;
		for (int j=0; j<50; j++) {//loop over phases
		  for (int k=0; k<4; k++) {
		    a[k]=map_scid_ofca_allPhases.at(SC_list[i].channelId).at(j)[k];
		    b[k]=map_scid_ofcb_allPhases.at(SC_list[i].channelId).at(j)[k];
		  }	
		  float ET=0;
		  float ETTau=0.;
		  float optim_et=0.;
		  float optim_EtTau=0.;
		  for (int l=0; l<4; l++) {
		    ET+=e2et*(SC_list[i].samples[l+phase]-map_scid_ped.at(SC_list[i].channelId))*a[l];
		    ETTau+=e2et*(SC_list[i].samples[l+phase]-map_scid_ped.at(SC_list[i].channelId))*b[l];
		    optim_et+=e2et*(SC_list[i].samples[l+optim_phase]-map_scid_ped.at(SC_list[i].channelId))*a[l];
		    optim_EtTau+=e2et*(SC_list[i].samples[l+optim_phase]-map_scid_ped.at(SC_list[i].channelId))*b[l];
		  }
		  if (ET>maxET_ofc) {
		    maxET_ofcphase=j;
		    maxET_ofc=ET;
		    maxphase_tau=(float)ETTau/ET;
		  }
		  if (fabs((float)ETTau/ET)<mintau_ofc && ET>0) {
		    minTau_ofcphase=j;
		    mintau_ofc=fabs((float)ETTau/ET);
		    minphase_tau=(float)ETTau/ET;
		  }
		  if (j==channel_bcid_ofc[SC_list[i].channelId].second) {
		    optim_maxet=optim_et;
		    optim_mintau=(float)optim_EtTau/optim_et;
		  }

		  //cout<<"At channelId "<<SC_list[i].channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(SC_list[i].channelId)<<endl;
		}
		//maxET_ofcphase=channel_bcid_ofc[SC_list[i].channelId].second; //optimized ofc phase from splash
		
		th0->Fill(maxET_ofcphase);
		th1->Fill(minTau_ofcphase);
		th2->Fill(SC_list[i].eta,maxET_ofcphase);
		th7->Fill(SC_list[i].eta,minTau_ofcphase);
		th8->Fill(SC_list[i].eta,minphase_tau);
		th9->Fill(SC_list[i].eta,maxET_ofcphase-minTau_ofcphase);
		th10->Fill(SC_list[i].eta,maxphase_tau);
		th11->Fill(SC_list[i].eta,SC_list[i].peakrms);
		if (SC_list[i].eta<0) th12->Fill(optim_maxet,maxET_ofc); //beam1: eta<0 / beam2: eta>0
		th3->Fill(phase);
		th4->Fill(SC_list[i].eta,maxET_ofcphase+(2-phase)*24);		
		double all_shift=0;
		if (detector!=4) {
		  all_shift=(23-maxET_ofcphase)*1.04+(2-phase)*25;
		  th6->Fill(SC_list[i].eta,all_shift);
		  //th13->Fill(SC_list[i].eta,all_shift);
		  //cout<<"channelId="<<SC_list[i].channelId<<"; shift="<<all_shift<<endl;		  
		  //evt_shift[nevents]->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(2-phase)*25);
		}
		else {
		  all_shift=(23-maxET_ofcphase)*1.04+(1-phase)*25;
		  th6->Fill(SC_list[i].eta,all_shift);
		  //th13->Fill(SC_list[i].eta,all_shift);
		  //cout<<"channelId="<<SC_list[i].channelId<<"; shift="<<all_shift<<endl;
		  //evt_shift[nevents]->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(1-phase)*25);
		}
		th5->Fill(SC_list[i].eta,phase);
		
	}
	gStyle->SetPalette(1);
	th0->Write();
	th1->Write();
	th2->SetTitle("OFC phase with max Et;#eta;OFC phase");
	th2->Write();
	th3->Write();
	th4->Write();
	th5->Write();
	th6->SetTitle("Shift time scanned with OFC phase;#eta;shift time [ns]");
	th6->Write();
	th7->SetTitle("OFC phase with min tau;#eta;OFC phase");
	th7->Write();
	th8->Write();
	th9->SetTitle("Max Et and Min Tau phase difference;eta;Max Et phase - Min Tau phase");
        th9->Write();
	th10->SetTitle("Tau with Max ET ofc phase;#eta;#tau [ns]");
        th10->Write();
	th11->SetTitle(";#eta;RMS of peak samples");
        th11->Write();
	th12->SetTitle("MaxEt vs Optimized phase Et;Optimized phase Et [GeV];Scanned MaxEt [GeV]");
	th12->Write();
	hit_SC->Sumw2(false);
	hit_SC->Write();
	f->Close();
	


}

void loadSwrodToMap_allPhases_splash_average(string swrod_filename, 
        map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases_splash"<<endl;

	int bside=-1; //-1 for beam1, 1 for beam2 
	TFile file((swrod_filename).c_str(),"read");
	TFile *f = new TFile("ofcphase_check_splash.root","recreate");
	TH1F *th0 = new TH1F("th0","Max ET ofc phase", 200,0,50);
	TH1F *th1 = new TH1F("th1","Min Tau ofc phase", 200,0,50);
	TH2F *th2 = new TH2F("th2","Max ET ofc phase 2d", 100, -5, 5, 50,0,50);
	TH1F *th3 = new TH1F("th3","bcid phase", 32,0,32);
	TH2F *th4 = new TH2F("th4","Max ET ofc phase 2d", 100, -5, 5, 200,-50,150);
	TH2F *th5 = new TH2F("th5","phase_eta", 100, -5, 5, 30,-15,15);
	//TH2F *th6 = new TH2F("th6","allshift", 150, -5, 5, 150,-80,100);
	TH2F *th6 = new TH2F("th6","optimized bcid", 150, -5, 5, 20, 0,20); //optim_bcid plot
	TH2F *th7 = new TH2F("th7","Min Tau ofc phase 2d", 100, -5, 5, 50,0,50);
	TH2F *th8 = new TH2F("th8","Min Tau 2d", 100, -5, 5, 100, -5,5);
	TH2F *th9 = new TH2F("th9","Max Et Min Tau diff", 100, -5, 5, 50, -25,25);
	TH2F *th10 = new TH2F("th10","Tau with Max ET ofc phase", 100, -5, 5, 200, -25,25);
	TH2F *th11 = new TH2F("th11","Peak sample rms", 100, -5, 5, 100, 0,1);
	TH2F *th12 = new TH2F("th12","TOF distribution", 100, -5, 5, 100, -50,50);
	//TH2F *th13 = new TH2F("th13","allshift after TOF correction", 100, -5, 5, 150, -80,100);
	TH2F *th13 = new TH2F("th13","optimized ofc phase", 100, -5, 5, 50, 0,50); //optim_ofc plot
	TH2F* evt_shift[100];
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;
	Int_t layer;
	UShort_t bcidLATOMEHEAD;
	UShort_t bcidVec[32];


	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD);
	tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidVec", bcidVec);
	tree->SetBranchAddress("layer", &layer);

	int hit_SC_counter=0;
	int pre_IEvent=0;
	TH1F* hit_SC = new TH1F("SC_hitR","SC_hitR", 60, 0., 60.);
	int nevents=0;
	int event_counter=0;
	int height_threshold=30;
	int entries = tree->GetEntries();
	int maxevents=54;
	map<Int_t, vector<Short_t>[32] > channel_samples;
	map<Int_t, vector<Short_t> > channel_peakpos;
	map<Int_t, Double_t[32]> channel_peakrms;
	map<Int_t, Double_t[32]> channel_mean;
	map< Int_t,  Float_t > channel_eta;
	map< Int_t,  Float_t > channel_phi;
	map< Int_t,  Int_t > channel_layer;
	map< Int_t,  Int_t > channel_detector;
	map<Int_t, Double_t> channel_TOF;

	for (unsigned int i=0; i<entries; i++) {
	  tree->GetEntry(i);
	  //pulse check
	  int tmpmax=0;
	  int tmpmin=0;
	  int peak_flag = 0;
	  int saturation_flag = 0;
	  int trough_flag = 0;
	  int bipolar_flag = 0;
	  int flip_flag = 0;
	  int tail_flag = 0;

	  if (pre_IEvent==0 || pre_IEvent!=IEvent) {
	    hit_SC->Fill(nevents, double(hit_SC_counter)/34048.);
	    hit_SC_counter=0;		  
	    nevents++;
	    pre_IEvent=IEvent;
	  }


	  for (int isa=0;isa<32;isa++) {
	    if (samples[isa]>samples[tmpmax]) tmpmax=isa;
	    if (samples[isa]<samples[tmpmin] && samples[isa] !=0) tmpmin=isa;
	  }
	  int height = samples[tmpmax]-samples[0];
	  //peak detection                                              
	  if ((tmpmax >= 2 && samples[tmpmax-2] <= samples[tmpmax-1] && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold) || (tmpmax < 2 && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold)) peak_flag=1;
	  //saturation detection
	  if (samples[tmpmax]>=2500) saturation_flag=1;

	  //filp detection                                                                                                                                           
	  for (int jsa=1; jsa<32;jsa++) {
	    if (abs(samples[jsa]-samples[jsa-1])>=height) flip_flag = 1;
	  }
	  //trough detection                                            
	  for (int jsa=tmpmax+10;jsa<32;jsa++){
	    if (samples[jsa]>(double)(samples[tmpmax]+samples[0])/2) break;
	    if (jsa==31) trough_flag = 1;
	  }
	  //bipolar detection                                           
	  if (tmpmax < tmpmin) bipolar_flag = 1;
	  //tail detection                                              
	  for (int isa=0;isa<26;isa++) {
	    if (samples[isa]>samples[isa+1] && samples[isa+1] > samples[isa+2] && samples[isa+2] > samples[isa+3] && samples[isa+3] > samples[isa+4] && samples[isa+4] > samples[isa+5] && samples[isa+5] > samples[isa+6]) {
	      tail_flag = 1;
	    }
	  }
	  for (int isa=6;isa<32;isa++) {
	    if (samples[isa]>samples[isa-1] && samples[isa-1] > samples[isa-2] && samples[isa-2] > samples[isa-3] && samples[isa-3] > samples[isa-4] && samples[isa-4] > samples[isa-5] && samples[isa-5] > samples[isa-6]) {
	      tail_flag = 1;
	    }
	  }
	  event_counter++;
	  //if (!peak_flag || flip_flag || !trough_flag || !bipolar_flag) continue;
	  if (!peak_flag || !trough_flag || !bipolar_flag || saturation_flag) continue;
	  //if (!peak_flag) continue;
	  hit_SC_counter++;
	  //pulse check end
	  
	  channel_layer[channelId]=layer;
	  channel_detector[channelId]=detector;
	  channel_eta[channelId]=eta;
	  channel_phi[channelId]=phi;	   	  
	  channel_peakpos[channelId].push_back(tmpmax);
	  for (unsigned i=0;i<32;i++) {
	    if (samples[i] > 0) {
	      channel_samples[channelId][i].push_back(samples[i]);
	    }
	  }	  
	}
	cout<<"Detected SC numeber="<<channel_samples.size()<<endl;
	vector<SCinfo> SC_list;	
	for (auto it = channel_samples.begin(); it != channel_samples.end(); it++) {
	  double chi2=0;
	  double max_rms=0;
	  int sample_No=0;
	  pair<short, double> tmp_max; //SC peak position
	  SCinfo tmpSC;
	  for (int sam=0; sam<32; sam++) {
	    Double_t mean=0;
	    sample_No=it->second[sam].size();	      
	    Double_t sum = std::accumulate(std::begin(it->second[sam]), std::end(it->second[sam]), 0.0);
	    if (sample_No != 0) 
	      {
		mean = sum/double(sample_No);
		//find peak position                                                                                                                                
		if (mean>tmp_max.second) {
		  tmp_max.first=sam;
		  tmp_max.second=mean;
		}
	      }
	    else {
	      mean=0;
	      tmp_max.first=0;
	      tmp_max.second=0;
	    }
	      
	    tmpSC.samples[sam]=mean;	      
	  }
	  
	  //compute peak position rms
	  Double_t sum_peakpos = std::accumulate(std::begin(channel_peakpos[it->first]), std::end(channel_peakpos[it->first]), 0.0);
	  double mean_peakpos= sum_peakpos/double(channel_peakpos[it->first].size());
	  Double_t var=0;
	  Double_t diff=0;
	  double peakrms=0;
	  for (int i=0;i<channel_peakpos[it->first].size();i++) {
	    var += pow(channel_peakpos[it->first][i]-mean_peakpos,2);
	  }
	  if (channel_peakpos[it->first].size() != 0) peakrms = sqrt(var/double(channel_peakpos[it->first].size()));
	  else peakrms = 1/sqrt(12);
	  //end computation
	  //cout<<"variance="<<var<<"; Nevents="<< channel_peakpos[it->first].size()<<" peakrms="<<peakrms<<endl;
	  
	  int channelId=0;
	  channelId=it->first;
	  tmpSC.channelId=channelId;
	  tmpSC.eta=channel_eta[channelId];
	  tmpSC.phi=channel_phi[channelId];
	  tmpSC.layer=channel_layer[channelId];
	  tmpSC.detector=channel_detector[channelId];
	  tmpSC.peakrms=peakrms;
	  SC_list.push_back(tmpSC);
	  
	  if (peakrms>0.5) {
	    //cout<<"Large peakrms channel:"<<channelId<<"; eta="<<tmpSC.eta<<"; phi="<<tmpSC.phi<<"; layer="<<tmpSC.layer<<" detector="<<tmpSC.detector<<"; RMS="<<tmpSC.peakrms<<endl;
	    char histname[100];
	    sprintf(histname,"SC%d_largerms%f",channelId,tmpSC.peakrms);
	    TH1D *tmph = new TH1D(histname,histname, 32, 0, 32);
	    for (int i=0;i<32;i++) {
	      tmph->SetBinContent(i+1,tmpSC.samples[i]);
	    }
	    tmph->Write();
	    delete tmph;
	  }
	  

	  //TOF correction
	  double TOF=0;
	  float aeta=fabs(tmpSC.eta);
	  //barrel depth
	  if (tmpSC.detector==0) {
	    double rr=1600;
	    if (aeta<=1.4) {
	      if (tmpSC.layer==2) {
		if (aeta<0.8) rr=1730;
		else rr=1680;		
	      }
	      else if (tmpSC.layer==1) {
		if (aeta<0.8) rr=1540;
		else rr=1520;		
	      }
	      else if (tmpSC.layer==3) rr=1800;	      
	      else if (tmpSC.layer==0) rr=1450;
	    }
	    else {
	      if (tmpSC.layer==2) rr= (1689.621619 + 2.682993*aeta - 70.165741*aeta*aeta);
	      else rr = (1522.775373 + 27.970192*aeta - 21.104108*aeta*aeta);
	    }
	    TOF= rr/300.*(bside*sinh(tmpSC.eta)-cosh(tmpSC.eta)); //TOF factor
	    //if (aeta<=1.4) cout<<"eta="<<eta<<"; TOF="<<TOF<<endl;
	    //TOF=-fabs(rr)/300.*(bside*rr/fabs(rr)-1./fabs(tanh(tmpSC.eta)));
	    tmpSC.TOF=TOF;
	  }
	  else {
	    double zz=3800;
	    //end-cap depth
	    if (tmpSC.detector==1 || tmpSC.detector==2) {
	      if (tmpSC.layer==0) zz=3600;	    
	      else if (tmpSC.layer==1) {
		if (aeta<1.5) zz=(12453.297448 - 5735.787116*aeta);
		else zz=3790.671754;
	      }
	      else if (tmpSC.layer==2) {
		if (aeta < 1.5) zz=(8027.574119 - 2717.653528*aeta);
		else zz=(3473.473909 + 453.941515*aeta - 119.101945*aeta*aeta);
	      }
	      else if (tmpSC.layer==3) zz=4150;	      
	    }
	    //HEC depth
	    else if (tmpSC.detector==3) {
	      if (tmpSC.layer==0) zz=4510;
	      else if (tmpSC.layer==1) zz=4930;
	      else if (tmpSC.layer==2) zz=5390;
	      else if (tmpSC.layer==3) zz=5880;
	    }	   
	    //FCAL depth
	    else if (tmpSC.detector==4){
	      if (tmpSC.layer==1) zz=4920;
              else if (tmpSC.layer==2) zz=5380;
              else if (tmpSC.layer==3) zz=5840;
	    }   
	    if (tmpSC.eta<0) zz*=-1; 
	    TOF=fabs(zz)/300.*(bside*zz/fabs(zz)-1./fabs(tanh(tmpSC.eta)));
	    tmpSC.TOF=TOF;
	    //cout<<"eta="<<tmpSC.eta<<"; TOF="<<TOF<<endl;
	   }
	  //TOF correction end

	}
	cout<<"maxevent="<<nevents<<endl;
	
	//write ttree of shift phase
	TTree* newtree = new TTree("sc_shift","tree with shift ofc phase and shfit BCID");
	Int_t new_channelId,new_layer,new_detector;
	Float_t new_eta,new_phi;
	Short_t optim_bcid,optim_ofc;
	newtree->Branch("channelId", &new_channelId);
	newtree->Branch("layer", &new_layer);
	newtree->Branch("detector", &new_detector);
	newtree->Branch("eta", &new_eta);
	newtree->Branch("phi", &new_phi);
	newtree->Branch("optim_bcid", &optim_bcid);
	newtree->Branch("optim_ofc", &optim_ofc);
	
	for (unsigned int i=0; i<SC_list.size(); i++) {
		tree->GetEntry(i);
		//if (SC_list[i].layer!=0) continue; //layer_diff
		double e2et = TMath::Sin(2*TMath::ATan(exp(-SC_list[i].eta)));
		if (map_scid_lsb.find(SC_list[i].channelId)==map_scid_lsb.end() || map_scid_ped.find(SC_list[i].channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(SC_list[i].channelId)==0 || map_scid_lsb.at(SC_list[i].channelId)<=0) continue;
		if (map_scid_ofca_allPhases.find(SC_list[i].channelId)==map_scid_ofca_allPhases.end()) continue;
		//if (map_scid_ofca_allPhases.at(SC_list[i].channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<SC_list[i].channelId<<" = "<<map_scid_ofca_allPhases.at(SC_list[i].channelId).size()<<endl;	
		//get max et phase and min tau phase with averaged pulse
		int j=23;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(SC_list[i].channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(SC_list[i].channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==bcidLATOMEHEAD) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(SC_list[i].samples[l+k]-map_scid_ped.at(SC_list[i].channelId))*a[l];
				ETTau+=e2et*(SC_list[i].samples[l+k]-map_scid_ped.at(SC_list[i].channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		int phase=BCET;
		float maxET_ofcphase=0;
		float minTau_ofcphase=0;	
		float maxET_ofc=0;
		float mintau_ofc=999.;
		float maxphase_tau=999.;
		float minphase_tau=999.;
		for (int j=0; j<50; j++) {//loop over phases
		  for (int k=0; k<4; k++) {
		    a[k]=map_scid_ofca_allPhases.at(SC_list[i].channelId).at(j)[k];
		    b[k]=map_scid_ofcb_allPhases.at(SC_list[i].channelId).at(j)[k];
		  }	
		  float ET=0;
		  float ETTau=0.;
		  for (int l=0; l<4; l++) {
		    ET+=e2et*(SC_list[i].samples[l+phase]-map_scid_ped.at(SC_list[i].channelId))*a[l];
		    ETTau+=e2et*(SC_list[i].samples[l+phase]-map_scid_ped.at(SC_list[i].channelId))*b[l];
		  }
		  if (ET>maxET_ofc) {
		    maxET_ofcphase=j;
		    maxET_ofc=ET;
		    maxphase_tau=(float)ETTau/ET;
		  }
		  if (fabs((float)ETTau/ET)<mintau_ofc && ET>0) {
		    minTau_ofcphase=j;
		    mintau_ofc=fabs((float)ETTau/ET);
		    minphase_tau=(float)ETTau/ET;
		  }
		  //cout<<"At channelId "<<SC_list[i].channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(SC_list[i].channelId)<<endl;
		}
		th0->Fill(maxET_ofcphase);
		th1->Fill(minTau_ofcphase);
		th2->Fill(SC_list[i].eta,maxET_ofcphase);
		th7->Fill(SC_list[i].eta,minTau_ofcphase);
		th8->Fill(SC_list[i].eta,minphase_tau);
		th9->Fill(SC_list[i].eta,maxET_ofcphase-minTau_ofcphase);
		th10->Fill(SC_list[i].eta,maxphase_tau);
		th11->Fill(SC_list[i].eta,SC_list[i].peakrms);
		th3->Fill(phase);
		th4->Fill(SC_list[i].eta,maxET_ofcphase+(2-phase)*24);		
		//if (SC_list[i].eta>0) SC_list[i].TOF=0;
		th12->Fill(SC_list[i].eta,SC_list[i].TOF);
		/*
		double all_shift=0;
		if (detector!=4) {
		  all_shift=(23-maxET_ofcphase)*1.04+(2-phase)*25-SC_list[i].TOF;
		  //all_shift=(23-maxET_ofcphase)*1.04+(2-phase)*25; //allshift debug
		  th6->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(2-phase)*25);
		  th13->Fill(SC_list[i].eta,all_shift);
		  //cout<<"channelId="<<SC_list[i].channelId<<"; shift="<<all_shift<<endl;		  
		  //evt_shift[nevents]->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(2-phase)*25);
		}
		else {
		  all_shift=(23-maxET_ofcphase)*1.04+(1-phase)*25-SC_list[i].TOF;
		  //all_shift=(23-maxET_ofcphase)*1.04+(1-phase)*25;// allshift debug
		  th6->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(1-phase)*25);
		  th13->Fill(SC_list[i].eta,all_shift);
		  //cout<<"channelId="<<SC_list[i].channelId<<"; shift="<<all_shift<<endl;
		  //evt_shift[nevents]->Fill(SC_list[i].eta,(23-maxET_ofcphase)*1.04+(1-phase)*25);
		}
		*/
		th5->Fill(SC_list[i].eta,phase);
		
		//write output ttree
		new_channelId=SC_list[i].channelId;
		new_layer=SC_list[i].layer;
		new_detector=SC_list[i].detector;
		new_eta=SC_list[i].eta;
		new_phi=SC_list[i].phi;		
		//if ((bside==1 && new_eta<0) || (bside==-1 && new_eta>0)) continue; //-1 for beam1, 1 for beam2  
		
		/*
		if (new_detector!=4) {
		  optim_bcid=2-round(all_shift/25);
		  if (optim_bcid<0) optim_bcid=0;
		  optim_ofc=23-round((all_shift-round(all_shift/25)*25)/1.04);
		  //cout<<"channelId="<<SC_list[i].channelId<<"; shift="<<all_shift<<"; detector="<<<<endl;		  
		  th13->Fill(SC_list[i].eta,(23-optim_ofc)*1.04+(2-optim_bcid)*25);
		  SC_list[i].optim_bcid=optim_bcid;
		  SC_list[i].optim_ofc=optim_ofc;
		}
		else {
		  optim_bcid=1-round(all_shift/25);
		  if (optim_bcid<0) optim_bcid=0;
		  optim_ofc=23-round((all_shift-round(all_shift/25)*25)/1.04);
		  th13->Fill(SC_list[i].eta,(23-optim_ofc)*1.04+(1-optim_bcid)*25);
		  SC_list[i].optim_bcid=optim_bcid;
		  SC_list[i].optim_ofc=optim_ofc;
		}
		*/
		
		optim_bcid=phase; //4 samples with max Et
	        optim_ofc=maxET_ofcphase; //TOF uncorrected
		//optim_ofc=maxET_ofcphase+round(SC_list[i].TOF/1.04); //TOF corrected
		if (optim_bcid>3)  { //pulse check
		  cout<<"channelId="<<SC_list[i].channelId<<"; optim_bcid="<<phase<<"; optimc_ofc="<<maxET_ofcphase<<"; detector="<<SC_list[i].detector<<endl;
		  char histname[100];
		  sprintf(histname,"SC%d_optbcid%hd_maxEtofc%d_minTauofc%d_detector%d",SC_list[i].channelId,optim_bcid,int(maxET_ofcphase),int(minTau_ofcphase),SC_list[i].detector);
		  TH1D *tmph = new TH1D(histname,histname, 32, 0, 32);
		  for (int j=0;j<32;j++) {
		    tmph->SetBinContent(j+1,SC_list[i].samples[j]);
		  }
		  tmph->Write();
		  delete tmph;
		}
		th6->Fill(SC_list[i].eta,optim_bcid);
		th13->Fill(SC_list[i].eta,optim_ofc);
		
		newtree->Fill();	
	}
	/*
	th0->Write();
	th1->Write();
	th2->SetTitle("OFC phase with max Et;#eta;OFC phase");
	th2->Write();
	th3->Write();
	th4->Write();
	th5->Write();
	th6->SetTitle("Shift time scanned with OFC phase;#eta;shift time [ns]");
	th6->Write();
	th7->SetTitle("OFC phase with min tau;#eta;OFC phase");
	th7->Write();
	th8->Write();
	th9->SetTitle("Max Et and Min Tau phase difference;eta;Max Et phase - Min Tau phase");
        th9->Write();
	th10->SetTitle("Tau with Max ET ofc phase;#eta;#tau [ns]");
        th10->Write();
	th11->SetTitle(";#eta;RMS of peak samples");
        th11->Write();
	th12->Write();
	hit_SC->Sumw2(false);
	hit_SC->Write();
	*/
	th6->SetTitle("Optimized bcid phase;#eta;bcid phase");
	th6->Write();
	th13->SetTitle("Optimized OFC phase;#eta;ofc phase");
	th13->Write();
	newtree->AutoSave();
	f->Close();

}

void loadSwrodToMap_allPhases_collision(string collision_filename, 
	map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases_collision"<<endl;

	TFile file((collision_filename).c_str(),"read");
	TFile *f = new TFile("ofcphase_check_collision.root","recreate");
	TH1F *th0 = new TH1F("th0","Max ET ofc phase", 200,0,50);
	TH1F *th1 = new TH1F("th1","Min Tau ofc phase", 200,0,50);
	TH2F *th2 = new TH2F("th2","Max ET ofc phase 2d", 100, -5, 5, 50,0,50);
	TH1F *th3 = new TH1F("th3","bcid phase", 32,0,32);
	TH2F *th4 = new TH2F("th4","Max ET ofc phase 2d", 100, -5, 5, 200,-50,150);
	TH2F *th5 = new TH2F("th5","phase_eta", 100, -5, 5, 30,-15,15);
	TH2F *th6 = new TH2F("th6","allshift", 150, -5, 5, 150,-80,100);
	TH2F *th7 = new TH2F("th7","Min Tau ofc phase 2d", 100, -5, 5, 50,0,50);
	TH2F *th8 = new TH2F("th8","Min Tau 2d", 100, -5, 5, 100, -5,5);
	TH2F *th9 = new TH2F("th9","Max Et Min Tau diff", 100, -5, 5, 50, -25,25);
	TH2F *th10 = new TH2F("th10","Tau with Max ET ofc phase", 100, -5, 5, 50, -25,25);
	TH2F* evt_shift[100];
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;
	Int_t layer;
	UShort_t bcidLATOMEHEAD;
	UShort_t bcidVec[32];


	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD);
	tree->SetBranchAddress("bcidVec", bcidVec);
	tree->SetBranchAddress("layer", &layer);

	int hit_SC_counter=0;
	int pre_IEvent=0;
	TH1F* hit_SC = new TH1F("SC_hitR","SC_hitR", 1000, 0., 0.);
	int nevents=0;
	int event_counter=0;
	int height_threshold=30;
	int entries = tree->GetEntries();
	int maxevents=36;
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ofca_allPhases.find(channelId)==map_scid_ofca_allPhases.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
	
		//int l1id=l1idLATOMEHEAD&0xFFFFFF;
		//string key=myint_to_string(l1id,IEvent,bcidLATOMEHEAD);
		//if (key!="41&610978&151") continue;
		//if (samples[4]-samples[1]<10) continue;	       				

		if (map_scid_ofca_allPhases.at(channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<channelId<<" = "<<map_scid_ofca_allPhases.at(channelId).size()<<endl;	

		//pulse check
		int tmpmax=0;
		int tmpmin=0;
		int peak_flag = 0;
		int saturation_flag = 0;
		int trough_flag = 0;
		int bipolar_flag = 0;
		int flip_flag = 0;
		int tail_flag = 0;

		if (pre_IEvent==0 || pre_IEvent!=IEvent) {
		  hit_SC->Fill(nevents, double(hit_SC_counter)/34048.);
		  /*
		  TCanvas *c1 = new TCanvas("c1","c1",1500,800);
		 		  
		  if (nevents<maxevents-1 && nevents!=0) {
		    evt_shift[nevents]->SetTitle(";eta;shift time[ns]");
		    evt_shift[nevents]->SetTitleOffset(0.9);
		    evt_shift[nevents]->GetXaxis()->SetTitleSize(0.04);
		    evt_shift[nevents]->GetYaxis()->SetTitleSize(0.04);
		    evt_shift[nevents]->Draw("COLZ");
		    //c1->Print("shift_hist.pdf");
		    //evt_shift[nevents]->Write();
		    delete evt_shift[nevents];
		  }		    
		  else if (nevents==0) {
		    //c1->Print("shift_hist.pdf(");
		  }
		  else if (nevents==maxevents-1) {
		    //c1->Print("shift_hist.pdf)");
		  }
		  delete c1;
		  cout<<"nEvent="<<nevents<<"; hit_SC="<<hit_SC_counter<<endl;
		  char histname[100];
		  sprintf(histname,"Event%llu_allshift", IEvent);
		  evt_shift[nevents+1] = new TH2F(histname,histname, 150, -5, 5, 150,-80,100);
		  */
		  hit_SC_counter=0;		  
		  nevents++;
		  pre_IEvent=IEvent;
		}


		for (int isa=0;isa<32;isa++) {
		  if (samples[isa]>samples[tmpmax]) tmpmax=isa;
		  if (samples[isa]<samples[tmpmin] && samples[isa] !=0) tmpmin=isa;
		}
		int height = samples[tmpmax]-samples[0];
		//peak detection                                              
		if ((tmpmax >= 2 && samples[tmpmax-2] <= samples[tmpmax-1] && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold) || (tmpmax < 2 && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold)) peak_flag=1;
		//saturation detection                                        
		if (samples[tmpmax]>=2500) saturation_flag=1;

		//filp detection                                              
		for (int jsa=1; jsa<32;jsa++) {
		  if (abs(samples[jsa]-samples[jsa-1])>=height) flip_flag = 1;
		}
		//trough detection                                            
		for (int jsa=tmpmax+10;jsa<32;jsa++){
		  if (samples[jsa]>(double)(samples[tmpmax]+samples[0])/2) break;
		  if (jsa==31) trough_flag = 1;
		}
		//bipolar detection                                           
		if (tmpmax < tmpmin) bipolar_flag = 1;
		//tail detection                                              
		for (int isa=0;isa<26;isa++) {
		  if (samples[isa]>samples[isa+1] && samples[isa+1] > samples[isa+2] && samples[isa+2] > samples[isa+3] && samples[isa+3] > samples[isa+4] && samples[isa+4] > samples[isa+5] && samples[isa+5] > samples[isa+6]) {
		    tail_flag = 1;
		  }
		}
		for (int isa=6;isa<32;isa++) {
		  if (samples[isa]>samples[isa-1] && samples[isa-1] > samples[isa-2] && samples[isa-2] > samples[isa-3] && samples[isa-3] > samples[isa-4] && samples[isa-4] > samples[isa-5] && samples[isa-5] > samples[isa-6]) {
		    tail_flag = 1;
		  }
		}
		event_counter++;
		//if (!peak_flag || flip_flag || !trough_flag || !bipolar_flag) continue;
		//if (!peak_flag || !trough_flag || !bipolar_flag) continue;
		//if (!peak_flag) continue;
		if (height<15) continue;
		
		hit_SC_counter++;
		//pulse check end

		
		int j=23;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==bcidLATOMEHEAD) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		int phase=BCET;
		//int phase=tmpmax-2;
		float maxET_ofcphase=0;
		float minTau_ofcphase=0;	
		float maxET_ofc=0;
		float mintau_ofc=999.;
		float maxphase_tau=999.;
		float minphase_tau=999.;
		
		for (int j=0; j<50; j++) {//loop over phases
			for (int k=0; k<4; k++) {
				a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
				b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
			}	
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+phase]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+phase]-map_scid_ped.at(channelId))*b[l];
			}
			if (ET>maxET_ofc) {
				maxET_ofcphase=j;
				maxET_ofc=ET;
				maxphase_tau=(float)ETTau/ET;
			}
			if (fabs((float)ETTau/ET)<mintau_ofc && ET>0) {
				minTau_ofcphase=j;
				mintau_ofc=fabs((float)ETTau/ET);
				minphase_tau=(float)ETTau/ET;
			}
			//cout<<"At channelId "<<channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(channelId)<<endl;
		}
		th0->Fill(maxET_ofcphase);
		th1->Fill(minTau_ofcphase);
		th2->Fill(eta,maxET_ofcphase);
		th7->Fill(eta,minTau_ofcphase);
		th8->Fill(eta,minphase_tau);
		th9->Fill(eta,maxET_ofcphase-minTau_ofcphase);		
		th10->Fill(eta,maxphase_tau);
		th3->Fill(phase);
		//th3->Fill(tmpmax);
		th4->Fill(eta,maxET_ofcphase+(2-phase)*24);
		//evt_shift[nevents]->Fill(eta,(24-maxET_ofcphase)*1.04+(2-phase)*25);
		if (detector!=3) {
		  //evt_shift[nevents]->Fill(eta,(23-maxET_ofcphase)*1.04+(2-phase)*25);
		  th6->Fill(eta,(23-maxET_ofcphase)*1.04+(2-phase)*25);
		}
		else {
		  //evt_shift[nevents]->Fill(eta,(23-maxET_ofcphase)*1.04+(1-phase)*25);		
		  th6->Fill(eta,(23-maxET_ofcphase)*1.04+(1-phase)*25);
		}
		th5->Fill(eta,phase);

	}
	cout<<"maxevent="<<nevents<<endl;
	th0->Write();
	th1->Write();
	th2->Write();
	th3->Write();
	th4->Write();
	th5->Write();
	th6->Write();
	th7->Write();
	th8->Write();
	th9->SetTitle("Max Et and Min Tau phase difference;#eta;Max Et phase - Min Tau phase");
	th9->Write();
	th10->SetTitle("Tau with Max ET ofc phase;#eta;tau [ns]");
	th10->Write();
	hit_SC->Sumw2(false);
	hit_SC->Write();
	f->Close();

}


//jiaqi3
void load_csvDb_to_map(map<int,float> &mymap, int no, string filename){
	int scid(0);
	float output(0.);
	string line;
	ifstream f (filename);
	string stop0="#";
	string stop1="o";
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {   
		if (line.at(0)==(string)"#" || line.at(0)==(string)"o") continue;
		int count=0;
		output=0;
		scid=0;
        string word;               
        stringstream s (line);     
      	while (getline (s, word, ',')){  
		    stringstream ss(word);
			if (count==0) ss>>scid;
			if (count==no) ss>>output;
			count++;
		}
		mymap[scid]=output;
		//cout<<"SCID = "<<scid<<", output = "<<output<<endl;
	}
}


//Jiaqi1
void load_tree_to_map(string OFC_filename, map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {
	TFile file((OFC_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("OFC");
	Int_t channelId;
	Float_t OFCa[4];
	Float_t OFCb[4];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("OFCa", OFCa);
	tree->SetBranchAddress("OFCb", OFCb);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (map_scid_ofca_allPhases.find(channelId)!=map_scid_ofca_allPhases.end() && map_scid_ofca_allPhases.at(channelId).size()>=50) continue;
		array<float,4> tmp_arraya;
		array<float,4> tmp_arrayb;
		for (int j=0; j<4; j++) {
			tmp_arraya[j]=OFCa[j];
			tmp_arrayb[j]=OFCb[j];
		}
		map_scid_ofca_allPhases[channelId].push_back(tmp_arraya);
		map_scid_ofcb_allPhases[channelId].push_back(tmp_arrayb);
	}

	file.Close();
}


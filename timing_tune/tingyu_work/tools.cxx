string myint_to_string(int l1id, int IEvent, int BCID) {
    string output="";
    output+=to_string(l1id);
    output+="&";
    output+=to_string(IEvent);
    output+="&";
    output+=to_string(BCID);
    return output;
}

vector<int> mystring_to_int(string input) {
    vector<int> output;
    string tmp_str[3]={""};
    string indicator="&";
    int counter=0;
    for (std::string::iterator it=input.begin(); it!=input.end(); it++) {
        if (*it==indicator) {
            counter++;
            continue;
        }
        tmp_str[counter]+=*it;
    }
    output.push_back(stoi(tmp_str[0]));
    output.push_back(stoi(tmp_str[1]));
    output.push_back(stoi(tmp_str[2]));
    return output;
}
#include "tools.cxx"

pair<float,float> ETpolyFit(vector<float> &vec_ET, int maxET_phase);
pair<float,float> ETpolyFit2(vector<float> &vec_ET, int maxET_phase);

struct pulseShape {
	int count=0;
	int adc[32]={0};
	int layer=-1;
	int detector=-1;
	float eta=-1;

};

void check_pulse(string filename1, string filename2) {

	int samp=4;//No of sampling point in plot

    map<int, struct pulseShape> map_scid_pulseShape1;
    map<int, struct pulseShape> map_scid_pulseShape2;

	TFile file1((filename1).c_str(),"read");
	TTree *tree1 = (TTree*)file1.Get("LARDIGITS");
	TFile file2((filename2).c_str(),"read");
	TTree *tree2 = (TTree*)file2.Get("LARDIGITS");

  	ofstream output_txt;
  	output_txt.open ("output/shape_compare.txt",ios::trunc);

	int channelId1, channelId2, layer1, layer2, detector1, detector2;
	float eta1, eta2;
	short samples1[32], samples2[32];

	tree1->SetBranchAddress("channelId", &channelId1);
	tree1->SetBranchAddress("samples", samples1);
	tree1->SetBranchAddress("layer", &layer1);
	tree1->SetBranchAddress("detector", &detector1);
	tree1->SetBranchAddress("eta", &eta1);
	tree2->SetBranchAddress("channelId", &channelId2);
	tree2->SetBranchAddress("samples", samples2);
	tree2->SetBranchAddress("layer", &layer2);
	tree2->SetBranchAddress("detector", &detector2);
	tree2->SetBranchAddress("eta", &eta2);

	unsigned int entries1 = tree1->GetEntries();
	unsigned int entries2 = tree2->GetEntries();
	for (unsigned int i=0; i<entries1; i++) {
		tree1->GetEntry(i);
		map_scid_pulseShape1[channelId1].count++;
		map_scid_pulseShape1[channelId1].layer=layer1;
		map_scid_pulseShape1[channelId1].eta=eta1;
		map_scid_pulseShape1[channelId1].detector=detector1;
		for (int j=0; j<32; j++) {
			map_scid_pulseShape1[channelId1].adc[j]+=samples1[j];
		}
	}
	for (unsigned int i=0; i<entries2; i++) {
		tree2->GetEntry(i);
		map_scid_pulseShape2[channelId2].count++;
		map_scid_pulseShape2[channelId2].layer=layer2;
		map_scid_pulseShape2[channelId2].eta=eta2;
		map_scid_pulseShape2[channelId2].detector=detector2;
		for (int j=0; j<32; j++) {
			map_scid_pulseShape2[channelId2].adc[j]+=samples2[j];
		}
	}

	TProfile *tp_layer0_det0 = new TProfile("tp_layer0_det0","layer0 det0;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer0_det1 = new TProfile("tp_layer0_det1","layer0 det1;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer0_det2 = new TProfile("tp_layer0_det2","layer0 det2;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer0_det3 = new TProfile("tp_layer0_det3","layer0 det3;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer0_det4 = new TProfile("tp_layer0_det4","layer0 det4;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer1_det0 = new TProfile("tp_layer1_det0","layer1 det0;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer1_det1 = new TProfile("tp_layer1_det1","layer1 det1;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer1_det2 = new TProfile("tp_layer1_det2","layer1 det2;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer1_det3 = new TProfile("tp_layer1_det3","layer1 det3;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer1_det4 = new TProfile("tp_layer1_det4","layer1 det4;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer2_det0 = new TProfile("tp_layer2_det0","layer2 det0;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer2_det1 = new TProfile("tp_layer2_det1","layer2 det1;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer2_det2 = new TProfile("tp_layer2_det2","layer2 det2;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer2_det3 = new TProfile("tp_layer2_det3","layer2 det3;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer2_det4 = new TProfile("tp_layer2_det4","layer2 det4;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer3_det0 = new TProfile("tp_layer3_det0","layer3 det0;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer3_det1 = new TProfile("tp_layer3_det1","layer3 det1;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer3_det2 = new TProfile("tp_layer3_det2","layer3 det2;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer3_det3 = new TProfile("tp_layer3_det3","layer3 det3;eta;adc shift", 500, -6, 6, -1000, 1000);
	TProfile *tp_layer3_det4 = new TProfile("tp_layer3_det4","layer3 det4;eta;adc shift", 500, -6, 6, -1000, 1000);

	string output_filename="output_" + to_string(samp) + ".root"; 
	TFile *output = new TFile(output_filename.c_str(),"recreate");

	int count_large5=0;
	int count_large50=0;
	int count_large100=0;
	int count_total=0;

	for (auto it: map_scid_pulseShape1) {
		if (map_scid_pulseShape2.find(it.first)==map_scid_pulseShape2.end()) {
			cout<<"SCID "<<it.first<<" not found in file 2"<<endl;
			continue;
		}

		switch (10*it.second.layer+it.second.detector) {
			case 0:
				tp_layer0_det0->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 1:
				tp_layer0_det1->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 2:
				tp_layer0_det2->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 3:
				tp_layer0_det3->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 4:
				tp_layer0_det4->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 10:
				tp_layer1_det0->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 11:
				tp_layer1_det1->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 12:
				tp_layer1_det2->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 13:
				tp_layer1_det3->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 14:
				tp_layer1_det4->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 20:
				tp_layer2_det0->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 21:
				tp_layer2_det1->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 22:
				tp_layer2_det2->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 23:
				tp_layer2_det3->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 24:
				tp_layer2_det4->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 30:
				tp_layer3_det0->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 31:
				tp_layer3_det1->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 32:
				tp_layer3_det2->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 33:
				tp_layer3_det3->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
			case 34:
				tp_layer3_det4->Fill(it.second.eta, it.second.adc[samp]/it.second.count - map_scid_pulseShape2.at(it.first).adc[samp]/map_scid_pulseShape2.at(it.first).count);
				break;
		}

		bool flag5=false;
		bool flag50=false;
		bool flag100=false;
		for (int i=0; i<32; i++) {
			if (abs(it.second.adc[i]/it.second.count - map_scid_pulseShape2.at(it.first).adc[i]/map_scid_pulseShape2.at(it.first).count) > 5) {
				output_txt<<"samples["<<i<<"]: "<<it.second.adc[i]/it.second.count
				<<", "<<map_scid_pulseShape2.at(it.first).adc[i]/map_scid_pulseShape2.at(it.first).count
				<<", diff: "<<it.second.adc[i]/it.second.count - map_scid_pulseShape2.at(it.first).adc[i]/map_scid_pulseShape2.at(it.first).count<<"\n";
				flag5=true;
			}
			if (abs(it.second.adc[i]/it.second.count - map_scid_pulseShape2.at(it.first).adc[i]/map_scid_pulseShape2.at(it.first).count) > 50) {
				flag50=true;
			}
			if (abs(it.second.adc[i]/it.second.count - map_scid_pulseShape2.at(it.first).adc[i]/map_scid_pulseShape2.at(it.first).count) > 100) {
				flag100=true;
			}

		}
		count_total++;

		if (flag5) {
			count_large5++;
			output_txt<<"for SCID "<<it.first<<"\n\n";
		}
		if (flag50) {
			count_large50++;
		}
		if (flag100) {
			count_large100++;
		}
	}
	cout<<"number of SCs: "<<count_total<<endl;
	cout<<"number of SCs with adc difference > 5: "<<count_large5<<endl;
	cout<<"number of SCs with adc difference > 50: "<<count_large50<<endl;
	cout<<"number of SCs with adc difference > 100: "<<count_large100<<endl;
	tp_layer0_det0->Write();
	tp_layer0_det1->Write();
	tp_layer0_det2->Write();
	tp_layer0_det3->Write();
	tp_layer0_det4->Write();
	tp_layer1_det0->Write();
	tp_layer1_det1->Write();
	tp_layer1_det2->Write();
	tp_layer1_det3->Write();
	tp_layer1_det4->Write();
	tp_layer2_det0->Write();
	tp_layer2_det1->Write();
	tp_layer2_det2->Write();
	tp_layer2_det3->Write();
	tp_layer2_det4->Write();
	tp_layer3_det0->Write();
	tp_layer3_det1->Write();
	tp_layer3_det2->Write();
	tp_layer3_det3->Write();
	tp_layer3_det4->Write();
	output->Close();

	TCanvas *c1 = new TCanvas("c1","c1",800,600);
	gStyle->SetOptStat(0);
	tp_layer0_det0->Draw();
	c1->Print("output.pdf(","pdf");
	tp_layer0_det1->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer0_det2->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer0_det3->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer0_det4->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer1_det0->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer1_det1->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer1_det2->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer1_det3->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer1_det4->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer2_det0->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer2_det1->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer2_det2->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer2_det3->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer2_det4->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer3_det0->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer3_det1->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer3_det2->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer3_det3->Draw();
	c1->Print("output.pdf","pdf");
	tp_layer3_det4->Draw();
	c1->Print("output.pdf)","pdf");

}

//jiaqi3
void load_csvDb_to_map(map<int,float> &mymap, int no, string filename){
	int scid(0);
	float output(0.);
	string line;
	ifstream f (filename);
	string stop0="#";
	string stop1="o";
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {   
		if (line.at(0)==(string)"#" || line.at(0)==(string)"o") continue;
		int count=0;
		output=0;
		scid=0;
        string word;               
        stringstream s (line);     
      	while (getline (s, word, ',')){  
		    stringstream ss(word);
			if (count==0) ss>>scid;
			if (count==no) ss>>output;
			count++;
		}
		mymap[scid]=output;
		//cout<<"SCID = "<<scid<<", output = "<<output<<endl;
	}
}


void load_txt_to_map(vector<int> &vec_larid, string filename){ /*For vector type storing*/
	int larid(0);
	int scid(0);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {         
        string word;               
        stringstream s (line);     
      	while (getline (s, word, ' ')){  
		    stringstream ss(word);
			larid=0;
			ss>>larid;
		}
		vec_larid.push_back(larid);
	}
}

void load_txt_to_map(map<int,string> &map_scname, string filename){ /*For string type storing*/
	string scname;
	int flag(1);
	int scid(0);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening SCNAME file");

	while (getline (f, line)) {         /* read each line into line */
		flag=1;
        	string word;               /* string to hold words */
        	stringstream s (line);     /* create stringstream from line */
      		while (getline (s, word, ' ')){  /* read space separated words */
			if (flag) {
				stringstream ss(word);
				scid=0;
				ss>>scid;
				flag=0;
			} else {
				scname=word;
			}
    		}
		if (scid!=0) map_scname[scid]=scname;
	}
}

void load_txt_to_map(map<int,int> &map_larid, string filename){
	int larid(0);
	int flag(1);
	int scid(0);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {         
	flag=1;
    	string word;               
    	stringstream s (line);     
    	while (getline (s, word, ' ')){  
			if (flag) {
				stringstream ss(word);
				larid=0;
				ss>>larid;
				flag=0;
			} else {
				stringstream ss(word);
				scid=0;
				ss>>scid;
			}
    	}
	map_larid[larid]=scid;
	}
}

void load_txt_to_map(map<int,float> &map_larid, string filename){
	int larid(0);
	int flag(1);
	float eta(0.);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {         
	flag=1;
    	string word;               
    	stringstream s (line);     
    	while (getline (s, word, ' ')){  
			if (flag) {
				stringstream ss(word);
				larid=0;
				ss>>larid;
				flag=0;
			} else {
				stringstream ss(word);
				eta=0;
				ss>>eta;
			}
    	}
	if (larid!=0) map_larid[larid]=eta;
	}
}

void load_txt_to_map(map<int,vector<int>> &map_larid, string filename){ /*For vector type storing*/
	int larid(0);
	int flag(1);
	int scid(0);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {         
		flag=1;
        string word;               
        stringstream s (line);     
      	while (getline (s, word, ' ')){  
			if (flag) {
				stringstream ss(word);
				scid=0;
				ss>>scid;
				flag=0;
			} else {
				stringstream ss(word);
				larid=0;
				ss>>larid;
				map_larid[scid].push_back(larid);
			}
    	}
	}
}

void load_txt_to_map(map<int,vector<float>> &map_larid, string filename){ /*For vector type storing*/
	float larid(0.);
	int flag(1);
	int scid(0);
	string line;
	ifstream f (filename);
	if (!f.is_open()) perror ("error opening LARID file");

	while (getline (f, line)) {
		vector<float> tmp_vec;
		tmp_vec.clear();         
		flag=1;
        	string word;               
        	stringstream s (line);     
      		while (getline (s, word, ' ')){  
				if (flag) {
					stringstream ss(word);
					scid=0;
					ss>>scid;
					flag=0;
				} else {
					stringstream ss(word);
					larid=0;
					ss>>larid;
					tmp_vec.push_back(larid);
				}
    		}
		if (scid!=0) map_larid[scid]=tmp_vec;
	}
}

void loadMonToMap(string mon_filename, map<int,vector<float>> &map_scid_eta_phi, map<int,map<UInt_t,short[5]>> &map_mon_samples) {

	cout<<"Processing loadMonToMap"<<endl;

	int nduplicate=0;

	TFile file_Mon((mon_filename).c_str(),"read");
	TTree *tree_Mon = (TTree*)file_Mon.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;

	tree_Mon->SetBranchAddress("channelId", &channelId);
	tree_Mon->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree_Mon->SetBranchAddress("eta", &eta);
	tree_Mon->SetBranchAddress("phi", &phi);
	tree_Mon->SetBranchAddress("samples", samples);
	tree_Mon->SetBranchAddress("detector", &detector);

	int entries = tree_Mon->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree_Mon->GetEntry(i);
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		UInt_t IEvent=l1idLATOMEHEAD;
		/*
		if (map_mon[channelId].find(IEvent)!=map_mon[channelId].end()) {
			nduplicate++;
			vector <float> tmp_vec;
			tmp_vec.push_back(-111);
			tmp_vec.push_back(-111);
			tmp_vec.push_back(-111);
			map_mon.at(channelId).at(IEvent)=tmp_vec;
			tmp_vec.clear();
			continue;
		}
		*/

		//float peak=0;
		for (int j=0; j<5; j++) {
			map_mon_samples[channelId][IEvent][j]=samples[j+1];
		}

		//if (peak<10) continue;

		vector <float> tmp_vec;
		tmp_vec.push_back(eta);
		tmp_vec.push_back(phi);
		//tmp_vec.push_back(peak);
		map_scid_eta_phi[channelId]=tmp_vec;

		tmp_vec.clear();
	}


}

void loadSwrodToMap(string mon_filename, map<int,map<UInt_t,short[5]>> &map_mon_samples) {

	cout<<"Processing loadSwrodToMap"<<endl;

	//int nduplicate=0;

	TFile file_Mon((mon_filename).c_str(),"read");
	TTree *tree_Mon = (TTree*)file_Mon.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;

	tree_Mon->SetBranchAddress("channelId", &channelId);
	tree_Mon->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree_Mon->SetBranchAddress("eta", &eta);
	tree_Mon->SetBranchAddress("phi", &phi);
	tree_Mon->SetBranchAddress("samples", samples);
	tree_Mon->SetBranchAddress("detector", &detector);

	int entries = tree_Mon->GetEntries();
	for (unsigned int i=0; is-<entries; i++) {
		tree_Mon->GetEntry(i);
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		UInt_t IEvent=l1idLATOMEHEAD;
		/*
		if (map_mon[channelId].find(IEvent)!=map_mon[channelId].end()) {
			nduplicate++;
			vector <float> tmp_vec;
			tmp_vec.push_back(-111);
			tmp_vec.push_back(-111);
			tmp_vec.push_back(-111);
			map_mon.at(channelId).at(IEvent)=tmp_vec;
			tmp_vec.clear();
			continue;
		}
		*/

		//float peak=0;
		for (int j=0; j<5; j++) {
			map_mon_samples[channelId][IEvent][j]=samples[j];
		}

		//if (peak<10) continue;

	}


}

void loadSwrodToMap(string mon_filename, map<int,map<string,short[5]>> &map_mon_samples) {

	cout<<"Processing loadSwrodToMap"<<endl;

	//int nduplicate=0;

	TFile file_Mon((mon_filename).c_str(),"read");
	TTree *tree_Mon = (TTree*)file_Mon.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;



	tree_Mon->SetBranchAddress("channelId", &channelId);
	tree_Mon->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree_Mon->SetBranchAddress("eta", &eta);
	tree_Mon->SetBranchAddress("phi", &phi);
	tree_Mon->SetBranchAddress("samples", samples);
	tree_Mon->SetBranchAddress("detector", &detector);
	tree_Mon->SetBranchAddress("IEvent", &IEvent);
	tree_Mon->SetBranchAddress("BCID", &BCID);

	int entries = tree_Mon->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree_Mon->GetEntry(i);
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		if (samples[3]-samples[1]<10) continue;
		for (int j=0; j<5; j++) {
			map_mon_samples[channelId][key][j]=samples[j];
		}


	}
}

void loadSwrodToMap(string filename, map<int,map<string,vector<float>>> &map_swrod_ET, map<int,float> &map_scid_ped, map<int,float> &map_scid_ofca0, map<int,float> &map_scid_ofca1, map<int,float> &map_scid_ofca2, map<int,float> &map_scid_ofca3, map<int,float> &map_scid_lsb, map<int,float> &map_scid_ofcb0, map<int,float> &map_scid_ofcb1, map<int,float> &map_scid_ofcb2, map<int,float> &map_scid_ofcb3) {

	cout<<"Processing loadSwrodToMap"<<endl;

	int ET=0;
	//int nduplicate=0;

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;



	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		if (samples[4]-samples[1]<10) continue;
		float a[4];
		a[0]=map_scid_ofca0.at(channelId);
		a[1]=map_scid_ofca1.at(channelId);
		a[2]=map_scid_ofca2.at(channelId);
		a[3]=map_scid_ofca3.at(channelId);
		float b[4];
		b[0]=map_scid_ofcb0.at(channelId);
		b[1]=map_scid_ofcb1.at(channelId);
		b[2]=map_scid_ofcb2.at(channelId);
		b[3]=map_scid_ofcb3.at(channelId);

		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		for (int k=0; k<15; k++) {
			float ET=0;
			float ETTau=0.;
			for (int j=0; j<4; j++) {
				//cout<<"Samples-Ped = "<<samples[j]-map_scid_ped.at(channelId)<<endl;
				//cout<<"OFCa = "<<a[j]<<endl;
				//cout<<"LSB = "<<map_scid_lsb.at(channelId)<<endl;
				ET+=e2et*(samples[j+k]-map_scid_ped.at(channelId))*a[j];
				ETTau+=e2et*(samples[j+k]-map_scid_ped.at(channelId))*b[j];
			}
			if (channelId==939524096&&key=="12&91469&1") cout<<"BC = "<<k<<", ET = "<<ET<<endl;
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
			//cout<<"ET = "<<map_swrod_ET[channelId][key]<<endl;
		}
		/*
		cout<<"SCId "<<channelId<<": ";	
		cout<<"Tau selected tau = "<<mintau<<", BC = "<<BC<<", ET = "<<selectedET;
		cout<<", ET selected tau = "<<tauMaxET<<", BC = "<<BCET<<", ET = "<<maxET<<endl;
		*/
		//if (BC==BCET) {
		if (BCET!=-2 && BC==BCET && fabs(tauMaxET)<25) {
			vector<float> tmp_vec;
			tmp_vec.push_back(12.5*maxET);
			tmp_vec.push_back(tauMaxET);
			tmp_vec.push_back(BCET);
			map_swrod_ET[channelId][key]=tmp_vec;
		}
	}


}

//Jiaqi2
void loadSwrodToMap_allPhases_splash(string swrod_filename, 
	map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases_splash"<<endl;

	TFile file((swrod_filename).c_str(),"read");
	TFile *f = new TFile("ofcphase_check_splash.root","recreate");
	TH1F *th0 = new TH1F("th0","Max ET ofc phase", 200,-50,150);
	TH1F *th1 = new TH1F("th1","Min Tau ofc phase", 200,-50,150);
	TH2F *th2 = new TH2F("th2","Max ET ofc phase 2d", 100, -5, 5, 200,-50,150);
	TH1F *th3 = new TH1F("th3","bcid phase", 32,0,32);
	TH2F *th4 = new TH2F("th4","Max ET ofc phase 2d", 100, -5, 5, 200,-50,150);
	TH2F *th5 = new TH2F("th5","phase_eta", 100, -5, 5, 50,-25,25);
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;
	UShort_t bcidLATOMEHEAD;
	UShort_t bcidVec[32];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD);
	tree->SetBranchAddress("bcidVec", bcidVec);

	int hit_SC_counter=0;
	int pre_IEvent=0;
	TH1F* hit_SC = new TH1F("SC_hitR","SC_hitR", 1000, 0., 0.);
	int nevents=0;
	int event_counter=0;
	int height_threshold=30;
	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		//if (key!="41&610978&151") continue;
		//if (samples[4]-samples[1]<10) continue;	       				
		if (map_scid_ofca_allPhases.find(channelId)==map_scid_ofca_allPhases.end()) continue;
		if (map_scid_ofca_allPhases.at(channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<channelId<<" = "<<map_scid_ofca_allPhases.at(channelId).size()<<endl;	

		//pulse check
		int tmpmax=0;
		int tmpmin=0;
		int peak_flag = 0;
		int saturation_flag = 0;
		int trough_flag = 0;
		int bipolar_flag = 0;
		int flip_flag = 0;
		int tail_flag = 0;

		if (pre_IEvent==0 || pre_IEvent!=IEvent) {
		  hit_SC->Fill(nevents, double(hit_SC_counter)/34048.);
		  hit_SC_counter=0;
		  nevents++;
		  pre_IEvent=IEvent;
		}
		for (int isa=0;isa<32;isa++) {
		  if (samples[isa]>samples[tmpmax]) tmpmax=isa;
		  if (samples[isa]<samples[tmpmin] && samples[isa] !=0) tmpmin=isa;
		}
		int height = samples[tmpmax]-samples[0];
		//peak detection                                                                                                                                                                                                                          
		if ((tmpmax >= 2 && samples[tmpmax-2] <= samples[tmpmax-1] && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold) || (tmpmax < 2 && samples[tmpmax+2] < samples[tmpmax+1] && height >= height_threshold)) peak_flag=1;
		//saturation detection                                                                                                                                                                                                                    
		if (samples[tmpmax]>=2500) saturation_flag=1;

		//filp detection                                                                                                                                                                                                                          
		for (int jsa=1; jsa<32;jsa++) {
		  if (abs(samples[jsa]-samples[jsa-1])>=height) flip_flag = 1;
		}
		//trough detection                                                                                                                                                                                                                        
		for (int jsa=tmpmax+10;jsa<32;jsa++){
		  if (samples[jsa]>(double)(samples[tmpmax]+samples[0])/2) break;
		  if (jsa==31) trough_flag = 1;
		}
		//bipolar detection                                                                                                                                                                                                                       
		if (tmpmax < tmpmin) bipolar_flag = 1;
		//tail detection                                                                                                                                                                                                                          
		for (int isa=0;isa<26;isa++) {
		  if (samples[isa]>samples[isa+1] && samples[isa+1] > samples[isa+2] && samples[isa+2] > samples[isa+3] && samples[isa+3] > samples[isa+4] && samples[isa+4] > samples[isa+5] && samples[isa+5] > samples[isa+6]) {
		    tail_flag = 1;
		    //cout <<"0samples="<<samples[isa]<<"; 1samples="<<samples[isa+1]<<"; 2samples"<<samples[isa+2]<<"; 3samples"<<samples[isa+3]<<"; 4samples"<<samples[isa+4]<<"; 5samples"<<samples[isa+5]<<endl;//debug                               
		  }
		}
		for (int isa=6;isa<32;isa++) {
		  if (samples[isa]>samples[isa-1] && samples[isa-1] > samples[isa-2] && samples[isa-2] > samples[isa-3] && samples[isa-3] > samples[isa-4] && samples[isa-4] > samples[isa-5] && samples[isa-5] > samples[isa-6]) {
		    tail_flag = 1;
		    //cout <<"0samples="<<samples[isa]<<"; 1samples="<<samples[isa-1]<<"; 2samples"<<samples[isa-2]<<"; 3samples"<<samples[isa-3]<<"; 4samples"<<samples[isa-4]<<"; 5samples"<<samples[isa-5]<<endl;//debug                               
		  }
		}
		event_counter++;
		//if (!peak_flag || flip_flag || !trough_flag || !bipolar_flag) continue;
		if (!peak_flag || !trough_flag || !bipolar_flag) continue;
		//if (!peak_flag) continue;

		hit_SC_counter++;
		//pulse check end

		
		int j=23;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==BCID) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		//int phase=BCET;
		int phase=tmpmax;
		float maxET_ofcphase=0;
		float minTau_ofcphase=0;	
		float maxET_ofc=0;
		float mintau_ofc=999.;
		for (int j=0; j<50; j++) {//loop over phases
			for (int k=0; k<4; k++) {
				a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
				b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
			}	
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+phase]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+phase]-map_scid_ped.at(channelId))*b[l];
			}
			if (ET>maxET_ofc) {
				maxET_ofcphase=j;
				maxET_ofc=ET;
			}
			if (fabs((float)ETTau/ET)<mintau_ofc && ET>0) {
				minTau_ofcphase=j;
				mintau_ofc=fabs((float)ETTau/ET);
			}
			//cout<<"At channelId "<<channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(channelId)<<endl;
		}
		th0->Fill(maxET_ofcphase+(phase-2)*24);
		th1->Fill(minTau_ofcphase+(phase-2)*24);
		th2->Fill(eta,maxET_ofcphase+(phase-2)*24);
		//th3->Fill(phase);
		th3->Fill(tmpmax-2);
		th4->Fill(eta,maxET_ofcphase+(2-phase)*24);
		th5->Fill(eta,tmpmax-2);
	}
	cout<<"maxevent="<<nevents<<endl;
	th0->Write();
	th1->Write();
	th2->Write();
	th3->Write();
	th4->Write();
	th5->Write();
	hit_SC->Sumw2(false);
	hit_SC->Write();
	f->Close();

}

void loadSwrodToMap_allPhases_splash_BCIDphase(string swrod_filename,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases,
	int bcidphase) {


	TFile file((swrod_filename).c_str(),"read");
	TFile *t = new TFile("phase_check_splash.root","recreate");
	TH1F *th0 = new TH1F("th0","Max ET phase", 32,0,32);
	TH1F *th1 = new TH1F("th1","Min Tau phase", 32,0,32);
	TH1F *th2 = new TH1F("th2","bcidVec phase", 32,0,32);
	TH1F *th3 = new TH1F("th3","phase diff", 64,-32,32);
	//TTree *tree = (TTree*)file.Get("T");
	TTree *tree = (TTree*)file.Get("T");
	Int_t channelId;
	//UInt_t l1idLATOMEHEAD;
	Int_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	//Short_t samples[32];
	Int_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	//Short_t BCID;
	Int_t BCID;
	UShort_t bcidVec[32];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	//tree->SetBranchAddress("BCID", &BCID);
	tree->SetBranchAddress("bcidLATOMEHEAD", &BCID);
	tree->SetBranchAddress("bcidVec", bcidVec);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		if (samples[4]-samples[1]<10) continue;
		if (map_scid_ofca_allPhases.find(channelId)==map_scid_ofca_allPhases.end()) continue;
		if (map_scid_ofca_allPhases.at(channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<channelId<<" = "<<map_scid_ofca_allPhases.at(channelId).size()<<endl;
		int j=bcidphase;
		float a[4], b[4];
		for (int k=0; k<4; k++) {
			a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
			b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
		}
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;
		int vecBCID=-3;
		cout<<"SCID "<<channelId<<" ";
		for (int k=0; k<32; k++) {
			if (bcidVec[k]==BCID) {
				vecBCID=k;
				break;
			}
		}
		for (int k=0; k<28; k++) {
			float ET=0;
			float ETTau=0.;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*b[l];
			}
			//cout<<(float)ETTau/ET<<", ";
			if (ET>maxET) {
				tauMaxET=(float)ETTau/ET;
				maxET=ET;
				BCET=k;
			}
			if (fabs((float)ETTau/ET)<mintau && ET>0) {
				mintau=fabs((float)ETTau/ET);
				selectedET=ET;
				BC=k;
			}
		}
		cout<<"max ET phase = "<<BCET<<", min Tau phase = "<<BC<<", bcidVec phase = "<<vecBCID<<"\n";
		if (BC!=-1) {
			th0->Fill(BCET);
			th1->Fill(BC);
			th2->Fill(vecBCID);
			th3->Fill(BCET-BC);
		}
		//cout<<"At channelId "<<channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(channelId)<<endl;

	}	
	file.Close();
	th0->Write();
	th1->Write();
	th2->Write();
	th3->Write();
	t->Close();

}

void loadSwrodToMap_allPhases(string swrod_filename, 
	map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {

	cout<<"Processing loadSwrodToMap_allPhases"<<endl;

	TFile file((swrod_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		if (samples[4]-samples[1]<10) continue;
		if (map_scid_ofca_allPhases.find(channelId)==map_scid_ofca_allPhases.end()) continue;
		if (map_scid_ofca_allPhases.at(channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<channelId<<" = "<<map_scid_ofca_allPhases.at(channelId).size()<<endl;
		for (int j=0; j<50; j++) {//loop over phases
			float a[4], b[4];
			for (int k=0; k<4; k++) {
				a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
				b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
			}
			float maxET=0;
			float selectedET=0;
			float mintau=999.;
			float tauMaxET=0;
			int BC=-1;
			int BCET=-2;
			for (int k=0; k<15; k++) {
				float ET=0;
				float ETTau=0.;
				for (int l=0; l<4; l++) {
					ET+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*a[l];
					ETTau+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*b[l];
				}
				if (ET>maxET) {
					tauMaxET=(float)ETTau/ET;
					maxET=ET;
					BCET=k;
				}
				if (fabs((float)ETTau/ET)<mintau && ET>0) {
					mintau=fabs((float)ETTau/ET);
					selectedET=ET;
					BC=k;
				}
			}
			//cout<<"At channelId "<<channelId<<", phase "<<j<<", BC = "<<BCET<<", maxET = "<<maxET*map_scid_lsb.at(channelId)<<endl;
			if (BCET!=-2 && BC==BCET) {
				map_swrod_ET_allPhases[channelId][key].push_back(maxET*map_scid_lsb.at(channelId));
				map_swrod_Tau_allPhases[channelId][key].push_back(tauMaxET);
				map_swrod_BC_allPhases[channelId][key].push_back(BCET);
			} else {
				map_swrod_ET_allPhases[channelId][key].push_back(-999.);
				map_swrod_Tau_allPhases[channelId][key].push_back(-999.);
				map_swrod_BC_allPhases[channelId][key].push_back(-999.);
			}
		}


	}



}

void loadSwrodToMap_allPhases(string swrod_filename, 
	map<int,map<string,short[32]>> &map_swrod_samples, 
	map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,float> &map_scid_ped, map<int,float> &map_scid_pedrms, map<int,float> &map_scid_lsb, 
	map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases, int phase, bool doEventCleaning) {

	cout<<"Processing loadSwrodToMap_allPhases"<<endl;

	TFile file((swrod_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_pedrms.find(channelId)==map_scid_pedrms.end()) continue;
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		short peak_adc=0.;
		for (int j=0; j<32; j++) {
			map_swrod_samples[channelId][key][j]=samples[j]-map_scid_ped.at(channelId);
			if (samples[j]-map_scid_ped.at(channelId)>peak_adc) peak_adc=samples[j]-map_scid_ped.at(channelId);
		}
		if (doEventCleaning && peak_adc/map_scid_pedrms.at(channelId)<40) continue;
		if (samples[4]-samples[1]<10) continue;
		if (map_scid_ofca_allPhases.find(channelId)==map_scid_ofca_allPhases.end()) continue;
		if (map_scid_ofca_allPhases.at(channelId).size()!=50) cout<<"Size of map_scid_ofca_allPhases at channelId "<<channelId<<" = "<<map_scid_ofca_allPhases.at(channelId).size()<<endl;
		for (int j=0; j<50; j++) {//loop over phases
			float a[4], b[4];
			for (int k=0; k<4; k++) {
				a[k]=map_scid_ofca_allPhases.at(channelId).at(j)[k];
				b[k]=map_scid_ofcb_allPhases.at(channelId).at(j)[k];
			}
			float ET=0.;
			float ETTau=0.;
			int k=phase;
			for (int l=0; l<4; l++) {
				ET+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*a[l];
				ETTau+=e2et*(samples[l+k]-map_scid_ped.at(channelId))*b[l];
			}	
			float Tau=(ETTau==0?-999.:(float)ETTau/ET);
			//cout<<"At channelId "<<channelId<<", phase "<<j<<", BC = "<<k<<", ET = "<<ET*map_scid_lsb.at(channelId)<<", Tau = "<<Tau*map_scid_lsb.at(channelId)<<endl;
			map_swrod_ET_allPhases[channelId][key].push_back(ET*map_scid_lsb.at(channelId));
			map_swrod_Tau_allPhases[channelId][key].push_back(Tau);
			map_swrod_BC_allPhases[channelId][key].push_back(k);
		}


	}


}

void check_phase(
	map<int,map<string,vector<float>>> &map_swrod_ET_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_Tau_allPhases, 
	map<int,map<string,vector<float>>> &map_swrod_BC_allPhases,
	map<int,map<string,float>> &map_swrod_fittedET, map<int,map<string,float>> &map_swrod_fittedPhase, map<int,map<string,float>> &map_swrod_Tau,
	map<int,vector<int>> &map_scid_det_ac,
	string event) {
		cout<<"Prcessing check_phase\n";
		TH1F *th_ET = new TH1F("th_ET","Max ET phase",50,-0.5,49.5);
		TH1F *th_Tau = new TH1F("th_Tau","Min Tau phase",50,-0.5,49.5);
		TH1F *th_ET_EMBA = new TH1F("th_ET_EMBA","Max ET phase EMBA",50,-0.5,49.5);
		TH1F *th_Tau_EMBA = new TH1F("th_Tau_EMBA","Min Tau phase EMBA",50,-0.5,49.5);
		TH1F *th_ET_EMBC = new TH1F("th_ET_EMBC","Max ET phase EMBC",50,-0.5,49.5);
		TH1F *th_Tau_EMBC = new TH1F("th_Tau_EMBC","Min Tau phase EMBC",50,-0.5,49.5);
		TH1F *th_ET_EMECA = new TH1F("th_ET_EMECA","Max ET phase EMECA",50,-0.5,49.5);
		TH1F *th_Tau_EMECA = new TH1F("th_Tau_EMECA","Min Tau phase EMECA",50,-0.5,49.5);
		TH1F *th_ET_EMECC = new TH1F("th_ET_EMECC","Max ET phase EMECC",50,-0.5,49.5);
		TH1F *th_Tau_EMECC = new TH1F("th_Tau_EMECC","Min Tau phase EMECC",50,-0.5,49.5);
		TH1F *deltaET = new TH1F("deltaET", "Fitted Max ET - Max ET", 100, -1000, 1000);
		TH1F *deltaPhase = new TH1F("deltaPhase", "Fitted Phase - Max ET OFC phase", 50, -5, 5);
		for (auto it: map_swrod_ET_allPhases) {
			for (auto itr: it.second) {
				//if (itr.first!=event) continue;
				float maxET=itr.second.at(0);
				int maxET_phase=0;
				for (int i=0; i<50; i++) {
					if (itr.second.at(i)>maxET) {
						maxET_phase=i;
						maxET=itr.second.at(i);
					}
				}
				th_ET->Fill(maxET_phase);
				if (map_scid_det_ac.at(it.first).at(0)==0 && map_scid_det_ac.at(it.first).at(1)==1) th_ET_EMBA->Fill(maxET_phase);
				if (map_scid_det_ac.at(it.first).at(0)==0 && map_scid_det_ac.at(it.first).at(1)==-1) th_ET_EMBC->Fill(maxET_phase);
				if (map_scid_det_ac.at(it.first).at(0)==1 && map_scid_det_ac.at(it.first).at(1)==1) th_ET_EMECA->Fill(maxET_phase);
				if (map_scid_det_ac.at(it.first).at(0)==1 && map_scid_det_ac.at(it.first).at(1)==-1) th_ET_EMECC->Fill(maxET_phase);
				float fit=ETpolyFit(itr.second, maxET_phase).second;
				float fitPhase=ETpolyFit(itr.second, maxET_phase).first;
				map_swrod_fittedET[it.first][itr.first]=fit;
				map_swrod_fittedPhase[it.first][itr.first]=(fitPhase-23)/24;
				map_swrod_Tau[it.first][itr.first]=map_swrod_Tau_allPhases.at(it.first).at(itr.first).at(maxET_phase);
				deltaET->Fill(fit-maxET);
				deltaPhase->Fill(fitPhase-maxET_phase);
			}
		}

		for (auto it: map_swrod_Tau_allPhases) {
			for (auto itr: it.second) {
				float minTau=fabs(itr.second.at(0));
				int minTau_phase=0;
				for (int i=0; i<50; i++) {
					if (fabs(itr.second.at(i))<minTau) {
						minTau_phase=i;
						minTau=fabs(itr.second.at(i));
					}
				}
				th_Tau->Fill(minTau_phase);
			}
		}

	TFile myhist("myplot.root","RECREATE");
	th_ET->Write();
	th_ET_EMBA->Write();
	th_ET_EMBC->Write();
	th_ET_EMECA->Write();
	th_ET_EMECC->Write();
	th_Tau->Write();
	deltaET->Write();
	deltaPhase->Write();
	myhist.Close();

}

void loadMainToMap(string filename, map<int,map<string,vector<float>>> &map_main_ET, map<int,float> &map_larid_ped_high, map<int,float> &map_larid_ped_medium, map<int,float> &map_larid_ped_low, map<int,float[4]> &map_larid_ofc_high, map<int,float[4]> &map_larid_ofc_medium, map<int,float[4]> &map_larid_ofc_low, map<int,vector<float>> &map_larid_LSB, map<int,float[4]> &map_larid_ofcb_high, map<int,float[4]> &map_larid_ofcb_medium, map<int,float[4]> &map_larid_ofcb_low) {

	cout<<"Processing loadMainToMap"<<endl;

	//int nduplicate=0;

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	Short_t ELVL1Id;
	Float_t eta;
	Float_t phi;
	Short_t Gain;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;



	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("ELVL1Id", &ELVL1Id);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("Gain", &Gain);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		double e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (detector!=0 && detector!=1) continue;

		int l1id=ELVL1Id;
		string key=myint_to_string(l1id,IEvent,BCID);
		
		float maxET=0;
		float selectedET=0;
		float mintau=999.;
		float tauMaxET=0;
		int BC=-1;
		int BCET=-2;

		switch (Gain) {
			case 0://High gain
				if (map_larid_ped_high.find(channelId)==map_larid_ped_high.end()) break;
				if (map_larid_ofc_high.find(channelId)==map_larid_ofc_high.end()) break;
				if (map_larid_LSB.find(channelId)==map_larid_LSB.end()) break;
				for (int k=0; k<15; k++) {
					float ET=0;
					float ETTau=0.;
					for (int j=0; j<4; j++) {
						ET+=e2et*(samples[j+k]-map_larid_ped_high.at(channelId))*map_larid_ofc_high.at(channelId)[j]*map_larid_LSB.at(channelId).at(0);
						ETTau+=e2et*(samples[j+k]-map_larid_ped_high.at(channelId))*map_larid_ofcb_high.at(channelId)[j]*map_larid_LSB.at(channelId).at(0);
					}
					if (ET>maxET) {
						tauMaxET=fabs((float)ETTau/ET);
						maxET=ET;
						BCET=k;
					}
					if (fabs((float)ETTau/ET)<mintau && ET>0) {
						mintau=fabs((float)ETTau/ET);
						selectedET=ET;
						BC=k;
					}
				}
				/*
				cout<<"LArId "<<channelId<<": ";	
				cout<<"Tau selected tau = "<<mintau<<", BC = "<<BC<<", ET = "<<selectedET;
				cout<<", ET selected tau = "<<tauMaxET<<", BC = "<<BCET<<", ET = "<<maxET<<endl;
				*/
				//if (BC==BCET) {
				if (BCET!=-2 && BC==BCET) {
					vector<float> tmp_vec;
					tmp_vec.push_back(maxET);
					tmp_vec.push_back(tauMaxET);
					tmp_vec.push_back(BCET);
					map_main_ET[channelId][key]=tmp_vec;
				}
				//cout<<"ET high = "<<map_main_ET[channelId][key]<<endl;
				break;

			case 1://Medium gain
				if (map_larid_ped_medium.find(channelId)==map_larid_ped_medium.end()) break;
				if (map_larid_ofc_medium.find(channelId)==map_larid_ofc_medium.end()) break;
				if (map_larid_LSB.find(channelId)==map_larid_LSB.end()) break;
				for (int k=0; k<15; k++) {
					float ET=0;
					float ETTau=0.;
					for (int j=0; j<4; j++) {
						ET+=e2et*(samples[j+k]-map_larid_ped_medium.at(channelId))*map_larid_ofc_medium.at(channelId)[j]*map_larid_LSB.at(channelId).at(1);
						ETTau+=e2et*(samples[j+k]-map_larid_ped_medium.at(channelId))*map_larid_ofcb_medium.at(channelId)[j]*map_larid_LSB.at(channelId).at(1);
					}
					if (channelId==983341568&&key=="12&91469&1") cout<<"983341568 BC = "<<k<<", ET = "<<ET<<endl;
					if (ET>maxET) {
						tauMaxET=fabs((float)ETTau/ET);
						maxET=ET;
						BCET=k;
					}
					if (fabs((float)ETTau/ET)<mintau && ET>0) {
						mintau=fabs((float)ETTau/ET);
						selectedET=ET;
						BC=k;
					}
				}
				/*
				cout<<"LArId "<<channelId<<": ";	
				cout<<"Tau selected tau = "<<mintau<<", BC = "<<BC<<", ET = "<<selectedET;
				cout<<", ET selected tau = "<<tauMaxET<<", BC = "<<BCET<<", ET = "<<maxET<<endl;
				*/
				//if (BC==BCET) {
				if (BCET!=-2 && BC==BCET) {
					vector<float> tmp_vec;
					tmp_vec.push_back(maxET);
					tmp_vec.push_back(tauMaxET);
					tmp_vec.push_back(BCET);
					map_main_ET[channelId][key]=tmp_vec;
				}
				//cout<<"ET medium = "<<map_main_ET[channelId][key]<<endl;
				break;

			case 2://Low gain
				if (map_larid_ped_low.find(channelId)==map_larid_ped_low.end()) break;
				if (map_larid_ofc_low.find(channelId)==map_larid_ofc_low.end()) break;
				if (map_larid_LSB.find(channelId)==map_larid_LSB.end()) break;
				if (map_larid_LSB.at(channelId).at(2)==0) break;
				for (int k=0; k<15; k++) {
					float ET=0;
					float ETTau=0.;
					for (int j=0; j<4; j++) {
						ET+=e2et*(samples[j+k]-map_larid_ped_low.at(channelId))*map_larid_ofc_low.at(channelId)[j]*map_larid_LSB.at(channelId).at(2);
						ETTau+=e2et*(samples[j+k]-map_larid_ped_low.at(channelId))*map_larid_ofcb_low.at(channelId)[j]*map_larid_LSB.at(channelId).at(2);
					}
					if (ET>maxET) {
						tauMaxET=fabs((float)ETTau/ET);
						maxET=ET;
						BCET=k;
					}
					if (fabs((float)ETTau/ET)<mintau && ET>0) {
						mintau=fabs((float)ETTau/ET);
						selectedET=ET;
						BC=k;
					}
				}
				/*
				cout<<"LArId "<<channelId<<": ";	
				cout<<"Tau selected tau = "<<mintau<<", BC = "<<BC<<", ET = "<<selectedET;
				cout<<", ET selected tau = "<<tauMaxET<<", BC = "<<BCET<<", ET = "<<maxET<<endl;
				*/
				//if (BC==BCET) {
				if (BCET!=-2 && BC==BCET) {
					vector<float> tmp_vec;
					tmp_vec.push_back(maxET);
					tmp_vec.push_back(tauMaxET);
					tmp_vec.push_back(BCET);
					map_main_ET[channelId][key]=tmp_vec;
				}
				//cout<<"ET high = "<<map_main_ET[channelId][key]<<endl;
				break;

		}

	}


}


void plot_ped_eta_phi(map<int,float> &map_ped, map<int,vector<float>> &map_eta_phi, map<int,string> &map_ftname, string indicator) {

	float min=9999.;
	float max=-9999.;
	int min_scid=0;
	int max_scid=0;
	int count_missingSC=0;
	vector<int> scid_missingSC;
	int count_zeroPed=0;
	vector<int> scid_zeroPed;

	gStyle->SetOptStat(0);
	TCanvas *c1 = new TCanvas(("c1_"+indicator).c_str(),"c1",800,600);

	TProfile2D *tp = new TProfile2D(indicator.c_str(), indicator.c_str(), 100, -5, 5, 100, -5, 5, -10000, 10000);

	for (auto it:map_eta_phi) {

		if (map_ped.find(it.first)==map_ped.end()) {
			count_missingSC++;
			scid_missingSC.push_back(it.first);
			//cout<<"Missing SC "<<it.first<<endl;
			continue;
		}

		if (map_ped.at(it.first)==0) {
			count_zeroPed++;
			scid_zeroPed.push_back(it.first);
			//cout<<"Zero Ped"<<it.first<<endl;
			continue;
		}
		//cout<<it.second.at(0)<<", "<<it.second.at(1)<<", "<<map_ped.at(it.first)<<endl;
		if (map_ped.at(it.first)>max) {
			max=map_ped.at(it.first);
			max_scid=it.first;
		}
		if (map_ped.at(it.first)<min) {
			min_scid=it.first;
			min=map_ped.at(it.first);
		}
		tp->Fill(it.second.at(0),it.second.at(1),map_ped.at(it.first));

	}	

   	TFile *f = new TFile("plots_eta_phi.root","update");
	tp->SetTitle((indicator+";#eta;#phi").c_str());
	tp->Write();
	f->Close();

	if (indicator==(string)"LSB") {
		ofstream f_missingSC;
		f_missingSC.open("logs/missingSCinfo.txt",ios::trunc);	
		ofstream f_zeroPed;
		f_zeroPed.open("logs/zeroPed.txt",ios::trunc);	
		for (int i=0; i<scid_missingSC.size(); i++) {
			string detector="";
			switch ((int)map_eta_phi.at(scid_missingSC.at(i)).at(2)) {
				case 0:
					detector.append((string)" EMB");
					break;
				case 1:
					detector.append((string)"EMEC");
					break;
				case 2:
					detector.append((string)" HEC");
					break;
				case 3:
					detector.append((string)"FCAL");
					break;
			}

			switch ((int)map_eta_phi.at(scid_missingSC.at(i)).at(3)) {
				case -1:
					detector.append((string)"C");
					break;
				case 1:
					detector.append((string)"A");
					break;
			}

			f_missingSC<<detector<<", "<<map_ftname.at(scid_missingSC.at(i))<<", SCID="<<scid_missingSC.at(i)<<", ETA="<<map_eta_phi.at(scid_missingSC.at(i)).at(0)<<", PHI="<<map_eta_phi.at(scid_missingSC.at(i)).at(1)<<endl;
		}
		f_missingSC.close();

		for (int i=0; i<scid_zeroPed.size(); i++) {
			string detector="";
			switch ((int)map_eta_phi.at(scid_zeroPed.at(i)).at(2)) {
				case 0:
					detector.append((string)" EMB");
					break;
				case 1:
					detector.append((string)"EMEC");
					break;
				case 2:
					detector.append((string)" HEC");
					break;
				case 3:
					detector.append((string)"FCAL");
					break;
			}

			switch ((int)map_eta_phi.at(scid_zeroPed.at(i)).at(3)) {
				case -1:
					detector.append((string)"C");
					break;
				case 1:
					detector.append((string)"A");
					break;
			}

			f_zeroPed<<detector<<", "<<map_ftname.at(scid_zeroPed.at(i))<<", SCID="<<scid_zeroPed.at(i)<<", ETA="<<map_eta_phi.at(scid_zeroPed.at(i)).at(0)<<", PHI="<<map_eta_phi.at(scid_zeroPed.at(i)).at(1)<<endl;
		}
		f_zeroPed.close();
	}

	string detector="";
	switch ((int)map_eta_phi.at(min_scid).at(2)) {
		case 0:
			detector.append((string)" EMB");
			break;
		case 1:
			detector.append((string)"EMEC");
			break;
		case 2:
			detector.append((string)" HEC");
			break;
		case 3:
			detector.append((string)"FCAL");
			break;
	}

	switch ((int)map_eta_phi.at(min_scid).at(3)) {
		case -1:
			detector.append((string)"C");
			break;
		case 1:
			detector.append((string)"A");
			break;
	}
	cout<<"\n"<<indicator<<": Min="<<min<<" at "<<detector<<", "<<map_ftname.at(min_scid)<<", SCID="<<min_scid<<", ETA="<<map_eta_phi.at(min_scid).at(0)<<", PHI="<<map_eta_phi.at(min_scid).at(1)<<endl;

	detector="";
	switch ((int)map_eta_phi.at(max_scid).at(2)) {
		case 0:
			detector.append((string)" EMB");
			break;
		case 1:
			detector.append((string)"EMEC");
			break;
		case 2:
			detector.append((string)" HEC");
			break;
		case 3:
			detector.append((string)"FCAL");
			break;
	}

	switch ((int)map_eta_phi.at(max_scid).at(3)) {
		case -1:
			detector.append((string)"C");
			break;
		case 1:
			detector.append((string)"A");
			break;
	}
	cout<<indicator<<": Max="<<max<<" at "<<detector<<", "<<map_ftname.at(max_scid)<<", SCID="<<max_scid<<", ETA="<<map_eta_phi.at(max_scid).at(0)<<", PHI="<<map_eta_phi.at(max_scid).at(1)<<endl;


}

void cosmic_validate(map<int,map<UInt_t,vector<float>>> map_mon, map<int,map<UInt_t,vector<float>>> map_swrod, map<int,map<UInt_t,short[32]>> map_mon_samples, map<int,map<UInt_t,short[32]>> map_swrod_samples) {


	gStyle->SetOptStat(0);
	TCanvas *c1 = new TCanvas("c1","c1",800,600);
	c1->Divide(2,1,0.01,0.01);
	//c1->Print("output.pdf(","pdf");

	TH2D *th3 = new TH2D("hist3d", "hist3d", 100, -5, 5, 100, -5, 5);
	TProfile2D *tp = new TProfile2D("tp", "tp", 100, -5, 5, 100, -5, 5, -100, 100);

	int count_error0=0;
	int count_normal0=0;
	unsigned long long int count_error1=0;
	unsigned long long int count_normal1=0;

	for (auto itl:map_mon) {
		if (map_swrod.find(itl.first)==map_swrod.end()) {
			//cout<<"Cannot find SCID "<<itl.first<<" in SWRod readout"<<endl;
			count_error0++;
			continue;
		}
		count_normal0++;
		for (auto it:itl.second) {
			if (map_swrod.at(itl.first).find(it.first)==map_swrod.at(itl.first).end()) {
				//cout<<"<<<<<<<<Cannot find matched l1id "<<it.first<<" in SCID "<<itl.first<<"<<<<<<<<"<<endl;
				count_error1++;
				continue;
			}
			count_normal1++;

			double x[32];
			double y[32];
			double min=map_mon_samples.at(itl.first).at(it.first)[0];
			double max=map_mon_samples.at(itl.first).at(it.first)[0];
			double diff=0;

			for (int i=0; i<5; i++) {
				diff+=map_mon_samples.at(itl.first).at(it.first)[i+1]-map_swrod_samples.at(itl.first).at(it.first)[i];
			}

			for (int i=0; i<32; i++) {
				if (map_mon_samples.at(itl.first).at(it.first)[i]>max) max=map_mon_samples.at(itl.first).at(it.first)[i];
				if (map_mon_samples.at(itl.first).at(it.first)[i]<min) min=map_mon_samples.at(itl.first).at(it.first)[i];
				x[i]=i;
				y[i]=map_mon_samples.at(itl.first).at(it.first)[i];
				//cout<<y[i]<<", ";
			}
			c1->Clear();
			c1->Divide(2,1,0.01,0.05);
			string str_txt;
			str_txt="SCID = "+ to_string(itl.first) + ", l1id = " + to_string(it.first) + ", diff = " + to_string((int)(diff));
			TText *t1 = new TText(0.15,0.9,str_txt.c_str());
   			t1->SetTextSize(0.05);
   			t1->Draw();
			c1->cd(1);
			TGraph *tg1 = new TGraph(32, x, y);
			tg1->SetTitle(";channel;");
			tg1->GetYaxis()->SetLimits(min*0.9, max*1.1);
			tg1->GetYaxis()->SetRangeUser(min*0.9, max*1.1);
			tg1->Draw("");
			

			double x1[32];
			double y1[32];

			for (int i=0; i<32; i++) {
				x1[i]=i;
				y1[i]=map_swrod_samples.at(itl.first).at(it.first)[i];
			}
			c1->cd(2);
			TGraph *tg2 = new TGraph(32, x1, y1);
			tg2->SetTitle(";channel;");
			tg2->GetYaxis()->SetLimits(min*0.9, max*1.1);
			tg2->GetYaxis()->SetRangeUser(min*0.9, max*1.1);
			tg2->Draw("");
			//c1->Print("output.pdf","pdf");

			//cout<<"At l1id "<<it.first<<", Mon readout: "<<it.second.at(2)<<", SWRod readout: "<<map_swrod.at(itl.first).at(it.first).at(2)<<endl;
			th3->SetBinContent((int)((it.second.at(0)+5)/0.1),(int)((it.second.at(1)+5)/0.1),it.second.at(2)-map_swrod.at(itl.first).at(it.first).at(2));
			tp->Fill(it.second.at(0),it.second.at(1),it.second.at(2)-map_swrod.at(itl.first).at(it.first).at(2));
			cout<<"channelId = "<<itl.first<<" at l1id "<<it.first<<", Eta = "<<it.second.at(0)<<", Phi = "<<it.second.at(1)<<", diff = "<<diff<<endl;		}
	}
	//c1->Print("output.pdf)","pdf");
	cout<<"-----Number of events not matched: "<<count_error1<<"-----"<<endl;
	cout<<"-----Number of events matched: "<<count_normal1<<"-----"<<endl;
	cout<<"-----Number of SCIDs not matched: "<<count_error0<<"-----"<<endl;
	cout<<"-----Number of SCIDs matched: "<<count_normal0<<"-----"<<endl;

   	TFile *f = new TFile("output_test.root","recreate");




	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);
	th3->Write();
	tp->Write();
	th3->Draw("COLZ");

   	f->Close();

}

void cosmic_validate_Mon_Swrod(map<int,vector<float>> &map_scid_eta_phi, map<int,map<UInt_t,short[5]>> &map_mon, map<int,map<UInt_t,short[5]>> &map_swrod, int No) {

	map<float,map<float,float>> tmp_map;

	gStyle->SetOptStat(0);
	//TCanvas *c1 = new TCanvas("c1","c1",800,600);
	//c1->Divide(2,1,0.01,0.01);
	//c1->Print("output.pdf(","pdf");

	//TH2D *th3 = new TH2D("hist3d", "hist3d", 100, -5, 5, 100, -5, 5);
	TProfile2D *tp = new TProfile2D("tp", "tp", 100, -5, 5, 100, -5, 5, -100, 100);

	int count_error0=0;
	int count_normal0=0;
	unsigned long long int count_error1=0;
	unsigned long long int count_normal1=0;

	for (auto itl:map_mon) {
		if (map_swrod.find(itl.first)==map_swrod.end() || map_scid_eta_phi.find(itl.first)==map_scid_eta_phi.end()) {
			//cout<<"Cannot find SCID "<<itl.first<<" in SWRod readout"<<endl;
			count_error0++;
			continue;
		}
		count_normal0++;
		for (auto it:itl.second) {
			if (map_swrod.at(itl.first).find(it.first)==map_swrod.at(itl.first).end()) {
				//cout<<"<<<<<<<<Cannot find matched l1id "<<it.first<<" in SCID "<<itl.first<<"<<<<<<<<"<<endl;
				count_error1++;
				continue;
			}
			count_normal1++;

			//double x[32];
			//double y[32];
			//double min=map_mon_samples.at(itl.first).at(it.first)[0];
			//double max=map_mon_samples.at(itl.first).at(it.first)[0];
			double diff=0;

			for (int i=0; i<5; i++) {
				//if (it.second[i]-map_swrod.at(itl.first).at(it.first)[i]!=0) cout<<"!!!NonZero at SCID "<<itl.first<<" and l1id "<<it.first<<endl;
				diff+=abs(it.second[i]-map_swrod.at(itl.first).at(it.first)[i]);
			}
			
			if (diff!=0) cout<<"!!!NonZero at SCID "<<itl.first<<" and l1id "<<it.first<<endl;
			tmp_map[map_scid_eta_phi.at(itl.first).at(0)][map_scid_eta_phi.at(itl.first).at(1)]+=diff;
			//tp->Fill(map_scid_eta_phi.at(itl.first).at(0),map_scid_eta_phi.at(itl.first).at(1),diff);	
		}
	}

	for (auto it:tmp_map) {
		for (auto itl:it.second) {
			tp->Fill(it.first,itl.first,itl.second);
		}
	}

	//c1->Print("output.pdf)","pdf");
	//cout<<"-----Number of events not matched: "<<count_error1<<"-----"<<endl;
	//cout<<"-----Number of events matched: "<<count_normal1<<"-----"<<endl;
	//cout<<"-----Number of SCIDs not matched: "<<count_error0<<"-----"<<endl;
	//cout<<"-----Number of SCIDs matched: "<<count_normal0<<"-----"<<endl;

	string name_str = "Mon_vs_SWRod_No" + to_string(No) + ".root";
   	TFile *f = new TFile(name_str.c_str(),"recreate");




	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);
	//th3->Write();
	tp->Write();
	//th3->Draw("COLZ");

   	f->Close();

}

void load_tree_to_map(string ped_filename, map<int,float> &map_scid_pedrms) {
		
	TFile file((ped_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("PEDESTALS");
	Int_t channelId;
	Double_t rms;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("rms", &rms);
	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		map_scid_pedrms[channelId]=rms;	
	}

	file.Close();
}

//Jiaqi1
void load_tree_to_map(string OFC_filename, map<int,vector<array<float,4>>> &map_scid_ofca_allPhases, map<int,vector<array<float,4>>> &map_scid_ofcb_allPhases) {
	TFile file((OFC_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("OFC");
	Int_t channelId;
	Float_t OFCa[4];
	Float_t OFCb[4];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("OFCa", OFCa);
	tree->SetBranchAddress("OFCb", OFCb);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (map_scid_ofca_allPhases.find(channelId)!=map_scid_ofca_allPhases.end() && map_scid_ofca_allPhases.at(channelId).size()>=50) continue;
		array<float,4> tmp_arraya;
		array<float,4> tmp_arrayb;
		for (int j=0; j<4; j++) {
			tmp_arraya[j]=OFCa[j];
			tmp_arrayb[j]=OFCb[j];
		}
		map_scid_ofca_allPhases[channelId].push_back(tmp_arraya);
		map_scid_ofcb_allPhases[channelId].push_back(tmp_arrayb);
	}

	file.Close();
}

void load_tree_to_map(string main_filename, string latome_filename, map<int, map<int,float>> &map_main, map<int, vector<int>> &map_latome, map<int,int> &map_scid_layer, map<int,float> &map_scid_eta, string timestamp, bool ETID, vector<int> vec_larid) {

//Setup TTree for main readout
	TFile file_main((main_filename).c_str(),"read");
	TTree *tree_main = (TTree*)file_main.Get("LARDIGITS");
	Int_t channelId_main;
	ULong64_t IEvent_main;
	Float_t  raw_energy;
	tree_main->SetBranchAddress("channelId", &channelId_main);
	tree_main->SetBranchAddress("IEvent", &IEvent_main);
	tree_main->SetBranchAddress("raw_energy", &raw_energy);
//Setup TTree for LATOME readout
	TFile file_latome((latome_filename).c_str(),"read");
	TTree *tree_latome = (TTree*)file_latome.Get("LARDIGITS");
	Int_t channelId_latome;
	UInt_t l1idLATOMEHEAD_latome;
    Int_t energyVec_ET[32];
	Int_t energyVec_ET_ID;
	Int_t layer;
	Float_t eta;	
	short BCID;
	UShort_t bcidVec_ET[32];
	tree_latome->SetBranchAddress("channelId", &channelId_latome);
	tree_latome->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD_latome);
	tree_latome->SetBranchAddress("BCID", &BCID);	
	tree_latome->SetBranchAddress("bcidVec_ET", bcidVec_ET);	
	if (ETID) {
		tree_latome->SetBranchAddress("energyVec_ET_ID", energyVec_ET);
	}	else {
		tree_latome->SetBranchAddress("energyVec_ET", energyVec_ET);
	}
	tree_latome->SetBranchAddress("eta", &eta);
	tree_latome->SetBranchAddress("layer", &layer);


//Check the structure of LATOME readout TTree
	int entries_latome = tree_latome->GetEntries();
	tree_latome->GetEntry(entries_latome-1);
	int nEvent_latome = l1idLATOMEHEAD_latome - pow(2,24) + 1;

	int nlatome_latome = entries_latome/nEvent_latome;

	if (entries_latome%nEvent_latome==0) {
		cout<<"LATOME readout: "<<entries_latome/nEvent_latome<<" SCs in output and "<<nEvent_latome<<" events for each SC"<<endl;
	} else {
		cout<<"Warning: something is wrong with total event number of LATOME readout"<<endl;;
	}



	int entries_main = tree_main->GetEntries();
	for (int i=0; i<entries_main; i++) {
        if (i%(int)(entries_main/100)==0) cout<<"In process "<<i/(int)(entries_main/100)<<"%"<<endl;
		tree_main->GetEntry(i);
        if (find(vec_larid.begin(),vec_larid.end(),channelId_main)==vec_larid.end()) continue;
		map_main[channelId_main][IEvent_main] = raw_energy;
	}

	int nlatome_main = map_main.size();

	if (entries_main%nlatome_main==0) {
		cout<<"Main readout: "<<nlatome_main<<" LAr cells in output and "<<entries_main/nlatome_main<<" events for each LAr cell"<<endl;;
//		int nEvent_main = entries_main/nlatome_main;
	} else {
		cout<<"Warning: something is wrong with total event number of main readout"<<endl;
	}

	int k;	
	for (int i=0; i<tree_latome->GetEntries(); i++) {
		tree_latome->GetEntry(i);
		k=0;
		energyVec_ET_ID=0;
		for (int j=0; j<32; j++) {
			if (BCID==bcidVec_ET[j]) {
				energyVec_ET_ID=energyVec_ET[j];
				k=1;
			}
		}
		if (k==0) cout<<"Warning: no matching BCID in SC "<<channelId_latome<<endl;

		map_latome[channelId_latome].push_back(energyVec_ET_ID*12.5);	
		map_scid_eta[channelId_latome]=eta;
		map_scid_layer[channelId_latome]=layer;	
	

	}	


}


	void compare_energy(string timestamp, map<int, vector<int>> &map_latome, map<int, map<int,float>> map_main, map<int,string> &map_scid_scname, map<int,vector<int>> &map_scid_larid, map<int,int> map_scid_layer, map<int,float> &map_scid_eta, map<int,double> &map_scid_diff, map<int,double> &map_scid_diff_rms, map<int,int> &map_averagedEt_latome, map<int,int> &map_averagedEt_main, int &delay, vector<pair<double,double>> &vec_latome_main, vector<pair<double,double>> vec_latome_main_sc[3], map<int,pair<double,double>> &map_sc_latome_main) {

	if (delay<0) delay=-1;
	int eventperdelay(100);

	int flagg(1);
	int flagb(1);
	ofstream goodsc;
	ofstream badsc;

	if (delay<0) {
		goodsc.open("output_files/goodsc_"+timestamp+".txt",ios::trunc);
		badsc.open("output_files/badsc_"+timestamp+".txt",ios::trunc);
	} else {
		goodsc.open("output_files/goodsc_delay_"+to_string(delay)+"_"+timestamp+".txt",ios::trunc);
		badsc.open("output_files/badsc_delay_"+to_string(delay)+"_"+timestamp+".txt",ios::trunc);		
	}

	//badsc.open("/home/zhangty/LAr/energy_validation/output_files/badsc"+timestamp+"_negative.txt",ios::trunc);
	int countg(0);
	int countb(0);
	int energy_latome, energy_main;
	double sum(0);
	double squaredsum(0);
	int sum_latome(0);
	int sum_main(0);
	int count(0);
	for (auto itl:map_latome) {	/*Loop over all SCs*/
        if (map_scid_larid.find(itl.first)==map_scid_larid.end()) {
            continue;
        }
		int larid = map_scid_larid[itl.first].at(0); /*first LAr in SC*/
		squaredsum=0;
		sum=0;
		flagg=1;
		flagb=1;
		sum_latome=0;
		sum_main=0;
		count=0;
		if (map_scid_larid[itl.first].size() == 0) {
			badsc<<"Warning: SCID "<<itl.first<<" does not exist in the mapping txt file, please check";
			continue;
		}
		auto itm0 = map_main.find(map_scid_larid[itl.first].at(0));
		if (itm0==map_main.end()) {
			badsc<<"LArID "<<map_scid_larid[itl.first].at(0)<<" from SC "<<itl.first<<" does not have match in main readout"<<endl;
			continue;
		}

		//cout<<"layer = "<<map_scid_layer[itl.first]<<", scid = "<<itl.first<<", eta = "<<map_scid_eta[itl.first]<<", size = "<<map_main[map_scid_larid[itl.first].at(0)].size()<<endl;


		for (unsigned int i=0; i<5800; i++) {	/*Loop over IEvent in main readout*/

			if ( (delay!=-1) && ((i<eventperdelay*delay) || (i>eventperdelay*(delay+1))) ) continue;	/*Select only IEvent with specific delay phase*/
			count+=1;
			energy_main=0;
			
			for (int larid: map_scid_larid[itl.first]) {	/*Loop over all LAr cells in a SC*/
/*
                if (map_main[larid].size()<i+1) {
                    cout<<"bug1"<<endl;
                    cout<<"scid = "<<itl.first<<", i = "<<i<<", size = "<<map_main[larid].size()<<", larid = "<<larid<<endl;
                }
*/
				//energy_main += map_main[larid].at(i).second;
				if (itl.first==959552000 && i==0) {
					cout<<"energy_main = "<<energy_main<<endl;
					cout<<"LArId = "<<larid<<", ";
					cout<<map_main[larid][i]<<endl;

				}
				energy_main += map_main[larid][i];
			}
            //if (itl.second.size()<map_main[map_scid_larid[itl.first].at(0)].at(i).first+1) cout<<"bug2"<<endl;
			energy_latome = itl.second.at(i);
			double e2et = TMath::Sin(2*TMath::ATan(exp(-map_scid_eta[itl.first])));
//			if (map_scid_scname[itl.first]=="5E_P1" || map_scid_scname[itl.first]=="5E_F2" || map_scid_scname[itl.first]=="5E_M2" || map_scid_scname[itl.first]=="5E_B1") {
			vec_latome_main.push_back(make_pair(energy_latome,e2et*energy_main));
			if (itl.first==959522816) vec_latome_main_sc[0].push_back(make_pair(energy_latome,e2et*energy_main));	
			if (itl.first==959552000) {
				if (i==0) {
					cout<<"e2et = "<<e2et<<endl;
					cout<<"energy_main = "<<energy_main<<endl;
				}
				vec_latome_main_sc[1].push_back(make_pair(energy_latome,e2et*energy_main));	
				map_sc_latome_main[i]=make_pair(energy_latome,e2et*energy_main);
			}
			if (itl.first==960079360) {
				vec_latome_main_sc[2].push_back(make_pair(energy_latome,e2et*energy_main));		
			}
			sum_main+=e2et*energy_main;
			sum_latome+=energy_latome;
			double percentage=-1;
			if (energy_main!=0) percentage = 1.*(energy_latome-e2et*(energy_main))/(e2et*(energy_main));

/*
			if (flagg) {
				goodsc<<"\n\nSCID "<<itl.first<<", SCNAME "<<map_scid_scname[itl.first]<<", ETA = "<<map_scid_eta[itl.first]<<", layer = "<<map_scid_layer[itl.first]<<" =>\n";
				goodsc<<"Including LArID: ";
				for (int larid: map_scid_larid[itl.first]) {
					goodsc<<larid<<", ";
				}
				goodsc<<endl;
				countg+=1;
				flagg=0;
			}
            if (map_main[larid].size()<i+1) cout<<"bug3"<<endl;
			goodsc<<"at IEvent "<<map_main[larid].at(i).first;
			goodsc<<", energy readout from latome is "<<energy_latome<<" and energy readout from main is "<<e2et*energy_main<<", "<<percentage*100<<"%"<<endl;
*/
/*
			if (abs(percentage)<0.2 && energy_latome>0 && energy_main>0) {
				if (flagg) {
					goodsc<<"\n\nSCID "<<itl.first<<", SCNAME "<<map_scid_scname[itl.first]<<", ETA = "<<map_scid_eta[itl.first]<<", layer = "<<map_scid_layer[itl.first]<<" =>\n";
					goodsc<<"Including LArID: ";
					for (int larid: map_scid_larid[itl.first]) {
						goodsc<<larid<<", ";
					}
					goodsc<<endl;
					countg+=1;
					flagg=0;
				}
                if (map_main[larid].size()<i+1) cout<<"bug3"<<endl;
				goodsc<<"at IEvent "<<map_main[larid].at(i).first;
				goodsc<<", energy readout from latome is "<<energy_latome<<" and energy readout from main is "<<e2et*energy_main<<", "<<percentage*100<<"%"<<endl;
//			} else if (energy_main<0) {
			}	else {
				if (flagb) {
					badsc<<"\n\nSCID "<<itl.first<<", SCNAME "<<map_scid_scname[itl.first]<<", ETA = "<<map_scid_eta[itl.first]<<", layer = "<<map_scid_layer[itl.first]<<" =>\n";
					badsc<<"Including LArID: ";
					for (int larid: map_scid_larid[itl.first]) {
						badsc<<larid<<", ";
					}
					badsc<<endl;
					countb+=1;
					flagb=0;
				}
                if (map_main[larid].size()<i+1) cout<<"bug4"<<endl;
				badsc<<"at IEvent "<<map_main[larid].at(i).first;
				badsc<<", energy readout from latome is "<<energy_latome<<" and energy readout from main is "<<e2et*energy_main<<", "<<percentage*100<<"%"<<endl;
			}
*/			
			sum+=percentage;	
			squaredsum+=percentage*percentage;

		
		}
		if (count==0) {
			map_averagedEt_latome[itl.first] = -1;
			map_averagedEt_main[itl.first] = -1;			
			map_scid_diff[itl.first] = -1;
			map_scid_diff_rms[itl.first] = -1;
			continue;
		}

		map_averagedEt_latome[itl.first] = sum_latome/count;
		map_averagedEt_main[itl.first] = sum_main/count;				
		map_scid_diff[itl.first] = sum/count;
		map_scid_diff_rms[itl.first] = sqrt(squaredsum/count);

	}

	goodsc.close();
	badsc.close();

	cout<<"good channels: "<<countg<<endl;
	cout<<"bad channels: "<<countb<<endl;

}


void plot_diff_eta(string title, string timestamp, map<int, vector<int>> &map_latome, map<int,float> &map_scid_eta, map<int,int> &map_scid_layer, map<int,double> &map_scid_diff, int delay) {

	const int n = map_latome.size();
	double x0[n], y0[n];
	double x1[n], y1[n];
	double x2[n], y2[n];
	double x3[n], y3[n];

	int k0=0;
	int k1=0;
	int k2=0;
	int k3=0;

	for (auto it: map_latome) {
		switch (map_scid_layer[it.first]) {
			case 0:
				x0[k0] = map_scid_eta[it.first];
				y0[k0] = map_scid_diff[it.first];
				k0+=1;
				break;
			case 1:
				x1[k1] = map_scid_eta[it.first];
				y1[k1] = map_scid_diff[it.first];
				k1+=1;
				break;
			case 2:
				x2[k2] = map_scid_eta[it.first];
				y2[k2] = map_scid_diff[it.first];
				k2+=1;
				break;
			case 3:
				x3[k3] = map_scid_eta[it.first];
				y3[k3] = map_scid_diff[it.first];
				k3+=1;
				break;
		}
	}

	TGraph *g0 = new TGraph(k0, x0, y0);
	TGraph *g1 = new TGraph(k1, x1, y1);
	TGraph *g2 = new TGraph(k2, x2, y2);
	TGraph *g3 = new TGraph(k3, x3, y3);

	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	g0->SetTitle("layer 0");
	g1->SetTitle("layer 1");
	g2->SetTitle("layer 2");
	g3->SetTitle("layer 3");

	g1->SetLineColor(kBlack);
	g1->SetMarkerColor(kBlack);
	g2->SetLineColor(kRed);
	g2->SetMarkerColor(kRed);
	g3->SetLineColor(kGreen);
	g3->SetMarkerColor(kGreen);
	g0->SetLineColor(kBlue);
	g0->SetMarkerColor(kBlue);


	g0->SetMarkerStyle(8);
	g1->SetMarkerStyle(8);
	g2->SetMarkerStyle(8);
	g3->SetMarkerStyle(8);


	g0->GetYaxis()->SetRangeUser(-1, 1);
	//g->GetXaxis()->SetRangeUser(18, 28);
	g0->GetXaxis()->SetTitle("eta");
	g0->GetYaxis()->SetTitle(title.c_str());

	g0->Draw("ap");
	g1->Draw("psame");
	g2->Draw("psame");
	g3->Draw("psame");

	g0->SetLineWidth(4);
	g1->SetLineWidth(4);
	g2->SetLineWidth(4);
	g3->SetLineWidth(4);

	auto legend = new TLegend(0.75,0.67,0.9,0.87);
	legend->AddEntry(g0, "layer 0", "l");
	legend->AddEntry(g1, "layer 1", "l");
	legend->AddEntry(g2, "layer 2", "l");
	legend->AddEntry(g3, "layer 3", "l");
	legend->SetBorderSize(0);
	legend->Draw();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = title + "_eta_" + timestamp+".png";
	} else {
		filename = title + "_eta_delay_" + to_string(delay) + "_" + timestamp+".png";
	}

	c1->SaveAs((path+filename).c_str());
	c1->Close();

}


void plot_main_latome(string timestamp, map<int,int> &map_averagedEt_latome, map<int,int> &map_averagedEt_main, map<int,int> &map_scid_layer, int delay) {
	const int n = map_averagedEt_latome.size();
	double x0[n], y0[n];
	double x1[n], y1[n];
	double x2[n], y2[n];
	double x3[n], y3[n];

	int k0=0;
	int k1=0;
	int k2=0;
	int k3=0;
	for (auto it: map_averagedEt_latome) {
		if (map_averagedEt_main[it.first] < 0) continue;
		switch (map_scid_layer[it.first]) {
			case 0:
				x0[k0] = map_averagedEt_main[it.first];
				y0[k0] = it.second;
				k0+=1;
				break;
			case 1:
				x1[k1] = map_averagedEt_main[it.first];
				y1[k1] = it.second;
				k1+=1;
				break;
			case 2:
				x2[k2] = map_averagedEt_main[it.first];
				y2[k2] = it.second;
				k2+=1;
				break;
			case 3:
				x3[k3] = map_averagedEt_main[it.first];
				y3[k3] = it.second;
				k3+=1;
				break;
		}
	}


	TGraph *g0 = new TGraph(k0, x0, y0);
	TGraph *g1 = new TGraph(k1, x1, y1);
	TGraph *g2 = new TGraph(k2, x2, y2);
	TGraph *g3 = new TGraph(k3, x3, y3);


	int sx[2] = {0, 300000};
	int sy[2] = {0, 300000};
	TGraph *slope = new TGraph(2, sx, sy);
	slope->SetLineWidth(4);
	slope->SetLineStyle(10);
	slope->SetTitle("slope 1");


	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	g0->SetTitle("layer 0");
	g1->SetTitle("layer 1");
	g2->SetTitle("layer 2");
	g3->SetTitle("layer 3");

	g1->SetLineColor(kBlack);
	g1->SetMarkerColor(kBlack);
	g2->SetLineColor(kRed);
	g2->SetMarkerColor(kRed);
	g3->SetLineColor(kGreen);
	g3->SetMarkerColor(kGreen);
	g0->SetLineColor(kBlue);
	g0->SetMarkerColor(kBlue);


	g0->SetMarkerStyle(8);
	g1->SetMarkerStyle(8);
	g2->SetMarkerStyle(8);
	g3->SetMarkerStyle(8);


	g0->GetYaxis()->SetRangeUser(0, 200000);
	g0->GetXaxis()->SetRangeUser(0, 200000);
	g0->GetXaxis()->SetLimits(0,200000);
	g0->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	g0->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");

	g0->Draw("ap");
	g1->Draw("psame");
	g2->Draw("psame");
	g3->Draw("psame");
	slope->Draw("same");

	g0->SetLineWidth(4);
	g1->SetLineWidth(4);
	g2->SetLineWidth(4);
	g3->SetLineWidth(4);

	auto legend = new TLegend(0.75,0.67,0.9,0.87);
	legend->AddEntry(g0, "layer 0", "l");
	legend->AddEntry(g1, "layer 1", "l");
	legend->AddEntry(g2, "layer 2", "l");
	legend->AddEntry(g3, "layer 3", "l");
	legend->AddEntry(slope, "slope 1", "l");
	legend->SetBorderSize(0);
	legend->Draw();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = "ET_main_latome_" + timestamp + ".png";
	} else {
		filename = "ET_main_latome_delay_" + to_string(delay) + "_" + timestamp + ".png";
	}

	c1->SaveAs((path+filename).c_str());
	c1->Close();


}


void plot_Et_eta(string title, string timestamp, map<int,int> &map_averagedEt, map<int,float> &map_scid_eta, map<int,int> &map_scid_layer, int delay) {

	const int n = map_averagedEt.size();
	double x0[n], y0[n];
	double x1[n], y1[n];
	double x2[n], y2[n];
	double x3[n], y3[n];

	int k0=0;
	int k1=0;
	int k2=0;
	int k3=0;
	for (auto it: map_averagedEt) {
//		if (it.second < 0) continue;
		switch (map_scid_layer[it.first]) {
			case 0:
				x0[k0] = map_scid_eta[it.first];
				y0[k0] = it.second;
				k0+=1;
				break;
			case 1:
				x1[k1] = map_scid_eta[it.first];
				y1[k1] = it.second;
				k1+=1;
				break;
			case 2:
				x2[k2] = map_scid_eta[it.first];
				y2[k2] = it.second;
				k2+=1;
				break;
			case 3:
				x3[k3] = map_scid_eta[it.first];
				y3[k3] = it.second;
				k3+=1;
				break;
		}
	}

	TGraph *g0 = new TGraph(k0, x0, y0);
	TGraph *g1 = new TGraph(k1, x1, y1);
	TGraph *g2 = new TGraph(k2, x2, y2);
	TGraph *g3 = new TGraph(k3, x3, y3);

	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	g0->SetTitle("layer 0");
	g1->SetTitle("layer 1");
	g2->SetTitle("layer 2");
	g3->SetTitle("layer 3");

	g1->SetLineColor(kBlack);
	g1->SetMarkerColor(kBlack);
	g2->SetLineColor(kRed);
	g2->SetMarkerColor(kRed);
	g3->SetLineColor(kGreen);
	g3->SetMarkerColor(kGreen);
	g0->SetLineColor(kBlue);
	g0->SetMarkerColor(kBlue);


	g0->SetMarkerStyle(8);
	g1->SetMarkerStyle(8);
	g2->SetMarkerStyle(8);
	g3->SetMarkerStyle(8);


	g0->GetYaxis()->SetRangeUser(0, 200000);
	g0->GetXaxis()->SetTitle("#eta");
	g0->GetYaxis()->SetTitle(("E_{T}^{" + title + "} [MeV]").c_str());

	g0->Draw("ap");
	g1->Draw("psame");
	g2->Draw("psame");
	g3->Draw("psame");

	g0->SetLineWidth(4);
	g1->SetLineWidth(4);
	g2->SetLineWidth(4);
	g3->SetLineWidth(4);

	auto legend = new TLegend(0.75,0.67,0.9,0.87);
	legend->AddEntry(g0, "layer 0", "l");
	legend->AddEntry(g1, "layer 1", "l");
	legend->AddEntry(g2, "layer 2", "l");
	legend->AddEntry(g3, "layer 3", "l");
	legend->SetBorderSize(0);
	legend->Draw();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = "ET_" + title + "_eta_" + timestamp + ".png";
	} else {
		filename = "ET_" + title + "_eta_delay_" + to_string(delay) + "_" + timestamp + ".png";
	}
	
	c1->SaveAs((path+filename).c_str());
	c1->Close();

}

void plot_main_latome_sc(string timestamp, map<int,int> &map_averagedEt_latome, map<int,int> &map_averagedEt_main, map<int,int> &map_scid_layer, int delay, vector<pair<double,double>> vec_latome_main_sc[3]) {

	TProfile *tp0 = new TProfile("Et_latome_main_eta0", "latome vs main 959522816", 100, 0, 1200000, 0, 1200000);
	TProfile *tp1 = new TProfile("Et_latome_main_eta1", "latome vs main 959552000", 100, 0, 1200000, 0, 1200000);
	TProfile *tp2 = new TProfile("Et_latome_main_eta2", "latome vs main 960079360", 100, 0, 1200000, 0, 1200000);

	cout<<"vec size = "<<vec_latome_main_sc[0].size()<<endl;

	double x[5800];
	double y[5800];

	for (auto it: vec_latome_main_sc[0]) {
		tp0->Fill(it.second,it.first);
	}
	int count=0;
	for (auto it: vec_latome_main_sc[1]) {
		x[count]=it.second;
		y[count]=it.first;	
		count++;	
		tp1->Fill(it.second,it.first);
	}
	cout<<"Count = "<<count<<endl;
	for (auto it: vec_latome_main_sc[2]) {
		tp2->Fill(it.second,it.first);
	}

	TFile myhist("plots_0913.root","RECREATE");

	TGraph *tg = new TGraph (5800, x, y);
	tg->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tg->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tg->Write();

	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	tp0->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp0->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp1->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp1->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp2->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp2->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp0->Draw();

	tp0->Write();
	tp1->Write();
	tp2->Write();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = "ET_main_latome_v2_" + timestamp + ".png";
	} else {
		filename = "ET_main_latome_v2_delay_" + to_string(delay) + "_" + timestamp + ".png";
	}

	c1->SaveAs((path+filename).c_str());
	c1->Close();


}

void plot_main_latome_v2(string timestamp, map<int,int> &map_averagedEt_latome, map<int,int> &map_averagedEt_main, map<int,int> &map_scid_layer, int delay, vector<pair<double,double>> &vec_latome_main) {

	TProfile *tp = new TProfile("Et_latome_main", "Main vs latome", 100, 0, 200000, 0, 200000);

	for (auto it: vec_latome_main) {
		tp->Fill(it.second,it.first);
	}

	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	tp->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp->Draw();

	
	TFile myhist("plots_v2.root","RECREATE");
	tp->Write();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = "ET_main_latome_v2_" + timestamp + ".png";
	} else {
		filename = "ET_main_latome_v2_delay_" + to_string(delay) + "_" + timestamp + ".png";
	}

	c1->SaveAs((path+filename).c_str());
	c1->Close();


}


void plot_main_latome_v3(string timestamp, map<int,int> &map_averagedEt_latome, map<int,int> &map_averagedEt_main, map<int,int> &map_scid_layer, int delay, vector<pair<double,double>> &vec_latome_main) {

	TProfile *tp1 = new TProfile("Et_latome_main_layer0_5E_P1", "Main vs latome", 100, 0, 200000, 0, 200000);
	TProfile *tp2 = new TProfile("Et_latome_main_layer1_5E_F2", "Main vs latome", 100, 0, 200000, 0, 200000);
	TProfile *tp3 = new TProfile("Et_latome_main_layer2_5E_M2", "Main vs latome", 100, 0, 200000, 0, 200000);
	TProfile *tp4 = new TProfile("Et_latome_main_layer3_5E_B1", "Main vs latome", 100, 0, 200000, 0, 200000);
	TH2D *th1 = new TH2D("hist2d_layer0_5E_P1", "Main vs latome", 100, 0, 200000, 100, 0, 200000);
	TH2D *th2 = new TH2D("hist2d_layer1_5E_F2", "Main vs latome", 100, 0, 200000, 100, 0, 200000);
	TH2D *th3 = new TH2D("hist2d_layer2_5E_M2", "Main vs latome", 100, 0, 200000, 100, 0, 200000);
	TH2D *th4 = new TH2D("hist2d_layer3_5E_B1", "Main vs latome", 100, 0, 200000, 100, 0, 200000);

	for (int i=0; i<500; i++) {
		tp1->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);
		th1->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);			
	}
	
	for (int i=500; i<1000; i++) {
		tp2->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);
		th2->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);			
	}
	
	for (int i=1000; i<1500; i++) {
		tp3->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);
		th3->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);			
	}

	for (int i=1500; i<2000; i++) {
		tp4->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);
		th4->Fill(vec_latome_main.at(i).second,vec_latome_main.at(i).first);			
	}

	TCanvas *c1 = new TCanvas("c1","c1",2800,1400);
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	tp1->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp1->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp2->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp2->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp3->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp3->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp4->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	tp4->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");

	th1->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	th1->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	th2->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	th2->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	th3->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	th3->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	th4->GetXaxis()->SetTitle("E_{T}^{main} [MeV]");
	th4->GetYaxis()->SetTitle("E_{T}^{latome} [MeV]");
	tp1->Draw();

	
	TFile myhist("plots_v3.root","RECREATE");
	tp1->Write();
	tp2->Write();
	tp3->Write();
	tp4->Write();
	th1->Write();
	th2->Write();
	th3->Write();
	th4->Write();


	string path = "output_files/hist/eta_distribution/";
	string filename;
	if (delay<0) {
		filename = "ET_main_latome_v3_" + timestamp + ".png";
	} else {
		filename = "ET_main_latome_v3_delay_" + to_string(delay) + "_" + timestamp + ".png";
	}

	c1->SaveAs((path+filename).c_str());
	c1->Close();


}


void print_comparison(map<int,pair<double,double>> map_sc_latome_main, int n) {

	ofstream outf;
	outf.open("959552000_main_sc.txt",ios::trunc);

	for (int i=0; i<n; i++) {
		if (map_sc_latome_main.find(i)!=map_sc_latome_main.end()) {
			outf<<map_sc_latome_main.at(i).second<<" "<<map_sc_latome_main.at(i).first<<endl;
		}
	}

	outf.close();

}

void getADCtoDAC(map<int,float> &map_larid_ADCtoDAC, map<int,float> &map_larid_rampRMS, string filename) {

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("RAMPS");
	Int_t channelId;
	Float_t X[2];
	UInt_t Xi;
	Float_t RampRMS;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("X", X);
	tree->SetBranchAddress("Xi", &Xi);
	tree->SetBranchAddress("RampRMS", &RampRMS);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (Xi!=2) continue;
		if (map_larid_ADCtoDAC.find(channelId)==map_larid_ADCtoDAC.end()) {
			map_larid_ADCtoDAC[channelId]=X[1];
			map_larid_rampRMS[channelId]=RampRMS;
		} else {
			if (map_larid_ADCtoDAC.at(channelId)/map_larid_rampRMS.at(channelId)>X[1]/RampRMS) {
				map_larid_ADCtoDAC[channelId]=X[1];
				map_larid_rampRMS[channelId]=RampRMS;
				continue;
			}

			if (map_larid_rampRMS.at(channelId)>RampRMS) {
				map_larid_ADCtoDAC[channelId]=X[1];
				map_larid_rampRMS[channelId]=RampRMS;
				continue;				
			}

		}
	}

	file.Close();

}

void getPed(map<int,float> &map_larid_ped, string filename) {

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("PEDESTALS");
	Int_t channelId;
	Double_t ped;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("ped", &ped);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (ped==0) continue;
		map_larid_ped[channelId]=ped;
	}

	file.Close();

}

void getofc(map<int,float[4]> &map_larid_ofc, string filename) {

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("OFC");
	Int_t channelId;
	Float_t OFCa[4];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("OFCa", OFCa);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (map_larid_ofc.find(channelId)!=map_larid_ofc.end()) continue;
		for (int j=0; j<4; j++) {
			map_larid_ofc[channelId][j]=OFCa[j];
		}
	}

	file.Close();

}


void getofcb(map<int,float[4]> &map_larid_ofcb, string filename) {

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("OFC");
	Int_t channelId;
	Float_t OFCb[4];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("OFCb", OFCb);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		if (map_larid_ofcb.find(channelId)!=map_larid_ofcb.end()) continue;
		for (int j=0; j<4; j++) {
			map_larid_ofcb[channelId][j]=OFCb[j];
		}
	}

	file.Close();

}

void getDACtoMeV(map<int,float> &map_larid_DACtoMeV, string filename) {

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("ADCMEV");
	Int_t channelId;
	Float_t uAMeV;
	Float_t DAC2uA;
	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("uAMeV", &uAMeV);
	tree->SetBranchAddress("DAC2uA", &DAC2uA);
	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		if (channelId==0) continue;
		map_larid_DACtoMeV[channelId]=DAC2uA*uAMeV;
	}
	file.Close();
}

void getLSB(map<int,vector<float>> &map_larid_LSB, map<int,float> &map_larid_ADCtoDAC_high, map<int,float> &map_larid_ADCtoDAC_medium, map<int,float> &map_larid_ADCtoDAC_low, map<int,float>& map_larid_DACtoMeV, map<int,float> &map_larid_eta) {

	for (auto it: map_larid_eta) {
		
		float e2et = TMath::Sin(2*TMath::ATan(exp(-it.second)));
		vector<float> tmp_vec;
		//High gain
		if (map_larid_ADCtoDAC_high.find(it.first)==map_larid_ADCtoDAC_high.end() || map_larid_DACtoMeV.find(it.first)==map_larid_DACtoMeV.end()) {
			tmp_vec.push_back(0.);
		} else {
			tmp_vec.push_back(map_larid_ADCtoDAC_high.at(it.first)*map_larid_DACtoMeV.at(it.first)/**e2et*/);
		}

		//Medium gain
		if (map_larid_ADCtoDAC_medium.find(it.first)==map_larid_ADCtoDAC_medium.end() || map_larid_DACtoMeV.find(it.first)==map_larid_DACtoMeV.end()) {
			tmp_vec.push_back(0.);
		} else {
			tmp_vec.push_back(map_larid_ADCtoDAC_medium.at(it.first)*map_larid_DACtoMeV.at(it.first)/**e2et*/);
		}

		//Low gain
		if (map_larid_ADCtoDAC_low.find(it.first)==map_larid_ADCtoDAC_low.end() || map_larid_DACtoMeV.find(it.first)==map_larid_DACtoMeV.end()) {
			tmp_vec.push_back(0.);
		} else {
			tmp_vec.push_back(map_larid_ADCtoDAC_low.at(it.first)*map_larid_DACtoMeV.at(it.first)/**e2et*/);
		}

		map_larid_LSB[it.first]=tmp_vec;
		if (tmp_vec.size()!=3) cout<<"BUG on LSB map"<<endl;
		tmp_vec.clear();
	}

}

void validate_main_ADCtoDAC(map<int,float> &map_larid_ADCtoDAC, map<int,int> &map_larid_scid, string tag) {

	for (auto it: map_larid_scid) {
		if (it.second!=0) if (map_larid_ADCtoDAC.find(it.first)==map_larid_ADCtoDAC.end()) cout<<"Warning: LArID "<<it.first<<" not found for "<<tag<<" from ramp trees"<<endl;
	}

}


void cosmic_findMatch(map<int,map<string,short[5]>> &map_swrod_samples, map<int,map<string,short[5]>> &map_main_samples, map<int,vector<int>> map_scid_larid) {

	int count_matched=0;

	for (auto it: map_swrod_samples) {//scid
		if (map_scid_larid.find(it.first)==map_scid_larid.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the map!"<<endl;
			continue;
		}

		if (map_main_samples.find(map_scid_larid.at(it.first).at(0))==map_main_samples.end()) {
			//no LArId corresponded to SWRod readout found in main readout
			continue;
		}

		bool indicator=false;
		for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
			if (map_main_samples.find(map_scid_larid.at(it.first).at(i))==map_main_samples.end()) {
				//cout<<"Warning: only some of the cells in a SC are found in main readout"<<endl;
				//cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(i)<<endl;
				indicator=true;
			}	
		}	
		if (indicator) {
			continue;
		}

		for (auto itr: it.second) {//event

			bool indicator_cell = true;

			if (map_main_samples.at(map_scid_larid.at(it.first).at(0)).find(itr.first)==map_main_samples.at(map_scid_larid.at(it.first).at(0)).end()) {
				continue;
			}

			for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
				if (map_main_samples.at(map_scid_larid.at(it.first).at(i)).find(itr.first)==map_main_samples.at(map_scid_larid.at(it.first).at(i)).end()) {
					//cout<<"Warning: only some of the cells in a SC are found in main readout in the same event"<<endl;
					//cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(i)<<", event = "<<itr.first<<endl;
					indicator_cell = false;
					continue;
				}	
			}	

			if (indicator_cell) {
				cout<<"Found match in main!"<<endl;
				cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(0)<<", event = "<<itr.first<<endl;
				count_matched++;//matched number of scid
			}

		}
	}
	cout<<"Total number of matched SCId is "<<count_matched<<endl;

}


void compare_main_swrod(map<int,map<string,vector<float>>> &map_swrod_ET, map<int,map<string,vector<float>>> &map_main_ET, map<int,vector<int>> &map_scid_larid, map<int,float> &map_scid_eta, map<int,int> &map_scid_layer) {

	TH2F *tp = new TH2F("Et_main_swrod", "Main vs SWrod", 200, 0, 50, 100, 0, 50);
	vector<float> vec_x;
	vector<float> vec_y;
	vector<float> vec_x_layer0;
	vector<float> vec_y_layer0;
	vector<float> vec_x_layer1;
	vector<float> vec_y_layer1;
	vector<float> vec_x_layer2;
	vector<float> vec_y_layer2;
	vector<float> vec_x_layer3;
	vector<float> vec_y_layer3;
	TH1F *th_BC = new TH1F("th_BC","th_BC",32,-0.5,31.5);
	TH1F *th_tau = new TH1F("th_tau","th_tau",100,-50,50);
	vector<float> vec_BC_layer1;
	vector<float> vec_BC_layer2;
	vector<float> vec_BC_layer3;
	vector<float> vec_tau_layer1;
	vector<float> vec_tau_layer2;
	vector<float> vec_tau_layer3;


	int count_matched=0;

	for (auto it: map_swrod_ET) {//scid
		if (map_scid_larid.find(it.first)==map_scid_larid.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the map!"<<endl;
			continue;
		}

		if (map_scid_eta.find(it.first)==map_scid_eta.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the eta map!"<<endl;
			continue;
		}
		if (map_scid_layer.find(it.first)==map_scid_layer.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the layer map!"<<endl;
			continue;
		}

		//float e2et = TMath::Sin(2*TMath::ATan(exp(-map_scid_eta.at(it.first))));

		bool indicator=false;
		for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
			if (map_main_ET.find(map_scid_larid.at(it.first).at(i))==map_main_ET.end()) {
				//cout<<"Warning: only some of the cells in a SC are found in main readout"<<endl;
				//cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(i)<<endl;
				indicator=true;
			}	
		}	
		if (indicator) {
			continue;
		}

		for (auto itr: it.second) {//event

			float ET_main=0;
			float ET_swrod=itr.second.at(0);
			float tau_swrod=itr.second.at(1);
			float BC_swrod=itr.second.at(2);
			th_BC->Fill(BC_swrod);
			th_tau->Fill(tau_swrod);
			bool indicator2=true;
			for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
				if (map_main_ET.at(map_scid_larid.at(it.first).at(i)).find(itr.first)!=map_main_ET.at(map_scid_larid.at(it.first).at(i)).end()) {
					ET_main+=map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first).at(0);
					//if (itr.first=="12&91469&1" && it.first==939524096) cout<<"LArID "<<map_scid_larid.at(it.first).at(i)<<", ET = "<<map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first)<<endl;
				} else indicator2=false;
			}	
			if (ET_main>0 && indicator2) {
				tp->Fill(ET_main/1000,ET_swrod/1000);
				vec_x.push_back(ET_main);
				vec_y.push_back(ET_swrod);
				if (map_scid_layer.at(it.first)==0) {
					vec_x_layer0.push_back(map_scid_eta.at(it.first));
					vec_y_layer0.push_back((float)ET_swrod/ET_main);
				}
				if (map_scid_layer.at(it.first)==1) {
					vec_x_layer1.push_back(map_scid_eta.at(it.first));
					vec_y_layer1.push_back((float)ET_swrod/ET_main);
					vec_BC_layer1.push_back(BC_swrod-0.1);
					vec_tau_layer1.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==2) {
					vec_x_layer2.push_back(map_scid_eta.at(it.first));
					vec_y_layer2.push_back((float)ET_swrod/ET_main);
					vec_BC_layer2.push_back(BC_swrod);
					vec_tau_layer2.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==3) {
					vec_x_layer3.push_back(map_scid_eta.at(it.first));
					vec_y_layer3.push_back((float)ET_swrod/ET_main);
					vec_BC_layer3.push_back(BC_swrod+0.1);
					vec_tau_layer3.push_back(tau_swrod);
				}

				cout<<"At SCID "<<it.first<<", event "<<itr.first<<", ET swrod = "<<ET_swrod<<", ET main = "<<ET_main<<endl;
			}
		}
	}
	float *x=vec_x.data();
	float *y=vec_y.data();
	TGraph *tg = new TGraph(vec_x.size(), x, y);

	float *x_layer0=vec_x_layer0.data();
	float *y_layer0=vec_y_layer0.data();
	TGraph *tg0 = new TGraph(vec_x_layer0.size(), x_layer0, y_layer0);
	float *x_layer1=vec_x_layer1.data();
	float *y_layer1=vec_y_layer1.data();
	TGraph *tg1 = new TGraph(vec_x_layer1.size(), x_layer1, y_layer1);
	float *x_layer2=vec_x_layer2.data();
	float *y_layer2=vec_y_layer2.data();
	TGraph *tg2 = new TGraph(vec_x_layer2.size(), x_layer2, y_layer2);
	float *x_layer3=vec_x_layer3.data();
	float *y_layer3=vec_y_layer3.data();
	TGraph *tg3 = new TGraph(vec_x_layer3.size(), x_layer3, y_layer3);

	float *y_BC_layer1=vec_BC_layer1.data();
	TGraph *tg1_BC = new TGraph(vec_x_layer1.size(), x_layer1, y_BC_layer1);
	float *y_BC_layer2=vec_BC_layer2.data();
	TGraph *tg2_BC = new TGraph(vec_x_layer2.size(), x_layer2, y_BC_layer2);
	float *y_BC_layer3=vec_BC_layer3.data();
	TGraph *tg3_BC = new TGraph(vec_x_layer3.size(), x_layer3, y_BC_layer3);


	float *y_tau_layer1=vec_tau_layer1.data();
	TGraph *tg1_tau = new TGraph(vec_x_layer1.size(), x_layer1, y_tau_layer1);
	float *y_tau_layer2=vec_tau_layer2.data();
	TGraph *tg2_tau = new TGraph(vec_x_layer2.size(), x_layer2, y_tau_layer2);
	float *y_tau_layer3=vec_tau_layer3.data();
	TGraph *tg3_tau = new TGraph(vec_x_layer3.size(), x_layer3, y_tau_layer3);

	TFile myhist("myplot.root","RECREATE");
	tp->Write();
	tg->Write();
	tg0->SetName("tg0");
	tg1->SetName("tg1");
	tg2->SetName("tg2");
	tg3->SetName("tg3");
	tg1_BC->SetName("tg1_BC");
	tg2_BC->SetName("tg2_BC");
	tg3_BC->SetName("tg3_BC");
	tg1_tau->SetName("g1_tau");
	tg2_tau->SetName("g2_tau");
	tg3_tau->SetName("g3_tau");
	tg0->Write();
	tg1->Write();
	tg2->Write();
	tg3->Write();
	tg1_BC->Write();
	tg2_BC->Write();
	tg3_BC->Write();
	tg1_tau->Write();
	tg2_tau->Write();
	tg3_tau->Write();
	th_BC->Write();
	th_tau->Write();


}



void compare_main_swrod(map<int,map<string,vector<float>>> &map_swrod_ET, map<int,map<string,vector<float>>> &map_main_ET, map<int,vector<int>> &map_scid_larid, map<int,float> &map_scid_eta, map<int,int> &map_scid_layer, string stamp) {

	TH2F *tp = new TH2F("Et_main_swrod", "Main vs SWrod", 200, 0, 50, 100, 0, 50);
	vector<float> vec_x;
	vector<float> vec_y;
	vector<float> vec_x_layer0;
	vector<float> vec_y_layer0;
	vector<float> vec_x_layer1;
	vector<float> vec_y_layer1;
	vector<float> vec_x_layer2;
	vector<float> vec_y_layer2;
	vector<float> vec_x_layer3;
	vector<float> vec_y_layer3;
	TH1F *th_BC = new TH1F("th_BC","th_BC",32,-0.5,31.5);
	TH1F *th_tau = new TH1F("th_tau","th_tau",100,-50,50);
	vector<float> vec_BC_layer1;
	vector<float> vec_BC_layer2;
	vector<float> vec_BC_layer3;
	vector<float> vec_tau_layer1;
	vector<float> vec_tau_layer2;
	vector<float> vec_tau_layer3;


	int count_matched=0;

	for (auto it: map_swrod_ET) {//scid
		if (map_scid_larid.find(it.first)==map_scid_larid.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the map!"<<endl;
			continue;
		}

		if (map_scid_eta.find(it.first)==map_scid_eta.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the eta map!"<<endl;
			continue;
		}
		if (map_scid_layer.find(it.first)==map_scid_layer.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the layer map!"<<endl;
			continue;
		}

		//float e2et = TMath::Sin(2*TMath::ATan(exp(-map_scid_eta.at(it.first))));

		bool indicator=false;
		for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
			if (map_main_ET.find(map_scid_larid.at(it.first).at(i))==map_main_ET.end()) {
				//cout<<"Warning: only some of the cells in a SC are found in main readout"<<endl;
				//cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(i)<<endl;
				indicator=true;
			}	
		}	
		if (indicator) {
			continue;
		}

		for (auto itr: it.second) {//event
			if (itr.first!=stamp) continue;
			float ET_main=0;
			float ET_swrod=itr.second.at(0);
			float tau_swrod=itr.second.at(1);
			float BC_swrod=itr.second.at(2);
			th_BC->Fill(BC_swrod);
			th_tau->Fill(tau_swrod);
			bool indicator2=true;
			for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
				if (map_main_ET.at(map_scid_larid.at(it.first).at(i)).find(itr.first)!=map_main_ET.at(map_scid_larid.at(it.first).at(i)).end()) {
					ET_main+=map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first).at(0);
					//if (itr.first=="12&91469&1" && it.first==939524096) cout<<"LArID "<<map_scid_larid.at(it.first).at(i)<<", ET = "<<map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first)<<endl;
				} else indicator2=false;
			}	
			if (ET_main>0 && indicator2) {
				tp->Fill(ET_main/1000,ET_swrod/1000);
				vec_x.push_back(ET_main);
				vec_y.push_back(ET_swrod);
				if (map_scid_layer.at(it.first)==0) {
					vec_x_layer0.push_back(map_scid_eta.at(it.first));
					vec_y_layer0.push_back((float)ET_swrod/ET_main);
				}
				if (map_scid_layer.at(it.first)==1) {
					vec_x_layer1.push_back(map_scid_eta.at(it.first));
					vec_y_layer1.push_back((float)ET_swrod/ET_main);
					vec_BC_layer1.push_back(BC_swrod-0.1);
					vec_tau_layer1.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==2) {
					vec_x_layer2.push_back(map_scid_eta.at(it.first));
					vec_y_layer2.push_back((float)ET_swrod/ET_main);
					vec_BC_layer2.push_back(BC_swrod);
					vec_tau_layer2.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==3) {
					vec_x_layer3.push_back(map_scid_eta.at(it.first));
					vec_y_layer3.push_back((float)ET_swrod/ET_main);
					vec_BC_layer3.push_back(BC_swrod+0.1);
					vec_tau_layer3.push_back(tau_swrod);
				}

				cout<<"At SCID "<<it.first<<", event "<<itr.first<<", ET swrod = "<<ET_swrod<<", ET main = "<<ET_main<<endl;
			}
		}
	}
	float *x=vec_x.data();
	float *y=vec_y.data();
	TGraph *tg = new TGraph(vec_x.size(), x, y);

	float *x_layer0=vec_x_layer0.data();
	float *y_layer0=vec_y_layer0.data();
	TGraph *tg0 = new TGraph(vec_x_layer0.size(), x_layer0, y_layer0);
	float *x_layer1=vec_x_layer1.data();
	float *y_layer1=vec_y_layer1.data();
	TGraph *tg1 = new TGraph(vec_x_layer1.size(), x_layer1, y_layer1);
	float *x_layer2=vec_x_layer2.data();
	float *y_layer2=vec_y_layer2.data();
	TGraph *tg2 = new TGraph(vec_x_layer2.size(), x_layer2, y_layer2);
	float *x_layer3=vec_x_layer3.data();
	float *y_layer3=vec_y_layer3.data();
	TGraph *tg3 = new TGraph(vec_x_layer3.size(), x_layer3, y_layer3);

	float *y_BC_layer1=vec_BC_layer1.data();
	TGraph *tg1_BC = new TGraph(vec_x_layer1.size(), x_layer1, y_BC_layer1);
	float *y_BC_layer2=vec_BC_layer2.data();
	TGraph *tg2_BC = new TGraph(vec_x_layer2.size(), x_layer2, y_BC_layer2);
	float *y_BC_layer3=vec_BC_layer3.data();
	TGraph *tg3_BC = new TGraph(vec_x_layer3.size(), x_layer3, y_BC_layer3);


	float *y_tau_layer1=vec_tau_layer1.data();
	TGraph *tg1_tau = new TGraph(vec_x_layer1.size(), x_layer1, y_tau_layer1);
	float *y_tau_layer2=vec_tau_layer2.data();
	TGraph *tg2_tau = new TGraph(vec_x_layer2.size(), x_layer2, y_tau_layer2);
	float *y_tau_layer3=vec_tau_layer3.data();
	TGraph *tg3_tau = new TGraph(vec_x_layer3.size(), x_layer3, y_tau_layer3);

	TFile myhist("myplot.root","RECREATE");
	tp->Write();
	tg->Write();
	tg0->SetName("tg0");
	tg1->SetName("tg1");
	tg2->SetName("tg2");
	tg3->SetName("tg3");
	tg1_BC->SetName("tg1_BC");
	tg2_BC->SetName("tg2_BC");
	tg3_BC->SetName("tg3_BC");
	tg1_tau->SetName("g1_tau");
	tg2_tau->SetName("g2_tau");
	tg3_tau->SetName("g3_tau");
	tg0->Write();
	tg1->Write();
	tg2->Write();
	tg3->Write();
	tg1_BC->Write();
	tg2_BC->Write();
	tg3_BC->Write();
	tg1_tau->Write();
	tg2_tau->Write();
	tg3_tau->Write();
	th_BC->Write();
	th_tau->Write();


}

void compare_main_swrod(map<int,map<string,vector<float>>> &map_swrod_ET, map<int,map<string,vector<float>>> &map_main_ET, map<int,vector<int>> &map_scid_larid, map<int,float> &map_scid_eta, map<int,int> &map_scid_layer, string stamp, map<int,vector<int>> &map_scid_det_ac) {

	TH2F *tp = new TH2F("Et_main_swrod", "Main vs SWrod", 200, 0, 50, 100, 0, 50);
	vector<float> vec_x;
	vector<float> vec_y;
	vector<float> vec_x_layer0;
	vector<float> vec_y_layer0;
	vector<float> vec_x_layer1;
	vector<float> vec_y_layer1;
	vector<float> vec_x_layer2;
	vector<float> vec_y_layer2;
	vector<float> vec_x_layer3;
	vector<float> vec_y_layer3;
	TH1F *th_BC = new TH1F("th_BC","th_BC",32,-0.5,31.5);
	TH1F *th_tau = new TH1F("th_tau","th_tau",100,-50,50);
	vector<float> vec_BC_layer1;
	vector<float> vec_BC_layer2;
	vector<float> vec_BC_layer3;
	vector<float> vec_tau_layer1;
	vector<float> vec_tau_layer2;
	vector<float> vec_tau_layer3;


	int count_matched=0;

	for (auto it: map_swrod_ET) {//scid
		if (map_scid_larid.find(it.first)==map_scid_larid.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the map!"<<endl;
			continue;
		}

		if (map_scid_det_ac.at(it.first).at(1)!=1) continue;

		if (map_scid_eta.find(it.first)==map_scid_eta.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the eta map!"<<endl;
			continue;
		}
		if (map_scid_layer.find(it.first)==map_scid_layer.end()) {
			cout<<"Warning: cannot find SCID "<<it.first<<" in the layer map!"<<endl;
			continue;
		}

		//float e2et = TMath::Sin(2*TMath::ATan(exp(-map_scid_eta.at(it.first))));

		bool indicator=false;
		for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
			if (map_main_ET.find(map_scid_larid.at(it.first).at(i))==map_main_ET.end()) {
				//cout<<"Warning: only some of the cells in a SC are found in main readout"<<endl;
				//cout<<"SCID = "<<it.first<<", LArID = "<<map_scid_larid.at(it.first).at(i)<<endl;
				indicator=true;
			}	
		}	
		if (indicator) {
			continue;
		}

		for (auto itr: it.second) {//event
			if (itr.first!=stamp) continue;
			float ET_main=0;
			float ET_swrod=itr.second.at(0);
			float tau_swrod=itr.second.at(1);
			float BC_swrod=itr.second.at(2);
			th_BC->Fill(BC_swrod);
			th_tau->Fill(tau_swrod);
			bool indicator2=true;
			for (unsigned int i=0; i<map_scid_larid.at(it.first).size(); i++) {
				if (map_main_ET.at(map_scid_larid.at(it.first).at(i)).find(itr.first)!=map_main_ET.at(map_scid_larid.at(it.first).at(i)).end()) {
					ET_main+=map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first).at(0);
					//if (itr.first=="12&91469&1" && it.first==939524096) cout<<"LArID "<<map_scid_larid.at(it.first).at(i)<<", ET = "<<map_main_ET.at(map_scid_larid.at(it.first).at(i)).at(itr.first)<<endl;
				} else indicator2=false;
			}	
			if (ET_main>0 && indicator2) {
				tp->Fill(ET_main/1000,ET_swrod/1000);
				vec_x.push_back(ET_main);
				vec_y.push_back(ET_swrod);
				if (map_scid_layer.at(it.first)==0) {
					vec_x_layer0.push_back(map_scid_eta.at(it.first));
					vec_y_layer0.push_back((float)ET_swrod/ET_main);
				}
				if (map_scid_layer.at(it.first)==1) {
					vec_x_layer1.push_back(map_scid_eta.at(it.first));
					vec_y_layer1.push_back((float)ET_swrod/ET_main);
					vec_BC_layer1.push_back(BC_swrod-0.1);
					vec_tau_layer1.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==2) {
					vec_x_layer2.push_back(map_scid_eta.at(it.first));
					vec_y_layer2.push_back((float)ET_swrod/ET_main);
					vec_BC_layer2.push_back(BC_swrod);
					vec_tau_layer2.push_back(tau_swrod);
				}
				if (map_scid_layer.at(it.first)==3) {
					vec_x_layer3.push_back(map_scid_eta.at(it.first));
					vec_y_layer3.push_back((float)ET_swrod/ET_main);
					vec_BC_layer3.push_back(BC_swrod+0.1);
					vec_tau_layer3.push_back(tau_swrod);
				}

				cout<<"At SCID "<<it.first<<", event "<<itr.first<<", ET swrod = "<<ET_swrod<<", ET main = "<<ET_main<<endl;
			}
		}
	}
	float *x=vec_x.data();
	float *y=vec_y.data();
	TGraph *tg = new TGraph(vec_x.size(), x, y);

	float *x_layer0=vec_x_layer0.data();
	float *y_layer0=vec_y_layer0.data();
	TGraph *tg0 = new TGraph(vec_x_layer0.size(), x_layer0, y_layer0);
	float *x_layer1=vec_x_layer1.data();
	float *y_layer1=vec_y_layer1.data();
	TGraph *tg1 = new TGraph(vec_x_layer1.size(), x_layer1, y_layer1);
	float *x_layer2=vec_x_layer2.data();
	float *y_layer2=vec_y_layer2.data();
	TGraph *tg2 = new TGraph(vec_x_layer2.size(), x_layer2, y_layer2);
	float *x_layer3=vec_x_layer3.data();
	float *y_layer3=vec_y_layer3.data();
	TGraph *tg3 = new TGraph(vec_x_layer3.size(), x_layer3, y_layer3);

	float *y_BC_layer1=vec_BC_layer1.data();
	TGraph *tg1_BC = new TGraph(vec_x_layer1.size(), x_layer1, y_BC_layer1);
	float *y_BC_layer2=vec_BC_layer2.data();
	TGraph *tg2_BC = new TGraph(vec_x_layer2.size(), x_layer2, y_BC_layer2);
	float *y_BC_layer3=vec_BC_layer3.data();
	TGraph *tg3_BC = new TGraph(vec_x_layer3.size(), x_layer3, y_BC_layer3);


	float *y_tau_layer1=vec_tau_layer1.data();
	TGraph *tg1_tau = new TGraph(vec_x_layer1.size(), x_layer1, y_tau_layer1);
	float *y_tau_layer2=vec_tau_layer2.data();
	TGraph *tg2_tau = new TGraph(vec_x_layer2.size(), x_layer2, y_tau_layer2);
	float *y_tau_layer3=vec_tau_layer3.data();
	TGraph *tg3_tau = new TGraph(vec_x_layer3.size(), x_layer3, y_tau_layer3);

	TFile myhist("myplot.root","RECREATE");
	tp->Write();
	tg->Write();
	tg0->SetName("tg0");
	tg1->SetName("tg1");
	tg2->SetName("tg2");
	tg3->SetName("tg3");
	tg1_BC->SetName("tg1_BC");
	tg2_BC->SetName("tg2_BC");
	tg3_BC->SetName("tg3_BC");
	tg1_tau->SetName("g1_tau");
	tg2_tau->SetName("g2_tau");
	tg3_tau->SetName("g3_tau");
	tg0->Write();
	tg1->Write();
	tg2->Write();
	tg3->Write();
	tg1_BC->Write();
	tg2_BC->Write();
	tg3_BC->Write();
	tg1_tau->Write();
	tg2_tau->Write();
	tg3_tau->Write();
	th_BC->Write();
	th_tau->Write();


}


void drawSwrod_ET_Tau(string filename, map<int,float> &map_scid_ped, map<int,float> &map_scid_ofca0, map<int,float> &map_scid_ofca1, map<int,float> &map_scid_ofca2, map<int,float> &map_scid_ofca3, map<int,float> &map_scid_lsb, map<int,float> &map_scid_ofcb0, map<int,float> &map_scid_ofcb1, map<int,float> &map_scid_ofcb2, map<int,float> &map_scid_ofcb3, string stamp, int scid) {

	cout<<"Processing drawSwrod_ET_Tau"<<endl;

	float x[32];
	float y0[32];
	float y1[32];
	float y2[32];

	TFile file((filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	UInt_t l1idLATOMEHEAD;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];
	Int_t detector;
	ULong64_t IEvent;
	Short_t BCID;



	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);
	tree->SetBranchAddress("detector", &detector);
	tree->SetBranchAddress("IEvent", &IEvent);
	tree->SetBranchAddress("BCID", &BCID);

	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {
		tree->GetEntry(i);
		float e2et = TMath::Sin(2*TMath::ATan(exp(-eta)));
		if (map_scid_lsb.find(channelId)==map_scid_lsb.end() || map_scid_ped.find(channelId)==map_scid_ped.end()) continue;
		if (map_scid_ped.at(channelId)==0 || map_scid_lsb.at(channelId)<=0) continue;
		if (detector!=0 && detector!=1) continue;
		//int IEvent=(l1idLATOMEHEAD&0xFFFFFF);
		int l1id=l1idLATOMEHEAD&0xFFFFFF;
		string key=myint_to_string(l1id,IEvent,BCID);
		if (key!=stamp) continue;
		if (channelId!=scid) continue;
		float a[4];
		a[0]=map_scid_ofca0.at(channelId);
		a[1]=map_scid_ofca1.at(channelId);
		a[2]=map_scid_ofca2.at(channelId);
		a[3]=map_scid_ofca3.at(channelId);
		float b[4];
		b[0]=map_scid_ofcb0.at(channelId);
		b[1]=map_scid_ofcb1.at(channelId);
		b[2]=map_scid_ofcb2.at(channelId);
		b[3]=map_scid_ofcb3.at(channelId);

		for (int k=0; k<32; k++) {
			x[k]=k;
			y0[k]=samples[k];
			y1[k]=0;
			y2[k]=0;
			for (int j=0; j<4; j++) {
				//cout<<"Samples-Ped = "<<samples[j]-map_scid_ped.at(channelId)<<endl;
				//cout<<"OFCa = "<<a[j]<<endl;
				//cout<<"LSB = "<<map_scid_lsb.at(channelId)<<endl;
				float samp=0.;
				if (k>28) samp=0.; else samp=samples[j+k]-map_scid_ped.at(channelId);
				y1[k]+=e2et*(samp)*a[j];
				y2[k]+=e2et*(samp)*b[j];
			}
			if (y1[k]==0) y2[k]=0; else y2[k]=y2[k]/y1[k];
			//cout<<"ET = "<<map_swrod_ET[channelId][key]<<endl;
		}
	}

	TGraph *tg1 = new TGraph(32, x, y0);
	TGraph *tg2 = new TGraph(32, x, y1);
	TGraph *tg3 = new TGraph(32, x, y2);

	tg1->SetName("tg1");
	tg2->SetName("tg2");
	tg3->SetName("tg3");

	TFile myhist("et_tau.root","RECREATE");
	tg1->Write();
	tg2->Write();
	tg3->Write();

	myhist.Close();
}

void readPulseAllTree(string pulseAllTree_filename, map<int,pair<float,float>> &map_scid_avg_stdev, map<int,vector<int>> &map_scid_larid) {

	map<int, vector<short>> map_scid_height;

	TFile file((pulseAllTree_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("LARDIGITS");
	Int_t channelId;
	Float_t eta;
	Float_t phi;
	Short_t samples[32];

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("eta", &eta);
	tree->SetBranchAddress("phi", &phi);
	tree->SetBranchAddress("samples", samples);

	cout<<"Reading digit tree..."<<endl;
	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {	
		tree->GetEntry(i);
		if (map_scid_larid.find(channelId)==map_scid_larid.end()) continue;
		short height=0;
		for (int j=0; j<32; j++) {
			if (samples[j]-samples[0]>height) height=samples[j]-samples[0];
		}
		map_scid_height[channelId].push_back(height);
	}

	cout<<"Processing avg, stdev for digit tree..."<<endl;
	for (auto it: map_scid_height) {
		float avg=0.;
		float stdev=0.;
		if (it.second.size()==0) continue;
		for (unsigned int i=0; i<it.second.size(); i++) {
			avg+=it.second.at(i);
		}
		avg=avg/it.second.size();

		for (unsigned int i=0; i<it.second.size(); i++) {
			stdev+=pow(it.second.at(i)-avg,2);
		}
		
		stdev=sqrt(stdev/it.second.size());
		map_scid_avg_stdev[it.first]=make_pair(avg,stdev);
		//cout<<"At channelId "<<it.first<<", avg = "<<avg<<", stdev = "<<stdev<<endl;
	}

}

void readCaliWaveTree(string CaliWaveTree_filename, map<int,pair<float,float>> &map_scid_height, map<int,vector<int>> &map_scid_larid) {

	TFile file((CaliWaveTree_filename).c_str(),"read");
	TTree *tree = (TTree*)file.Get("CALIWAVE");
	Int_t channelId;
	Int_t DAC;
	Double_t MaxAmp;

	tree->SetBranchAddress("channelId", &channelId);
	tree->SetBranchAddress("DAC", &DAC);
	tree->SetBranchAddress("MaxAmp", &MaxAmp);

	cout<<"Reading CaliWave tree..."<<endl;
	int entries = tree->GetEntries();
	for (unsigned int i=0; i<entries; i++) {	
		tree->GetEntry(i);
		if (map_scid_larid.find(channelId)==map_scid_larid.end()) continue;
		map_scid_height[channelId]=make_pair(MaxAmp,DAC);
		//cout<<"At channelId "<<channelId<<", height/DAC = "<<MaxAmp/DAC<<endl;
	}


}

void validate_DACOverADC(map<int,pair<float,float>> &map_scid_avg_stdev, map<int,pair<float,float>> &map_scid_height, map<int,vector<int>> &map_scid_larid) {

	cout<<"SCID, No. CLs, Digit ADC-ped, CaliWave peak, ratio, 800*(#cell*calibline), DAC from CaliWave, ratio"<<endl;
	for (auto it: map_scid_height) {
		if (map_scid_avg_stdev.find(it.first)==map_scid_avg_stdev.end()) {
			cout<<"channelId "<<it.first<<" not found in digit tree"<<endl;
			continue;
		}
		float height=map_scid_avg_stdev.at(it.first).first;
		float DACOverheight=(map_scid_larid.at(it.first).size()*800)/height;
		cout<<it.first<<", "<<map_scid_larid.at(it.first).size()<<", "<<height<<", "<<it.second.first<<", "<<height/it.second.first<<", "<<map_scid_larid.at(it.first).size()*800<<", "<<it.second.second<<", "<<map_scid_larid.at(it.first).size()*800/it.second.second<<endl;
	}

}

pair<float,float> ETpolyFit(vector<float> &vec_ET, int maxET_phase) {

	if (maxET_phase>=48 || maxET_phase<=1) return make_pair(maxET_phase, vec_ET.at(maxET_phase));
	float x[5], y[5];
	for (int i=0; i<5; i++) {
		x[i]=i;
		y[i]=vec_ET.at(i+maxET_phase-2);
	}
	TGraph *tg = new TGraph(5, x, y);
	tg->Fit("pol2","Q");
	TF1 *fit = tg->GetFunction("pol2");
	float fittedMax=0.;
	float fittedMaxPhase=0.;
	fittedMax=fit->GetMaximum(0,4,1.E-10,100,false);
	fittedMaxPhase=fit->GetMaximumX(0,4,1.E-10,100,false)+maxET_phase-2;
	return make_pair(fittedMaxPhase, fittedMax);
}

pair<float,float> ETpolyFit2(vector<float> &vec_ET, int maxET_phase) {

	float x[50], y[50];
	for (int i=0; i<50; i++) {
		x[i]=i;
		y[i]=vec_ET.at(i);
	}
	TGraph *tg = new TGraph(5, x, y);
	tg->Fit("pol2","Q");
	TF1 *fit = tg->GetFunction("pol2");
	float fittedMax=0.;
	float fittedMaxPhase=0.;
	fittedMax=fit->GetMaximum(0,49,1.E-10,100,false);
	fittedMaxPhase=fit->GetMaximumX(0,49,1.E-10,100,false);
	return make_pair(fittedMaxPhase, fittedMax);
}

void resampling(map<int,map<string,float>> &map_swrod_fittedPhase, 
	map<int,map<string,float>> &map_swrod_fittedET, 
	map<int,map<string,short[32]>> &map_swrod_samples,
	map<int,float> &map_scid_eta, map<int,vector<int>> &map_scid_det_ac, map<int,int> &map_scid_layer,
	string event, float eta, int detector, int layer) {

	cout<<"Processing resampling\n";
	TFile *output = new TFile("resampling.root","recreate");
	TProfile *tg1 = new TProfile("shape", "resampled shape" ,80 ,-0.5 ,31.5, -1, 1);
	for (auto it: map_swrod_samples) {
		if (map_scid_eta.find(it.first)==map_scid_eta.end()) {
			cout<<"Cannot find channel "<<it.first<<" in LArIDtranslator!\n";
			continue;
		}
		if (map_swrod_fittedPhase.find(it.first)==map_swrod_fittedPhase.end()) continue;
		if (eta>0 && !(map_scid_eta.at(it.first)>0.99*eta && map_scid_eta.at(it.first)<1.01*eta)) continue;
		if (eta<=0 && !(map_scid_eta.at(it.first)<0.99*eta && map_scid_eta.at(it.first)>1.01*eta)) continue;
		if (map_scid_det_ac.at(it.first).at(0)!=detector) continue;
		if (map_scid_layer.at(it.first)!=layer) continue;
		for (auto itr: it.second) {
			if (itr.first!=event) continue;
			if (map_swrod_fittedPhase.at(it.first).find(itr.first)==map_swrod_fittedPhase.at(it.first).end()) continue;
			for (int i=0; i<32; i++) {
				tg1->Fill(i+map_swrod_fittedPhase.at(it.first).at(itr.first),itr.second[i]/map_swrod_fittedET.at(it.first).at(itr.first));
			}
			
		}
	}

	tg1->Write();

	output->Close();

}

void saturation_resampling(map<int,map<string,float>> &map_swrod_fittedPhase, 
	map<int,map<string,float>> &map_swrod_fittedET, 
	map<int,map<string,float>> &map_swrod_Tau,
	map<int,map<string,short[32]>> &map_swrod_samples,
	map<int,float> &map_scid_eta, map<int,float> &map_scid_phi, map<int,vector<int>> &map_scid_det_ac, map<int,int> &map_scid_layer,
	string event, float eta, int detector, int layer) {

	cout<<"Processing saturation_resampling\n";
	TFile *output = new TFile("saturation_resampling.root","recreate");
	TProfile *tg1 = new TProfile("shape", "resampled shape" ,80 ,-0.5 ,31.5, -1, 1);
	TH2F *th2 = new TH2F("sat", "Saturated SCs", 120, -3, 3, 120, -3, 3);
	for (auto it: map_swrod_samples) {
		if (map_scid_eta.find(it.first)==map_scid_eta.end()) {
			cout<<"Cannot find channel "<<it.first<<" in LArIDtranslator!\n";
			continue;
		}
		if (map_swrod_fittedPhase.find(it.first)==map_swrod_fittedPhase.end()) continue;
		if (eta>0 && !(map_scid_eta.at(it.first)>0.99*eta && map_scid_eta.at(it.first)<1.01*eta)) continue;
		if (eta<=0 && !(map_scid_eta.at(it.first)<0.99*eta && map_scid_eta.at(it.first)>1.01*eta)) continue;
		if (map_scid_det_ac.at(it.first).at(0)!=detector) continue;
		if (map_scid_layer.at(it.first)!=layer) continue;
		string detector_str="UNKNOWN";
		if (map_scid_det_ac.at(it.first).at(0)==0 && map_scid_det_ac.at(it.first).at(1)==1) detector_str="EMBA";
		if (map_scid_det_ac.at(it.first).at(0)==0 && map_scid_det_ac.at(it.first).at(1)==-1) detector_str="EMBC";
		if (map_scid_det_ac.at(it.first).at(0)==1 && map_scid_det_ac.at(it.first).at(1)==1) detector_str="EMECA";
		if (map_scid_det_ac.at(it.first).at(0)==1 && map_scid_det_ac.at(it.first).at(1)==-1) detector_str="EMECC";
		for (auto itr: it.second) {
			if (itr.first!=event) continue;
			if (map_swrod_fittedPhase.at(it.first).find(itr.first)==map_swrod_fittedPhase.at(it.first).end()) continue;
			if (map_swrod_fittedET.at(it.first).at(itr.first)>565816 
			&& map_swrod_fittedET.at(it.first).at(itr.first)<848724 
			&& map_swrod_Tau.at(it.first).at(itr.first)<5
			&& map_swrod_Tau.at(it.first).at(itr.first)>-20) {
				cout<<"Found saturated pulse at SCID "<<it.first<<", event "<<itr.first<<", "<<detector_str<<", layer "<<map_scid_layer.at(it.first)<<"\n";
				th2->Fill(map_scid_eta.at(it.first),map_scid_phi.at(it.first));
				for (int i=0; i<32; i++) {
					tg1->Fill(i+map_swrod_fittedPhase.at(it.first).at(itr.first),itr.second[i]/map_swrod_fittedET.at(it.first).at(itr.first));
				}
			}
			
		}
	}

	tg1->Write();
	th2->Write();
	output->Close();

}


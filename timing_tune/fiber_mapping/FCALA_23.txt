# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: FCAL2
# Side: A
# QuadrantID: 0
# AMCHashNumber: 0
# CarrierID: 4
# AMCNumberInCarrier: 4

# Input information section
# Number connected LTDBs: 1
# Connected LTDBs: FCAL2(A04F1)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   A04F1_01_10 RX01_03  4J_1_F3  3J_1_F3  1J_1_F3  2J_1_F3  2B_1_F3  1B_1_F3  4B_1_F3  3B_1_F3  
IN F4   A04F1_01_09 RX01_04  4M_1_F2  4M_2_F2  GND      GND      GND      GND      3M_2_F2  3M_1_F2  
IN F5   A04F1_01_08 RX01_05  GND      GND      1M_1_F2  1M_2_F2  2M_1_F2  2M_2_F2  GND      GND      
IN F6   A04F1_01_07 RX01_06  1E_1_F2  1E_2_F2  GND      GND      2E_1_F2  2E_2_F2  GND      GND      
IN F7   A04F1_01_06 RX01_07  4B_2_F2  4B_1_F2  GND      GND      GND      GND      3B_1_F2  3B_2_F2  
IN F8   A04F1_01_05 RX01_08  4E_1_F3  3E_1_F3  1E_1_F3  2E_1_F3  4G_1_F3  3G_1_F3  2G_1_F3  1G_1_F3  
IN F9   A04F1_01_04 RX01_09  GND      GND      2B_2_F2  2B_1_F2  1B_1_F2  1B_2_F2  GND      GND      
IN F10  A04F1_01_03 RX01_10  2A_1_F2  2A_2_F2  GND      GND      1A_1_F2  1A_2_F2  GND      GND      
IN F11  A04F1_01_02 RX01_11  GND      GND      4E_1_F2  4E_2_F2  3E_2_F2  3E_1_F2  GND      GND      
IN F12  A04F1_01_01 RX01_12  4A_1_F2  4A_2_F2  GND      GND      GND      GND      3A_2_F2  3A_1_F2  
IN F15  A04F1_02_10 RX02_03  1N_1_F3  2N_1_F3  3N_1_F3  4N_1_F3  1H_1_F3  2H_1_F3  4H_1_F3  3H_1_F3  
IN F16  A04F1_02_09 RX02_04  GND      GND      4N_2_F2  4N_1_F2  3N_2_F2  3N_1_F2  GND      GND      
IN F17  A04F1_02_08 RX02_05  3P_2_F2  3P_1_F2  GND      GND      4P_2_F2  4P_1_F2  GND      GND      
IN F18  A04F1_02_07 RX02_06  3F_1_F3  4F_1_F3  1F_1_F3  2F_1_F3  3D_1_F3  4D_1_F3  1D_1_F3  2D_1_F3  
IN F19  A04F1_02_06 RX02_07  GND      GND      4I_1_F2  4I_2_F2  3I_2_F2  3I_1_F2  GND      GND      
IN F20  A04F1_02_05 RX02_08  GND      GND      2F_2_F2  2F_1_F2  GND      GND      1F_1_F2  1F_2_F2  
IN F21  A04F1_02_04 RX02_09  2I_2_F2  2I_1_F2  GND      GND      GND      GND      1I_1_F2  1I_2_F2  
IN F22  A04F1_02_03 RX02_10  GND      GND      4J_2_F2  4J_1_F2  GND      GND      3J_2_F2  3J_1_F2  
IN F23  A04F1_02_02 RX02_11  4F_2_F2  4F_1_F2  GND      GND      GND      GND      3F_2_F2  3F_1_F2  
IN F24  A04F1_02_01 RX02_12  GND      GND      1J_1_F2  1J_2_F2  2J_1_F2  2J_2_F2  GND      GND      
IN F27  A04F1_03_10 RX03_03  GND      GND      3L_2_F2  3L_1_F2  4L_1_F2  4L_2_F2  GND      GND      
IN F28  A04F1_03_09 RX03_04  4L_1_F3  3L_1_F3  1L_1_F3  2L_1_F3  1P_1_F3  2P_1_F3  4P_1_F3  3P_1_F3  
IN F29  A04F1_03_08 RX03_05  GND      GND      2L_1_F2  2L_2_F2  1L_2_F2  1L_1_F2  GND      GND      
IN F30  A04F1_03_07 RX03_06  GND      GND      1H_2_F2  1H_1_F2  2H_1_F2  2H_2_F2  GND      GND      
IN F31  A04F1_03_06 RX03_07  4H_1_F2  4H_2_F2  GND      GND      GND      GND      3H_2_F2  3H_1_F2  
IN F32  A04F1_03_05 RX03_08  3K_2_F2  3K_1_F2  GND      GND      GND      GND      4K_1_F2  4K_2_F2  
IN F33  A04F1_03_04 RX03_09  GND      GND      2K_1_F2  2K_2_F2  GND      GND      1K_1_F2  1K_2_F2  
IN F34  A04F1_03_03 RX03_10  4C_1_F3  3C_1_F3  2C_1_F3  1C_1_F3  4I_1_F3  3I_1_F3  2I_1_F3  1I_1_F3  
IN F35  A04F1_03_02 RX03_11  GND      GND      2P_1_F2  2P_2_F2  1P_1_F2  1P_2_F2  GND      GND      
IN F36  A04F1_03_01 RX03_12  GND      GND      2N_1_F2  2N_2_F2  1N_1_F2  1N_2_F2  GND      GND      
IN F39  A04F1_04_10 RX04_03  GND      GND      2C_1_F2  2C_2_F2  1C_1_F2  1C_2_F2  GND      GND      
IN F40  A04F1_04_09 RX04_04  3D_1_F2  3D_2_F2  GND      GND      GND      GND      4D_2_F2  4D_1_F2  
IN F41  A04F1_04_08 RX04_05  4C_2_F2  4C_1_F2  GND      GND      GND      GND      3C_2_F2  3C_1_F2  
IN F42  A04F1_04_07 RX04_06  1D_2_F2  1D_1_F2  GND      GND      GND      GND      2D_1_F2  2D_2_F2  
IN F43  A04F1_04_06 RX04_07  GND      GND      3G_1_F2  3G_2_F2  GND      GND      4G_2_F2  4G_1_F2  
IN F44  A04F1_04_05 RX04_08  4O_2_F2  4O_1_F2  GND      GND      GND      GND      3O_2_F2  3O_1_F2  
IN F45  A04F1_04_04 RX04_09  1G_2_F2  1G_1_F2  GND      GND      GND      GND      2G_1_F2  2G_2_F2  
IN F46  A04F1_04_03 RX04_10  3K_1_F3  4K_1_F3  2K_1_F3  1K_1_F3  2O_1_F3  1O_1_F3  3O_1_F3  4O_1_F3  
IN F47  A04F1_04_02 RX04_11  GND      GND      2O_2_F2  2O_1_F2  1O_1_F2  1O_2_F2  GND      GND      
IN F48  A04F1_04_01 RX04_12  2A_1_F3  1A_1_F3  3A_1_F3  4A_1_F3  4M_1_F3  3M_1_F3  2M_1_F3  1M_1_F3  

# Output information section
# Number connected output fibers: 40 (unique=36)
# Number eFEX fibers: 16 (unique=16)
# Number jFEX fibers: 16 (unique=16)
# Number gFEX fibers: 8 (unique=4)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   1A_1_F2       1A_1_F3       1A_2_F2       2A_1_F2       2A_1_F3       2A_2_F2       3A_1_F2       3A_1_F3       3A_2_F2       4A_1_F2       4A_1_F3       4A_2_F2       
OUT F2   eFEX   1B_1_F2       1B_1_F3       1B_2_F2       2B_1_F2       2B_1_F3       2B_2_F2       3B_1_F2       3B_1_F3       3B_2_F2       4B_1_F2       4B_1_F3       4B_2_F2       
OUT F3   eFEX   1C_1_F2       1C_1_F3       1C_2_F2       2C_1_F2       2C_1_F3       2C_2_F2       3C_1_F2       3C_1_F3       3C_2_F2       4C_1_F2       4C_1_F3       4C_2_F2       
OUT F4   eFEX   1D_1_F2       1D_1_F3       1D_2_F2       2D_1_F2       2D_1_F3       2D_2_F2       3D_1_F2       3D_1_F3       3D_2_F2       4D_1_F2       4D_1_F3       4D_2_F2       
OUT F5   eFEX   1E_1_F2       1E_1_F3       1E_2_F2       2E_1_F2       2E_1_F3       2E_2_F2       3E_1_F2       3E_1_F3       3E_2_F2       4E_1_F2       4E_1_F3       4E_2_F2       
OUT F6   eFEX   1F_1_F2       1F_1_F3       1F_2_F2       2F_1_F2       2F_1_F3       2F_2_F2       3F_1_F2       3F_1_F3       3F_2_F2       4F_1_F2       4F_1_F3       4F_2_F2       
OUT F7   eFEX   1G_1_F2       1G_1_F3       1G_2_F2       2G_1_F2       2G_1_F3       2G_2_F2       3G_1_F2       3G_1_F3       3G_2_F2       4G_1_F2       4G_1_F3       4G_2_F2       
OUT F8   eFEX   1H_1_F2       1H_1_F3       1H_2_F2       2H_1_F2       2H_1_F3       2H_2_F2       3H_1_F2       3H_1_F3       3H_2_F2       4H_1_F2       4H_1_F3       4H_2_F2       
OUT F10  eFEX   1I_1_F2       1I_1_F3       1I_2_F2       2I_1_F2       2I_1_F3       2I_2_F2       3I_1_F2       3I_1_F3       3I_2_F2       4I_1_F2       4I_1_F3       4I_2_F2       
OUT F11  eFEX   1J_1_F2       1J_1_F3       1J_2_F2       2J_1_F2       2J_1_F3       2J_2_F2       3J_1_F2       3J_1_F3       3J_2_F2       4J_1_F2       4J_1_F3       4J_2_F2       
OUT F12  eFEX   1K_1_F2       1K_1_F3       1K_2_F2       2K_1_F2       2K_1_F3       2K_2_F2       3K_1_F2       3K_1_F3       3K_2_F2       4K_1_F2       4K_1_F3       4K_2_F2       
OUT F13  eFEX   1L_1_F2       1L_1_F3       1L_2_F2       2L_1_F2       2L_1_F3       2L_2_F2       3L_1_F2       3L_1_F3       3L_2_F2       4L_1_F2       4L_1_F3       4L_2_F2       
OUT F14  eFEX   1M_1_F2       1M_1_F3       1M_2_F2       2M_1_F2       2M_1_F3       2M_2_F2       3M_1_F2       3M_1_F3       3M_2_F2       4M_1_F2       4M_1_F3       4M_2_F2       
OUT F15  eFEX   1N_1_F2       1N_1_F3       1N_2_F2       2N_1_F2       2N_1_F3       2N_2_F2       3N_1_F2       3N_1_F3       3N_2_F2       4N_1_F2       4N_1_F3       4N_2_F2       
OUT F16  eFEX   1O_1_F2       1O_1_F3       1O_2_F2       2O_1_F2       2O_1_F3       2O_2_F2       3O_1_F2       3O_1_F3       3O_2_F2       4O_1_F2       4O_1_F3       4O_2_F2       
OUT F17  eFEX   1P_1_F2       1P_1_F3       1P_2_F2       2P_1_F2       2P_1_F3       2P_2_F2       3P_1_F2       3P_1_F3       3P_2_F2       4P_1_F2       4P_1_F3       4P_2_F2       
OUT F19  jFEX   1A_1_F2       1A_1_F3       1A_2_F2       2A_1_F2       2A_1_F3       2A_2_F2       3A_1_F2       3A_1_F3       3A_2_F2       4A_1_F2       4A_1_F3       4A_2_F2       
OUT F20  jFEX   1B_1_F2       1B_1_F3       1B_2_F2       2B_1_F2       2B_1_F3       2B_2_F2       3B_1_F2       3B_1_F3       3B_2_F2       4B_1_F2       4B_1_F3       4B_2_F2       
OUT F21  jFEX   1C_1_F2       1C_1_F3       1C_2_F2       2C_1_F2       2C_1_F3       2C_2_F2       3C_1_F2       3C_1_F3       3C_2_F2       4C_1_F2       4C_1_F3       4C_2_F2       
OUT F22  jFEX   1D_1_F2       1D_1_F3       1D_2_F2       2D_1_F2       2D_1_F3       2D_2_F2       3D_1_F2       3D_1_F3       3D_2_F2       4D_1_F2       4D_1_F3       4D_2_F2       
OUT F23  jFEX   1E_1_F2       1E_1_F3       1E_2_F2       2E_1_F2       2E_1_F3       2E_2_F2       3E_1_F2       3E_1_F3       3E_2_F2       4E_1_F2       4E_1_F3       4E_2_F2       
OUT F24  jFEX   1F_1_F2       1F_1_F3       1F_2_F2       2F_1_F2       2F_1_F3       2F_2_F2       3F_1_F2       3F_1_F3       3F_2_F2       4F_1_F2       4F_1_F3       4F_2_F2       
OUT F25  jFEX   1G_1_F2       1G_1_F3       1G_2_F2       2G_1_F2       2G_1_F3       2G_2_F2       3G_1_F2       3G_1_F3       3G_2_F2       4G_1_F2       4G_1_F3       4G_2_F2       
OUT F26  jFEX   1H_1_F2       1H_1_F3       1H_2_F2       2H_1_F2       2H_1_F3       2H_2_F2       3H_1_F2       3H_1_F3       3H_2_F2       4H_1_F2       4H_1_F3       4H_2_F2       
OUT F28  jFEX   1I_1_F2       1I_1_F3       1I_2_F2       2I_1_F2       2I_1_F3       2I_2_F2       3I_1_F2       3I_1_F3       3I_2_F2       4I_1_F2       4I_1_F3       4I_2_F2       
OUT F29  jFEX   1J_1_F2       1J_1_F3       1J_2_F2       2J_1_F2       2J_1_F3       2J_2_F2       3J_1_F2       3J_1_F3       3J_2_F2       4J_1_F2       4J_1_F3       4J_2_F2       
OUT F30  jFEX   1K_1_F2       1K_1_F3       1K_2_F2       2K_1_F2       2K_1_F3       2K_2_F2       3K_1_F2       3K_1_F3       3K_2_F2       4K_1_F2       4K_1_F3       4K_2_F2       
OUT F31  jFEX   1L_1_F2       1L_1_F3       1L_2_F2       2L_1_F2       2L_1_F3       2L_2_F2       3L_1_F2       3L_1_F3       3L_2_F2       4L_1_F2       4L_1_F3       4L_2_F2       
OUT F32  jFEX   1M_1_F2       1M_1_F3       1M_2_F2       2M_1_F2       2M_1_F3       2M_2_F2       3M_1_F2       3M_1_F3       3M_2_F2       4M_1_F2       4M_1_F3       4M_2_F2       
OUT F33  jFEX   1N_1_F2       1N_1_F3       1N_2_F2       2N_1_F2       2N_1_F3       2N_2_F2       3N_1_F2       3N_1_F3       3N_2_F2       4N_1_F2       4N_1_F3       4N_2_F2       
OUT F34  jFEX   1O_1_F2       1O_1_F3       1O_2_F2       2O_1_F2       2O_1_F3       2O_2_F2       3O_1_F2       3O_1_F3       3O_2_F2       4O_1_F2       4O_1_F3       4O_2_F2       
OUT F35  jFEX   1P_1_F2       1P_1_F3       1P_2_F2       2P_1_F2       2P_1_F3       2P_2_F2       3P_1_F2       3P_1_F3       3P_2_F2       4P_1_F2       4P_1_F3       4P_2_F2       
OUT F37  gFEX   1A_1_F2+1A_2_F2+1A_1_F3  1B_1_F2+1B_2_F2+1B_1_F3  1C_1_F2+1C_2_F2+1C_1_F3  1D_1_F2+1D_2_F2+1D_1_F3  2A_1_F2+2A_2_F2+2A_1_F3  2B_1_F2+2B_2_F2+2B_1_F3  2C_1_F2+2C_2_F2+2C_1_F3  2D_1_F2+2D_2_F2+2D_1_F3  3A_1_F2+3A_2_F2+3A_1_F3  3B_1_F2+3B_2_F2+3B_1_F3  3C_1_F2+3C_2_F2+3C_1_F3  3D_1_F2+3D_2_F2+3D_1_F3  4A_1_F2+4A_2_F2+4A_1_F3  4B_1_F2+4B_2_F2+4B_1_F3  4C_1_F2+4C_2_F2+4C_1_F3  4D_1_F2+4D_2_F2+4D_1_F3  
OUT F43  gFEX   1A_1_F2+1A_2_F2+1A_1_F3  1B_1_F2+1B_2_F2+1B_1_F3  1C_1_F2+1C_2_F2+1C_1_F3  1D_1_F2+1D_2_F2+1D_1_F3  2A_1_F2+2A_2_F2+2A_1_F3  2B_1_F2+2B_2_F2+2B_1_F3  2C_1_F2+2C_2_F2+2C_1_F3  2D_1_F2+2D_2_F2+2D_1_F3  3A_1_F2+3A_2_F2+3A_1_F3  3B_1_F2+3B_2_F2+3B_1_F3  3C_1_F2+3C_2_F2+3C_1_F3  3D_1_F2+3D_2_F2+3D_1_F3  4A_1_F2+4A_2_F2+4A_1_F3  4B_1_F2+4B_2_F2+4B_1_F3  4C_1_F2+4C_2_F2+4C_1_F3  4D_1_F2+4D_2_F2+4D_1_F3  
OUT F38  gFEX   1E_1_F2+1E_2_F2+1E_1_F3  1F_1_F2+1F_2_F2+1F_1_F3  1G_1_F2+1G_2_F2+1G_1_F3  1H_1_F2+1H_2_F2+1H_1_F3  2E_1_F2+2E_2_F2+2E_1_F3  2F_1_F2+2F_2_F2+2F_1_F3  2G_1_F2+2G_2_F2+2G_1_F3  2H_1_F2+2H_2_F2+2H_1_F3  3E_1_F2+3E_2_F2+3E_1_F3  3F_1_F2+3F_2_F2+3F_1_F3  3G_1_F2+3G_2_F2+3G_1_F3  3H_1_F2+3H_2_F2+3H_1_F3  4E_1_F2+4E_2_F2+4E_1_F3  4F_1_F2+4F_2_F2+4F_1_F3  4G_1_F2+4G_2_F2+4G_1_F3  4H_1_F2+4H_2_F2+4H_1_F3  
OUT F44  gFEX   1E_1_F2+1E_2_F2+1E_1_F3  1F_1_F2+1F_2_F2+1F_1_F3  1G_1_F2+1G_2_F2+1G_1_F3  1H_1_F2+1H_2_F2+1H_1_F3  2E_1_F2+2E_2_F2+2E_1_F3  2F_1_F2+2F_2_F2+2F_1_F3  2G_1_F2+2G_2_F2+2G_1_F3  2H_1_F2+2H_2_F2+2H_1_F3  3E_1_F2+3E_2_F2+3E_1_F3  3F_1_F2+3F_2_F2+3F_1_F3  3G_1_F2+3G_2_F2+3G_1_F3  3H_1_F2+3H_2_F2+3H_1_F3  4E_1_F2+4E_2_F2+4E_1_F3  4F_1_F2+4F_2_F2+4F_1_F3  4G_1_F2+4G_2_F2+4G_1_F3  4H_1_F2+4H_2_F2+4H_1_F3  
OUT F40  gFEX   1I_1_F2+1I_2_F2+1I_1_F3  1J_1_F2+1J_2_F2+1J_1_F3  1K_1_F2+1K_2_F2+1K_1_F3  1L_1_F2+1L_2_F2+1L_1_F3  2I_1_F2+2I_2_F2+2I_1_F3  2J_1_F2+2J_2_F2+2J_1_F3  2K_1_F2+2K_2_F2+2K_1_F3  2L_1_F2+2L_2_F2+2L_1_F3  3I_1_F2+3I_2_F2+3I_1_F3  3J_1_F2+3J_2_F2+3J_1_F3  3K_1_F2+3K_2_F2+3K_1_F3  3L_1_F2+3L_2_F2+3L_1_F3  4I_1_F2+4I_2_F2+4I_1_F3  4J_1_F2+4J_2_F2+4J_1_F3  4K_1_F2+4K_2_F2+4K_1_F3  4L_1_F2+4L_2_F2+4L_1_F3  
OUT F46  gFEX   1I_1_F2+1I_2_F2+1I_1_F3  1J_1_F2+1J_2_F2+1J_1_F3  1K_1_F2+1K_2_F2+1K_1_F3  1L_1_F2+1L_2_F2+1L_1_F3  2I_1_F2+2I_2_F2+2I_1_F3  2J_1_F2+2J_2_F2+2J_1_F3  2K_1_F2+2K_2_F2+2K_1_F3  2L_1_F2+2L_2_F2+2L_1_F3  3I_1_F2+3I_2_F2+3I_1_F3  3J_1_F2+3J_2_F2+3J_1_F3  3K_1_F2+3K_2_F2+3K_1_F3  3L_1_F2+3L_2_F2+3L_1_F3  4I_1_F2+4I_2_F2+4I_1_F3  4J_1_F2+4J_2_F2+4J_1_F3  4K_1_F2+4K_2_F2+4K_1_F3  4L_1_F2+4L_2_F2+4L_1_F3  
OUT F41  gFEX   1M_1_F2+1M_2_F2+1M_1_F3  1N_1_F2+1N_2_F2+1N_1_F3  1O_1_F2+1O_2_F2+1O_1_F3  1P_1_F2+1P_2_F2+1P_1_F3  2M_1_F2+2M_2_F2+2M_1_F3  2N_1_F2+2N_2_F2+2N_1_F3  2O_1_F2+2O_2_F2+2O_1_F3  2P_1_F2+2P_2_F2+2P_1_F3  3M_1_F2+3M_2_F2+3M_1_F3  3N_1_F2+3N_2_F2+3N_1_F3  3O_1_F2+3O_2_F2+3O_1_F3  3P_1_F2+3P_2_F2+3P_1_F3  4M_1_F2+4M_2_F2+4M_1_F3  4N_1_F2+4N_2_F2+4N_1_F3  4O_1_F2+4O_2_F2+4O_1_F3  4P_1_F2+4P_2_F2+4P_1_F3  
OUT F47  gFEX   1M_1_F2+1M_2_F2+1M_1_F3  1N_1_F2+1N_2_F2+1N_1_F3  1O_1_F2+1O_2_F2+1O_1_F3  1P_1_F2+1P_2_F2+1P_1_F3  2M_1_F2+2M_2_F2+2M_1_F3  2N_1_F2+2N_2_F2+2N_1_F3  2O_1_F2+2O_2_F2+2O_1_F3  2P_1_F2+2P_2_F2+2P_1_F3  3M_1_F2+3M_2_F2+3M_1_F3  3N_1_F2+3N_2_F2+3N_1_F3  3O_1_F2+3O_2_F2+3O_1_F3  3P_1_F2+3P_2_F2+3P_1_F3  4M_1_F2+4M_2_F2+4M_1_F3  4N_1_F2+4N_2_F2+4N_1_F3  4O_1_F2+4O_2_F2+4O_1_F3  4P_1_F2+4P_2_F2+4P_1_F3  

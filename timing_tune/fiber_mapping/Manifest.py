# Project description
description = 'Generates mappings for production'

# Simulating the design
action = 'script'

# Top module used for script
top_module = [
    {'../generate_mapping_data.sh':          {'type': 'shell', 'arguments': '. data'}},
]

# List of modules
modules = {
    'local': [
    ],
}

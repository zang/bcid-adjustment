# BCID calibration

This is an offline analysis tool for BCID calibration developed by [Jiaqi](https://gitlab.cern.ch/zang) and [Yuji](https://gitlab.cern.ch/yenari)

Layer: EMB - 0, EMEC OW -1 EMEC IW - 2 HEC - 3 FCAL - 4

Please keep LArId_Info.dat at [P1processing](https://gitlab.cern.ch/AtlasLArCalibration/P1CalibrationProcessing/-/blob/master/run/latome_validation/LArId_Info.dat) up to date.
Also avaiable at lxplus:
```
/afs/cern.ch/work/l/lardaq/public/detlar/athena/P1CalibrationProcessing/run/latome_validation/LArId_Info.dat
```
## Get the BCID offsets:

1. For single latome, please fill in the filename of [***LATOME***], [***latomeNo***], [***inputfilename***] in [BCID_check.sh](https://gitlab.cern.ch/zang/bcid-adjustment/-/blob/master/BCID_check.sh)
  - [***LATOME***] is latome's input fiber info file in [fiber_mapping](https://gitlab.cern.ch/zang/bcid-adjustment/-/tree/master/fiber_mapping), which can be found at
```
/det/lar/project/firmware/LATOME/config_test/[current fw]/LATOME/mapping/production/
```
  - [***latomeNo***] is the ini file in [LATOME_ini_file](https://gitlab.cern.ch/zang/bcid-adjustment/-/tree/master/LATOME_ini_file), which can be found at
```
  /det/lar/project/LDPB_configuration/BCID_Calibration/LAR/current
```  
  - [***inputfilename***] is the pulse-all run, Please use correcponding pusle-all run file and put them at inputfile.

2. Change the parameters in [tree.C](https://gitlab.cern.ch/zang/bcid-adjustment/-/blob/master/tree.C) if necessary.

3. Run the main script
```
sh BCID_check.sh
```

4. The htogram, log file and result .ini files will be generated at [outputs](https://gitlab.cern.ch/zang/bcid-adjustment/-/tree/master/outputs) and [results](https://gitlab.cern.ch/zang/bcid-adjustment/-/tree/master/results).

5. To process region by region, please fill in the pulse-all data and then run script with option [-t <EMBA/EMBC/EMECA/EMECC/FCALA/FCALC>] refer to [myjobs.sh](https://gitlab.cern.ch/zang/bcid-adjustment/-/blob/e77102352c4ece1472463c58955f23f89c45dd1d/myjobs.sh).
```
sh BCID_check.sh [-f fcal option] [ -i <input_filename> ] [-t <EMBA/EMBC/EMECA/EMECC/FCALA/FCALC>]
```

## Generate new version of ini files

1. Check and keep the old ini direcory at [combine.py](https://gitlab.cern.ch/zang/bcid-adjustment/-/blob/master/combine.py) update.

2. Fill in the [combine_macro.sh](https://gitlab.cern.ch/zang/bcid-adjustment/-/blob/master/combine.sh) with correct result directory.

3. Run the combine script.
```
sh combine.sh
```
4. Check your new BCID calibration files at [combined_output](https://gitlab.cern.ch/zang/bcid-adjustment/-/tree/master/combined_output)

## Chi2 test

1. Change the chisq_cut and rms_cut in chi2_test.sh if necessary.

2. Run the shell script, and generate reference run TTree with option -r if necessary.
```
./chi2_test.sh [-i <test run fullfilename>] [-o <outputdirectory>] [-r <refrence run fullfilename>]
```

3. Check the result pdf/txt/root files stored at the output directory. 


_2022.04.14_

_Jiaqi_
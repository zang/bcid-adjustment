#!/bin/sh

usage() {
    cat<<EOF

Usage: ./chi2_test.sh [-i <test run fullfilename>] [-o <outputdirectory>] [-r <refrence run fullfilename>]

Few options are provided.

-i    input the test run full filename

-o    set the output file path

-r    regenerate refrence tree with inputed reference run full filename(Please make sure that your reference run is taken with the same BCID setting!)

EOF
    }

function generate_reftree() { 
    echo 
    echo "********************************************"
    echo "***** Start to generate reference tree *****"
    echo "********************************************"
    root -b -q -l  "reftree.C+(\"${reffile}\",\"${ref_output}\")"
    echo  "refrence file: ${reffile}"
    echo  "Result file: ${ref_output}"
}

function run_test() { 
    echo 
    echo "*********************************************"
    echo "********* Start to process test run *********"
    echo "*********************************************"
    echo
    root -b -q -l "chi2_test.C+(\"${inputfile}\",\"${ref_output}\",\"${outputpath}\",${chi2_cut},${rms_cut})"  |tee ${outputpath}/chi2_processlog
    echo  "inputfile: ${inputfile}"
    echo  "chisq_cut=${chi2_cut}, rms_cut=${rms_cut}"
    echo  "processlog: ${outputpath}/chi2_processlog"
    echo  "Histgrams: ${outputpath}/chi2_hist.root"
    echo  "result list: ${outputpath}/chi2_hist.txt"
    echo  "result no pulse SC list: ${outputpath}/not_pulsed_SC.txt"
    echo  "result PNG path: ${outputpath}"
    echo  "result PDF: ${outputpath}/chi2_hist.pdf"
}

#initial="$(echo $USER | head -c 1)"
inputfile=inputfile/LArDigits_pulseall_414596.root #default test run
#reffile=inputfile/LArDigits_pulseall_414504.root #default refrence run
reffile=LArDigits_pulseall_414504.root #default refrence run
ref_output=reftree.root
chi2_cut=5000
rms_cut=50
outputpath=test_result
test_flag=false
ref_flag=false

while getopts ":i:o:r:h" arg;
do
    case $arg in
	i) 
	    inputfile="$OPTARG"	    
	    test_flag=true
	    echo "inputfile: ${inputfile}"
	    ;;
	o) 
	    outputpath="$OPTARG"
	    echo "outputpath: ${outputpath}"
	    ;;
	r)
	    reffile="$OPTARG"
	    ref_flag=true
	    echo "reference file: ${reffile}"
	    ;;
	h)
	    usage
	    exit
	    ;;
	?)
	    echo "unregisted argument"
	    exit
	    ;;
	esac
done

#main
if [ ! -d "${outputpath}" ]
then
    echo "Can't find output directory!"
    echo "Creating ${outputpath} folder"
    mkdir "${outputpath}"
fi

if [ ${test_flag} == false ] 
then
    echo "no test run input, use default file: ${inputfile}"
fi

if [ ${ref_flag} == false ] 
then
    echo "no new reference run input, use last setting: ${ref_output}"
else
    if [ ! -f "${reffile}" ]
    then
        echo "Can't find file: ${reffile}! Exiting..."
        exit
    else
	generate_reftree
    fi
fi

if [ ! -f "${ref_output}" ]
then
    echo "Can't find ${ref_output}! Regenerate with file: ${reffile}"
    if [ ! -f "${reffile}" ]
    then
	echo "Can't find file: ${reffile}! Exiting..."
	exit
    else	
	generate_reftree
    fi
else
    echo "Refrence tree found: ${ref_output}"
fi

if [ ! -f "${inputfile}" ] 
then
    echo "Can't find target test run!"
    exit 0
else
    rm ${outputpath}/*.png
    run_test
fi

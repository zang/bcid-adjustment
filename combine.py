import sys
import os

print('sys.argv         : ', sys.argv)
print('sys.argv[0]      : ', sys.argv[0])
print('sys.argv[1]      : ', sys.argv[1])
print('sys.argv[2]      : ', sys.argv[2])

print(' len argv = ',len(sys.argv))

print(os.path.exists(sys.argv[2]))

f_command=open("extra_files.sh","a")
f_caution=open("caution.txt","a")

if (os.path.exists(sys.argv[2])) : 
    print("OK, files are there!")
else:
    print(" no files! going to exit.")
    #f_command.write("cp /det/lar/project/LDPB_configuration/BCID_Calibration/LAR/current/%s.ini ./combined_output/%s.ini \n"%(sys.argv[1],sys.argv[1]))
    f_command.write("cp v8/%s.ini ./combined_output/%s.ini \n"%(sys.argv[1],sys.argv[1]))
    exit()


## a=[1,2,3]
## b=[4,5,6]
## c=[7,8,5,6]
## input = []
## print('input=',input)
## input.append(a)
## print('input[0]=',input[0])
## input.append(b)
## input.append(c)
## print('input=',input)
## print('len ofinput=',len(input))
## print(input[0])
## print(input[1])
## print(input[2])

# print('input[1]=',input[1])

l_input=[]
l_fiber=[]
l_bcid=[]

for k in  range(len(sys.argv)-2) :
    filename=sys.argv[k+2]
    print(filename)
    f=open(filename)
    t=f.read().splitlines()
    t_fiber=[]
    t_bcid=[]
    for l in range (len(t)) :
        con = t[l].split("_")
        tmp = con[3].split("=")
        bcid = tmp[1]
        print (con[1],con[3],bcid)
        t_fiber.append(con[1])
        t_bcid.append(bcid)

    # print (t)
    #input[k]=f.read().splitlines()
    l_input.append(t)
    l_bcid.append(t_bcid)
    l_fiber.append(t_fiber)
    print(' numbere of lines = ',len(t))
    print(' content of l_input = ',l_input[k])

print(len(l_fiber), len(l_bcid))
#print(l_fiber[0])
#print(l_fiber[1])

f_bcid=[]
for f in range(len(l_bcid[0])): ## 0-47
    tmp_bcid = -99
    for t in range (len(l_bcid)): ## 0 to number of input file
        if (l_fiber[0][f] != l_fiber[t][f]):
            print (' caution!!!  something wrong!  Why fiber number is not matched???',sys.argv[t+2])
            f_caution.write(' caution!!!  something wrong!  Why fiber number is not matched???   %s \n'%(sys.argv[t+2]))

        if (tmp_bcid > -1 and int(l_bcid[t][f]) > -1 and tmp_bcid != int(l_bcid[t][f])):
            print ('caution!  multiple good number!!! ',tmp_bcid,l_bcid[t][f],sys.argv[t+2])
            f_caution.write('caution!  multiple good number!!! fiber %d  %d  %s %s \n'%(f,tmp_bcid,l_bcid[t][f],sys.argv[t+2]))
           
        if (tmp_bcid < int(l_bcid[t][f])):
            tmp_bcid = int(l_bcid[t][f])
        ## print(f,l_fiber[t][f],l_bcid[t][f])

    f_bcid.append(tmp_bcid)


print("final value ----------------- ")

f_w_name="combined_output/"+sys.argv[1]+".ini"
f_w=open(f_w_name,"w")
for f in range(len(f_bcid)):
    print(f,l_fiber[0][f],f_bcid[f])
    fiber_num=l_fiber[0][f].split("F")
    if(f_bcid[f] == -2):
        f_bcid[f]=0
    f_w.write('istage.decode_stage_%s.control.ltdb_bcid_bcr=%d \n'%(fiber_num[1],f_bcid[f]))

f_w.closed

f_caution.closed

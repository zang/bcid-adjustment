//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul  1 18:02:25 2019 by ROOT version 6.16/00
// from TTree tree/Event specific information
// found on file: 10GbE_mon-190513-181247_bin.root
//////////////////////////////////////////////////////////

#ifndef tree_h
#define tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
// Header file for the classes stored in the TTree if any.
#include "vector"

class tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t      channelId;
   Int_t      barrel_ec; 
   Int_t      pos_neg;
   Int_t      FT;
   Int_t      slot;
   Int_t      channel;
   Int_t      calibLine;
   Int_t      isConnected;
   Int_t      channelHash;
   Int_t      febHash;
   Int_t      oflHash;
   Int_t      offlineId;
   Int_t      layer;
   Float_t    eta;
   Float_t    phi;
   Int_t      IEvent;
   Short_t    latomeChannel;
   Int_t      Nsamples;
   Short_t    samples[32];
   Short_t    BCID;
   Short_t    bcidLATOMEHEAD;
   Short_t    bcidVec[32];
   //Short_t    SCDataBCID;
   //Short_t    latomeSourceID;
   Int_t    latomeSourceId;
   Int_t    l1idLATOMEHEAD;
   Int_t    detector;
 
   // List of branches
   TBranch        *b_channelId;
   TBranch        *b_barrel_ec;
   TBranch        *b_pos_neg;
   TBranch        *b_FT;
   TBranch        *b_slot;
   TBranch        *b_channel;   
   TBranch        *b_calibLine;   
   TBranch        *b_isConnected;
   TBranch        *b_channelHash;
   TBranch        *b_febHash;
   TBranch        *b_oflHash;
   TBranch        *b_offlineId;
   TBranch        *b_layer;
   TBranch        *b_eta;
   TBranch        *b_phi;
   TBranch        *b_IEvent;
   TBranch        *b_latomeChannel;
   TBranch        *b_Nsamples;
   TBranch        *b_samples;
   TBranch        *b_BCID;
   TBranch        *b_bcidLATOMEHEAD;
   TBranch        *b_bcidVec;
   //TBranch        *b_SCDataBCID;
   //TBranch        *b_latomeSourceID;
   TBranch        *b_latomeSourceId;
   TBranch        *b_l1idLATOMEHEAD;
   TBranch        *b_detector;

   TString m_rootfilename;
   void SetRootFileName(TString fname) { m_rootfilename = fname; }
   
   TString ttoutput;
   void Setoutputfile(TString outputname) {
     ttoutput = outputname;
   }                                                                                                                                                                                                       

   string LATOME;
   void SetLATOMEname(string name) {
     LATOME = name;
   }                                                                                                                                                                                                       

   string latomeNo;
   void SetlatomeNo(string name) {
     latomeNo = name;
   }                                                                                                                                                                                                       

   string Resultfile;
   void SetResultfile(string result) {
     Resultfile = result;
   }                                                                                                                                                                                                       
   
   bool result_file;
   void SetListOpt(bool list_opt) {
     result_file = list_opt;
   }                                                                                                                                                         

   int base_bcid;
   void Setvalue(int value) {
     base_bcid = value;
   }                                                                                                                                                         

   bool FCAL;
   void FCal_set(bool fcal_opt) {
     FCAL = fcal_opt;
   }                                                                                                                                                                                                       


   tree(TTree *tree=0);
   virtual ~tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef tree_cxx
tree::tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
     TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("10GbE_mon-190621-190053_bin.root");//10GbE_mon-190530-154642
      if (!f || !f->IsOpen()) {
	f = new TFile("10GbE_mon-190621-190053_bin.root");//10GbE_mon-190530-154642
      }
      f->GetObject("tree",tree);

   }
   Init(tree);
}

tree::~tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer


   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("channelId", &channelId, &b_channelId);
   fChain->SetBranchAddress("barrel_ec", &barrel_ec, &b_barrel_ec);
   fChain->SetBranchAddress("pos_neg", &pos_neg, &b_pos_neg);
   fChain->SetBranchAddress("FT", &FT, &b_FT);
   fChain->SetBranchAddress("slot", &slot, &b_slot);
   fChain->SetBranchAddress("channel", &channel, &b_channel);
   fChain->SetBranchAddress("calibLine", &calibLine, &b_calibLine);
   fChain->SetBranchAddress("isConnected", &isConnected, &b_isConnected);
   fChain->SetBranchAddress("channelHash", &channelHash, &b_channelHash);
   fChain->SetBranchAddress("febHash", &febHash, &b_febHash);
   fChain->SetBranchAddress("oflHash", &oflHash, &b_oflHash);
   fChain->SetBranchAddress("offlineId", &offlineId, &b_offlineId);
   fChain->SetBranchAddress("layer", &layer, &b_layer);
   fChain->SetBranchAddress("eta", &eta, &b_eta);
   fChain->SetBranchAddress("phi", &phi, &b_phi);
   fChain->SetBranchAddress("IEvent", &IEvent, &b_IEvent);
   fChain->SetBranchAddress("latomeChannel", &latomeChannel, &b_latomeChannel);
   fChain->SetBranchAddress("Nsamples", &Nsamples, &b_Nsamples);
   fChain->SetBranchAddress("samples", &samples, &b_samples);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("bcidLATOMEHEAD", &bcidLATOMEHEAD, &b_bcidLATOMEHEAD);
   fChain->SetBranchAddress("bcidVec", &bcidVec, &b_bcidVec);
   //fChain->SetBranchAddress("SCDataBCID", &SCDataBCID, &b_SCDataBCID);
   //fChain->SetBranchAddress("latomeSourceID", &latomeSourceID, &b_latomeSourceID);
   fChain->SetBranchAddress("latomeSourceId", &latomeSourceId, &b_latomeSourceId);
   fChain->SetBranchAddress("l1idLATOMEHEAD", &l1idLATOMEHEAD, &b_l1idLATOMEHEAD);
   fChain->SetBranchAddress("detector", &detector, &b_detector);
   Notify();
}

Bool_t tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tree_cxx

#!/bin/sh                                                                                                                                                    

#inputfiles                                               
latome_list=latome-fiber_list.txt

#LATOME="EMBC-EMECC_8" 
#LATOME="FCALA_1"  #A04R
#LATOME="FCALA_23" #A04L
#LATOME="FCALC_1"  #C04R
LATOME="FCALC_23" #C04L  #debug
#LATOME="EMECA-HECA_6" #debug
#LATOME="EMBC-EMECC_11" #debug
#LATOME="EMBC_1" #debug
#LATOME="EMECA_1" #debug
#LATOME="EMBC_9" #debug
#LATOME="EMBA-EMECA_1" #debug
#LATOME="EMECA_2" #debug
#LATOME="EMBA-EMECA_14" #debug
#LATOME="EMECA_8" #debug
#LATOME="EMECA_1" #debug
#LATOME="EMBC-EMECC_15" #debug

#latomeNo="latome155"
#latomeNo="latome123" #A04R
#latomeNo="latome124" #A04L
#latomeNo="latome125" #C04R
latomeNo="latome126" #C04L debug
#latomeNo="latome56" #EMECA-HECA_6
#latomeNo="latome162" #EMBC-EMECC_11
#latomeNo="latome30" #EMECA_1
#latomeNo="latome115" #EMBC_9
#latomeNo="latome136" #EMBA-EMECA_1
#latomeNo="latome149" #EMBA-EMECA_14
#latomeNo="latome90" #EMECA_8 
#latomeNo="latome30" #EMECA_1 
#latomeNo="latome166" #EMBC-EMECC_15 


#inputfilename="LArDigits_210425-095253_H08R"
#inputfilename="LArDigits_210708-123426_A04R"
#inputfilename="LArDigits_210708-123820_C04"
#inputfilename="LArDigits_210818-182913_EMBA"
#inputfilename="LArDigits_210818-180443_EMBC"
#inputfilename="LArDigits_210818-182212_EMECC"
#inputfilename="LArDigits_210818-184010_EMECA"
#inputfilename="LArDigits_211013-144636_EMECA" #123,124
#inputfilename="LArDigits_211013-143407_EMECC" #125,126 debug
#inputfilename="LArDigits_1000evts"
#inputfilename="LArDigits_v8_new"
#inputfilename="LArDigits_pusleall_00407549"
#inputfilename="LArDigits_pusleall_00407551"
#inputfilename="LArDigits_pusleall_00407554"
#inputfilename="LArDigits_pusleall_00407557"
#inputfilename="data21_comm.00404791.physics_L1Calo.merge.RAW._lb0601._SFO-ALL._0001"
#inputfilename="LArDigits_pusleall_408524"
#inputfilename="LArDigits_pusleall_408522"
#inputfilename="LArDigits_pusleall_408520"
#inputfilename="LArDigits_pusleall_408590"
#inputfilename="LArDigits_pusleall_408591"
#inputfilename="LArDigits_pulseall_408670"
inputfilename="LArDigits_pulseall_408678"


FCal=false

function usage() {
    cat <<EOF
Usage: ./BCID_check.sh [-f] [-i <inputfile name>] [-t <target LATOMEs>]

Only short options are provided.
-f        enable FCAL detection
-i        inputfile name
-t        target LATOMEs (support EMBA, EMBC, EMECA, EMECC)
EOF
}

function do_script() { 
    root -q -l  "macro.C+(\"inputfile/${inputfilename}.root\",\"${LATOME}\",\"outputs/hist/${inputfilename}/${LATOME}_hist.root\",\"${resultpath}\",\"LATOME_ini_file/${latomeNo}.ini\",${FCal})"  >& ./outputs/logs/${inputfilename}/${LATOME}.log
    #root -q -l  "macro.C+(\"inputfile/${inputfilename}.root\",\"${LATOME}\",\"outputs/hist/${inputfilename}/${LATOME}_hist.root\",\"${resultpath}\",\"LATOME_ini_file/${latomeNo}.ini\",${FCal})"  |tee ./outputs/logs/${inputfilename}/${LATOME}.log
    echo  "inputfile: inputfile/${inputfilename}.root"
    echo  "input_BCID: LATOME_ini_file/${latomeNo}.ini"
    echo  "Histgrams store at \"outputs/hist/${inputfilename}/${LATOME}_hist.root\""
    echo  "Process log at \"./outputs/logs/${inputfilename}/${LATOME}.log\""
    echo -e "Result file at \"${resultpath}\"\n"
}

function While_read_LINE(){
    cat $latome_list |while read LINE
    do
	#seperate str to array
	str=$LINE
	OLD_IFS="$IFS"
	IFS=","
	arr=($str)
	IFS="$OLD_IFS"

        latomeNo=${arr[0]}
        LATOME=${arr[1]}
	if [[ "$LATOME" == *"$target"* ]]
	then
	    resultpath="results/${inputfilename}/${latomeNo}.ini"
	    echo "$LINE" #debug
	    do_script 
	fi

    done
}

#get options
while getopts ":ft:i:h" opt;
do
    case $opt in
	f) 
	    FCal=true
	    echo "This is for FCal calibration!"
	    ;;
	t)
	    target="$OPTARG"	    
	    ;;
	i)
	    inputfilename="$OPTARG"
	    ;;
	h)
	    usage
	    exit
	    ;;
	?)
	    echo "unregisted argument"
	    exit
	    ;;
    esac
done

#main
if [ ! -d "results/${inputfilename}" ]
then
    echo "creating results/${inputfilename} folder"
    mkdir "results/${inputfilename}"
fi

if [ ! -d "outputs/logs/${inputfilename}" ]
then
    echo "creating outputs/logs/${inputfilename} folder"
    mkdir "outputs/logs/${inputfilename}"
fi

if [ ! -d "outputs/hist/${inputfilename}" ]
then
    echo "creating outputs/hist/${inputfilename} folder"
    mkdir "outputs/hist/${inputfilename}"
fi


if [ ! ${target} ] 
then
    echo "no target input, use default file path"
    resultpath="results/${inputfilename}/${latomeNo}.ini" #output file
    do_script
else
    While_read_LINE
fi


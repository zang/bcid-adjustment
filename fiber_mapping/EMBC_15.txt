# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMB
# Side: C
# QuadrantID: 3
# AMCHashNumber: 2
# CarrierID: 27
# AMCNumberInCarrier: 1

# Input information section
# Number connected LTDBs: 2
# Connected LTDBs: EMB(H15R)  EMB(H16L)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   H15R_01_10  RX01_03  B_1J_B1  B_2J_B1  B_4J_B1  B_3J_B1  B_6J_B1  B_5J_B1  B_8J_B1  B_7J_B1  
IN F4   H15R_01_09  RX01_04  B_2J_M2  B_2J_M1  B_2J_M4  B_2J_M3  B_1J_M3  B_1J_M4  B_1J_M1  B_1J_M2  
IN F5   H15R_01_08  RX01_05  B_3J_M1  B_3J_M2  B_3J_M4  B_3J_M3  B_4J_M4  B_4J_M3  B_4J_M2  B_4J_M1  
IN F6   H15R_01_07  RX01_06  B_6J_F4  B_6J_F3  B_6J_F2  B_6J_F1  B_5J_F1  B_5J_F2  B_5J_F3  B_5J_F4  
IN F7   H15R_01_06  RX01_07  B_7J_M1  B_7J_M2  B_7J_M3  B_7J_M4  B_8J_M3  B_8J_M4  B_8J_M2  B_8J_M1  
IN F8   H15R_01_05  RX01_08  B_8J_P1  B_7J_P1  B_5J_P1  B_6J_P1  B_4J_P1  B_3J_P1  B_2J_P1  B_1J_P1  
IN F9   H15R_01_04  RX01_09  B_5J_M1  B_5J_M2  B_5J_M3  B_5J_M4  B_6J_M4  B_6J_M3  B_6J_M1  B_6J_M2  
IN F10  H15R_01_03  RX01_10  B_4J_F1  B_4J_F2  B_4J_F4  B_4J_F3  B_3J_F4  B_3J_F3  B_3J_F2  B_3J_F1  
IN F11  H15R_01_02  RX01_11  B_7J_F1  B_7J_F2  B_7J_F3  B_7J_F4  B_8J_F1  B_8J_F2  B_8J_F4  B_8J_F3  
IN F12  H15R_01_01  RX01_12  B_2J_F3  B_2J_F4  B_2J_F2  B_2J_F1  B_1J_F4  B_1J_F3  B_1J_F2  B_1J_F1  
IN F15  H15R_04_10  RX02_03  B_1I_F2  B_1I_F1  B_1I_F4  B_1I_F3  B_2I_F1  B_2I_F2  B_2I_F3  B_2I_F4  
IN F16  H15R_04_09  RX02_04  B_8I_M2  B_8I_M1  B_8I_M4  B_8I_M3  B_7I_M4  B_7I_M3  B_7I_M1  B_7I_M2  
IN F17  H15R_04_08  RX02_05  B_3I_F1  B_3I_F2  B_3I_F3  B_3I_F4  B_4I_F1  B_4I_F2  B_4I_F4  B_4I_F3  
IN F18  H15R_04_07  RX02_06  B_6I_M3  B_6I_M4  B_6I_M1  B_6I_M2  B_5I_M2  B_5I_M1  B_5I_M4  B_5I_M3  
IN F19  H15R_04_06  RX02_07  B_5I_F2  B_5I_F1  B_5I_F3  B_5I_F4  B_6I_F4  B_6I_F3  B_6I_F1  B_6I_F2  
IN F20  H15R_04_05  RX02_08  B_3I_M1  B_3I_M2  B_3I_M3  B_3I_M4  B_4I_M4  B_4I_M3  B_4I_M1  B_4I_M2  
IN F21  H15R_04_04  RX02_09  B_7I_F2  B_7I_F1  B_7I_F3  B_7I_F4  B_8I_F2  B_8I_F1  B_8I_F4  B_8I_F3  
IN F22  H15R_04_03  RX02_10  B_7I_P1  B_8I_P1  B_6I_P1  B_5I_P1  B_2I_P1  B_1I_P1  B_3I_P1  B_4I_P1  
IN F23  H15R_04_02  RX02_11  B_1I_M2  B_1I_M1  B_1I_M3  B_1I_M4  B_2I_M4  B_2I_M3  B_2I_M1  B_2I_M2  
IN F24  H15R_04_01  RX02_12  B_7I_B1  B_8I_B1  B_6I_B1  B_5I_B1  B_1I_B1  B_2I_B1  B_3I_B1  B_4I_B1  
IN F27  H16L_01_10  RX03_03  B_1L_B1  B_2L_B1  B_4L_B1  B_3L_B1  B_6L_B1  B_5L_B1  B_8L_B1  B_7L_B1  
IN F28  H16L_01_09  RX03_04  B_2L_M2  B_2L_M1  B_2L_M4  B_2L_M3  B_1L_M3  B_1L_M4  B_1L_M1  B_1L_M2  
IN F29  H16L_01_08  RX03_05  B_3L_M1  B_3L_M2  B_3L_M4  B_3L_M3  B_4L_M4  B_4L_M3  B_4L_M2  B_4L_M1  
IN F30  H16L_01_07  RX03_06  B_6L_F4  B_6L_F3  B_6L_F2  B_6L_F1  B_5L_F1  B_5L_F2  B_5L_F3  B_5L_F4  
IN F31  H16L_01_06  RX03_07  B_7L_M1  B_7L_M2  B_7L_M3  B_7L_M4  B_8L_M3  B_8L_M4  B_8L_M2  B_8L_M1  
IN F32  H16L_01_05  RX03_08  B_8L_P1  B_7L_P1  B_5L_P1  B_6L_P1  B_4L_P1  B_3L_P1  B_2L_P1  B_1L_P1  
IN F33  H16L_01_04  RX03_09  B_5L_M1  B_5L_M2  B_5L_M3  B_5L_M4  B_6L_M4  B_6L_M3  B_6L_M1  B_6L_M2  
IN F34  H16L_01_03  RX03_10  B_4L_F1  B_4L_F2  B_4L_F4  B_4L_F3  B_3L_F4  B_3L_F3  B_3L_F2  B_3L_F1  
IN F35  H16L_01_02  RX03_11  B_7L_F1  B_7L_F2  B_7L_F3  B_7L_F4  B_8L_F1  B_8L_F2  B_8L_F4  B_8L_F3  
IN F36  H16L_01_01  RX03_12  B_2L_F3  B_2L_F4  B_2L_F2  B_2L_F1  B_1L_F4  B_1L_F3  B_1L_F2  B_1L_F1  
IN F39  H16L_04_10  RX04_03  B_1K_F2  B_1K_F1  B_1K_F4  B_1K_F3  B_2K_F1  B_2K_F2  B_2K_F3  B_2K_F4  
IN F40  H16L_04_09  RX04_04  B_8K_M2  B_8K_M1  B_8K_M4  B_8K_M3  B_7K_M4  B_7K_M3  B_7K_M1  B_7K_M2  
IN F41  H16L_04_08  RX04_05  B_3K_F1  B_3K_F2  B_3K_F3  B_3K_F4  B_4K_F1  B_4K_F2  B_4K_F4  B_4K_F3  
IN F42  H16L_04_07  RX04_06  B_6K_M3  B_6K_M4  B_6K_M1  B_6K_M2  B_5K_M2  B_5K_M1  B_5K_M4  B_5K_M3  
IN F43  H16L_04_06  RX04_07  B_5K_F2  B_5K_F1  B_5K_F3  B_5K_F4  B_6K_F4  B_6K_F3  B_6K_F1  B_6K_F2  
IN F44  H16L_04_05  RX04_08  B_3K_M1  B_3K_M2  B_3K_M3  B_3K_M4  B_4K_M4  B_4K_M3  B_4K_M1  B_4K_M2  
IN F45  H16L_04_04  RX04_09  B_7K_F2  B_7K_F1  B_7K_F3  B_7K_F4  B_8K_F2  B_8K_F1  B_8K_F4  B_8K_F3  
IN F46  H16L_04_03  RX04_10  B_7K_P1  B_8K_P1  B_6K_P1  B_5K_P1  B_2K_P1  B_1K_P1  B_3K_P1  B_4K_P1  
IN F47  H16L_04_02  RX04_11  B_1K_M2  B_1K_M1  B_1K_M3  B_1K_M4  B_2K_M4  B_2K_M3  B_2K_M1  B_2K_M2  
IN F48  H16L_04_01  RX04_12  B_7K_B1  B_8K_B1  B_6K_B1  B_5K_B1  B_1K_B1  B_2K_B1  B_3K_B1  B_4K_B1  

# Output information section
# Number connected output fibers: 38 (unique=19)
# Number eFEX fibers: 30 (unique=16)
# Number jFEX fibers: 6 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   B_7I          B_8I          
OUT F2   eFEX   B_7I          B_8I          
OUT F3   eFEX   B_5I          B_6I          
OUT F4   eFEX   B_3I          B_4I          
OUT F5   eFEX   B_1I          B_2I          
OUT F7   eFEX   B_7J          B_8J          
OUT F8   eFEX   B_7J          B_8J          
OUT F13  eFEX   B_7J          B_8J          
OUT F14  eFEX   B_7J          B_8J          
OUT F9   eFEX   B_5J          B_6J          
OUT F15  eFEX   B_5J          B_6J          
OUT F10  eFEX   B_3J          B_4J          
OUT F16  eFEX   B_3J          B_4J          
OUT F11  eFEX   B_1J          B_2J          
OUT F17  eFEX   B_1J          B_2J          
OUT F19  eFEX   B_7K          B_8K          
OUT F20  eFEX   B_7K          B_8K          
OUT F25  eFEX   B_7K          B_8K          
OUT F26  eFEX   B_7K          B_8K          
OUT F21  eFEX   B_5K          B_6K          
OUT F27  eFEX   B_5K          B_6K          
OUT F22  eFEX   B_3K          B_4K          
OUT F28  eFEX   B_3K          B_4K          
OUT F23  eFEX   B_1K          B_2K          
OUT F29  eFEX   B_1K          B_2K          
OUT F31  eFEX   B_7L          B_8L          
OUT F32  eFEX   B_7L          B_8L          
OUT F33  eFEX   B_5L          B_6L          
OUT F34  eFEX   B_3L          B_4L          
OUT F35  eFEX   B_1L          B_2L          
OUT F37  jFEX   B_5I          B_5J          B_5K          B_5L          B_6I          B_6J          B_6K          B_6L          B_7I          B_7J          B_7K          B_7L          B_8I          B_8J          B_8K          B_8L          
OUT F38  jFEX   B_5I          B_5J          B_5K          B_5L          B_6I          B_6J          B_6K          B_6L          B_7I          B_7J          B_7K          B_7L          B_8I          B_8J          B_8K          B_8L          
OUT F39  jFEX   B_5I          B_5J          B_5K          B_5L          B_6I          B_6J          B_6K          B_6L          B_7I          B_7J          B_7K          B_7L          B_8I          B_8J          B_8K          B_8L          
OUT F41  jFEX   B_1I          B_1J          B_1K          B_1L          B_2I          B_2J          B_2K          B_2L          B_3I          B_3J          B_3K          B_3L          B_4I          B_4J          B_4K          B_4L          
OUT F42  jFEX   B_1I          B_1J          B_1K          B_1L          B_2I          B_2J          B_2K          B_2L          B_3I          B_3J          B_3K          B_3L          B_4I          B_4J          B_4K          B_4L          
OUT F43  jFEX   B_1I          B_1J          B_1K          B_1L          B_2I          B_2J          B_2K          B_2L          B_3I          B_3J          B_3K          B_3L          B_4I          B_4J          B_4K          B_4L          
OUT F47  gFEX   B_1I+B_2I+B_1J+B_2J  B_1K+B_2K+B_1L+B_2L  B_3I+B_4I+B_3J+B_4J  B_3K+B_4K+B_3L+B_4L  B_5I+B_6I+B_5J+B_6J  B_5K+B_6K+B_5L+B_6L  B_7I+B_8I+B_7J+B_8J  B_7K+B_8K+B_7L+B_8L  
OUT F48  gFEX   B_1I+B_2I+B_1J+B_2J  B_1K+B_2K+B_1L+B_2L  B_3I+B_4I+B_3J+B_4J  B_3K+B_4K+B_3L+B_4L  B_5I+B_6I+B_5J+B_6J  B_5K+B_6K+B_5L+B_6L  B_7I+B_8I+B_7J+B_8J  B_7K+B_8K+B_7L+B_8L  

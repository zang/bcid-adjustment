# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMEC
# Side: A
# QuadrantID: 2
# AMCHashNumber: 2
# CarrierID: 11
# AMCNumberInCarrier: 1

# Input information section
# Number connected LTDBs: 1
# Connected LTDBs: EMEC(A10R)  
# Number connected input MTPs: 4
# Number connected input fibers: 40
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F3   A10R_01_10  RX01_03  E_7I_B1  E_8I_B1  E_8J_B1  E_7J_B1  E_10I_B1 E_9I_B1  E_10J_B1 E_9J_B1  
IN F4   A10R_01_09  RX01_04  E_4I_M2  E_4I_M1  E_4I_M4  E_4I_M3  E_4J_M3  E_4J_M4  E_4J_M1  E_4J_M2  
IN F5   A10R_01_08  RX01_05  E_3J_M1  E_3J_M2  E_3J_M4  E_3J_M3  E_3I_M4  E_3I_M3  E_3I_M2  E_3I_M1  
IN F6   A10R_01_07  RX01_06  E_5J_F4  E_5J_F3  E_5J_F2  E_5J_F1  E_5I_F4  E_5I_F3  E_5I_F2  E_5I_F1  
IN F7   A10R_01_06  RX01_07  E_6I_M1  E_6I_M2  E_6I_M3  E_6I_M4  E_6J_M3  E_6J_M4  E_6J_M2  E_6J_M1  
IN F8   A10R_01_05  RX01_08  GND      GND      GND      GND      E_4J_P1  E_4I_P1  E_3J_P1  E_3I_P1  
IN F9   A10R_01_04  RX01_09  E_5I_M1  E_5I_M2  E_5I_M3  E_5I_M4  E_5J_M4  E_5J_M3  E_5J_M1  E_5J_M2  
IN F10  A10R_01_03  RX01_10  E_4J_F1  E_4J_F2  E_4J_F4  E_4J_F3  E_4I_F4  E_4I_F3  E_4I_F2  E_4I_F1  
IN F11  A10R_01_02  RX01_11  E_6I_F2  E_6I_F1  E_5I_F6  E_5I_F5  E_5J_F5  E_5J_F6  E_6J_F2  E_6J_F1  
IN F12  A10R_01_01  RX01_12  E_3J_F3  E_3J_F4  E_3J_F2  E_3J_F1  E_3I_F4  E_3I_F3  E_3I_F2  E_3I_F1  
IN F15  A10R_02_10  RX02_03  E_4L_B1  E_3L_B1  E_4K_B1  E_3K_B1  E_6L_B1  E_5L_B1  E_5K_B1  E_6K_B1  
IN F16  A10R_02_09  RX02_04  E_9J_F2  E_9J_F1  E_9J_F4  E_9J_F3  E_9I_F4  E_9I_F3  E_9I_F1  E_9I_F2  
IN F17  A10R_02_08  RX02_05  E_10L_F4 E_10L_F3 E_10L_F2 E_10L_F1 E_10K_F4 E_10K_F3 E_10K_F2 E_10K_F1 
IN F18  A10R_02_07  RX02_06  E_6J_F4  E_6J_F3  E_6J_F6  E_6J_F5  E_6I_F5  E_6I_F6  E_6I_F3  E_6I_F4  
IN F19  A10R_02_06  RX02_07  E_8I_F3  E_8I_F4  E_8I_F2  E_8I_F1  E_8J_F1  E_8J_F2  E_8J_F4  E_8J_F3  
IN F20  A10R_02_05  RX02_08  E_10J_M4 E_10J_M3 E_10J_M2 E_10J_M1 E_10I_M4 E_10I_M3 E_10I_M1 E_10I_M2 
IN F21  A10R_02_04  RX02_09  E_7I_F2  E_7I_F1  E_7I_F4  E_7I_F3  E_7J_F1  E_7J_F2  E_7J_F4  E_7J_F3  
IN F22  A10R_02_03  RX02_10  E_8I_M4  E_8I_M3  E_8I_M1  E_8I_M2  E_8J_M4  E_8J_M3  E_8J_M1  E_8J_M2  
IN F23  A10R_02_02  RX02_11  E_9J_M4  E_9J_M3  E_9J_M2  E_9J_M1  E_9I_M1  E_9I_M2  E_9I_M4  E_9I_M3  
IN F24  A10R_02_01  RX02_12  E_7J_M2  E_7J_M1  E_7J_M4  E_7J_M3  E_7I_M4  E_7I_M3  E_7I_M1  E_7I_M2  
IN F27  A10R_03_10  RX03_03  E_9L_M1  E_9L_M2  E_9L_M4  E_9L_M3  E_9K_M3  E_9K_M4  E_9K_M1  E_9K_M2  
IN F28  A10R_03_09  RX03_04  E_5K_F1  E_5K_F2  E_5K_F4  E_5K_F3  E_5L_F4  E_5L_F3  E_5L_F1  E_5L_F2  
IN F29  A10R_03_08  RX03_05  E_10K_M4 E_10K_M3 E_10K_M1 E_10K_M2 E_10L_M2 E_10L_M1 E_10L_M4 E_10L_M3 
IN F30  A10R_03_07  RX03_06  E_8L_M4  E_8L_M3  E_8L_M2  E_8L_M1  E_8K_M1  E_8K_M2  E_8K_M3  E_8K_M4  
IN F31  A10R_03_06  RX03_07  E_7K_M3  E_7K_M4  E_7K_M1  E_7K_M2  E_7L_M1  E_7L_M2  E_7L_M4  E_7L_M3  
IN F32  A10R_03_05  RX03_08  E_8L_F1  E_8L_F2  E_8L_F3  E_8L_F4  E_8K_F1  E_8K_F2  E_8K_F3  E_8K_F4  
IN F33  A10R_03_04  RX03_09  E_7K_F2  E_7K_F1  E_7K_F4  E_7K_F3  E_7L_F2  E_7L_F1  E_7L_F4  E_7L_F3  
IN F34  A10R_03_03  RX03_10  E_5I_B1  E_6I_B1  E_5J_B1  E_6J_B1  E_3I_B1  E_4I_B1  E_3J_B1  E_4J_B1  
IN F35  A10R_03_02  RX03_11  E_9K_F4  E_9K_F3  E_9K_F1  E_9K_F2  E_9L_F1  E_9L_F2  E_9L_F4  E_9L_F3  
IN F36  A10R_03_01  RX03_12  E_10J_F4 E_10J_F3 E_10J_F1 E_10J_F2 E_10I_F1 E_10I_F2 E_10I_F4 E_10I_F3 
IN F39  A10R_04_10  RX04_03  E_3K_F2  E_3K_F1  E_3K_F4  E_3K_F3  E_3L_F1  E_3L_F2  E_3L_F3  E_3L_F4  
IN F40  A10R_04_09  RX04_04  E_5L_M3  E_5L_M4  E_5L_M1  E_5L_M2  E_5K_M1  E_5K_M2  E_5K_M4  E_5K_M3  
IN F41  A10R_04_08  RX04_05  E_4K_F1  E_4K_F2  E_4K_F3  E_4K_F4  E_4L_F1  E_4L_F2  E_4L_F4  E_4L_F3  
IN F42  A10R_04_07  RX04_06  E_6L_M2  E_6L_M1  E_6L_M4  E_6L_M3  E_6K_M3  E_6K_M4  E_6K_M1  E_6K_M2  
IN F43  A10R_04_06  RX04_07  E_5K_F6  E_5K_F5  E_6K_F1  E_6K_F2  E_5L_F5  E_5L_F6  E_6L_F2  E_6L_F1  
IN F44  A10R_04_05  RX04_08  E_4K_M4  E_4K_M3  E_4K_M2  E_4K_M1  E_4L_M4  E_4L_M3  E_4L_M1  E_4L_M2  
IN F45  A10R_04_04  RX04_09  E_6K_F4  E_6K_F3  E_6K_F5  E_6K_F6  E_6L_F5  E_6L_F6  E_6L_F3  E_6L_F4  
IN F46  A10R_04_03  RX04_10  GND      GND      GND      GND      E_3L_P1  E_3K_P1  E_4K_P1  E_4L_P1  
IN F47  A10R_04_02  RX04_11  E_3K_M2  E_3K_M1  E_3K_M3  E_3K_M4  E_3L_M4  E_3L_M3  E_3L_M1  E_3L_M2  
IN F48  A10R_04_01  RX04_12  E_9L_B1  E_10L_B1 E_10K_B1 E_9K_B1  E_7K_B1  E_8K_B1  E_7L_B1  E_8L_B1  

# Output information section
# Number connected output fibers: 30 (unique=19)
# Number eFEX fibers: 24 (unique=16)
# Number jFEX fibers: 4 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F2   eFEX   E_3I          E_4I          
OUT F3   eFEX   E_5I          E_6I          
OUT F4   eFEX   E_7I          E_8I          
OUT F5   eFEX   E_10I         E_9I          
OUT F8   eFEX   E_3J          E_4J          
OUT F14  eFEX   E_3J          E_4J          
OUT F9   eFEX   E_5J          E_6J          
OUT F15  eFEX   E_5J          E_6J          
OUT F10  eFEX   E_7J          E_8J          
OUT F16  eFEX   E_7J          E_8J          
OUT F11  eFEX   E_10J         E_9J          
OUT F17  eFEX   E_10J         E_9J          
OUT F20  eFEX   E_3K          E_4K          
OUT F26  eFEX   E_3K          E_4K          
OUT F21  eFEX   E_5K          E_6K          
OUT F27  eFEX   E_5K          E_6K          
OUT F22  eFEX   E_7K          E_8K          
OUT F28  eFEX   E_7K          E_8K          
OUT F23  eFEX   E_10K         E_9K          
OUT F29  eFEX   E_10K         E_9K          
OUT F32  eFEX   E_3L          E_4L          
OUT F33  eFEX   E_5L          E_6L          
OUT F34  eFEX   E_7L          E_8L          
OUT F35  eFEX   E_10L         E_9L          
OUT F37  jFEX   E_3I          E_3J          E_3K          E_3L          E_4I          E_4J          E_4K          E_4L          E_5I          E_5J          E_5K          E_5L          E_6I          E_6J          E_6K          E_6L          
OUT F38  jFEX   E_3I          E_3J          E_3K          E_3L          E_4I          E_4J          E_4K          E_4L          E_5I          E_5J          E_5K          E_5L          E_6I          E_6J          E_6K          E_6L          
OUT F41  jFEX   E_10I         E_10J         E_10K         E_10L         E_7I          E_7J          E_7K          E_7L          E_8I          E_8J          E_8K          E_8L          E_9I          E_9J          E_9K          E_9L          
OUT F42  jFEX   E_10I         E_10J         E_10K         E_10L         E_7I          E_7J          E_7K          E_7L          E_8I          E_8J          E_8K          E_8L          E_9I          E_9J          E_9K          E_9L          
OUT F47  gFEX   E_3I+E_4I+E_3J+E_4J  E_3K+E_4K+E_3L+E_4L  E_5I+E_6I+E_5J+E_6J  E_5K+E_6K+E_5L+E_6L  E_7I+E_8I+E_7J+E_8J  E_7K+E_8K+E_7L+E_8L  E_9I+E_10I+E_9J+E_10J  E_9K+E_10K+E_9L+E_10L  
OUT F48  gFEX   E_3I+E_4I+E_3J+E_4J  E_3K+E_4K+E_3L+E_4L  E_5I+E_6I+E_5J+E_6J  E_5K+E_6K+E_5L+E_6L  E_7I+E_8I+E_7J+E_8J  E_7K+E_8K+E_7L+E_8L  E_9I+E_10I+E_9J+E_10J  E_9K+E_10K+E_9L+E_10L  

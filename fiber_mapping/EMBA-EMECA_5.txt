# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMBEMEC
# Side: A
# QuadrantID: 1
# AMCHashNumber: 0
# CarrierID: 6
# AMCNumberInCarrier: 1

# Input information section
# Number connected LTDBs: 3
# Connected LTDBs: EMB(I05L)  EMB(I06R)  EMECSpec0(A06S0)  
# Number connected input MTPs: 5
# Number connected input fibers: 44
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F1   A06S0_01_08 RX01_01  E_2C_M1  E_2C_M2  E_2C_M3  E_2C_M4  E_2D_M4  E_2D_M3  E_2D_M1  E_2D_M2  
IN F2   A06S0_01_07 RX01_02  E_2C_B1  E_2D_B1  E_2B_B1  E_2A_B1  E_2B_P1  E_2A_P1  E_2C_P1  E_2D_P1  
IN F4   I05L_03_09  RX01_04  GND      B_15B_P1 B_13B_P1 B_14B_P1 B_9B_P1  B_10B_P1 B_12B_P1 B_11B_P1 
IN F5   I05L_03_08  RX01_05  B_13B_M1 B_13B_M2 B_13B_M4 B_13B_M3 B_14B_M3 B_14B_M4 B_14B_M1 B_14B_M2 
IN F6   I05L_03_07  RX01_06  B_10B_M1 B_10B_M2 B_10B_M3 B_10B_M4 B_9B_M4  B_9B_M3  B_9B_M2  B_9B_M1  
IN F7   I05L_03_06  RX01_07  B_11B_M3 B_11B_M4 B_11B_M1 B_11B_M2 B_12B_M1 B_12B_M2 B_12B_M4 B_12B_M3 
IN F8   I05L_03_05  RX01_08  B_12B_F1 B_12B_F2 B_12B_F3 B_12B_F4 B_11B_F1 B_11B_F2 B_11B_F3 B_11B_F4 
IN F9   I05L_03_04  RX01_09  B_9B_F2  B_9B_F1  B_9B_F4  B_9B_F3  B_10B_F2 B_10B_F1 B_10B_F4 B_10B_F3 
IN F10  I05L_03_03  RX01_10  B_13A_B1 B_14A_B1 GND      GND      B_9A_B1  B_10A_B1 B_11A_B1 B_12A_B1 
IN F11  I05L_03_02  RX01_11  B_13B_F4 B_13B_F3 B_13B_F1 B_13B_F2 B_14B_F1 B_14B_F2 B_14B_F4 B_14B_F3 
IN F12  I05L_03_01  RX01_12  B_15A_B4 B_15A_B3 B_15A_B1 B_15A_B2 GND      GND      GND      GND      
IN F13  A06S0_01_04 RX02_01  E_2C_F4  E_2C_F3  E_2C_F2  E_2C_F1  E_2D_F1  E_2D_F2  E_2D_F3  E_2D_F4  
IN F14  A06S0_01_03 RX02_02  E_1D_M2  E_1D_M1  E_1D_M4  E_1D_M3  E_1C_M4  E_1C_M3  E_1C_M1  E_1C_M2  
IN F15  I06R_02_10  RX02_03  B_12D_B1 B_11D_B1 B_10D_B1 B_9D_B1  GND      GND      B_13D_B1 B_14D_B1 
IN F16  I06R_02_09  RX02_04  B_14C_F2 B_14C_F1 B_14C_F4 B_14C_F3 B_13C_F4 B_13C_F3 B_13C_F1 B_13C_F2 
IN F17  I06R_02_08  RX02_05  B_15D_B1 B_15D_B2 B_15D_B3 B_15D_B4 GND      GND      GND      GND      
IN F18  I06R_02_07  RX02_06  B_14C_P1 B_13C_P1 GND      B_15C_P1 B_11C_P1 B_12C_P1 B_9C_P1  B_10C_P1 
IN F19  I06R_02_06  RX02_07  B_11C_F2 B_11C_F1 B_11C_F3 B_11C_F4 B_12C_F1 B_12C_F2 B_12C_F4 B_12C_F3 
IN F20  I06R_02_05  RX02_08  B_9C_M1  B_9C_M2  B_9C_M3  B_9C_M4  B_10C_M1 B_10C_M2 B_10C_M4 B_10C_M3 
IN F21  I06R_02_04  RX02_09  B_9C_F2  B_9C_F1  B_9C_F4  B_9C_F3  B_10C_F1 B_10C_F2 B_10C_F4 B_10C_F3 
IN F22  I06R_02_03  RX02_10  B_14C_M4 B_14C_M3 B_14C_M1 B_14C_M2 B_13C_M4 B_13C_M3 B_13C_M1 B_13C_M2 
IN F23  I06R_02_02  RX02_11  B_11C_M1 B_11C_M2 B_11C_M3 B_11C_M4 B_12C_M4 B_12C_M3 B_12C_M1 B_12C_M2 
IN F25  A06S0_01_02 RX03_01  GND      GND      GND      GND      E_1D_F1  E_1C_F1  E_1A_F1  E_1B_F1  
IN F26  A06S0_01_01 RX03_02  E_1B_M2  E_1B_M1  E_1B_M3  E_1B_M4  E_1A_M4  E_1A_M3  E_1A_M2  E_1A_M1  
IN F28  I06R_03_09  RX03_04  GND      B_15D_P1 B_13D_P1 B_14D_P1 B_9D_P1  B_10D_P1 B_12D_P1 B_11D_P1 
IN F29  I06R_03_08  RX03_05  B_13D_M1 B_13D_M2 B_13D_M4 B_13D_M3 B_14D_M3 B_14D_M4 B_14D_M1 B_14D_M2 
IN F30  I06R_03_07  RX03_06  B_10D_M1 B_10D_M2 B_10D_M3 B_10D_M4 B_9D_M4  B_9D_M3  B_9D_M2  B_9D_M1  
IN F31  I06R_03_06  RX03_07  B_11D_M3 B_11D_M4 B_11D_M1 B_11D_M2 B_12D_M1 B_12D_M2 B_12D_M4 B_12D_M3 
IN F32  I06R_03_05  RX03_08  B_12D_F1 B_12D_F2 B_12D_F3 B_12D_F4 B_11D_F1 B_11D_F2 B_11D_F3 B_11D_F4 
IN F33  I06R_03_04  RX03_09  B_9D_F2  B_9D_F1  B_9D_F4  B_9D_F3  B_10D_F2 B_10D_F1 B_10D_F4 B_10D_F3 
IN F34  I06R_03_03  RX03_10  B_13C_B1 B_14C_B1 GND      GND      B_9C_B1  B_10C_B1 B_11C_B1 B_12C_B1 
IN F35  I06R_03_02  RX03_11  B_13D_F4 B_13D_F3 B_13D_F1 B_13D_F2 B_14D_F1 B_14D_F2 B_14D_F4 B_14D_F3 
IN F36  I06R_03_01  RX03_12  B_15C_B4 B_15C_B3 B_15C_B1 B_15C_B2 GND      GND      GND      GND      
IN F37  A06S0_01_06 RX04_01  E_2A_M2  E_2A_M1  E_2A_M3  E_2A_M4  E_2B_M1  E_2B_M2  E_2B_M4  E_2B_M3  
IN F38  A06S0_01_05 RX04_02  E_2B_F1  E_2B_F2  E_2B_F4  E_2B_F3  E_2A_F4  E_2A_F3  E_2A_F1  E_2A_F2  
IN F39  I05L_02_10  RX04_03  B_12B_B1 B_11B_B1 B_10B_B1 B_9B_B1  GND      GND      B_13B_B1 B_14B_B1 
IN F40  I05L_02_09  RX04_04  B_14A_F2 B_14A_F1 B_14A_F4 B_14A_F3 B_13A_F4 B_13A_F3 B_13A_F1 B_13A_F2 
IN F41  I05L_02_08  RX04_05  B_15B_B1 B_15B_B2 B_15B_B3 B_15B_B4 GND      GND      GND      GND      
IN F42  I05L_02_07  RX04_06  B_14A_P1 B_13A_P1 GND      B_15A_P1 B_11A_P1 B_12A_P1 B_9A_P1  B_10A_P1 
IN F43  I05L_02_06  RX04_07  B_11A_F2 B_11A_F1 B_11A_F3 B_11A_F4 B_12A_F1 B_12A_F2 B_12A_F4 B_12A_F3 
IN F44  I05L_02_05  RX04_08  B_9A_M1  B_9A_M2  B_9A_M3  B_9A_M4  B_10A_M1 B_10A_M2 B_10A_M4 B_10A_M3 
IN F45  I05L_02_04  RX04_09  B_9A_F2  B_9A_F1  B_9A_F4  B_9A_F3  B_10A_F1 B_10A_F2 B_10A_F4 B_10A_F3 
IN F46  I05L_02_03  RX04_10  B_14A_M4 B_14A_M3 B_14A_M1 B_14A_M2 B_13A_M4 B_13A_M3 B_13A_M1 B_13A_M2 
IN F47  I05L_02_02  RX04_11  B_11A_M1 B_11A_M2 B_11A_M3 B_11A_M4 B_12A_M4 B_12A_M3 B_12A_M1 B_12A_M2 

# Output information section
# Number connected output fibers: 38 (unique=19)
# Number eFEX fibers: 30 (unique=16)
# Number jFEX fibers: 6 (unique=2)
# Number gFEX fibers: 2 (unique=1)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   B_10A         B_9A          
OUT F2   eFEX   B_10A         B_9A          
OUT F3   eFEX   B_11A         B_12A         
OUT F4   eFEX   B_13A         B_14A         
OUT F5   eFEX   B_15A+E_1A    E_2A          
OUT F7   eFEX   B_10B         B_9B          
OUT F8   eFEX   B_10B         B_9B          
OUT F13  eFEX   B_10B         B_9B          
OUT F14  eFEX   B_10B         B_9B          
OUT F9   eFEX   B_11B         B_12B         
OUT F15  eFEX   B_11B         B_12B         
OUT F10  eFEX   B_13B         B_14B         
OUT F16  eFEX   B_13B         B_14B         
OUT F11  eFEX   B_15B+E_1B    E_2B          
OUT F17  eFEX   B_15B+E_1B    E_2B          
OUT F19  eFEX   B_10C         B_9C          
OUT F20  eFEX   B_10C         B_9C          
OUT F25  eFEX   B_10C         B_9C          
OUT F26  eFEX   B_10C         B_9C          
OUT F21  eFEX   B_11C         B_12C         
OUT F27  eFEX   B_11C         B_12C         
OUT F22  eFEX   B_13C         B_14C         
OUT F28  eFEX   B_13C         B_14C         
OUT F23  eFEX   B_15C+E_1C    E_2C          
OUT F29  eFEX   B_15C+E_1C    E_2C          
OUT F31  eFEX   B_10D         B_9D          
OUT F32  eFEX   B_10D         B_9D          
OUT F33  eFEX   B_11D         B_12D         
OUT F34  eFEX   B_13D         B_14D         
OUT F35  eFEX   B_15D+E_1D    E_2D          
OUT F37  jFEX   B_10A         B_10B         B_10C         B_10D         B_11A         B_11B         B_11C         B_11D         B_12A         B_12B         B_12C         B_12D         B_9A          B_9B          B_9C          B_9D          
OUT F38  jFEX   B_10A         B_10B         B_10C         B_10D         B_11A         B_11B         B_11C         B_11D         B_12A         B_12B         B_12C         B_12D         B_9A          B_9B          B_9C          B_9D          
OUT F39  jFEX   B_10A         B_10B         B_10C         B_10D         B_11A         B_11B         B_11C         B_11D         B_12A         B_12B         B_12C         B_12D         B_9A          B_9B          B_9C          B_9D          
OUT F41  jFEX   B_13A         B_13B         B_13C         B_13D         B_14A         B_14B         B_14C         B_14D         B_15A+E_1A    B_15B+E_1B    B_15C+E_1C    B_15D+E_1D    E_2A          E_2B          E_2C          E_2D          
OUT F42  jFEX   B_13A         B_13B         B_13C         B_13D         B_14A         B_14B         B_14C         B_14D         B_15A+E_1A    B_15B+E_1B    B_15C+E_1C    B_15D+E_1D    E_2A          E_2B          E_2C          E_2D          
OUT F43  jFEX   B_13A         B_13B         B_13C         B_13D         B_14A         B_14B         B_14C         B_14D         B_15A+E_1A    B_15B+E_1B    B_15C+E_1C    B_15D+E_1D    E_2A          E_2B          E_2C          E_2D          
OUT F47  gFEX   B_11A+B_12A+B_11B+B_12B  B_11C+B_12C+B_11D+B_12D  B_13A+B_14A+B_13B+B_14B  B_13C+B_14C+B_13D+B_14D  B_15A+B_15B+E_1A+E_2A+E_1B+E_2B  B_15C+B_15D+E_1C+E_2C+E_1D+E_2D  B_9A+B_10A+B_9B+B_10B  B_9C+B_10C+B_9D+B_10D  
OUT F48  gFEX   B_11A+B_12A+B_11B+B_12B  B_11C+B_12C+B_11D+B_12D  B_13A+B_14A+B_13B+B_14B  B_13C+B_14C+B_13D+B_14D  B_15A+B_15B+E_1A+E_2A+E_1B+E_2B  B_15C+B_15D+E_1C+E_2C+E_1D+E_2D  B_9A+B_10A+B_9B+B_10B  B_9C+B_10C+B_9D+B_10D  

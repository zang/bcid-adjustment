# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMECHEC
# Side: C
# QuadrantID: 2
# AMCHashNumber: 1
# CarrierID: 26
# AMCNumberInCarrier: 2

# Input information section
# Number connected LTDBs: 2
# Connected LTDBs: EMECSpec1(C09S1)  HEC(C09H)  
# Number connected input MTPs: 3
# Number connected input fibers: 22
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F7   C09H_03_06  RX01_07  H_7I     H_7J     H_7K     H_7L     H_6L     H_6K     H_6I     H_6J     
IN F8   C09H_03_05  RX01_08  H_13F    H_13E    H_13H    H_13G    H_12G    H_12H    H_12E    H_12F    
IN F9   C09H_03_04  RX01_09  H_15H    H_15G    H_15F    H_15E    H_14E    H_14F    H_14G    H_14H    
IN F10  C09H_03_03  RX01_10  H_7P     H_7O     H_7N     H_7M     H_6M     H_6N     H_6O     H_6P     
IN F11  C09H_03_02  RX01_11  H_11M    H_11N    H_11O    H_11P    H_11I    H_11J    H_11L    H_11K    
IN F12  C09H_03_01  RX01_12  H_4I     H_4J     H_4K     H_4L     H_5I     H_5J     H_5K     H_5L     
IN F19  C09H_01_06  RX02_07  H_5O     H_5P     H_5M     H_5N     H_4N     H_4M     H_4P     H_4O     
IN F20  C09H_01_05  RX02_08  H_8M     H_8N     H_8O     H_8P     H_8L     H_8K     H_8I     H_8J     
IN F21  C09H_01_04  RX02_09  H_9J     H_9I     H_9L     H_9K     H_9P     H_9O     H_9M     H_9N     
IN F22  C09H_01_03  RX02_10  H_10O    H_10P    H_10M    H_10N    H_10J    H_10I    H_10L    H_10K    
IN F23  C09H_01_02  RX02_11  H_2L     H_2K     H_2J     H_2I     H_2P     H_2O     H_2N     H_2M     
IN F24  C09H_01_01  RX02_12  H_3K     H_3L     H_3J     H_3I     H_3M     H_3N     H_3O     H_3P     
IN F27  C09S1_01_10 RX03_03  E_15E_M1 E_15F_M1 E_15G_M1 E_15H_M1 E_15E_F1 E_15F_F1 E_15H_F1 E_15G_F1 
IN F28  C09S1_01_09 RX03_04  E_14G_F1 E_14H_F1 E_14E_F1 E_14F_F1 E_14E_M1 E_14F_M1 E_14H_M1 E_14G_M1 
IN F29  C09S1_01_08 RX03_05  E_12G_F1 E_12H_F1 E_12E_F1 E_12F_F1 E_12F_M1 E_12E_M1 E_12H_M1 E_12G_M1 
IN F30  C09S1_01_07 RX03_06  E_13F_M1 E_13E_M1 E_13H_M1 E_13G_M1 E_13H_F1 E_13G_F1 E_13E_F1 E_13F_F1 
IN F31  C09S1_01_06 RX03_07  E_11J_M4 E_11J_M3 E_11J_M1 E_11J_M2 E_11I_M4 E_11I_M3 E_11I_M1 E_11I_M2 
IN F32  C09S1_01_05 RX03_08  E_11M_M2 E_11M_M1 E_11M_M4 E_11M_M3 E_11N_M4 E_11N_M3 E_11N_M1 E_11N_M2 
IN F33  C09S1_01_04 RX03_09  E_11K_F1 E_11L_F1 E_11I_F1 E_11J_F1 E_11J_B1 E_11I_B1 E_11L_B1 E_11K_B1 
IN F34  C09S1_01_03 RX03_10  E_11P_B1 E_11O_B1 E_11N_B1 E_11M_B1 E_11M_F1 E_11N_F1 E_11P_F1 E_11O_F1 
IN F35  C09S1_01_02 RX03_11  E_11K_M1 E_11K_M2 E_11K_M4 E_11K_M3 E_11L_M4 E_11L_M3 E_11L_M2 E_11L_M1 
IN F36  C09S1_01_01 RX03_12  E_11O_M2 E_11O_M1 E_11O_M3 E_11O_M4 E_11P_M4 E_11P_M3 E_11P_M2 E_11P_M1 

# Output information section
# Number connected output fibers: 38 (unique=23)
# Number eFEX fibers: 15 (unique=10)
# Number jFEX fibers: 15 (unique=9)
# Number gFEX fibers: 8 (unique=4)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   E_11I         E_11J         E_12E         E_13E         E_14E         E_15E         
OUT F2   eFEX   E_11I         E_11J         E_12E         E_13E         E_14E         E_15E         
OUT F3   eFEX   E_11K         E_11L         E_12F         E_13F         E_14F         E_15F         
OUT F4   eFEX   E_11K         E_11L         E_12F         E_13F         E_14F         E_15F         
OUT F5   eFEX   E_11M         E_11N         E_12G         E_13G         E_14G         E_15G         
OUT F6   eFEX   E_11O         E_11P         E_12H         E_13H         E_14H         E_15H         
OUT F7   eFEX   H_10I         H_10J         H_10K         H_10L         H_11I         H_11J         H_11K         H_11L         H_12E         H_12F         H_13E         H_13F         H_14E         H_14F         H_15E         H_15F         
OUT F8   eFEX   H_10I         H_10J         H_10K         H_10L         H_11I         H_11J         H_11K         H_11L         H_12E         H_12F         H_13E         H_13F         H_14E         H_14F         H_15E         H_15F         
OUT F9   eFEX   H_10M         H_10N         H_10O         H_10P         H_11M         H_11N         H_11O         H_11P         H_12G         H_12H         H_13G         H_13H         H_14G         H_14H         H_15G         H_15H         
OUT F10  eFEX   H_6I          H_6J          H_6K          H_6L          H_7I          H_7J          H_7K          H_7L          H_8I          H_8J          H_8K          H_8L          H_9I          H_9J          H_9K          H_9L          
OUT F11  eFEX   H_6I          H_6J          H_6K          H_6L          H_7I          H_7J          H_7K          H_7L          H_8I          H_8J          H_8K          H_8L          H_9I          H_9J          H_9K          H_9L          
OUT F12  eFEX   H_2I          H_2J          H_2K          H_2L          H_3I          H_3J          H_3K          H_3L          H_4I          H_4J          H_4K          H_4L          H_5I          H_5J          H_5K          H_5L          
OUT F13  eFEX   H_2I          H_2J          H_2K          H_2L          H_3I          H_3J          H_3K          H_3L          H_4I          H_4J          H_4K          H_4L          H_5I          H_5J          H_5K          H_5L          
OUT F14  eFEX   H_6M          H_6N          H_6O          H_6P          H_7M          H_7N          H_7O          H_7P          H_8M          H_8N          H_8O          H_8P          H_9M          H_9N          H_9O          H_9P          
OUT F15  eFEX   H_2M          H_2N          H_2O          H_2P          H_3M          H_3N          H_3O          H_3P          H_4M          H_4N          H_4O          H_4P          H_5M          H_5N          H_5O          H_5P          
OUT F19  jFEX   E_11I         E_11J         E_11K         E_11L         E_12E         E_12F         E_13E         E_13F         E_14E         E_14F         E_15E         E_15F         
OUT F20  jFEX   E_11M         E_11N         E_11O         E_11P         E_12G         E_12H         E_13G         E_13H         E_14G         E_14H         E_15G         E_15H         
OUT F21  jFEX   H_11I         H_11J         H_11K         H_11L         H_12E         H_12F         H_13E         H_13F         H_14E         H_14F         H_15E         H_15F         
OUT F22  jFEX   H_11M         H_11N         H_11O         H_11P         H_12G         H_12H         H_13G         H_13H         H_14G         H_14H         H_15G         H_15H         
OUT F23  jFEX   H_10I         H_10J         H_10K         H_10L         H_7I          H_7J          H_7K          H_7L          H_8I          H_8J          H_8K          H_8L          H_9I          H_9J          H_9K          H_9L          
OUT F24  jFEX   H_10I         H_10J         H_10K         H_10L         H_7I          H_7J          H_7K          H_7L          H_8I          H_8J          H_8K          H_8L          H_9I          H_9J          H_9K          H_9L          
OUT F25  jFEX   H_3I          H_3J          H_3K          H_3L          H_4I          H_4J          H_4K          H_4L          H_5I          H_5J          H_5K          H_5L          H_6I          H_6J          H_6K          H_6L          
OUT F26  jFEX   H_3I          H_3J          H_3K          H_3L          H_4I          H_4J          H_4K          H_4L          H_5I          H_5J          H_5K          H_5L          H_6I          H_6J          H_6K          H_6L          
OUT F27  jFEX   H_10M         H_10N         H_10O         H_10P         H_7M          H_7N          H_7O          H_7P          H_8M          H_8N          H_8O          H_8P          H_9M          H_9N          H_9O          H_9P          
OUT F28  jFEX   H_10M         H_10N         H_10O         H_10P         H_7M          H_7N          H_7O          H_7P          H_8M          H_8N          H_8O          H_8P          H_9M          H_9N          H_9O          H_9P          
OUT F29  jFEX   H_3M          H_3N          H_3O          H_3P          H_4M          H_4N          H_4O          H_4P          H_5M          H_5N          H_5O          H_5P          H_6M          H_6N          H_6O          H_6P          
OUT F30  jFEX   H_3M          H_3N          H_3O          H_3P          H_4M          H_4N          H_4O          H_4P          H_5M          H_5N          H_5O          H_5P          H_6M          H_6N          H_6O          H_6P          
OUT F31  jFEX   H_2I          H_2J          H_2K          H_2L          H_2M          H_2N          H_2O          H_2P          
OUT F32  jFEX   H_2I          H_2J          H_2K          H_2L          H_2M          H_2N          H_2O          H_2P          
OUT F33  jFEX   H_2I          H_2J          H_2K          H_2L          H_2M          H_2N          H_2O          H_2P          
OUT F37  gFEX   E_12E  E_12F  E_13E  E_13F  E_14E  E_14F  E_15E  E_15F  H_12E  H_12F  H_13E  H_13F  H_14E  H_14F  H_15E  H_15F  
OUT F41  gFEX   E_12E  E_12F  E_13E  E_13F  E_14E  E_14F  E_15E  E_15F  H_12E  H_12F  H_13E  H_13F  H_14E  H_14F  H_15E  H_15F  
OUT F38  gFEX   E_12G  E_12H  E_13G  E_13H  E_14G  E_14H  E_15G  E_15H  H_12G  H_12H  H_13G  H_13H  H_14G  H_14H  H_15G  H_15H  
OUT F42  gFEX   E_12G  E_12H  E_13G  E_13H  E_14G  E_14H  E_15G  E_15H  H_12G  H_12H  H_13G  H_13H  H_14G  H_14H  H_15G  H_15H  
OUT F39  gFEX   E_11I+E_11J  E_11K+E_11L  H_11I+H_11J  H_11K+H_11L  H_2I+H_2J  H_2K+H_2L  H_3I+H_4I+H_3J+H_4J  H_3K+H_4K+H_3L+H_4L  H_5I+H_6I+H_5J+H_6J  H_5K+H_6K+H_5L+H_6L  H_7I+H_8I+H_7J+H_8J  H_7K+H_8K+H_7L+H_8L  H_9I+H_10I+H_9J+H_10J  H_9K+H_10K+H_9L+H_10L  
OUT F43  gFEX   E_11I+E_11J  E_11K+E_11L  H_11I+H_11J  H_11K+H_11L  H_2I+H_2J  H_2K+H_2L  H_3I+H_4I+H_3J+H_4J  H_3K+H_4K+H_3L+H_4L  H_5I+H_6I+H_5J+H_6J  H_5K+H_6K+H_5L+H_6L  H_7I+H_8I+H_7J+H_8J  H_7K+H_8K+H_7L+H_8L  H_9I+H_10I+H_9J+H_10J  H_9K+H_10K+H_9L+H_10L  
OUT F40  gFEX   E_11M+E_11N  E_11O+E_11P  H_11M+H_11N  H_11O+H_11P  H_2M+H_2N  H_2O+H_2P  H_3M+H_4M+H_3N+H_4N  H_3O+H_4O+H_3P+H_4P  H_5M+H_6M+H_5N+H_6N  H_5O+H_6O+H_5P+H_6P  H_7M+H_8M+H_7N+H_8N  H_7O+H_8O+H_7P+H_8P  H_9M+H_10M+H_9N+H_10N  H_9O+H_10O+H_9P+H_10P  
OUT F44  gFEX   E_11M+E_11N  E_11O+E_11P  H_11M+H_11N  H_11O+H_11P  H_2M+H_2N  H_2O+H_2P  H_3M+H_4M+H_3N+H_4N  H_3O+H_4O+H_3P+H_4P  H_5M+H_6M+H_5N+H_6N  H_5O+H_6O+H_5P+H_6P  H_7M+H_8M+H_7N+H_8N  H_7O+H_8O+H_7P+H_8P  H_9M+H_10M+H_9N+H_10N  H_9O+H_10O+H_9P+H_10P  

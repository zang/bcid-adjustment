# LATOME input mapping v03.00.00 2021-10-6
# Comments: ATLAS mapping (folder names changed)
# AMCType: EMECHEC
# Side: C
# QuadrantID: 3
# AMCHashNumber: 0
# CarrierID: 29
# AMCNumberInCarrier: 3

# Input information section
# Number connected LTDBs: 2
# Connected LTDBs: EMECSpec1(C12S1)  HEC(C12H)  
# Number connected input MTPs: 3
# Number connected input fibers: 22
# Input_ID LTDB_MTP_PIN RX_PIN SC1 ... SC8
IN F7   C12H_04_06  RX01_07  H_7B     H_7A     H_7D     H_7C     H_6D     H_6C     H_6B     H_6A     
IN F8   C12H_04_05  RX01_08  H_7F     H_7E     H_7G     H_7H     H_6E     H_6F     H_6H     H_6G     
IN F9   C12H_04_04  RX01_09  H_5B     H_5A     H_5C     H_5D     H_4C     H_4D     H_4B     H_4A     
IN F10  C12H_04_03  RX01_10  H_11G    H_11H    H_11F    H_11E    H_11A    H_11B    H_11D    H_11C    
IN F11  C12H_04_02  RX01_11  H_15A    H_15B    H_15D    H_15C    H_14C    H_14D    H_14A    H_14B    
IN F12  C12H_04_01  RX01_12  H_12D    H_12C    H_12A    H_12B    H_13B    H_13A    H_13D    H_13C    
IN F19  C12H_02_06  RX02_07  H_5G     H_5H     H_5E     H_5F     H_4E     H_4F     H_4H     H_4G     
IN F20  C12H_02_05  RX02_08  H_3G     H_3H     H_3E     H_3F     H_3A     H_3B     H_3D     H_3C     
IN F21  C12H_02_04  RX02_09  H_8E     H_8F     H_8H     H_8G     H_8A     H_8B     H_8D     H_8C     
IN F22  C12H_02_03  RX02_10  H_10H    H_10G    H_10E    H_10F    H_10C    H_10D    H_10A    H_10B    
IN F23  C12H_02_02  RX02_11  H_9E     H_9F     H_9G     H_9H     H_9C     H_9D     H_9B     H_9A     
IN F24  C12H_02_01  RX02_12  H_2D     H_2C     H_2A     H_2B     H_2H     H_2G     H_2F     H_2E     
IN F27  C12S1_02_10 RX03_03  E_12B_F1 E_12A_F1 E_12D_F1 E_12C_F1 E_12D_M1 E_12C_M1 E_12B_M1 E_12A_M1 
IN F28  C12S1_02_09 RX03_04  E_13B_F1 E_13A_F1 E_13D_F1 E_13C_F1 E_13C_M1 E_13D_M1 E_13A_M1 E_13B_M1 
IN F29  C12S1_02_08 RX03_05  E_11A_M3 E_11A_M4 E_11A_M2 E_11A_M1 E_11B_M4 E_11B_M3 E_11B_M1 E_11B_M2 
IN F30  C12S1_02_07 RX03_06  E_11C_M3 E_11C_M4 E_11C_M2 E_11C_M1 E_11D_M2 E_11D_M1 E_11D_M4 E_11D_M3 
IN F31  C12S1_02_06 RX03_07  E_11B_B1 E_11A_B1 E_11C_B1 E_11D_B1 E_11C_F1 E_11D_F1 E_11B_F1 E_11A_F1 
IN F32  C12S1_02_05 RX03_08  E_15A_F1 E_15B_F1 E_15D_F1 E_15C_F1 E_15C_M1 E_15D_M1 E_15A_M1 E_15B_M1 
IN F33  C12S1_02_04 RX03_09  E_11E_F1 E_11F_F1 E_11H_F1 E_11G_F1 E_11H_B1 E_11G_B1 E_11E_B1 E_11F_B1 
IN F34  C12S1_02_03 RX03_10  E_14D_M1 E_14C_M1 E_14A_M1 E_14B_M1 E_14B_F1 E_14A_F1 E_14D_F1 E_14C_F1 
IN F35  C12S1_02_02 RX03_11  E_11G_M1 E_11G_M2 E_11G_M3 E_11G_M4 E_11H_M1 E_11H_M2 E_11H_M3 E_11H_M4 
IN F36  C12S1_02_01 RX03_12  E_11F_M1 E_11F_M2 E_11F_M3 E_11F_M4 E_11E_M1 E_11E_M2 E_11E_M4 E_11E_M3 

# Output information section
# Number connected output fibers: 38 (unique=23)
# Number eFEX fibers: 15 (unique=10)
# Number jFEX fibers: 15 (unique=9)
# Number gFEX fibers: 8 (unique=4)
# hash_ID Type C1 C2 .... 
OUT F1   eFEX   E_11A         E_11B         E_12A         E_13A         E_14A         E_15A         
OUT F2   eFEX   E_11A         E_11B         E_12A         E_13A         E_14A         E_15A         
OUT F3   eFEX   E_11C         E_11D         E_12B         E_13B         E_14B         E_15B         
OUT F4   eFEX   E_11C         E_11D         E_12B         E_13B         E_14B         E_15B         
OUT F5   eFEX   E_11E         E_11F         E_12C         E_13C         E_14C         E_15C         
OUT F6   eFEX   E_11G         E_11H         E_12D         E_13D         E_14D         E_15D         
OUT F7   eFEX   H_10A         H_10B         H_10C         H_10D         H_11A         H_11B         H_11C         H_11D         H_12A         H_12B         H_13A         H_13B         H_14A         H_14B         H_15A         H_15B         
OUT F8   eFEX   H_10A         H_10B         H_10C         H_10D         H_11A         H_11B         H_11C         H_11D         H_12A         H_12B         H_13A         H_13B         H_14A         H_14B         H_15A         H_15B         
OUT F9   eFEX   H_10E         H_10F         H_10G         H_10H         H_11E         H_11F         H_11G         H_11H         H_12C         H_12D         H_13C         H_13D         H_14C         H_14D         H_15C         H_15D         
OUT F10  eFEX   H_6A          H_6B          H_6C          H_6D          H_7A          H_7B          H_7C          H_7D          H_8A          H_8B          H_8C          H_8D          H_9A          H_9B          H_9C          H_9D          
OUT F11  eFEX   H_6A          H_6B          H_6C          H_6D          H_7A          H_7B          H_7C          H_7D          H_8A          H_8B          H_8C          H_8D          H_9A          H_9B          H_9C          H_9D          
OUT F12  eFEX   H_2A          H_2B          H_2C          H_2D          H_3A          H_3B          H_3C          H_3D          H_4A          H_4B          H_4C          H_4D          H_5A          H_5B          H_5C          H_5D          
OUT F13  eFEX   H_2A          H_2B          H_2C          H_2D          H_3A          H_3B          H_3C          H_3D          H_4A          H_4B          H_4C          H_4D          H_5A          H_5B          H_5C          H_5D          
OUT F14  eFEX   H_6E          H_6F          H_6G          H_6H          H_7E          H_7F          H_7G          H_7H          H_8E          H_8F          H_8G          H_8H          H_9E          H_9F          H_9G          H_9H          
OUT F15  eFEX   H_2E          H_2F          H_2G          H_2H          H_3E          H_3F          H_3G          H_3H          H_4E          H_4F          H_4G          H_4H          H_5E          H_5F          H_5G          H_5H          
OUT F19  jFEX   E_11A         E_11B         E_11C         E_11D         E_12A         E_12B         E_13A         E_13B         E_14A         E_14B         E_15A         E_15B         
OUT F20  jFEX   E_11E         E_11F         E_11G         E_11H         E_12C         E_12D         E_13C         E_13D         E_14C         E_14D         E_15C         E_15D         
OUT F21  jFEX   H_11A         H_11B         H_11C         H_11D         H_12A         H_12B         H_13A         H_13B         H_14A         H_14B         H_15A         H_15B         
OUT F22  jFEX   H_11E         H_11F         H_11G         H_11H         H_12C         H_12D         H_13C         H_13D         H_14C         H_14D         H_15C         H_15D         
OUT F23  jFEX   H_10A         H_10B         H_10C         H_10D         H_7A          H_7B          H_7C          H_7D          H_8A          H_8B          H_8C          H_8D          H_9A          H_9B          H_9C          H_9D          
OUT F24  jFEX   H_10A         H_10B         H_10C         H_10D         H_7A          H_7B          H_7C          H_7D          H_8A          H_8B          H_8C          H_8D          H_9A          H_9B          H_9C          H_9D          
OUT F25  jFEX   H_3A          H_3B          H_3C          H_3D          H_4A          H_4B          H_4C          H_4D          H_5A          H_5B          H_5C          H_5D          H_6A          H_6B          H_6C          H_6D          
OUT F26  jFEX   H_3A          H_3B          H_3C          H_3D          H_4A          H_4B          H_4C          H_4D          H_5A          H_5B          H_5C          H_5D          H_6A          H_6B          H_6C          H_6D          
OUT F27  jFEX   H_10E         H_10F         H_10G         H_10H         H_7E          H_7F          H_7G          H_7H          H_8E          H_8F          H_8G          H_8H          H_9E          H_9F          H_9G          H_9H          
OUT F28  jFEX   H_10E         H_10F         H_10G         H_10H         H_7E          H_7F          H_7G          H_7H          H_8E          H_8F          H_8G          H_8H          H_9E          H_9F          H_9G          H_9H          
OUT F29  jFEX   H_3E          H_3F          H_3G          H_3H          H_4E          H_4F          H_4G          H_4H          H_5E          H_5F          H_5G          H_5H          H_6E          H_6F          H_6G          H_6H          
OUT F30  jFEX   H_3E          H_3F          H_3G          H_3H          H_4E          H_4F          H_4G          H_4H          H_5E          H_5F          H_5G          H_5H          H_6E          H_6F          H_6G          H_6H          
OUT F31  jFEX   H_2A          H_2B          H_2C          H_2D          H_2E          H_2F          H_2G          H_2H          
OUT F32  jFEX   H_2A          H_2B          H_2C          H_2D          H_2E          H_2F          H_2G          H_2H          
OUT F33  jFEX   H_2A          H_2B          H_2C          H_2D          H_2E          H_2F          H_2G          H_2H          
OUT F37  gFEX   E_12A  E_12B  E_13A  E_13B  E_14A  E_14B  E_15A  E_15B  H_12A  H_12B  H_13A  H_13B  H_14A  H_14B  H_15A  H_15B  
OUT F41  gFEX   E_12A  E_12B  E_13A  E_13B  E_14A  E_14B  E_15A  E_15B  H_12A  H_12B  H_13A  H_13B  H_14A  H_14B  H_15A  H_15B  
OUT F38  gFEX   E_12C  E_12D  E_13C  E_13D  E_14C  E_14D  E_15C  E_15D  H_12C  H_12D  H_13C  H_13D  H_14C  H_14D  H_15C  H_15D  
OUT F42  gFEX   E_12C  E_12D  E_13C  E_13D  E_14C  E_14D  E_15C  E_15D  H_12C  H_12D  H_13C  H_13D  H_14C  H_14D  H_15C  H_15D  
OUT F39  gFEX   E_11A+E_11B  E_11C+E_11D  H_11A+H_11B  H_11C+H_11D  H_2A+H_2B  H_2C+H_2D  H_3A+H_4A+H_3B+H_4B  H_3C+H_4C+H_3D+H_4D  H_5A+H_6A+H_5B+H_6B  H_5C+H_6C+H_5D+H_6D  H_7A+H_8A+H_7B+H_8B  H_7C+H_8C+H_7D+H_8D  H_9A+H_10A+H_9B+H_10B  H_9C+H_10C+H_9D+H_10D  
OUT F43  gFEX   E_11A+E_11B  E_11C+E_11D  H_11A+H_11B  H_11C+H_11D  H_2A+H_2B  H_2C+H_2D  H_3A+H_4A+H_3B+H_4B  H_3C+H_4C+H_3D+H_4D  H_5A+H_6A+H_5B+H_6B  H_5C+H_6C+H_5D+H_6D  H_7A+H_8A+H_7B+H_8B  H_7C+H_8C+H_7D+H_8D  H_9A+H_10A+H_9B+H_10B  H_9C+H_10C+H_9D+H_10D  
OUT F40  gFEX   E_11E+E_11F  E_11G+E_11H  H_11E+H_11F  H_11G+H_11H  H_2E+H_2F  H_2G+H_2H  H_3E+H_4E+H_3F+H_4F  H_3G+H_4G+H_3H+H_4H  H_5E+H_6E+H_5F+H_6F  H_5G+H_6G+H_5H+H_6H  H_7E+H_8E+H_7F+H_8F  H_7G+H_8G+H_7H+H_8H  H_9E+H_10E+H_9F+H_10F  H_9G+H_10G+H_9H+H_10H  
OUT F44  gFEX   E_11E+E_11F  E_11G+E_11H  H_11E+H_11F  H_11G+H_11H  H_2E+H_2F  H_2G+H_2H  H_3E+H_4E+H_3F+H_4F  H_3G+H_4G+H_3H+H_4H  H_5E+H_6E+H_5F+H_6F  H_5G+H_6G+H_5H+H_6H  H_7E+H_8E+H_7F+H_8F  H_7G+H_8G+H_7H+H_8H  H_9E+H_10E+H_9F+H_10F  H_9G+H_10G+H_9H+H_10H  
